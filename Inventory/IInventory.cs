﻿using eBay.Service.Core.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Inventory
{
    public interface IInventory
    {
        /// <summary>
        /// downloads inventory from ebay account
        /// </summary>
        void Download();

        /// <summary>
        /// updates the downloaded inventory into database
        /// </summary>
        void UpdateDB();

        ApiContext GetApiContext();
    }
}
