﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using eBay.Service.Core.Sdk;
using eBay.Service.Core.Soap;
using eBay.Service.Util;
using eBay.Service.Call;

using System.IO;
using DNEbaySystem.ListingDatabase.Interfaces;
using DNEbaySystem.ListingDatabase;
using DN_Classes.Agent;
namespace DNEbaySystem.Inventory
{
    public class EbayAccountInventory : IInventory
    {
        IListingDatabaseInventory ListingDatabase = null;
        ApiContext apiContext = null;
        List<InventoryItem> InventoryItems = new List<InventoryItem>();
        private Agent agent = null;

        private StringBuilder sbNoSKU = new StringBuilder();

        public EbayAccountInventory(int agentid)
        {
            ListingDatabaseFactory listingDbFact = new ListingDatabaseFactory();

            this.agent = new Agent(agentid);

            this.ListingDatabase = listingDbFact.GetListingDatabaseInventory(agent.agentEntity.agent.ebayname);

            this.apiContext = GetApiContext();
        }

        public EbayAccountInventory()
        {
            ListingDatabaseFactory listingDbFact = new ListingDatabaseFactory();

            this.agent = new Agent();

            this.ListingDatabase = listingDbFact.GetListingDatabaseInventory(agent.agentEntity.agent.ebayname);

            this.apiContext = GetApiContext();
        }

        public void Download()
        {
            //var getItemCall = new GetItemCall(apiContext);
            //var itemTest = getItemCall.GetItem("261892993899");
            //test token validation
            var timeCall = new GeteBayOfficialTimeCall(apiContext);
            Console.WriteLine(timeCall.GeteBayOfficialTime());
            //
            
            var sellerlistCall = new GetSellerListCall(apiContext);

            sellerlistCall.DetailLevelList.Add(DetailLevelCodeType.ReturnAll);

            bool hasMoreItems = true;
            int pageNumber = 0;

            PaginationType pt = new PaginationType();
            pt.EntriesPerPage = 200;
            //sellerlistCall.Pagination = pt;
            //pt.PageNumber = pageNumber;

            TimeFilter timeFilter = new TimeFilter(DateTime.Now.AddDays(-5), DateTime.Now.AddMonths(3));
            sellerlistCall.EndTimeFilter = timeFilter;
            sellerlistCall.StartTimeFilter = timeFilter;
           

            int counter = 0;
            while (hasMoreItems)
            {
                pageNumber++;

                pt.PageNumber = pageNumber;
                sellerlistCall.Pagination = pt;
                sellerlistCall.EndTimeFilter = timeFilter;
                sellerlistCall.StartTimeFilter = timeFilter;

                sellerlistCall.Execute();
                
                foreach (var item in sellerlistCall.ItemList.ToArray())
                {
                    if (item.SKU != null)
                    {

                        double buyItNowPrice = 0, startPrice = 0;
                        double.TryParse(item.BuyItNowPrice.Value.ToString(), out buyItNowPrice);
                        double.TryParse(item.StartPrice.Value.ToString(), out startPrice);

                        InventoryItems.Add(new InventoryItem
                        {
                            ListingStatus = item.SellingStatus.ListingStatus,
                            ItemId = item.ItemID,
                            Ean = item.SKU,
                            Quantity = item.Quantity,
                            QuantityAvailable = item.QuantityAvailable,
                            BuyItNowPrice = buyItNowPrice,
                            StartPrice = startPrice
                        });
                        Console.WriteLine("{0}\t{1}", item.ItemID, counter); counter++;
                    }
                    else { sbNoSKU.AppendLine(item.ItemID); }
                }

                hasMoreItems = sellerlistCall.HasMoreItems;

            }

            File.WriteAllText(@"C:\EbayInventory\Log_NoSKU_Items.txt", sbNoSKU.ToString());
            File.WriteAllText(@"C:\EbayInventory\Log_ItemCount.txt", counter.ToString());

            //var sellingCall = new GetMyeBaySellingCall(apiContext);

            //sellingCall.DetailLevelList.Add(DetailLevelCodeType.ReturnAll);
            
            //sellingCall.Execute();


            //counter = 0;

            //foreach (ItemType item in sellingCall.ApiResponse.ActiveList.ItemArray)
            //{
            //    double buyItNowPrice = 0;
            //    double.TryParse(item.BuyItNowPrice.ToString(), out buyItNowPrice);
            //    InventoryItems.Add(new InventoryItem
            //    {

            //        ItemId = item.ItemID,
            //        Ean = item.SKU,
            //        Quantity = item.QuantityAvailable,
            //        BuyItNowPrice = buyItNowPrice
            //    });
            //    Console.WriteLine("{0}\t{1}", item.ItemID, counter); counter++;
            //}
            

        }

        public void UpdateDB()
        {
            StringBuilder sbStatusLog = new StringBuilder();
            StringBuilder Log_CompletedorEnded_Items = new StringBuilder();
            StringBuilder sbDups = new StringBuilder();
            sbDups.AppendLine("Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID\tCustomLabel");

            foreach (InventoryItem i in InventoryItems.OrderBy(x=>x.Ean))
            {
                if (i.ListingStatus == ListingStatusCodeType.Completed || i.ListingStatus == ListingStatusCodeType.Ended)
                {
                    Log_CompletedorEnded_Items.AppendLine(i.Ean + "\t" + i.ItemId);
                }

                

                var testDuplicate = InventoryItems.FindAll(x => x.Ean == i.Ean);
                if (testDuplicate.Count() > 1)
                {
                    if (testDuplicate.FindAll(x => x.ListingStatus == ListingStatusCodeType.Active).Count() > 1)
                    {
                        foreach (var dup in testDuplicate)
                        {
                            var testExist = ListingDatabase.Find(dup.ItemId);
                            if (testExist.Ean == null)//indicator as not existing in db
                            {
                                sbDups.AppendLine("EndItem" + "\t" + i.ItemId + "\t" + i.Ean);
                            }
                            else
                            {
                                ListingDatabase.UpdateInventory(testExist.Ean, testExist.ItemID, testExist.BuyItNowPrice, testExist.StartPrice, testExist.Quantity, testExist.QuantityAvailable);
                            }
                        }
                       
                    }
                    else
                    {
                        var testActiveDuplicate = testDuplicate.Find(x => x.ListingStatus == ListingStatusCodeType.Active);
                        ListingDatabase.UpdateInventory(testActiveDuplicate.Ean, testActiveDuplicate.ItemId, testActiveDuplicate.BuyItNowPrice, testActiveDuplicate.StartPrice, testActiveDuplicate.Quantity, testActiveDuplicate.QuantityAvailable);
                        

                        var testEndedDuplicate = testDuplicate.Find(x => x.ListingStatus == ListingStatusCodeType.Completed || x.ListingStatus == ListingStatusCodeType.Ended);
                        ListingDatabase.Remove(testEndedDuplicate.ItemId);
                       
                    }
                }
                else
                {
                    ListingDatabase.UpdateInventory(i.Ean, i.ItemId, i.BuyItNowPrice, i.StartPrice, i.Quantity, i.QuantityAvailable);
                }
               

                sbStatusLog.AppendLine(i.ListingStatus + "\t" + i.Ean + "\t" + i.ItemId + "\t" + i.StartPrice + "\t" + i.Quantity);

                
            }

            File.WriteAllText(@"C:\EbayInventory\Log_Inventory_Items.txt", sbStatusLog.ToString());
            File.WriteAllText(@"C:\EbayInventory\Log_Duplicate_Items.txt", sbDups.ToString());
            File.WriteAllText(@"C:\EbayInventory\Log_CompletedorEnded_Items.txt", Log_CompletedorEnded_Items.ToString());
        }


        public ApiContext GetApiContext()
        {
            apiContext = new ApiContext();

            //set Api Server Url
            apiContext.SoapApiServerUrl = "https://api.ebay.com/wsapi";

            //set Api Token to access eBay Api Server
            ApiCredential apiCredential = new ApiCredential();

            apiCredential.eBayToken = agent.agentEntity.agent.keys.ebay.token;
                 

            apiContext.ApiCredential = apiCredential;
            //set eBay Site target to US
            apiContext.Site = SiteCodeType.US;

            

            //set Api logging
            apiContext.ApiLogManager = new ApiLogManager();
            apiContext.ApiLogManager.ApiLoggerList.Add(
                new FileLogger("listing_log.txt", true, true, true)
                );
            apiContext.ApiLogManager.EnableLogging = true;


            return apiContext;
        }
    }
}
