﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Inventory
{
    public class InventoryProcessor
    {
        public static void Process(IInventory inventory)
        {
            inventory.Download();
            inventory.UpdateDB();
        }
    }
}
