﻿using DN_Classes.Logger;
using EbayCSV.Core.Settings;
using EbayUpload.Core.UploadResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EbayUpload.Core
{
    public class EbayUploader : IEbayUploader
    {
        private readonly IListingFileUploader listingFileUploader;
        private readonly IUploadResponseManager uploadResponseManager;
        private readonly ISettings settings;

        public EbayUploader(IListingFileUploader listingFileUploader, IUploadResponseManager uploadResponseManager, ISettings settings)
        {
            this.listingFileUploader = listingFileUploader;
            this.uploadResponseManager = uploadResponseManager;
            this.settings = settings;
        }

        public void BeginUpload<Tpath>(IEnumerable<Tpath> tValue)
        {
            List<string> csvData = (List<string>)tValue;

            AppLogger.Log("uploading revisions : " + csvData.FindAll(x => x.ToLower().Contains("revision")).Count());
            Upload(csvData.FindAll(x=>x.ToLower().Contains("revision")));

            if (!Boolean.Parse(settings.GetValue("revisionOnly")))
            {
                AppLogger.Log("uploading new listings : " + csvData.FindAll(x => !x.ToLower().Contains("revision")).Count());
                Upload(csvData.FindAll(x => !x.ToLower().Contains("revision")));
            }
        }

        private void Upload(List<string> csvData)
        {
            foreach (string csv in csvData)
            {
                //Console.WriteLine(csv);

                string response = "";

                int errorCount = 0;
                while (errorCount < 2)
                {
                    try
                    {
                        AppLogger.Log("uploading " + csv);
                        response = listingFileUploader.BeginUpload<string>(csv);
                        errorCount = 2;
                    }
                    catch(Exception ex) {
                        Thread.Sleep(5000);
                        errorCount++;
                        AppLogger.Log("upload error :" + csv + "\n" + ex.Message + "\n" + ex.StackTrace);
                    }
                }

                if (response != "")
                {
                    Console.WriteLine(response);
                    uploadResponseManager.Manage(response);

                    if (response.Contains("Mit dieser hochzuladenden Datei wurde die Zahl der pro Tag zulässigen Vorgänge überschritten"))
                    {
                        break;
                    }
                }

                Thread.Sleep(5000);
            }
        }
    }
}
