﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayUpload.Core
{
    public interface IListingFileUploader
    {
        string BeginUpload<TPath>(TPath tValue);
    }
}
