﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayUpload.Core.UploadResponse
{
    public interface IUploadResponseManager
    {
        void Manage(string response);
    }
}
