﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayUpload.Core.UploadResponse
{
    public interface IReferenceParser
    {
        string Parse<TSource>(TSource tSource);
    }
}
