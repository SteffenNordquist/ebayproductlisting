﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayUpload.Core.UploadResponse
{
    public class UploadResponseManager : IUploadResponseManager
    {
        private readonly IDbConnection dbConnection;
        private readonly ISettings settings;
        private readonly IReferenceParser referenceParser;
        private readonly IMessageParser messageParser;

        public UploadResponseManager(IDbConnection dbConnection, ISettings settings, IReferenceParser referenceParser, IMessageParser messageParser)
        {
            this.dbConnection = dbConnection;
            this.settings = settings;
            this.referenceParser = referenceParser;
            this.messageParser = messageParser;
        }

        public void Manage(string response)
        {
            int agentId = int.Parse(settings.GetValue("agentid"));

            string referenceNumber = referenceParser.Parse<string>(response);
            string message = messageParser.Parse<string>(response);

            FileExchangeResponse fileExchangeResponse = new FileExchangeResponse
            {
                id = referenceNumber,
                ReferenceNumber = referenceNumber,
                SupplierName = settings.GetValue("supplier"),
                UploadMessage = message,
                UploadTime = DateTime.Now,
                AgentId = agentId

            };

            bool insertOp = dbConnection.DataAccess.Commands.Insert<FileExchangeResponse>(fileExchangeResponse);
        }
    }
}
