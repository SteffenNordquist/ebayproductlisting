﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace EbayUpload.Core.UploadResponse
{
    public class ReferenceParser : IReferenceParser
    {
        public string Parse<TSource>(TSource tSource)
        {
            string response = (string)(object)tSource;
            Match match = Regex.Match(response, @"lautet (\d+)");
            return match.Value.Replace("lautet", "");
        }
    }
}
