﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EbayUpload.Core
{
    public class FileExchangeListingUploader : IListingFileUploader
    {
        private readonly ISettings settings;

        public FileExchangeListingUploader(ISettings settings)
        {
            this.settings = settings;
        }

        public string BeginUpload<TPath>(TPath tPath)
        {
            string ebayToken = settings.GetValue("ebaytoken");
            Encoding isoEncoding = Encoding.GetEncoding(28591);
            Encoding utf8Encoding = Encoding.UTF8;
            

            string path = (string)(object)tPath;
            FileInfo fileInfo = new FileInfo(path);

            string boundary = "THIS_STRING_SEPARATES";
            string newLine = "\r\n";

            StringBuilder dataStream = new StringBuilder();
            dataStream.Append("--" + boundary + newLine);
            dataStream.Append("Content-Disposition: form-data; name=\"token\"" + newLine + newLine);
            dataStream.Append(ebayToken + newLine);
            dataStream.Append("--" + boundary + newLine);
            dataStream.Append("Content-disposition: form-data; name=\"file\"; filename=\"" + fileInfo.Name + "\"" + newLine);
            dataStream.Append(@"Content-type: text/plain;" + newLine + newLine);

            using (StreamReader sr = new StreamReader(fileInfo.FullName, utf8Encoding))
            {
                String line;
                while ((line = sr.ReadLine()) != null)
                {
                    dataStream.Append(line + "\n"); // read entire file into "dataStream"
                    //Console.WriteLine(line);
                }
            }
            dataStream.Append(newLine);
            dataStream.Append("--" + boundary + newLine);

            // now build the WebRequest...
            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(@"https://bulksell.ebay.de/ws/eBayISAPI.dll?FileExchangeUpload");
            webReq.Method = "POST";
            webReq.ContentType = @"multipart/form-data; boundary=" + boundary;
            webReq.UserAgent = "V.01";
            webReq.KeepAlive = true;
            webReq.ProtocolVersion = HttpVersion.Version10;

            //encoding conversion
            byte[] utfBytes = utf8Encoding.GetBytes(dataStream.ToString());
            byte[] postBuffer = Encoding.Convert(utf8Encoding, isoEncoding, utfBytes);
            //byte[] postBuffer = isoEncoding.GetBytes(dataStream.ToString());
            webReq.ContentLength = postBuffer.Length;

            // post the data
            Stream postDataStream = webReq.GetRequestStream();
            postDataStream.Write(postBuffer, 0, postBuffer.Length);
            postDataStream.Flush();
            postDataStream.Close();

            // Get the response
            HttpWebResponse response = (HttpWebResponse)webReq.GetResponse();

            // Read the response from the stream
            StreamReader responseStream = new StreamReader(response.GetResponseStream(), isoEncoding, true);

            response.Close();
            responseStream.Close();

            string responseString = "";

            try { responseString = responseStream.ReadToEnd(); }
            catch { }

            return responseString;
        }

    }
}
