﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingUploader
{
    public interface IListingUploader
    {
        string Upload(string path);
    }
}
