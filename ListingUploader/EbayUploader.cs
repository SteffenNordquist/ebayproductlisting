﻿using DNEbaySystem.Lister;
using DNEbaySystem.ListingUploader.UploadResponseManager;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingUploader
{
    public class EbayUploader
    {
        private List<string> Filenames;
        private IListingUploader listingUploader;
        private IUploadRespManager uploadRespManager;

        public EbayUploader(EbayLister ebayLister, IListingUploader listingUploader,IUploadRespManager uploadRespManager)
        {
            this.Filenames = ebayLister.GetFilenames();
            this.listingUploader = listingUploader;
            this.uploadRespManager = uploadRespManager;
        }

        public void BeginUpload()
        {
            foreach (string filename in Filenames)
            {
                string uploadResponse = listingUploader.Upload(filename);
                if (uploadResponse != "")
                {
                    uploadRespManager.Save(uploadResponse);
                }
            }
        }
    }
}
