﻿using DNEbaySystem.FileExchange;
using DNEbaySystem.ListingUploader.UploadResponseManager.ResponseParsers;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingUploader.UploadResponseManager
{
    public class BasicUploadRespManager : IUploadRespManager
    {
        private string supplierName;
        private int agentId;

        public BasicUploadRespManager(string supplierName, int agentId)
        {
            this.supplierName = supplierName;
            this.agentId = agentId;
        }

        public void Save(string uploadResponse)
        {
            FileExchangeDB fileExchangeDB = new FileExchangeDB();

            IResponseParser refNumberParser = new RefNumberParser();
            IResponseParser uploadMessageParser = new UploadMessageParser();

            string referenceNumber = refNumberParser.Parse(uploadResponse);
            string uploadMessage = uploadMessageParser.Parse(uploadResponse);

            FileExchangeEntity fe = new FileExchangeEntity
            {
                id = referenceNumber,
                ReferenceNumber = referenceNumber,
                SupplierName = supplierName,
                UploadMessage = uploadMessage,
                UploadTime = DateTime.Now,
                AgentId = agentId

            };

            fileExchangeDB.Insert(fe);
        }

    }
}
