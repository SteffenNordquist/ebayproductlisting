﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingUploader.UploadResponseManager.ResponseParsers
{
    public interface IResponseParser
    {
        string Parse(string reponse);
    }
}
