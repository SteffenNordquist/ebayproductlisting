﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingUploader.UploadResponseManager.ResponseParsers
{
    public class RefNumberParser : IResponseParser
    {
        public string Parse(string reponse)
        {
            Match match = Regex.Match(reponse, @"lautet (\d+)");
            return match.Value.Replace("lautet", "");
        }
    }
}
