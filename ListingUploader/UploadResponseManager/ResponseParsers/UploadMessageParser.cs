﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingUploader.UploadResponseManager.ResponseParsers
{
    public class UploadMessageParser : IResponseParser
    {
        public string Parse(string reponse)
        {
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(reponse);

            string documentText = doc.DocumentNode.InnerText;
            return documentText;
        }
    }
}
