﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingUploader.UploadResponseManager
{
    public interface IUploadRespManager
    {
        void Save(string uploadResponse);
    }
}
