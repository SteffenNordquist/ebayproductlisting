﻿using DN_Classes.Agent;
using EbayCSV.Core.Inventory.InventorySource;
using EbayCSV.Core.Inventory.InventorySource.ListingInventory.BK;
using EbayCSV.Core.Settings;
using EbayInventoryV2.Downloader;
using EbayInventoryV2.EbayAPI;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayInventoryV2
{
    public class EbayInventoryConfiguration
    {
        private readonly IUnityContainer container;
        private readonly Agent agent;

        public EbayInventoryConfiguration(Agent agent)
        {
            this.container = new UnityContainer();
            this.agent = agent;

            RegisterDependencies();
        }

        private void RegisterDependencies()
        {
            container.RegisterInstance(GetSettings());
            container.RegisterInstance(GetListingDatabase());
            container.RegisterType<IEbayAPIContext, EbayAPIContext>();
            container.RegisterType<IIventoryDownloader, InventoryDownloader>();
            container.RegisterType<IDbUpdater, DbUpdater>();
        }

        private ISettings GetSettings()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("ebaytoken", agent.agentEntity.agent.keys.ebay.token);
            settings.SetValue("listingdbconnectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/EAD_");
            settings.SetValue("listingdbdatabasename", "EAD_");
            settings.SetValue("listingdbcollectionname", "listing");

            return settings;
        }

        private IListingDatabase GetListingDatabase()
        {
            IListingDatabaseFactory listingDatabaseFactory = new BKListingDatabaseFactory(container.Resolve<ISettings>());

            IListingDatabase listingDatabase = listingDatabaseFactory.GetListingDatabase<string>(agent.agentEntity.agent.ebayname);

            return listingDatabase;
        }

        public TResult Resolve<TResult>()
        {
            return container.Resolve<TResult>();
        }
    }
}
