﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayInventoryV2.Downloader
{
    public interface IIventoryDownloader
    {
        IEnumerable<TResult> DownloadInventory<TResult>();
    }
}
