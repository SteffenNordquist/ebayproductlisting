﻿using eBay.Service.Core.Soap;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.Inventory.InventorySource;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayInventoryV2
{
    public class DbUpdater : IDbUpdater
    {
        private readonly IListingDatabase listingDatabase;

        public DbUpdater(IListingDatabase listingDatabase)
        {
            this.listingDatabase = listingDatabase;
        }

        public void Update<TSource>(IEnumerable<TSource> tSources)
        {
            List<EbayInventoryItem> inventoryItems = (List<EbayInventoryItem>)(IEnumerable<object>)tSources;

            StringBuilder sbStatusLog = new StringBuilder();
            StringBuilder sbCompletedLog = new StringBuilder();
            StringBuilder sbEndedLog = new StringBuilder();
            StringBuilder sbDups = new StringBuilder();
            sbDups.AppendLine("Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID\tCustomLabel");

            foreach (EbayInventoryItem i in inventoryItems.OrderBy(x => x.Ean))
            {
                if (i.ListingStatus == ListingStatusCodeType.Completed)
                {
                    sbCompletedLog.AppendLine(i.Ean + "\t" + i.ItemId);
                }
                else if ( i.ListingStatus == ListingStatusCodeType.Ended)
                {
                    sbEndedLog.AppendLine(i.Ean + "\t" + i.ItemId);
                }



                var testDuplicate = inventoryItems.FindAll(x => x.Ean == i.Ean);
                if (testDuplicate.Count() > 1)
                {
                    if (testDuplicate.FindAll(x => x.ListingStatus == ListingStatusCodeType.Active).Count() > 1)
                    {
                        foreach (var dup in testDuplicate)
                        {
                            string query = " { ItemID : \"" + dup.ItemId + "\" } ";

                            InventoryItem testExist = null;

                            try
                            {
                                testExist = listingDatabase.dataReader().Find<InventoryItem, string>(query).First();
                            }
                            catch { }

                            if (testExist == null || testExist.Ean == null)//indicator as not existing in db
                            {
                                sbDups.AppendLine("EndItem" + "\t" + i.ItemId + "\t" + i.Ean);
                            }
                            else
                            {
                                string updateQuery = " { _id : \"" + testExist.Ean + "\" } ";
                                listingDatabase.dataWriter().Update<double>(updateQuery, "BuyItNowPrice", testExist.BuyItNowPrice, false, true);
                                listingDatabase.dataWriter().Update<double>(updateQuery, "StartPrice", testExist.StartPrice);
                                listingDatabase.dataWriter().Update<int>(updateQuery, "Quantity", testExist.Quantity);
                                listingDatabase.dataWriter().Update<int>(updateQuery, "QuantityAvailable", testExist.QuantityAvailable);
                                listingDatabase.dataWriter().Update<string>(updateQuery, "ItemID", testExist.ItemID);
                                listingDatabase.dataWriter().Update<string>(updateQuery, "ListingStatus", i.ListingStatus.ToString());
                                listingDatabase.dataWriter().Update<string>(updateQuery, "Ean", testExist.Ean);
                            }
                        }

                    }
                    else
                    {
                        var testActiveDuplicate = testDuplicate.Find(x => x.ListingStatus == ListingStatusCodeType.Active);
                        string updateQuery = " { _id : \"" + testActiveDuplicate.Ean + "\" } ";
                        listingDatabase.dataWriter().Update<double>(updateQuery, "BuyItNowPrice", testActiveDuplicate.BuyItNowPrice, false, true);
                        listingDatabase.dataWriter().Update<double>(updateQuery, "StartPrice", testActiveDuplicate.StartPrice);
                        listingDatabase.dataWriter().Update<int>(updateQuery, "Quantity", testActiveDuplicate.Quantity);
                        listingDatabase.dataWriter().Update<int>(updateQuery, "QuantityAvailable", testActiveDuplicate.QuantityAvailable);
                        listingDatabase.dataWriter().Update<string>(updateQuery, "ItemID", testActiveDuplicate.ItemId);
                        listingDatabase.dataWriter().Update<string>(updateQuery, "ListingStatus", i.ListingStatus.ToString());
                        listingDatabase.dataWriter().Update<string>(updateQuery, "Ean", testActiveDuplicate.Ean);


                        var testEndedDuplicate = testDuplicate.Find(x => x.ListingStatus == ListingStatusCodeType.Completed || x.ListingStatus == ListingStatusCodeType.Ended);
                        string deleteQuery = " { ItemID : \"" + testEndedDuplicate.ItemId + "\" } ";
                        listingDatabase.dataWriter().Delete<string>(deleteQuery);

                    }
                }
                else
                {
                    string updateQuery = " { _id : \"" + i.Ean + "\" } ";
                    listingDatabase.dataWriter().Update<double>(updateQuery, "BuyItNowPrice", i.BuyItNowPrice, false, true);
                    listingDatabase.dataWriter().Update<double>(updateQuery, "StartPrice", i.StartPrice);
                    listingDatabase.dataWriter().Update<int>(updateQuery, "Quantity", i.Quantity);
                    listingDatabase.dataWriter().Update<int>(updateQuery, "QuantityAvailable", i.QuantityAvailable);
                    listingDatabase.dataWriter().Update<string>(updateQuery, "ItemID", i.ItemId);
                    listingDatabase.dataWriter().Update<string>(updateQuery, "ListingStatus", i.ListingStatus.ToString());
                    listingDatabase.dataWriter().Update<string>(updateQuery, "Ean", i.Ean);
                }


                sbStatusLog.AppendLine(i.ListingStatus + "\t" + i.Ean + "\t" + i.ItemId + "\t" + i.StartPrice + "\t" + i.Quantity);


            }

            File.WriteAllText(@"C:\EbayInventory\Log_Inventory_Items.txt", sbStatusLog.ToString());
            File.WriteAllText(@"C:\EbayInventory\Log_Duplicate_Items.txt", sbDups.ToString());
            File.WriteAllText(@"C:\EbayInventory\Log_Completed_Items.txt", sbCompletedLog.ToString());
            File.WriteAllText(@"C:\EbayInventory\Log_Ended_Items.txt", sbEndedLog.ToString());
        }
    }
}
