﻿using eBay.Service.Core.Soap;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayInventoryV2
{
    public class EbayInventoryItem
    {
        public ListingStatusCodeType ListingStatus { get; set; }
        public string ItemId { get; set; }
        public string Ean { get; set; }
        public int Quantity { get; set; }
        public int QuantityAvailable { get; set; }
        public double BuyItNowPrice { get; set; }
        public double StartPrice { get; set; }
    }
}
