﻿using DN_Classes.Agent;
using DN_Classes.AppStatus;
using EbayInventoryV2.Downloader;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayInventoryV2
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ApplicationStatus appStatus = new ApplicationStatus("EbayInventory_Francesco"))
            {
                try
                {
                    Agent agent = new Agent(4);

                    EbayInventoryConfiguration inventoryConfiguration = new EbayInventoryConfiguration(agent);

                    IIventoryDownloader inventoryDownloader = inventoryConfiguration.Resolve<IIventoryDownloader>();
                    List<EbayInventoryItem> inventoryItems = inventoryDownloader.DownloadInventory<EbayInventoryItem>().ToList();

                    IDbUpdater inventoryUpdater = inventoryConfiguration.Resolve<IDbUpdater>();
                    inventoryUpdater.Update<EbayInventoryItem>(inventoryItems);


                    appStatus.AddMessagLine("Success");
                    appStatus.Successful();
                }
                catch (Exception ex)
                {
                    appStatus.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
                }
            }
        }
    }
}
