﻿
using DN_Classes.Agent;
using DN_Classes.Supplier;
using ProcuctDB;
using ProcuctDB.DatabaseFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.EbayAgentSuppliers
{
    public class AgentSuppliers
    {
        private Agent agent = null;

        public AgentSuppliers(Agent agent)
        {
            this.agent = agent;
        }

        public List<IDatabase> GetList()
        {
            List<IDatabase> supplierList = new List<IDatabase>();

            foreach (Supplier supplier in this.agent.agentEntity.agent.platforms.Where(x => x.name == "ebay").First().suppliers)
            {
                supplierList.Add(DatabaseParser.Parse(supplier.name));
            }

            return supplierList;
        }
    }
}
