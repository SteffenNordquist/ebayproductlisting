﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;
using MongoDB.Driver;
using System.IO;
using MongoDB.Driver.Builders;

namespace ResetCategories
{
    class Program
    {
        static void Main(string[] args)
        {
            //francesco
            var listingCollection = new MongoClient("mongodb://client148:client148devnetworks@136.243.44.111/EAD_francesco-alberti")
                .GetServer()
                .GetDatabase("EAD_francesco-alberti")
                .GetCollection<BsonDocument>("listing");

            var supplierCollection = new MongoClient("mongodb://client148:client148devnetworks@136.243.44.111/PD_Haba")
                .GetServer()
                .GetDatabase("PD_Haba")
                .GetCollection<BsonDocument>("haba");

            var eBayCategoriesCollection = new MongoClient("mongodb://client148:client148devnetworks@136.243.44.111/EbayCategories")
                .GetServer()
                .GetDatabase("EbayCategories")
                .GetCollection<BsonDocument>("ebaycategoriespaths");

            List<string> failedEans = File.ReadAllLines("failedEans.txt").ToList();

            //step 1
            var listings = listingCollection.Find(Query.Matches("SupplierName","Haba")).SetFields("Ean", "SupplierName").ToList();
            foreach (string failedEan in failedEans)
            {
                if (listings.Exists(x => x["Ean"].AsString.Contains(failedEan)))
                {
                    var failedListing = listings.Find(x => x["Ean"].AsString.Contains(failedEan));
                    string failedListingEan = failedListing["Ean"].AsString;
                    var plusEbayCategoryUnsetOperation = supplierCollection.Update(Query.EQ("product.ean", failedListingEan), Update.Unset("plus.ebaycategory"), UpdateFlags.Multi);
                    if (plusEbayCategoryUnsetOperation.UpdatedExisting)
                    {
                        Console.WriteLine("plus.ebaycategory unset : " + failedListingEan);
                    }

                    var ebayCategoryDocument = eBayCategoriesCollection.FindOne(Query.EQ("eans", failedListingEan));
                    string categoryId = ebayCategoryDocument["_id"].AsString;
                    var removeEanFromListOperation = eBayCategoriesCollection.Update(Query.EQ("_id", categoryId),
                        Update.Pull("eans", failedListingEan));
                    if (removeEanFromListOperation.UpdatedExisting)
                    {
                        Console.WriteLine("remove ean from the category : " + failedListingEan);
                    }
                }
            }
        }
    }
}
