﻿using DNEbaySystem.ListingDatabase.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingDatabase.Interfaces
{
    public interface IListingDatabaseInventory
    {
        /// <summary>
        /// update item's database details
        /// </summary>
        /// <param name="ean"></param>
        /// <param name="itemid"></param>
        /// <param name="newBuyItNowPrice"></param>
        /// <param name="newStartPrice"></param>
        /// <param name="newQuantity"></param>
        /// <param name="newQuantityAvailable"></param>
        void UpdateInventory(string ean, string itemid, double newBuyItNowPrice, double newStartPrice, int newQuantity, int newQuantityAvailable);
        
        /// <summary>
        /// returns listing matched by query
        /// </summary>
        /// <param name="itemId"></param>
        /// <returns></returns>
        ListingEntity Find(string itemId);

        /// <summary>
        /// removes matched item
        /// </summary>
        /// <param name="itemId"></param>
        void Remove(string itemId);
    }
}
