﻿using DNEbaySystem.ListingDatabase.Objects;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingDatabase.Interfaces
{
    public interface IListingDatabase
    {
        /// <summary>
        /// returns the listing queried using ean
        /// </summary>
        /// <param name="ean"></param>
        /// <returns>Listing Object</returns>
        ListingEntity Find(string ean);

        /// <summary>
        /// removes listing record in database using itemId
        /// </summary>
        /// <param name="itemId"></param>
        void Remove(string itemId);

        /// <summary>
        /// set listing's descriptionUpdateFlag whether true or false
        /// </summary>
        /// <param name="ean"></param>
        /// <param name="supplierName"></param>
        /// <param name="update"></param>
        void SetDescriptionUpdateFlag(string ean, string supplierName, bool update);

        /// <summary>
        /// set listing's titleUpdateFlag whether true or false
        /// </summary>
        /// <param name="ean"></param>
        /// <param name="supplierName"></param>
        /// <param name="update"></param>
        void SetTitleUpdateFlag(string ean, string supplierName, bool update);

        /// <summary>
        /// inserts the listing in database, updates if already exists
        /// </summary>
        /// <param name="listing"></param>
        void Upsert(ListingEntity listing);

        /// <summary>
        /// checks if a listing exists
        /// </summary>
        /// <param name="ean"></param>
        /// <param name="supplierName"></param>
        /// <returns></returns>
        bool IsListingExists(string ean, string supplierName);

        /// <summary>
        /// checks if item is subject for deletion in ebay account
        /// </summary>
        /// <param name="ean"></param>
        /// <returns></returns>
        bool IsSubjectForDeletion(string ean);

        /// <summary>
        /// checks if item needs update
        /// </summary>
        /// <param name="ean"></param>
        /// <param name="stock"></param>
        /// <returns></returns>
        bool NeedsUpdate(string ean, int stock);
    }
}
