﻿using DNEbaySystem.ListingDatabase.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingDatabase.Interfaces
{
    public interface IListingDatabasePriceCalculation
    {
        /// <summary>
        /// insert or updates listing information in database
        /// </summary>
        /// <param name="listing"></param>
        void Upsert(ListingEntity listing);
    }
}
