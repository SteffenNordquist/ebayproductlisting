﻿using DNEbaySystem.FileExchange.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingDatabase.Interfaces
{
    public interface IListingDatabaseUploadStatuses
    {
        /// <summary>
        /// updates item's upload status
        /// </summary>
        /// <param name="itemStatusData"></param>
        void UpdateStatuses(ItemStatusData itemStatusData);
    }
}
