﻿using DNEbaySystem.ListingDatabase.Interfaces;
using DNEbaySystem.ListingDatabase.Objects;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingDatabase.Implementations
{
    public class AgentListingDatabasePriceCalculation : IListingDatabasePriceCalculation
    {
        private MongoCollection<ListingEntity> Collection = null;

        public AgentListingDatabasePriceCalculation(MongoCollection<ListingEntity> collection)
        {
            this.Collection = collection;
        }

        public void Upsert(Objects.ListingEntity listing)
        {
            Collection.Update
                (
                    Query.EQ("_id", listing.id),
                    Update<ListingEntity>.Set(x => x.Wanted, listing.Wanted)
                                         .Set(x => x.Ean, listing.Ean)
                                         .Set(x => x.SupplierName, listing.SupplierName),
                    UpdateFlags.Upsert
                );
        }
    }
}
