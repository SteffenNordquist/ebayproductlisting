﻿using DNEbaySystem.ListingDatabase.Interfaces;
using DNEbaySystem.ListingDatabase.Objects;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingDatabase.Implementations
{
    public class AgentListingDatabase : IListingDatabase
    {
        private MongoCollection<ListingEntity> Collection = null;

        public AgentListingDatabase(MongoCollection<ListingEntity> collection)
        {
            this.Collection = collection;
        }

        public ListingEntity Find(string ean)
        {
            var test = Collection.FindOne(Query.EQ("Ean", ean));

            if (test == null) { return new ListingEntity { plus = new BsonDocument() }; }

            return test;
        }

        public void Remove(string itemId)
        {
            Collection.Remove(Query.EQ("ItemID", itemId));
        }

        public void SetDescriptionUpdateFlag(string ean, string supplierName, bool update)
        {
            try
            {
                Collection.Update(Query.And(Query.EQ("Ean", ean), Query.Matches("SupplierName", supplierName)), Update.Set("UpdateDescription", update));
            }
            catch { }
        }

        public void SetTitleUpdateFlag(string ean, string supplierName, bool update)
        {
            try
            {
                Collection.Update(Query.And(Query.EQ("Ean", ean), Query.Matches("SupplierName", supplierName)), Update.Set("UpdateTitle", update));
            }
            catch { }
        }

        public void Upsert(ListingEntity listing)
        {
            if (Collection.Find(Query.EQ("_id", listing.id)).Count() == 0)
            {
                Collection.Insert(listing);
            }
            else
            {
                Collection.Update(Query.EQ("_id", listing.id), Update<ListingEntity>.Set(x => x.Wanted, listing.Wanted).Set(x => x.Ean, listing.Ean).Set(x => x.SupplierName, listing.SupplierName).Set(x => x.StartPrice, listing.StartPrice));
            }
        }

        public bool IsListingExists(string ean, string supplierName)
        {
            var listing = Collection.FindOne(Query.And(Query.EQ("Ean", ean), Query.EQ("SupplierName", supplierName)));
            if (listing == null)
            {
                return false;
            }
            else
            {
                if (listing.ItemID == null)
                {
                    return false;
                }
            }

            return true;
        }

        public bool IsSubjectForDeletion(string ean)
        {
            //var item = Collection.FindOne(Query.EQ("Ean", ean));

            //if (item != null)
            //{
            //    if (item.Views == null || item.Views == 0)
            //    {
            //        return true;
            //    }
            //}

            return false;
        }

        public bool NeedsUpdate(string ean, int stock)
        {
            var listing = Collection.FindOne(Query.EQ("Ean", ean));

            if (listing != null)
            {
                if (listing.Quantity != stock)
                {
                    return true;
                }
                else if (listing.Wanted != null && listing.StartPrice < listing.Wanted.Price)
                {
                    return true;
                }
            }

            return false;
        }
    }
}
