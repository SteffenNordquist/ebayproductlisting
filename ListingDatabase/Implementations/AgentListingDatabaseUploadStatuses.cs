﻿using DNEbaySystem.ListingDatabase.Interfaces;
using DNEbaySystem.ListingDatabase.Objects;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingDatabase.Implementations
{
    public class AgentListingDatabaseUploadStatuses : IListingDatabaseUploadStatuses
    {
        private MongoCollection<ListingEntity> Collection = null;

        public AgentListingDatabaseUploadStatuses(MongoCollection<ListingEntity> collection)
        {
            this.Collection = collection;
        }

        public void UpdateStatuses(FileExchange.Objects.ItemStatusData itemStatusData)
        {
            Collection.Update(Query.EQ("Ean", itemStatusData.Ean), Update<ListingEntity>.Set(x => x.ItemStatusData, itemStatusData));
        }
    }
}
