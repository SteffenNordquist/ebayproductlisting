﻿using DNEbaySystem.ListingDatabase.Interfaces;
using DNEbaySystem.ListingDatabase.Objects;
using MongoDB.Bson;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingDatabase.Implementations
{
    public class AgentListingDatabaseInventory : IListingDatabaseInventory
    {


        private MongoCollection<ListingEntity> Collection = null;

        public AgentListingDatabaseInventory(MongoCollection<ListingEntity> collection)
        {
            this.Collection = collection;
        }

        public void UpdateInventory(string ean, string itemid, double newBuyItNowPrice, double newStartPrice, int newQuantity, int newQuantityAvailable)
        {
            Collection.Update(Query.EQ("_id", ean), Update.Set("BuyItNowPrice", newBuyItNowPrice).Set("StartPrice", newStartPrice).Set("Quantity", newQuantity).Set("QuantityAvailable", newQuantityAvailable).Set("ItemID", itemid).Set("Ean", ean), UpdateFlags.Upsert);
        }

        public ListingEntity Find(string itemId)
        {
            return Collection.FindOne(Query.EQ("ItemID", itemId));
        }

        public void Remove(string itemId)
        {
            Collection.Remove(Query.EQ("ItemID", itemId));
        }
    }
}
