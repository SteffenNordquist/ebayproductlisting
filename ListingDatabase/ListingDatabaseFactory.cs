﻿using DNEbaySystem.ListingDatabase.Implementations;
using DNEbaySystem.ListingDatabase.Objects;
using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.ListingDatabase
{
    public class ListingDatabaseFactory
    {
        private MongoCollection<ListingEntity> Collection = null;
        private MongoDatabase Database = null;
        private MongoServer Server = null;
        private List<MongoDatabase> Databases = new List<MongoDatabase>();


        private MongoDatabase CreateDB(string ebayname)
        {

            MongoUser user = new MongoUser("client148", "client148devnetworks", false);


            string dbname = "EAD_" + ebayname.ToString();

            MongoDatabase NewDB = Server.GetDatabase(dbname);
            NewDB.CreateCollection("listing");

            NewDB.AddUser(user);

            Console.WriteLine("Database Created : " + dbname);

            return NewDB;

        }

        private MongoDatabase GetFromDatabaseList(string ebayname)
        {
            ebayname = ebayname.Replace(".", "");

            this.Server = new MongoClient("mongodb://steffennordquist:adminsteffen148devnetworks@148.251.0.235/admin")
                .GetServer();

            var dbs = Server.GetDatabaseNames().Select(x => x).Where(x => x.StartsWith("EAD") && x.Contains(ebayname));


            if (dbs.Count() == 0)
            {
                return CreateDB(ebayname);
            }

            return Server.GetDatabase(dbs.First());

        }

        private MongoDatabase GetDatabase(string ebayname)
        {
            if (this.Database == null)
            {
                Database = GetFromDatabaseList(ebayname);
            }

            return Database;
        }

        private MongoCollection<ListingEntity> GetCollection(string ebayname)
        {
            if (Collection == null)
            {
                Collection = GetDatabase(ebayname).GetCollection<ListingEntity>("listing");
            }

            return Collection;
        }




        public AgentListingDatabase GetListingDatabase(string ebayname)
        {

            AgentListingDatabase agentListingDatabase = new AgentListingDatabase(GetCollection(ebayname));

            return agentListingDatabase;

        }

        public AgentListingDatabaseInventory GetListingDatabaseInventory(string ebayname)
        {

            AgentListingDatabaseInventory agentListingDatabase = new AgentListingDatabaseInventory(GetCollection(ebayname));

            return agentListingDatabase;

        }

        public AgentListingDatabaseUploadStatuses GetListingDatabaseUploadStatuses(string ebayname)
        {

            AgentListingDatabaseUploadStatuses agentListingDatabase = new AgentListingDatabaseUploadStatuses(GetCollection(ebayname));

            return agentListingDatabase;

        }

        public AgentListingDatabasePriceCalculation GetListingDatabasePriceCalculation(string ebayname)
        {

            AgentListingDatabasePriceCalculation agentListingDatabase = new AgentListingDatabasePriceCalculation(GetCollection(ebayname));

            return agentListingDatabase;

        }
    }
}
