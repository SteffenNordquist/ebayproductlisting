﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.DatabaseConstructor
{
    public class DBConstructor<T>
    {
        public static MongoCollection<T> GetCollection(string connectionString, string dbName, string collectionName)
        {
            return new MongoClient(connectionString)
                .GetServer()
                .GetDatabase(dbName)
                .GetCollection<T>(collectionName);
        }
    }
}
