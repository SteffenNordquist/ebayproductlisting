﻿using EbayCSV.Core.Factories.TitleFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Decorators
{
    /// <summary>
    /// Manages the maximum length of the product title
    /// </summary>
    public class TitleLengthDecorator : ITitleDecorator
    {
        /// <summary>
        /// A property holding the maximum title length
        /// </summary>
        private readonly IMaxTitleLength maxTitleLength;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="maxTitleLength">A property holding the maximum title length</param>
        public TitleLengthDecorator(IMaxTitleLength maxTitleLength)
        {
            this.maxTitleLength = maxTitleLength;
        }

        /// <summary>
        /// Processes the title with the title max length rule
        /// </summary>
        /// <typeparam name="TResult">return type</typeparam>
        /// <typeparam name="TSource">parameter type</typeparam>
        /// <param name="tSource">The raw title</param>
        /// <returns>Returns the final title with the max length rule</returns>
        /// <seealso cref="EbayCSV.Core.Decorators.ITitleDecorator.GetDecoratedItem"/>
        public TResult GetDecoratedItem<TResult, TSource>(TSource tSource)
        {
            string source = (string)(object)tSource;

            return (TResult)(object)Truncate(source, maxTitleLength.GetMaxTitleLength());
        }

        /// <summary>
        /// Truncates the title with the max length and additional suffix
        /// </summary>
        /// <param name="value">the raw title</param>
        /// <param name="maxLength">maximum title length</param>
        /// <param name="suffix">additional suffix</param>
        /// <returns>Returns the final title with the max length rule</returns>
        private string Truncate(string value, int maxLength, string suffix = "")
        {
            maxLength -= suffix.Length;

            if (!string.IsNullOrEmpty(value) && value.Length > maxLength)
            {
                return value.Substring(0, maxLength) + suffix;
            }

            return value + suffix;
        }
    }
}
