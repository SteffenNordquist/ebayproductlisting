﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Decorators
{
    /// <summary>
    /// Manages the maximum length of the product title
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Decorators.TitleLengthDecorator"/>
    public interface ITitleDecorator
    {
        /// <summary>
        /// Processes the title with the title max length rule
        /// </summary>
        /// <typeparam name="TResult">return type</typeparam>
        /// <typeparam name="TSource">parameter type</typeparam>
        /// <param name="tSource">The raw title</param>
        /// <returns>Returns the final title with the max length rule</returns>
        TResult GetDecoratedItem<TResult, TSource>(TSource tSource);
    }
}
