﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvHeader
{
    public class BasicHeader : ICsvHeader
    {
        public string GetNewActionHeader()
        {
            return "Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tCategory\tTitle\tSubtitle\tDescription\tConditionID\tPicURL\tQuantity\tFormat\tStartPrice\tBuyItNowPrice\tDuration\tImmediatePayRequired\tLocation\tGalleryType\tPayPalAccepted\tPayPalEmailAddress\tPaymentInstructions\tStoreCategory\tShippingDiscountProfileID\tDomesticRateTable\tShippingType\tShippingService-1:Option\tShippingService-1:Cost\tShippingService-1:Priority\tShippingService-1:ShippingSurcharge\tShippingService-2:Option\tShippingService-2:Cost\tShippingService-2:Priority\tShippingService-2:ShippingSurcharge\tDispatchTimeMax\tCustomLabel\tReturnsAcceptedOption\tRefundOption\tReturnsWithinOption\tShippingCostPaidByOption\tAdditionalDetails\tShippingProfileName\tReturnProfileName\tPaymentProfileName\tProductBrand";
        }

        public string GetReviseActionHeader()
        {
            return "Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID\tTitle\tSiteID\tCurrency\tStartPrice\tBuyItNowPrice\tQuantity\tCustomLabel\tPicURL\tDescription\tProductBrand";
        }
    }
}
