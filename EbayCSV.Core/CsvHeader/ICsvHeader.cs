﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvHeader
{
    public interface ICsvHeader
    {
        string GetNewActionHeader();

        string GetReviseActionHeader();
    }
}
