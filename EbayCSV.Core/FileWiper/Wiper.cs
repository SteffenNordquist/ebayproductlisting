﻿using DN_Classes.Logger;
using EbayCSV.Core.ResultDirectory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.FileWiper
{
    public class Wiper : IWiper
    {
        private IResultDirectory resultDirectory;

        public Wiper(IResultDirectory resultDirectory)
        {
            this.resultDirectory = resultDirectory;
        }

        public void Wipe()
        { 
            DirectoryInfo resultDirectoryInfo = new DirectoryInfo(resultDirectory.GetResultDirectory());
            foreach (FileInfo file in resultDirectoryInfo.GetFiles())
            {
                File.Delete(file.FullName);
                AppLogger.Info(file.FullName + " wiped");
            }
        }
    }
}
