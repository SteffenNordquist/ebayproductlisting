﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.FileWiper
{
    public interface IWiper
    {
        void Wipe();
    }
}
