﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Logger
{
    public class DefaultLogger : ILogger
    {
        private List<string> logs = new List<string>();
        private string filename = "";

        public DefaultLogger(string filename)
        {
            this.filename = filename;
            logs.Add(DateTime.Now.ToShortTimeString());
        }

        public void Log(string log)
        {
            log = DateTime.Now.ToString("hh:mm:ss") + " " + log;
            this.logs.Add(log);
            Console.WriteLine(log);
            //File.WriteAllLines(filename, logs);
        }

        public void Dispose()
        {
            File.WriteAllLines(filename, logs);
        }
    }
}
