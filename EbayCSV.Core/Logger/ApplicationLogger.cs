﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Logger
{
    public static class ApplicationLogger
    {
        private static ILogger _logger = null;

        public static void Log(string log)
        {
            if (_logger == null)
            {
                _logger = new DefaultLogger("appLog.txt");
            }

            _logger.Log(log);
        }

        public static void SetLogger(ILogger logger)
        {
            _logger = logger;
        }
    }
}
