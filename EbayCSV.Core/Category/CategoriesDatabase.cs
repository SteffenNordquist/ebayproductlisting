﻿using DN.DataAccess.ConnectionFactory;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Category
{
    /// <summary>
    /// A class that enables interaction in getting the ebay category by ean
    /// </summary>
    public class CategoriesDatabase : ICategoriesDatabase
    {
        /// <summary>
        /// A delegate in communicating to the ebay categories collection.
        /// </summary>
        private readonly IDbConnection dbConnection;

        /// <summary>
        /// The construction of this class
        /// </summary>
        /// <param name="dbConnection">A delegate in communicating to the ebay categories collection.</param>
        public CategoriesDatabase(IDbConnection dbConnection)
        {
            this.dbConnection = dbConnection;
        }

        /// <summary>
        /// A method that uses the the param ean to get its ebay category from the collection.
        /// </summary>
        /// <param name="ean">13 digit European Article Number</param>
        /// <returns>returns an ebay category</returns>
        /// <seealso cref="EbayCSV.Core.Category.ICategoriesDatbase.GetCategory"/>
        public string GetCategory(string ean)
        {
            string query = " { eans : \"" + ean + "\" } ";
            string[] setfields = new string[]{"_id"};
            var records = dbConnection.DataAccess.Queries.Find<BsonDocument>(query, setfields);

            if (records == null || !records.Any())
            {
                return null;
            }

            return records.First()["_id"].AsString;
        }
    }
}
