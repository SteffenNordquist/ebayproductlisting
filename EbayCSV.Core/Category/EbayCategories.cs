﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Category
{
    public class EbayCategories
    {
        public static ICategoriesDatabase categoriesDatabase;

        public static string GetCategory(string ean)
        {
            if (categoriesDatabase == null)
            {
                throw new ArgumentNullException("'ICategoriesDatabase is null'");
            }

            return categoriesDatabase.GetCategory(ean);
        }
    }
}
