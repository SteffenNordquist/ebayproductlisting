﻿using DN.DataAccess;
using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Category.Factory
{
    /// <summary>
    /// It is a factory for the ebay categories collection.
    /// </summary>
    public class CategoriesDatabaseFactory : ICategoriesDatabaseFactory
    {
        /// <summary>
        /// The settings/configuration container
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// Constructor of this class.
        /// </summary>
        /// <param name="settings">The settings/configuration container</param>
        public CategoriesDatabaseFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// A method that constructs a class implementing ICategoriesDatabase
        /// </summary>
        /// <returns>returns a class that enables interaction in getting the ebay category by ean</returns>
        public ICategoriesDatabase GetCategoriesDatabase()
        {
            #region dn data access
            IDbConnectionFactory connectionFactory = new MongoConnectionFactory(new MongoConnectionProperties());
            connectionFactory.ConnectionProperties.SetProperty("connectionstring", settings.GetValue("ebaycategoriesconnectionstring"));
            connectionFactory.ConnectionProperties.SetProperty("databasename", settings.GetValue("ebaycategoriesdatabasename"));
            connectionFactory.ConnectionProperties.SetProperty("collectionname", settings.GetValue("ebaycategoriescollectionname"));
            #endregion

            IDbConnection dbConnection = connectionFactory.CreateConnection();

            return new CategoriesDatabase(dbConnection);
        }
    }
}
