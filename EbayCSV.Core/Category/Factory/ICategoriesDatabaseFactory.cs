﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Category.Factory
{
    /// <summary>
    /// It is a factory for the ebay categories collection.
    /// </summary>
    public interface ICategoriesDatabaseFactory
    {
        /// <summary>
        /// A method that constructs a class implementing ICategoriesDatabase
        /// </summary>
        /// <returns>returns a class that enables interaction in getting the ebay category by ean</returns>
        ICategoriesDatabase GetCategoriesDatabase();
    }
}
