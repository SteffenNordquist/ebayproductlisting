﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Category
{
    /// <summary>
    /// Enables interaction in getting the ebay category by ean
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Category.CategoriesDatabase"/>
    public interface ICategoriesDatabase
    {
        /// <summary>
        /// A method that uses the the param ean to get its ebay category from the collection.
        /// </summary>
        /// <param name="ean">13 digit European Article Number</param>
        /// <returns>returns an ebay category</returns>
        string GetCategory(string ean);
    }
}
