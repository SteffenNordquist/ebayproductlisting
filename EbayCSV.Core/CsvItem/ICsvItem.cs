﻿using EbayCSV.Core.ListingAction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem
{
    /// <summary>
    /// Represents an item/line of a csv file
    /// Contains ready-to-use properties for an item/line
    /// </summary>
    /// <seealso cref="EbayCSV.Core.CsvItem.BasicItem"/>
    /// <seealso cref="EbayCSV.Core.CsvItem.CatalogItem"/>
    public interface ICsvItem
    {
        /// <summary>
        /// Returns own ActionType property
        /// </summary>
        /// <returns>Returns own ActionType property</returns>
        ActionType GetActionType();

        /// <summary>
        /// Delegates the work for processing properties into a add-formatted line
        /// </summary>
        /// <returns>list of add-formatted lines for this item</returns>
        IEnumerable<string> GetNewActionFormat();

        /// <summary>
        /// Delegates the work for processing properties into a revision-formatted line
        /// </summary>
        /// <returns>list of revision-formatted lines for this item</returns>
        IEnumerable<string> GetReviseActionFormat();
    }
}
