﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.ActionFormats
{
    /// <summary>
    /// Manages the conversion from a csv line-ready item into a csv revision-formatted line
    /// </summary>
    /// <seealso cref="QuantityRevisionFormat"/>
    /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.Catalog.CatalogDefaultRevision"/>
    /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.Basic.BasicDefaultRevision"/>
    /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.Basic.BasicCsvRevision"/>
    public interface IRevisionFormat
    {
        /// <summary>
        /// Converts csv line-ready item into a revision-formatted csv line
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">e</param>
        /// <returns>Returns csv line converted item</returns>
        IEnumerable<string> GetRevisionFormat<T>(T value);
    }
}
