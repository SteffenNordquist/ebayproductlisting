﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.ActionFormats
{
    /// <summary>
    /// Manages the conversion from a csv line-ready item into a csv newlisting-formatted line
    /// </summary>
    /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.Catalog.CatalogDefaulAddition"/>
    /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.Basic.BasicCsvAddition"/>
    /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.Basic.BasicDefaultAddition"/>
    public interface IAdditionFormat
    {
        /// <summary>
        /// Converts csv line-ready item into a newlisting-formatted csv line
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="value">e</param>
        /// <returns>Returns csv line converted item</returns>
        IEnumerable<string> GetAdditionFormat<T>(T value);
    }
}
