﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.ActionFormats.Basic
{
    /// <summary>
    /// Manages the conversion from a csv line-ready basic item into a revision-formatted csv line
    /// </summary>
    public class BasicFullRevision : IRevisionFormat
    {
        /// <summary>
        /// Converts csv line-ready basic item into a revision-formatted csv line
        /// </summary>
        /// <typeparam name="T">Assumed to be BasicItem</typeparam>
        /// <param name="value">BasicItem instance</param>
        /// <returns>Returns csv line converted basic item</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.IRevisionFormat.GetRevisionFormat"/>
        public IEnumerable<string> GetRevisionFormat<T>(T value)
        {
            BasicItem basicItem = (BasicItem)(object)value;

            List<string> actionRevisionListingsFormat = new List<string>();

            var line = String.Format(
                            "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}\t{20}\t{21}\t{22}\t{23}\t{24}\t{25}\t{26}\t{27}\t{28}\t{29}\t{30}\t{31}\t{32}\t{33}\t{34}\t{35}\t{36}\t{37}\t{38}\t{39}\t{40}\t{41}\t{42}\t{43}\t{44}\t{45}\t{46}\t{47}\t{48}\t{49}",
                            basicItem.Action,
                            basicItem.CustomLabel,
                            basicItem.Category,
                            basicItem.Brand,
                            basicItem.MPN,
                            basicItem.Ean,
                            basicItem.Ean,
                            basicItem.Ean,
                            basicItem.Title,
                            basicItem.SubTitle,
                            basicItem.Relationship,
                            basicItem.RelationshipDetails,
                            basicItem.Description,
                            basicItem.ConditionID,
                            basicItem.PicUrl,
                            basicItem.Quantity,
                            basicItem.Format,
                            basicItem.StartPrice,
                            basicItem.BuyItNowPrice,
                            basicItem.Duration,
                            basicItem.ImmediatePayRequired,
                            basicItem.Location,
                            basicItem.GalleryType,
                            basicItem.PayPalAccepted,
                            basicItem.PayPalEmailAddress,
                            basicItem.PaymentInstructions,
                            basicItem.StoreCategory,
                            basicItem.ShippingDiscountProfileID,
                            basicItem.DomesticRateTable,
                            basicItem.ShippingType,
                            basicItem.ShippingService_1Option,
                            basicItem.ShippingService_1Cost,
                            basicItem.ShippingService_1FreeShipping,
                            basicItem.ShippingService_1Priority,
                            basicItem.ShippingService_1ShippingSurcharge,
                            basicItem.ShippingService_2Option,
                            basicItem.ShippingService_2Cost,
                            basicItem.ShippingService_2Priority,
                            basicItem.ShippingService_2ShippingSurcharge,
                            basicItem.DispatchTimeMax,
                            basicItem.Ean,
                            basicItem.ReturnsAcceptedOption,
                            basicItem.RefundOption,
                            basicItem.ReturnsWithinOption,
                            basicItem.ShippingCostPaidByOption,
                            basicItem.AdditionalDetails,
                            basicItem.ShippingProfileName,
                            basicItem.ReturnProfileName,
                            basicItem.PaymentProfileName,
                            basicItem.SalesTaxPercent
                //basicItem.ProductBrand
                        );

            actionRevisionListingsFormat.Add(line);

            foreach (var variation in basicItem.Variations)
            {
                actionRevisionListingsFormat.Add(variation.GetLineFormat());
            }

            return actionRevisionListingsFormat;
        }
    }
}
