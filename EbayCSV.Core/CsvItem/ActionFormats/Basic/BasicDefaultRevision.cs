﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Settings;

namespace EbayCSV.Core.CsvItem.ActionFormats.Basic
{
    /// <summary>
    /// Manages the conversion from a csv line-ready basic item into a revision-formatted csv line
    /// </summary>
    public class BasicDefaultRevision : IRevisionFormat
    {
        /// <summary>
        /// Container of the settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of the settings/configurations</param>
        public BasicDefaultRevision(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Converts csv line-ready basic item into a revision-formatted csv line
        /// </summary>
        /// <typeparam name="T">Assumed to be BasicItem</typeparam>
        /// <param name="value">BasicItem instance</param>
        /// <returns>Returns csv line converted basic item</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.IRevisionFormat.GetRevisionFormat"/>
        public IEnumerable<string> GetRevisionFormat<T>(T value)
        {
            BasicItem basicItem = (BasicItem)(object)value;

            List<string> actionReviseListingsFormat = new List<string>();

            string line = String.Format(
                            "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}",
                            basicItem.Action,
                            basicItem.CustomLabel,
                            basicItem.Relationship,
                            basicItem.RelationshipDetails,
                            basicItem.Title,
                            "Germany",
                            "EUR",
                            basicItem.StartPrice,
                            basicItem.BuyItNowPrice,
                            basicItem.Quantity,
                            basicItem.Ean,
                            basicItem.PicUrl,
                            basicItem.Description,
                            basicItem.ShippingService_1Option,
                            basicItem.ShippingService_1Cost,
                            basicItem.ShippingService_1FreeShipping
                        );

            if (Boolean.Parse(settings.GetValue("excludeDescription")))
            {
                line = String.Format(
                               "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}",
                               basicItem.Action,
                               basicItem.CustomLabel,
                               basicItem.Relationship,
                               basicItem.RelationshipDetails,
                               basicItem.Title,
                               "Germany",
                               "EUR",
                               basicItem.StartPrice,
                               basicItem.BuyItNowPrice,
                               basicItem.Quantity,
                               basicItem.Ean,
                               basicItem.PicUrl,
                               basicItem.ShippingService_1Option,
                               basicItem.ShippingService_1Cost,
                               basicItem.ShippingService_1FreeShipping
                           );
            }

            actionReviseListingsFormat.Add(line);

            foreach (var variation in basicItem.Variations)
            {
                actionReviseListingsFormat.Add(variation.GetLineFormat());
            }

            return actionReviseListingsFormat;
        }
    }
}
