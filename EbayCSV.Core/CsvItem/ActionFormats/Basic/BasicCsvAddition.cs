﻿using System;
using System.Globalization;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.ActionFormats.Basic
{
    /// <summary>
    /// Manages the conversion from a csv line-ready basic item into a newlisting-formatted csv line
    /// </summary>
    public class BasicCsvAddition : IAdditionFormat
    {
        /// <summary>
        /// Converts csv line-ready basic item into a newlisting-formatted csv line
        /// </summary>
        /// <typeparam name="T">Assumed to be BasicItem</typeparam>
        /// <param name="value">BasicItem instance</param>
        /// <returns>Returns csv line converted basic item</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.IAdditionFormat.GetAdditionFormat"/>
        public IEnumerable<string> GetAdditionFormat<T>(T value)
        {
            BasicItem basicItem = (BasicItem)(object)value;

            List<string> actionNewListingsFormat = new List<string>();

            var line = String.Format(
                            "\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",{8},\"{9}\",\"{10}\",\"{11}\",\"{12}\",\"{13}\",\"{14}\",\"{15}\",\"{16}\",\"{17}\",\"{18}\",\"{19}\",\"{20}\",\"{21}\",\"{22}\",\"{23}\",\"{24}\",\"{25}\",\"{26}\",\"{27}\",\"{28}\",\"{29}\",\"{30}\",\"{31}\",\"{32}\",\"{33}\",\"{34}\",\"{35}\",\"{36}\",\"{37}\",\"{38}\",\"{39}\",\"{40}\",\"{41}\",\"{42}\",\"{43}\",\"{44}\",\"{45}\",",
                            basicItem.Action.ToString(),
                            basicItem.Category,
                            basicItem.Brand,
                            basicItem.MPN,
                            basicItem.Title,
                            basicItem.SubTitle,
                            basicItem.Relationship,
                            basicItem.RelationshipDetails,
                            basicItem.Description,
                            basicItem.ConditionID,
                            basicItem.PicUrl,
                            basicItem.Quantity,
                            basicItem.Format,
                            basicItem.StartPrice.ToString(CultureInfo.InvariantCulture),
                            basicItem.BuyItNowPrice,
                            basicItem.Duration,
                            basicItem.ImmediatePayRequired,
                            basicItem.Location,
                            basicItem.GalleryType,
                            basicItem.PayPalAccepted,
                            basicItem.PayPalEmailAddress,
                            basicItem.PaymentInstructions,
                            basicItem.StoreCategory,
                            basicItem.ShippingDiscountProfileID,
                            basicItem.DomesticRateTable,
                            basicItem.ShippingType,
                            basicItem.ShippingService_1Option,
                            basicItem.ShippingService_1Cost,
                            basicItem.ShippingService_1FreeShipping,
                            basicItem.ShippingService_1Priority,
                            basicItem.ShippingService_1ShippingSurcharge,
                            basicItem.ShippingService_2Option,
                            basicItem.ShippingService_2Cost,
                            basicItem.ShippingService_2Priority,
                            basicItem.ShippingService_2ShippingSurcharge,
                            basicItem.DispatchTimeMax,
                            basicItem.CustomLabel,
                            basicItem.ReturnsAcceptedOption,
                            basicItem.RefundOption,
                            basicItem.ReturnsWithinOption,
                            basicItem.ShippingCostPaidByOption,
                            basicItem.AdditionalDetails,
                            basicItem.ShippingProfileName,
                            basicItem.ReturnProfileName,
                            basicItem.PaymentProfileName,
                            basicItem.SalesTaxPercent
                //basicItem.ProductBrand
                        );

            if (int.Parse(basicItem.Quantity) > 0)
            {
                if (!string.IsNullOrEmpty(basicItem.CFieldsOneLine))
                {
                    line += "\",\"" + basicItem.CFieldsOneLine;
                }

                actionNewListingsFormat.Add(line);
            }

            foreach (var variation in basicItem.Variations)
            {
                actionNewListingsFormat.Add(variation.GetLineFormat());
            }

            return actionNewListingsFormat;
        }
    }
}
