﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Settings;

namespace EbayCSV.Core.CsvItem.ActionFormats.Basic
{
    /// <summary>
    /// Manages the conversion from a csv line-ready basic item into a revision-formatted csv line
    /// </summary>
    public class BasicCsvRevision : IRevisionFormat
    {
        /// <summary>
        /// Container of the settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of the settings/configurations</param>
        public BasicCsvRevision(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Converts csv line-ready basic item into a revision-formatted csv line
        /// </summary>
        /// <typeparam name="T">Assumed to be BasicItem</typeparam>
        /// <param name="value">BasicItem instance</param>
        /// <returns>Returns csv line converted basic item</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.IRevisionFormat.GetRevisionFormat"/>
        public IEnumerable<string> GetRevisionFormat<T>(T value)
        {
            BasicItem basicItem = (BasicItem)(object)value;

            List<string> actionReviseListingsFormat = new List<string>();

            string line = String.Format(
                            "\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",{12},\"{13}\",\"{14}\",\"{15}\",",
                            basicItem.Action.ToString(),
                            basicItem.CustomLabel,
                            basicItem.Relationship,
                            basicItem.RelationshipDetails,
                            basicItem.Title,
                            "Germany",
                            "EUR",
                            basicItem.StartPrice,
                            basicItem.BuyItNowPrice,
                            basicItem.Quantity,
                            basicItem.Ean,
                            basicItem.PicUrl,
                            basicItem.Description,
                            basicItem.ShippingService_1Option,
                            basicItem.ShippingService_1Cost,
                            basicItem.ShippingService_1FreeShipping
                        );

            if (Boolean.Parse(settings.GetValue("excludeDescription")))
            {
                line = String.Format(
                            "\"{0}\",\"{1}\",\"{2}\",\"{3}\",\"{4}\",\"{5}\",\"{6}\",\"{7}\",\"{8}\",\"{9}\",\"{10}\",\"{11}\",{12},\"{13}\",\"{14}\",",
                            basicItem.Action.ToString(),
                            basicItem.CustomLabel,
                            basicItem.Relationship,
                            basicItem.RelationshipDetails,
                            basicItem.Title,
                            "Germany",
                            "EUR",
                            basicItem.StartPrice,
                            basicItem.BuyItNowPrice,
                            basicItem.Quantity,
                            basicItem.Ean,
                            basicItem.PicUrl,
                            basicItem.ShippingService_1Option,
                            basicItem.ShippingService_1Cost,
                            basicItem.ShippingService_1FreeShipping
                        );
            }

            actionReviseListingsFormat.Add(line);

            foreach (var variation in basicItem.Variations)
            {
                actionReviseListingsFormat.Add(variation.GetLineFormat());
            }

            return actionReviseListingsFormat;
        }
    }
}
