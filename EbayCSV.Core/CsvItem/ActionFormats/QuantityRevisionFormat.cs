﻿using System;
using System.Collections.Generic;

namespace EbayCSV.Core.CsvItem.ActionFormats
{
    /// <summary>
    /// Manages the conversion from a csv line-ready catalog item into a revision-formatted csv line
    /// </summary>
    public class QuantityRevisionFormat : IRevisionFormat
    {
        /// <summary>
        /// Converts csv line-ready catalog item into a revision-formatted csv line
        /// </summary>
        /// <typeparam name="T">Assumed to be CatalogItem</typeparam>
        /// <param name="value">CatalogItem instance</param>
        /// <returns>Returns csv line converted catalog item</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.IRevisionFormat.GetRevisionFormat"/>
        public IEnumerable<string> GetRevisionFormat<T>(T value)
        {
            CatalogItem catalogItem = (CatalogItem)(object)value;

            List<string> actionReviseListingsFormat = new List<string>();

            string line = String.Format(
                            "{0}\t{1}\t{2}",
                            catalogItem.Action,
                            catalogItem.CustomLabel,
                            catalogItem.Quantity
                        );

            actionReviseListingsFormat.Add(line);

            return actionReviseListingsFormat;
        }
    }
}
