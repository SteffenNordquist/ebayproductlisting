﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.ActionFormats.Catalog
{
    /// <summary>
    /// Manages the conversion from a csv line-ready catalog item into a revision-formatted csv line
    /// </summary>
    public class CatalogQuantityRevision : IRevisionFormat
    {
        /// <summary>
        /// Converts csv line-ready catalog item into a revision-formatted csv line
        /// </summary>
        /// <typeparam name="T">Assumed to be CatalogItem</typeparam>
        /// <param name="value">CatalogItem instance</param>
        /// <returns>Returns csv line converted catalog item</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.IRevisionFormat.GetRevisionFormat"/>
        public IEnumerable<string> GetRevisionFormat<T>(T value)
        {
            CatalogItem catalogItem = (CatalogItem)(object)value;

            List<string> actionReviseListingsFormat = new List<string>();

            string line = String.Format(
                            "{0}\t{1}\t{2}",
                            catalogItem.Action,
                            catalogItem.CustomLabel,
                            catalogItem.Quantity
                        );

            actionReviseListingsFormat.Add(line);

            return actionReviseListingsFormat;
        }
    }
}
