﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.ActionFormats.Catalog
{
    /// <summary>
    /// Manages the conversion from a csv line-ready catalog item into a newlisting-formatted csv line
    /// </summary>
    public class CatalogDefaultAddition : IAdditionFormat
    {
        /// <summary>
        /// Converts csv line-ready catalog item into a newlisting-formatted csv line
        /// </summary>
        /// <typeparam name="T">Assumed to be CatalogItem</typeparam>
        /// <param name="value">CatalogItem instance</param>
        /// <returns>Returns csv line converted catalog item</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.IAdditionFormat.GetAdditionFormat"/>
        public IEnumerable<string> GetAdditionFormat<T>(T value)
        {
            CatalogItem catalogItem = (CatalogItem)(object)value;

            List<string> actionNewListingsFormat = new List<string>();

            string line = String.Format(
                            "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}\t{20}\t{21}\t{22}\t{23}\t{24}\t{25}\t{26}\t{27}\t{28}\t{29}\t{30}\t{31}\t{32}\t{33}\t{34}\t{35}\t{36}\t{37}\t{38}\t{39}\t{40}\t{41}\t{42}\t{43}\t{44}\t{45}\t{46}\t{47}\t{48}\t{49}",
                            catalogItem.Action,
                            //catalogItem.Category,
                            catalogItem.ProductUPC,
                            catalogItem.ProductISBN,
                            catalogItem.ProductEAN,
                            catalogItem.ProductEPID,
                            catalogItem.ProductIncludePreFilledItemInformation,
                            catalogItem.ProductIncludeStockPhotoURL,
                            catalogItem.ProductReturnSearchResultsOnDuplicates,
                            catalogItem.Title,
                            catalogItem.SubTitle,
                            catalogItem.Relationship,
                            catalogItem.RelationshipDetails,
                            catalogItem.Description,
                            catalogItem.ConditionID,
                            catalogItem.PicUrl,
                            catalogItem.Quantity,
                            catalogItem.Format,
                            catalogItem.StartPrice,
                            catalogItem.BuyItNowPrice,
                            catalogItem.Duration,
                            catalogItem.ImmediatePayRequired,
                            catalogItem.Location,
                            catalogItem.GalleryType,
                            catalogItem.PayPalAccepted,
                            catalogItem.PayPalEmailAddress,
                            catalogItem.PaymentInstructions,
                            catalogItem.StoreCategory,
                            catalogItem.ShippingDiscountProfileID,
                            catalogItem.DomesticRateTable,
                            catalogItem.ShippingType,
                            catalogItem.ShippingService_1Option,
                            catalogItem.ShippingService_1Cost,
                            catalogItem.ShippingService_1FreeShipping,
                            catalogItem.ShippingService_1Priority,
                            catalogItem.ShippingService_1ShippingSurcharge,
                            catalogItem.ShippingService_2Option,
                            catalogItem.ShippingService_2Cost,
                            catalogItem.ShippingService_2Priority,
                            catalogItem.ShippingService_2ShippingSurcharge,
                            catalogItem.DispatchTimeMax,
                            catalogItem.CustomLabel,
                            catalogItem.ReturnsAcceptedOption,
                            catalogItem.RefundOption,
                            catalogItem.ReturnsWithinOption,
                            catalogItem.ShippingCostPaidByOption,
                            catalogItem.AdditionalDetails,
                            catalogItem.ShippingProfileName,
                            catalogItem.ReturnProfileName,
                            catalogItem.PaymentProfileName,
                            catalogItem.SalesTaxPercent
                        );

            if (int.Parse(catalogItem.Quantity) > 0)
            {
                if (!string.IsNullOrEmpty(catalogItem.CFieldsOneLine))
                {
                    line += "\t" + catalogItem.CFieldsOneLine;
                }


                actionNewListingsFormat.Add(line);
            }

            foreach (var variation in catalogItem.Variations)
            {
                actionNewListingsFormat.Add(variation.GetLineFormat());
            }

            return actionNewListingsFormat;
        }
    }
}
