﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.ActionFormats.Catalog
{
    /// <summary>
    /// Manages the conversion from a csv line-ready catalog item into a revision-formatted csv line
    /// </summary>
    public class CatalogDefaultRevision : IRevisionFormat
    {
        /// <summary>
        /// Container of the settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of the settings/configurations</param>
        public CatalogDefaultRevision(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Converts csv line-ready catalog item into a revision-formatted csv line
        /// </summary>
        /// <typeparam name="T">Assumed to be CatalogItem</typeparam>
        /// <param name="value">CatalogItem instance</param>
        /// <returns>Returns csv line converted catalog item</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.ActionFormats.IRevisionFormat.GetRevisionFormat"/>
        public IEnumerable<string> GetRevisionFormat<T>(T value)
        {
            CatalogItem catalogItem = (CatalogItem)(object)value;

            List<string> actionReviseListingsFormat = new List<string>();

            string line = String.Format(
                            "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}",
                            catalogItem.Action,
                            catalogItem.CustomLabel,
                            catalogItem.Relationship,
                            catalogItem.RelationshipDetails,
                            catalogItem.Title,
                            "Germany",
                            "EUR",
                            catalogItem.StartPrice,
                            catalogItem.BuyItNowPrice,
                            catalogItem.Quantity,
                            catalogItem.ProductEAN,
                            catalogItem.PicUrl,
                            catalogItem.Description,
                            catalogItem.ShippingService_1Option,
                            catalogItem.ShippingService_1Cost,
                            catalogItem.ShippingService_1FreeShipping
                        );

            if (Boolean.Parse(settings.GetValue("excludeDescription")))
            {
                line = String.Format(
                            "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}",
                            catalogItem.Action,
                            catalogItem.CustomLabel,
                            catalogItem.Relationship,
                            catalogItem.RelationshipDetails,
                            catalogItem.Title,
                            "Germany",
                            "EUR",
                            catalogItem.StartPrice,
                            catalogItem.BuyItNowPrice,
                            catalogItem.Quantity,
                            catalogItem.ProductEAN,
                            catalogItem.PicUrl,
                            catalogItem.ShippingService_1Option,
                            catalogItem.ShippingService_1Cost,
                            catalogItem.ShippingService_1FreeShipping
                        );
            }

            actionReviseListingsFormat.Add(line);

            foreach (var variation in catalogItem.Variations)
            {
                actionReviseListingsFormat.Add(variation.GetLineFormat());
            }

            return actionReviseListingsFormat;
        }
    }
}
