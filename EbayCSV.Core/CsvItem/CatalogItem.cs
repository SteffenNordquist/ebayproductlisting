﻿using EbayCSV.Core.CsvItem.ActionFormats;
using EbayCSV.Core.ListingAction;

using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.CsvItem.Variation;

namespace EbayCSV.Core.CsvItem
{
    /// <summary>
    /// A class represents an item/line of a csv file
    /// Contains ready-to-use properties for an item/line
    /// </summary>
    public class CatalogItem : ICsvItem
    {
        #region Properties represent every column on a csv file
        public ActionType Action { get; set; }
        public string Category { get; set; }
        public string ProductUPC { get; set; }
        public string ProductISBN { get; set; }
        public string ProductEAN { get; set; }
        public string ProductEPID { get; set; }
        public string ProductIncludePreFilledItemInformation { get; set; }
        public string ProductIncludeStockPhotoURL { get; set; }
        public string ProductReturnSearchResultsOnDuplicates { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Relationship { get; set; }
        public string RelationshipDetails { get; set; }
        public string Description { get; set; }
        public string ConditionID { get; set; }
        public string PicUrl { get; set; }
        public string Quantity { get; set; }
        public string Format { get; set; }
        public double StartPrice { get; set; }
        public string BuyItNowPrice { get; set; }
        public string Duration { get; set; }
        public string ImmediatePayRequired { get; set; }
        public string Location { get; set; }
        public string GalleryType { get; set; }
        public string PayPalAccepted { get; set; }
        public string PayPalEmailAddress { get; set; }
        public string PaymentInstructions { get; set; }
        public string StoreCategory { get; set; }
        public string ShippingDiscountProfileID { get; set; }
        public string DomesticRateTable { get; set; }
        public string ShippingType { get; set; }
        public string ShippingService_1Option { get; set; }
        public string ShippingService_1Cost { get; set; }
        public string ShippingService_1FreeShipping { get; set; }
        public string ShippingService_1Priority { get; set; }
        public string ShippingService_1ShippingSurcharge { get; set; }
        public string ShippingService_2Option { get; set; }
        public string ShippingService_2Cost { get; set; }
        public string ShippingService_2Priority { get; set; }
        public string ShippingService_2ShippingSurcharge { get; set; }
        public string DispatchTimeMax { get; set; }
        public string CustomLabel { get; set; }
        public string ReturnsAcceptedOption { get; set; }
        public string RefundOption { get; set; }
        public string ReturnsWithinOption { get; set; }
        public string ShippingCostPaidByOption { get; set; }
        public string AdditionalDetails { get; set; }
        public string ShippingProfileName { get; set; }
        public string ReturnProfileName { get; set; }
        public string PaymentProfileName { get; set; }
        public string SalesTaxPercent { get; set; }
        public List<ICsvVariationItem> Variations { get; set; }
        public string CFieldsOneLine { get; set; }
        #endregion

        /// <summary>
        /// Responsible for processing properties into a revision-formatted line
        /// </summary>
        private IRevisionFormat revisionFormat { get; set; }

        /// <summary>
        /// Responsible for processing properties into a add-formatted line
        /// </summary>
        private IAdditionFormat additionFormat { get; set; }

        #region A constructor of this class
        public CatalogItem(
            string ean,
            string customLabel,
            ActionType actiontype,
            string category,
            string title,
            string subTitle,
            string storeCategory,
            string imageLink,
            string description,
            double sellprice,
            string quantity,
            string paypalEmail,
            string dispatchTimeMax,
            string duration,
            string format,
            string location,
            string paypalAccept,
            string returnsOption,
            List<ICsvVariationItem> variations,
            string relationship,
            string relationshipDetails,
            string cFieldsOneLine,
            string shippingService1Option,
            string shippingService1Costs,
            string shippingService1FreeShipping,
            string returnProfileName,
            string paymentProfileName,
            string shippingProfileName,
            string salesTaxPercent,
            IRevisionFormat revisionFormat,
            IAdditionFormat additionFormat
            )
        {
            this.Action = actiontype;
            this.Category = category;
            this.ProductUPC = ean;
            this.ProductISBN = ean;
            this.ProductEAN = ean;
            this.ProductEPID = "";
            this.ProductIncludePreFilledItemInformation = "";
            this.ProductIncludeStockPhotoURL = "";
            this.ProductReturnSearchResultsOnDuplicates = "";
            this.Title = title;
            this.SubTitle = subTitle;
            this.Description = description;
            this.ConditionID = "1000";
            this.PicUrl = imageLink;
            this.Quantity = quantity.ToString();
            this.Format = format;
            this.StartPrice = sellprice;
            this.BuyItNowPrice = "";
            this.Duration = duration;
            this.ImmediatePayRequired = "";
            this.Location = location;
            this.GalleryType = "";
            this.PayPalAccepted = paypalAccept;
            this.PayPalEmailAddress = paypalEmail;
            this.PaymentInstructions = "";
            this.StoreCategory = storeCategory;
            this.ShippingDiscountProfileID = "";
            this.DomesticRateTable = "";
            this.ShippingType = "Flat";
            this.ShippingService_1Option = shippingService1Option;
            this.ShippingService_1Cost = shippingService1Costs;
            this.ShippingService_1FreeShipping = shippingService1FreeShipping;
            this.ShippingService_1Priority = "";
            this.ShippingService_1ShippingSurcharge = "";
            this.ShippingService_2Option = "";
            this.ShippingService_2Cost = "";
            this.ShippingService_2Priority = "";
            this.ShippingService_2ShippingSurcharge = "";
            this.DispatchTimeMax = dispatchTimeMax;
            this.CustomLabel = customLabel;
            this.ReturnsAcceptedOption = returnsOption;
            this.RefundOption = "";
            this.ReturnsWithinOption = "";
            this.ShippingCostPaidByOption = "";
            this.AdditionalDetails = "";
            this.ShippingProfileName = "";
            this.ReturnProfileName = "";
            this.PaymentProfileName = "";
            this.Variations = variations;
            this.Relationship = relationship;
            this.RelationshipDetails = relationshipDetails;
            this.CFieldsOneLine = cFieldsOneLine;
            this.ShippingProfileName = shippingProfileName;
            this.ReturnProfileName = returnProfileName;
            this.PaymentProfileName = paymentProfileName;
            this.SalesTaxPercent = salesTaxPercent;
            this.revisionFormat = revisionFormat;
            this.additionFormat = additionFormat;
        }
        #endregion

        /// <summary>
        /// Delegates the work for processing properties into a add-formatted line
        /// </summary>
        /// <returns>list of add-formatted lines for this item</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.ICsvItem.GetNewActionFormat"/>
        public IEnumerable<string> GetNewActionFormat()
        {
            return additionFormat.GetAdditionFormat<CatalogItem>(this);

            #region old without delegation work
            //List<string> actionNewListingsFormat = new List<string>();

            //string line = String.Format(
            //                "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}\t{20}\t{21}\t{22}\t{23}\t{24}\t{25}\t{26}\t{27}\t{28}\t{29}\t{30}\t{31}\t{32}\t{33}\t{34}\t{35}\t{36}\t{37}\t{38}\t{39}\t{40}\t{41}\t{42}\t{43}\t{44}\t{45}\t{46}\t{47}\t{48}",
            //                this.Action,
            //                this.ProductUPC,
            //                this.ProductISBN,
            //                this.ProductEAN,
            //                this.ProductEPID,
            //                this.ProductIncludePreFilledItemInformation,
            //                this.ProductIncludeStockPhotoURL,
            //                this.ProductReturnSearchResultsOnDuplicates,
            //                this.Title,
            //                this.SubTitle,
            //                this.Relationship,
            //                this.RelationshipDetails,
            //                this.Description,
            //                this.ConditionID,
            //                this.PicUrl,
            //                this.Quantity,
            //                this.Format,
            //                double.Parse(this.StartPrice).ToString(CultureInfo.InvariantCulture),
            //                this.BuyItNowPrice,
            //                this.Duration,
            //                this.ImmediatePayRequired,
            //                this.Location,
            //                this.GalleryType,
            //                this.PayPalAccepted,
            //                this.PayPalEmailAddress,
            //                this.PaymentInstructions,
            //                this.StoreCategory,
            //                this.ShippingDiscountProfileID,
            //                this.DomesticRateTable,
            //                this.ShippingType,
            //                this.ShippingService_1Option,
            //                this.ShippingService_1Cost,
            //                this.ShippingService_1FreeShipping,
            //                this.ShippingService_1Priority,
            //                this.ShippingService_1ShippingSurcharge,
            //                this.ShippingService_2Option,
            //                this.ShippingService_2Cost,
            //                this.ShippingService_2Priority,
            //                this.ShippingService_2ShippingSurcharge,
            //                this.DispatchTimeMax,
            //                this.CustomLabel,
            //                this.ReturnsAcceptedOption,
            //                this.RefundOption,
            //                this.ReturnsWithinOption,
            //                this.ShippingCostPaidByOption,
            //                this.AdditionalDetails,
            //                this.ShippingProfileName,
            //                this.ReturnProfileName,
            //                this.PaymentProfileName
            //            );

            //if (int.Parse(this.Quantity) > 0)
            //{
            //    if (!string.IsNullOrEmpty(CFieldsOneLine))
            //    {
            //        line += "\t" + CFieldsOneLine;
            //    }


            //    actionNewListingsFormat.Add(line);
            //}

            //foreach (var variation in Variations)
            //{
            //    actionNewListingsFormat.Add(variation.GetLineFormat());
            //}

            //return actionNewListingsFormat;
            #endregion
        }

        /// <summary>
        /// Delegates the work for processing properties into a revision-formatted line
        /// </summary>
        /// <returns>list of revision-formatted lines for this item</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.ICsvItem.GetReviseActionFormat"/>
        public IEnumerable<string> GetReviseActionFormat()
        {
            return revisionFormat.GetRevisionFormat<CatalogItem>(this);

            #region without delegation work
            //List<string> actionReviseListingsFormat = new List<string>();

            //string line = String.Format(
            //                "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}",
            //                this.Action,
            //                this.CustomLabel,
            //                this.Relationship,
            //                this.RelationshipDetails,
            //                this.Title,
            //                "Germany",
            //                "EUR",
            //                this.StartPrice.ToString(CultureInfo.InvariantCulture),
            //                this.BuyItNowPrice,
            //                this.Quantity,
            //                this.PicUrl.Split(new string[] { "/images/" }, StringSplitOptions.None)[1].Split(new string[] { "_1." }, StringSplitOptions.None)[0],
            //                this.PicUrl,
            //                this.Description,
            //                this.ShippingService_1Option,
            //                this.ShippingService_1Cost,
            //                this.ShippingService_1FreeShipping
            //            );

            //actionReviseListingsFormat.Add(line);

            //foreach (var variation in Variations)
            //{
            //    actionReviseListingsFormat.Add(variation.GetLineFormat());
            //}

            //return actionReviseListingsFormat;
            #endregion
        }

        /// <summary>
        /// Returns own ActionType property
        /// </summary>
        /// <returns>Returns own ActionType property</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.ICsvItem.GetActionType"/>
        public ActionType GetActionType()
        {
            return Action;
        }
    }
}
