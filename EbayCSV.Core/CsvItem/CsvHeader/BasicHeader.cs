﻿using EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats;
using EbayCSV.Core.Factories.CustomFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.CsvHeader
{
    /// <summary>
    /// A class manages for a basic template header
    /// </summary>
    public class BasicHeader : ICsvHeader
    {
        /// <summary>
        /// A delegate works for new listing header
        /// </summary>
        private readonly IAdditionHeader additionHeader;

        /// <summary>
        /// A delegate works for revision listing header
        /// </summary>
        private readonly IRevisionHeader revisionHeader;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="additionHeader">A delegate works for new listing header</param>
        /// <param name="revisionHeader">A delegate works for revision listing header</param>
        public BasicHeader(IAdditionHeader additionHeader, IRevisionHeader revisionHeader)
        {
            this.additionHeader = additionHeader;
            this.revisionHeader = revisionHeader;
        }

        /// <summary>
        /// Delegates the work on returning the new listing header
        /// </summary>
        /// <returns>Returns new listing header in a form of one line</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.ICsvHeader.GetNewActionHeader"/>
        public string GetNewActionHeader()
        {
            return additionHeader.GetHeader();
        }

        /// <summary>
        /// Delegates the work on returning the revision listing header
        /// </summary>
        /// <returns>Returns revision listing header in a form of one line</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.ICsvHeader.GetReviseActionHeader"/>
        public string GetReviseActionHeader()
        {
            return revisionHeader.GetHeader();
        }
    }
}
