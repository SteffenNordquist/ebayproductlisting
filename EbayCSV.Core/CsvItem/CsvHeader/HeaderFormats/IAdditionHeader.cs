﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats
{
    /// <summary>
    /// Manages new listing header for catalog template
    /// </summary>
    /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Catalog.CatalogDefaultAdditionHeader"/>
    /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Basic.BasicDefaultAdditionHeader"/>
    /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Basic.BasicCsvAdditionHeader"/>
    public interface IAdditionHeader
    {
        /// <summary>
        /// Returns the catalog template new listing header in a form of a line
        /// </summary>
        /// <returns>Returns the catalog template new listing header in a form of a line</returns>
        string GetHeader();
    }
}
