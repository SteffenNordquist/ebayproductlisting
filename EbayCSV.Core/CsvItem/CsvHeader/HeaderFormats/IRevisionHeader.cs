﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats
{
    /// <summary>
    /// Manages revision header
    /// </summary>
    /// <seealso cref="QuantityRevisionHeaderFormat"/>
    /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Catalog.CatalogDefaultRevisionHeader"/>
    /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Basic.BasicDefaultRevisionHeader"/>
    /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Basic.BasicCsvRevisionHeader"/>
    public interface IRevisionHeader
    {
        /// <summary>
        /// Returns the revision header in a form of a line
        /// </summary>
        /// <returns>Returns the revision header in a form of a line</returns>
        string GetHeader();
    }
}
