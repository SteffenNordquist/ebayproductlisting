﻿using EbayCSV.Core.Factories.CustomFields;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Basic
{
    /// <summary>
    /// Manages new listing header for basic template
    /// </summary>
    public class BasicCsvAdditionHeader : IAdditionHeader
    {
        /// <summary>
        /// A delegate for custom fields headers
        /// </summary>
        private readonly ICustomFieldsHeaders customFieldsHeaders;

        /// <summary>
        /// The constructor for this class
        /// </summary>
        /// <param name="customFieldsHeaders">A delegate for custom fields headers</param>
        public BasicCsvAdditionHeader(ICustomFieldsHeaders customFieldsHeaders)
        {
            this.customFieldsHeaders = customFieldsHeaders;
        }

        /// <summary>
        /// Returns the basic template new listing header in a form of a line
        /// </summary>
        /// <returns>Returns the basic template new listing header in a form of a line</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.IAdditionHeader.GetHeader"/>
        public string GetHeader()
        {
            List<string> cFields = customFieldsHeaders.GetHeaders().Select(x => x.Value).ToList();

            string cFieldsInALine = String.Join(",", cFields);

            return "Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745),Category,Product:Brand,Product:MPN,Title,Subtitle,Relationship,RelationshipDetails,Description,ConditionID,PicURL,Quantity,Format,StartPrice,BuyItNowPrice,Duration,ImmediatePayRequired,Location,GalleryType,PayPalAccepted,PayPalEmailAddress,PaymentInstructions,StoreCategory,ShippingDiscountProfileID,DomesticRateTable,ShippingType,ShippingService-1:Option,ShippingService-1:Cost,ShippingService-1:FreeShipping,ShippingService-1:Priority,ShippingService-1:ShippingSurcharge,ShippingService-2:Option,ShippingService-2:Cost,ShippingService-2:Priority,ShippingService-2:ShippingSurcharge,DispatchTimeMax,CustomLabel,ReturnsAcceptedOption,RefundOption,ReturnsWithinOption,ShippingCostPaidByOption,AdditionalDetails,ShippingProfileName,ReturnProfileName,PaymentProfileName,SalesTaxPercent," + cFieldsInALine;
        }
    }
}
