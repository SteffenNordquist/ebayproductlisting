﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Factories.CustomFields;

namespace EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Basic
{
    /// <summary>
    /// Manages revision header for basic template
    /// </summary>
    public class BasicFullRevisionFormat : IRevisionHeader
    {
        /// <summary>
        /// A delegate for custom fields headers
        /// </summary>
        private readonly ICustomFieldsHeaders customFieldsHeaders;

        /// <summary>
        /// The constructor for this class
        /// </summary>
        /// <param name="customFieldsHeaders">A delegate for custom fields headers</param>
        public BasicFullRevisionFormat(ICustomFieldsHeaders customFieldsHeaders)
        {
            this.customFieldsHeaders = customFieldsHeaders;
        }

        /// <summary>
        /// Returns the basic template new listing header in a form of a line
        /// </summary>
        /// <returns>Returns the basic template new listing header in a form of a line</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.IAdditionHeader.GetHeader"/>
        public string GetHeader()
        {
            List<string> cFields = customFieldsHeaders.GetHeaders().Select(x => x.Value).ToList();

            string cFieldsInALine = String.Join("\t", cFields);

            return "Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID\tCategory\tC:Hersteller\tC:Herstellernummer\tProduct:EAN\tProduct:UPC\tProduct:ISBN\tTitle\tSubtitle\tRelationship\tRelationshipDetails\tDescription\tConditionID\tPicURL\tQuantity\tFormat\tStartPrice\tBuyItNowPrice\tDuration\tImmediatePayRequired\tLocation\tGalleryType\tPayPalAccepted\tPayPalEmailAddress\tPaymentInstructions\tStoreCategory\tShippingDiscountProfileID\tDomesticRateTable\tShippingType\tShippingService-1:Option\tShippingService-1:Cost\tShippingService-1:FreeShipping\tShippingService-1:Priority\tShippingService-1:ShippingSurcharge\tShippingService-2:Option\tShippingService-2:Cost\tShippingService-2:Priority\tShippingService-2:ShippingSurcharge\tDispatchTimeMax\tCustomLabel\tReturnsAcceptedOption\tRefundOption\tReturnsWithinOption\tShippingCostPaidByOption\tAdditionalDetails\tShippingProfileName\tReturnProfileName\tPaymentProfileName\tSalesTaxPercent\t" + cFieldsInALine;
        }
    }
}
