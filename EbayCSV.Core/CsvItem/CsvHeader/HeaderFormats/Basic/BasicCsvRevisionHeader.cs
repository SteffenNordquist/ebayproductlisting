﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Settings;

namespace EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Basic
{
    /// <summary>
    /// Manages revision header for basic template
    /// </summary>
    public class BasicCsvRevisionHeader : IRevisionHeader
    {
        /// <summary>
        /// Container of the settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of the settings/configurations</param>
        public BasicCsvRevisionHeader(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Returns the basic template revision header in a form of line
        /// </summary>
        /// <returns>Returns the basic template revision header in a form of line</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.IRevisionHeader.GetHeader"/>
        public string GetHeader()
        {
            string header = "Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745),ItemID,Relationship,RelationshipDetails,Title,SiteID,Currency,StartPrice,BuyItNowPrice,Quantity,CustomLabel,PicURL,Description,ShippingService-1:Option,ShippingService-1:Cost,ShippingService-1:FreeShipping,";

            if (Boolean.Parse(settings.GetValue("excludeDescription")))
            {
                header = header.Replace(",Description", "");
            }

            return header;
        }
    }
}
