﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Catalog
{
    /// <summary>
    /// Manages revision header for catalog template
    /// </summary>
    public class CatalogQuantityRevisionHeader : IRevisionHeader
    {
        /// <summary>
        /// Returns the catalog template revision header in a form of a line
        /// </summary>
        /// <returns>Returns the catalog template revision header in a form of a line</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.IRevisionHeader.GetHeader"/>
        public string GetHeader()
        {
            return "Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID\tQuantity";
        }
    }
}
