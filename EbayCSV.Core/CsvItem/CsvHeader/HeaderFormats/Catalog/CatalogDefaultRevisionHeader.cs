﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Settings;

namespace EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Catalog
{
    /// <summary>
    /// Manages revision header for catalog template
    /// </summary>
    public class CatalogDefaultRevisionHeader : IRevisionHeader
    {
        /// <summary>
        /// Container of the settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of the settings/configurations</param>
        public CatalogDefaultRevisionHeader(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Returns the catalog template revision header in a form of line
        /// </summary>
        /// <returns>Returns the catalog template revision header in a form of line</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.IRevisionHeader.GetHeader"/>
        public string GetHeader()
        {
            string header = "Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID\tRelationship\tRelationshipDetails\tTitle\tSiteID\tCurrency\tStartPrice\tBuyItNowPrice\tQuantity\tCustomLabel\tPicURL\tDescription\tShippingService-1:Option\tShippingService-1:Cost\tShippingService-1:FreeShipping";

            if (Boolean.Parse(settings.GetValue("excludeDescription")))
            {
                header = header.Replace("\tDescription", "");
            }

            return header;
        }
    }
}
