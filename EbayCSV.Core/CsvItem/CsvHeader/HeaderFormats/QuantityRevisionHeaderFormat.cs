﻿namespace EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats
{
    /// <summary>
    /// Manages revision header for catalog template
    /// </summary>
    public class QuantityRevisionHeaderFormat : IRevisionHeader
    {
        /// <summary>
        /// Returns the catalog template revision header in a form of a line
        /// </summary>
        /// <returns>Returns the catalog template revision header in a form of a line</returns>
        /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.IRevisionHeader.GetHeader"/>
        public string GetHeader()
        {
            return "Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID\tQuantity";
        }
    }
}
