﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.CsvHeader
{
    /// <summary>
    /// Manages for a catalog template header
    /// </summary>
    /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.BasicHeader"/>
    /// <seealso cref="EbayCSV.Core.CsvItem.CsvHeader.CatalogHeader"/>
    public interface ICsvHeader
    {
        /// <summary>
        /// Delegates the work on returning the new listing header
        /// </summary>
        /// <returns>Returns new listing header in a form of one line</returns>
        string GetNewActionHeader();

        /// <summary>
        /// Delegates the work on returning the revision listing header
        /// </summary>
        /// <returns>Returns revision listing header in a form of one line</returns>
        string GetReviseActionHeader();
    }
}
