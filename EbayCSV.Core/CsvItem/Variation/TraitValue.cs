﻿namespace EbayCSV.Core.CsvItem.Variation
{
    public class TraitValue
    {
        public string Trait { get; private set; }

        public string Value { get; private set; }

        public TraitValue(string trait, string value)
        {
            Trait = trait;
            Value = value;
        }

        public override string ToString()
        {
            return this.Trait + "=" + this.Value;
        }
    }
}
