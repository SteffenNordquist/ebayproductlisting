﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Bson;

namespace EbayCSV.Core.CsvItem.Variation.Grouping
{
    /// <summary>
    /// Group indicator
    /// </summary>
    public class GroupIndicator : IGroupIndicator
    {
        /// <summary>
        /// Returns the group code of an object passed
        /// </summary>
        /// <typeparam name="T">Param type</typeparam>
        /// <param name="obj">Expected to be a BsonDocument</param>
        /// <exception cref="FormatException">BsonDocument expected</exception>
        /// <exception cref="ArgumentNullException">'obj' must not be null</exception>
        /// <returns>group code</returns>
        public string GetGroupCode<T>(T obj)
        {
            if (obj == null) throw new ArgumentNullException("obj");

            if (obj.GetType() != typeof(BsonDocument)) throw new FormatException("obj");

            BsonDocument objAsBsonDocument = obj as BsonDocument;

            string groupCode = "";

            if (objAsBsonDocument.Contains("group")) groupCode = objAsBsonDocument["group"].AsString;

            return groupCode;
        }
    }
}
