﻿namespace EbayCSV.Core.CsvItem.Variation.Grouping
{
    /// <summary>
    /// Group parent identifier
    /// </summary>
    public interface IGroupParent
    {
        /// <summary>
        /// Determines whether 'obj' is a parent
        /// </summary>
        /// <typeparam name="T">Param type</typeparam>
        /// <param name="obj">Object to examine</param>
        /// <returns>Returns whether 'obj' is a parent or not</returns>
        bool IsGroupParent<T>(T obj);
    }
}
