﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvItem.Variation.Grouping
{
    /// <summary>
    /// Group indicator
    /// </summary>
    public interface IGroupIndicator
    {
        /// <summary>
        /// Returns the group code of an object passed
        /// </summary>
        /// <typeparam name="T">Param type</typeparam>
        /// <param name="obj">Object to examine</param>
        /// <returns>group codet</returns>
        string GetGroupCode<T>(T obj);
    }
}
