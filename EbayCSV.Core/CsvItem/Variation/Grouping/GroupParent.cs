﻿using System;
using MongoDB.Bson;

namespace EbayCSV.Core.CsvItem.Variation.Grouping
{
    /// <summary>
    /// Group parent identifier
    /// </summary>
    public class GroupParent : IGroupParent
    {
        /// <summary>
        /// Determines whether 'obj' is a parent
        /// </summary>
        /// <typeparam name="T">Param type</typeparam>
        /// <param name="obj">Expected to be a BsonDocument</param>
        /// <exception cref="FormatException">BsonDocument expected</exception>
        /// <exception cref="ArgumentNullException">'obj' must not be null</exception>
        /// <returns>Returns whether 'obj' is a parent or not</returns>
        public bool IsGroupParent<T>(T obj)
        {
            if (obj == null) throw new ArgumentNullException("obj");

            if (obj.GetType() != typeof (BsonDocument)) throw new FormatException("obj");
            

            bool isParent = false;

            BsonDocument objAsBsonDocument = obj as BsonDocument;

            if (objAsBsonDocument.Contains("groupParent")) isParent = objAsBsonDocument["groupParent"].AsBoolean;

            return isParent;
        }
    }
}
