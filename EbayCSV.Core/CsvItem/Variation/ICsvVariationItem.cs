﻿using System.Collections.Generic;
using EbayCSV.Core.ListingAction;

namespace EbayCSV.Core.CsvItem.Variation
{
    public interface ICsvVariationItem
    {
        ActionType ActionType();

        IEnumerable<TResult> VariationTraits<TResult>();

        string GetLineFormat();
    }
}
