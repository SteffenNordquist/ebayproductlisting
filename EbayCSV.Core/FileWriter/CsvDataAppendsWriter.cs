﻿using EbayCSV.Core.CsvItem;
using EbayCSV.Core.CsvItem.CsvHeader;
using EbayCSV.Core.ListingAction;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.ResultDirectory;
using EbayCSV.Core.Upload;

namespace EbayCSV.Core.FileWriter
{
    /// <summary>
    /// Processes the work on writing the generated csv data
    /// </summary>
    public class CsvDataAppendsWriter : IFileWriter
    {
        /// <summary>
        /// Container of csv data list
        /// </summary>
        private List<string> csvData;

        /// <summary>
        /// A delegate for the result directory
        /// </summary>
        private readonly IResultDirectory resultDirectory;

        /// <summary>
        /// text encoding
        /// </summary>
        private readonly Encoding encoding;

        /// <summary>
        /// A delegate for csv header
        /// </summary>
        private readonly ICsvHeader csvHeader;

        /// <summary>
        /// A delegate for csv header
        /// </summary>
        private readonly IEbayUploader uploader;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="csvHeader">A delegate for csv header</param>
        public CsvDataAppendsWriter(IResultDirectory resultDirectory, ICsvHeader csvHeader, IEbayUploader uploader, Encoding encoding)
        {
            csvData = new List<string>();
            this.csvHeader = csvHeader;
            this.uploader = uploader;
            this.encoding = encoding;
            this.resultDirectory = resultDirectory;
        }

        /// <summary>
        /// Writes the generated csv data
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSources">Parameter value</param>
        /// <param name="factor1">Additional property placeholder</param>
        /// <param name="factor2">Additional property placeholder</param>
        /// <seealso cref="EbayCSV.Core.FileWriter.IFileWriter.Write"/>
        public void Write(IEnumerable<ICsvItem> csvItems, string factor1 = "", string factor2 = "")
        {
            WriteNewItems(csvItems.Where(x => x.GetActionType() == ActionType.Add).ToList());
            WriteReviseItems(csvItems.Where(x => x.GetActionType() == ActionType.Revise).ToList());
        }

        /// <summary>
        /// writes the csv lines
        /// </summary>
        /// <param name="csvLines"></param>
        /// <param name="path"></param>
        public void Write(IEnumerable<string> csvLines, string supplierName)
        {
        }

        /// <summary>
        /// Writes new items
        /// </summary>
        /// <param name="newItems">list of new items</param>
        private void WriteNewItems(List<ICsvItem> newItems)
        {
            if (newItems.Count() > 0)
            {
                List<string> csvlines = new List<string>();
                csvlines.Add(csvHeader.GetNewActionHeader());

                foreach (var item in newItems)
                {
                    foreach (string line in item.GetNewActionFormat())
                    {
                        csvlines.Add(line);
                    }
                }

                string path = resultDirectory.GetResultDirectory() + "new" + DateTime.Now.Ticks + "_" + newItems.Count() + ".csv";
                File.WriteAllText(path, csvlines.ToString(), encoding);

                List<string> paths = new List<string>();
                paths.Add(path);
                uploader.BeginUpload(paths);

                csvData.Add(string.Join("ItemSeparator", csvlines.ToArray()));
            }


        }

        /// <summary>
        /// Writes revision items
        /// </summary>
        /// <param name="reviseItems">list of revisions items</param>
        private void WriteReviseItems(List<ICsvItem> reviseItems)
        {
            if (reviseItems.Any())
            {
                List<string> csvlines = new List<string>();
                csvlines.Add(csvHeader.GetReviseActionHeader());

                foreach (var item in reviseItems)
                {
                    foreach (string line in item.GetReviseActionFormat())
                    {
                        csvlines.Add(line);
                    }
                }

                string path = resultDirectory.GetResultDirectory() + "revise" + DateTime.Now.Ticks + "_" + reviseItems.Count() + ".csv";
                File.WriteAllText(path, csvlines.ToString(), encoding);

                List<string> paths = new List<string>();
                paths.Add(path);
                uploader.BeginUpload(paths);

                csvData.Add(string.Join("ItemSeparator", csvlines.ToArray()));
            }


        }

        /// <summary>
        /// Processes on returning the generated data
        /// </summary>
        /// <returns>Returns generated data</returns>
        /// <seealso cref="EbayCSV.Core.FileWriter.IFileWriter.GeneratedData"/>
        public IEnumerable<string> GeneratedData()
        {
            return csvData;
        }
    }
}
