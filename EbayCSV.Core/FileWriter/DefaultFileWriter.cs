﻿
using EbayCSV.Core.CsvItem;
using EbayCSV.Core.CsvItem.CsvHeader;
using EbayCSV.Core.ListingAction;
using EbayCSV.Core.ResultDirectory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.FileWriter
{
    public class DefaultFileWriter : IFileWriter
    {
        private List<string> generatedCsvPaths = new List<string>();

        private readonly IResultDirectory resultDirectory;
        private readonly ICsvHeader csvHeader;
        private readonly Encoding encoding;

        public DefaultFileWriter(IResultDirectory resultDirectory, ICsvHeader csvHeader, Encoding encoding)
        {
            this.resultDirectory = resultDirectory;
            this.csvHeader = csvHeader;
            this.encoding = encoding;
        }

        public void Write<TSource>(IEnumerable<TSource> tSources, string factor1 = "", string factor2 = "")
        {
            List<ICsvItem> csvItems = (List<ICsvItem>)(IEnumerable<object>)tSources;
            WriteNewItems(csvItems.Where(x => x.GetActionType() == ActionType.Add).ToList(), factor1);
            WriteReviseItems(csvItems.Where(x => x.GetActionType() == ActionType.Revise).ToList(), factor1);
        }

        public IEnumerable<string> GeneratedData()
        {
            return generatedCsvPaths;
        }

        private void WriteNewItems(List<ICsvItem> newItems, string suppliername)
        {
            if (newItems.Count() > 0)
            {
                string path = resultDirectory.GetResultDirectory() + "new" + suppliername + DateTime.Now.Ticks + "_" + newItems.Count() + ".txt";
                

                StringBuilder csvlines = new StringBuilder();
                csvlines.AppendLine(csvHeader.GetNewActionHeader());

                foreach (var item in newItems)
                {
                    foreach (string line in item.GetNewActionFormat())
                    {
                        csvlines.AppendLine(line);
                    }
                }

            
                    //28591 is the codepage of ISO-8859-1
                    //Encoding encoding = Encoding.GetEncoding(28591);
                    File.WriteAllText(path, csvlines.ToString(), encoding);
                    generatedCsvPaths.Add(path);
            }

            
        }

        private void WriteReviseItems(List<ICsvItem> reviseItems, string suppliername)
        {
            if (reviseItems.Count() > 0)
            {
                string path = resultDirectory.GetResultDirectory() + "revision" + suppliername + DateTime.Now.Ticks + "_" + reviseItems.Count() + ".txt";
               

                StringBuilder csvlines = new StringBuilder();
                csvlines.AppendLine(csvHeader.GetReviseActionHeader());

                foreach (var item in reviseItems)
                {
                    foreach (string line in item.GetReviseActionFormat())
                    {
                        csvlines.AppendLine(line);
                    }
                }

            
                    //28591 is the codepage of ISO-8859-1
                    //Encoding encoding = Encoding.GetEncoding(28591);
                    File.WriteAllText(path, csvlines.ToString(), encoding);
                    generatedCsvPaths.Add(path);
            }

            
        }
    }
}
