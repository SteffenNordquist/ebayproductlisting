﻿
using EbayCSV.Core.CsvItem;
using EbayCSV.Core.CsvItem.CsvHeader;
using EbayCSV.Core.ListingAction;
using EbayCSV.Core.ResultDirectory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Upload;

namespace EbayCSV.Core.FileWriter
{
    /// <summary>
    /// Processes the work on writing the generated csv data in a form of txt file
    /// </summary>
    public class TxtFileWriter : IFileWriter
    {
        /// <summary>
        /// Container of generated csv paths
        /// </summary>
        private List<string> generatedCsvPaths = new List<string>();

        /// <summary>
        /// A delegate for the result directory
        /// </summary>
        private readonly IResultDirectory resultDirectory;

        /// <summary>
        /// A delegate for the csv header
        /// </summary>
        private readonly ICsvHeader csvHeader;

        /// <summary>
        /// A delegate for csv header
        /// </summary>
        private readonly IEbayUploader uploader;

        /// <summary>
        /// text encoding
        /// </summary>
        private readonly Encoding encoding;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="resultDirectory">A delegate for the result directory</param>
        /// <param name="csvHeader">A delegate for the csv header</param>
        /// <param name="encoding">text encoding</param>
        public TxtFileWriter(IResultDirectory resultDirectory, ICsvHeader csvHeader, Encoding encoding, IEbayUploader uploader)
        {
            this.resultDirectory = resultDirectory;
            this.csvHeader = csvHeader;
            this.encoding = encoding;
            this.uploader = uploader;
        }

        /// <summary>
        /// Writes the generated csv data
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSources">Parameter value</param>
        /// <param name="factor1">Additional property placeholder</param>
        /// <param name="factor2">Additional property placeholder</param>
        /// <seealso cref="EbayCSV.Core.FileWriter.IFileWriter.Write"/>
        public void Write(IEnumerable<ICsvItem> csvItems, string factor1 = "", string factor2 = "")
        {
            WriteNewItems(csvItems.Where(x => x.GetActionType() == ActionType.Add).ToList(), factor1);
            WriteReviseItems(csvItems.Where(x => x.GetActionType() == ActionType.Revise).ToList(), factor1);
        }

        /// <summary>
        /// writes the csv lines
        /// </summary>
        /// <param name="csvLines"></param>
        /// <param name="supplierName"></param>
        public void Write(IEnumerable<string> csvLines, string supplierName)
        {
            string path = resultDirectory.GetResultDirectory() + "end" + supplierName + DateTime.Now.Ticks + "_" + csvLines.Count() + ".txt";
            File.WriteAllLines(path, csvLines, encoding);

            uploader.BeginUpload(new List<string> { path });

            generatedCsvPaths.Add(path);
        }

        /// <summary>
        /// Processes on returning the generated data
        /// </summary>
        /// <returns>Returns generated data</returns>
        /// <seealso cref="EbayCSV.Core.FileWriter.IFileWriter.GeneratedData"/>
        public IEnumerable<string> GeneratedData()
        {
            return generatedCsvPaths;
        }

        /// <summary>
        /// Writes new items
        /// </summary>
        /// <param name="newItems">list of new items</param>
        private void WriteNewItems(List<ICsvItem> newItems, string suppliername)
        {
            if (newItems.Any())
            {
                string path = resultDirectory.GetResultDirectory() + "new" + suppliername + DateTime.Now.Ticks + "_" + newItems.Count() + ".txt";
                

                StringBuilder csvlines = new StringBuilder();
                csvlines.AppendLine(csvHeader.GetNewActionHeader());

                foreach (var item in newItems)
                {
                    foreach (string line in item.GetNewActionFormat())
                    {
                        csvlines.AppendLine(line);
                    }
                }

            
                    //28591 is the codepage of ISO-8859-1
                    //Encoding encoding = Encoding.GetEncoding(28591);
                    File.WriteAllText(path, csvlines.ToString(), encoding);

                    List<string> paths = new List<string>();
                    paths.Add(path);
                    uploader.BeginUpload(paths);

                    generatedCsvPaths.Add(path);
            }

            
        }

        /// <summary>
        /// Writes revision items
        /// </summary>
        /// <param name="reviseItems">list of revisions items</param>
        private void WriteReviseItems(List<ICsvItem> reviseItems, string suppliername)
        {
            if (reviseItems.Any())
            {
                string path = resultDirectory.GetResultDirectory() + "revision" + suppliername + DateTime.Now.Ticks + "_" + reviseItems.Count() + ".txt";
               

                StringBuilder csvlines = new StringBuilder();
                csvlines.AppendLine(csvHeader.GetReviseActionHeader());

                foreach (var item in reviseItems)
                {
                    foreach (string line in item.GetReviseActionFormat())
                    {
                        csvlines.AppendLine(line);
                    }
                }

            
                    //28591 is the codepage of ISO-8859-1
                    //Encoding encoding = Encoding.GetEncoding(28591);
                    File.WriteAllText(path, csvlines.ToString(), encoding);

                    List<string> paths = new List<string>();
                    paths.Add(path);
                    uploader.BeginUpload(paths);

                    generatedCsvPaths.Add(path);
            }

            
        }
    }
}
