﻿using EbayCSV.Core.CsvItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.FileWriter
{
    /// <summary>
    /// Processes the work on writing the generated csv data
    /// </summary>
    /// <seealso cref="EbayCSV.Core.FileWriter.CsvDataAppendsWriter"/>
    /// <seealso cref="EbayCSV.Core.FileWriter.CsvFileWriter"/>
    /// <seealso cref="EbayCSV.Core.FileWriter.TxtFileWriter"/>
    public interface IFileWriter
    {
        /// <summary>
        /// Writes the generated csv data
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="csvItems">Parameter value</param>
        /// <param name="factor1">Additional property placeholder</param>
        /// <param name="factor2">Additional property placeholder</param>
        void Write(IEnumerable<ICsvItem> csvItems, string factor1 = "", string factor2 = "");

        /// <summary>
        /// writes the csv lines
        /// </summary>
        /// <param name="csvLines"></param>
        /// <param name="path"></param>
        void Write(IEnumerable<string> csvLines, string supplierName);

        /// <summary>
        /// Processes on returning the generated data
        /// </summary>
        /// <returns>Returns generated data</returns>
        IEnumerable<string> GeneratedData();
    }
}
