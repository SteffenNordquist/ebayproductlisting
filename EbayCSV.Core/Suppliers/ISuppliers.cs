﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Suppliers
{
    public interface ISuppliers
    {
        IEnumerable<TResult> GetSuppliersDatabases<TResult, TSource>(IEnumerable<TSource> sourceList);
    }
}
