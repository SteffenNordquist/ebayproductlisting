﻿using ProcuctDB;
using ProcuctDB.DatabaseFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Suppliers
{
    public class AgentEbaySuppliers : IAgentEbaySuppliers
    {
        public IEnumerable<TResult> GetSuppliersDatabases<TResult, TSource>(IEnumerable<TSource> sourceList)
        {
            List<string> ebaySupplierNames = (List<string>)sourceList;

            List<IDatabase> supplierList = new List<IDatabase>();

            ProductFinder productFinder = new ProductFinder();

            foreach (string ebaySupplier in ebaySupplierNames)
            {
                //supplierList.Add(DatabaseParser.Parse(ebaySupplier));
                supplierList.Add(productFinder.GetSupplierDatabase(ebaySupplier));
            }

            return (IEnumerable<TResult>)supplierList;
        }
    }
}
