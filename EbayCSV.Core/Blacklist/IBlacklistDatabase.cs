﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Blacklist
{
    /// <summary>
    /// Enables interaction in checking ean existence in blacklist
    /// </summary>
    public interface IBlacklistDatabase
    {
        /// <summary>
        /// checks ean if exists in blacklist
        /// </summary>
        /// <param name="ean">13 digit number</param>
        /// <returns>returns boolean</returns>
        bool IsExists(string ean);
    }
}
