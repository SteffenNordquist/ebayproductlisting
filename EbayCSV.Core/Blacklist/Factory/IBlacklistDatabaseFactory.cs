﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Blacklist.Factory
{
    /// <summary>
    /// It is a factory for the blacklist database.
    /// </summary>
    public interface IBlacklistDatabaseFactory
    {
        /// <summary>
        /// A method that constructs a class implementing IBlacklistDatabase
        /// </summary>
        /// <returns>returns IBlacklistDatabase</returns>
        IBlacklistDatabase GetBlacklistDatabase();
    }
}
