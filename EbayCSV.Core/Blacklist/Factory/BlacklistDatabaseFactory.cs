﻿using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Blacklist.Factory
{
    /// <summary>
    /// It is a factory for the blacklist database.
    /// </summary>
    public class BlacklistDatabaseFactory : IBlacklistDatabaseFactory
    {
        /// <summary>
        /// The settings/configuration container
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// Constructor of this class.
        /// </summary>
        /// <param name="settings">The settings/configuration container</param>
        public BlacklistDatabaseFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// A method that constructs a class implementing IBlacklistDatabase
        /// </summary>
        /// <returns>returns IBlacklistDatabase</returns>
        public IBlacklistDatabase GetBlacklistDatabase()
        {
            #region dn data access
            IDbConnectionFactory connectionFactory = new MongoConnectionFactory(new MongoConnectionProperties());
            connectionFactory.ConnectionProperties.SetProperty("connectionstring", settings.GetValue("ebayblacklistconnectionstring"));
            connectionFactory.ConnectionProperties.SetProperty("databasename", settings.GetValue("ebayblacklistdatabasename"));
            connectionFactory.ConnectionProperties.SetProperty("collectionname", settings.GetValue("ebayblacklistcollectionname"));
            #endregion

            IDbConnection dbConnection = connectionFactory.CreateConnection();

            return new BlacklistDatabase(dbConnection);
        }
    }
}
