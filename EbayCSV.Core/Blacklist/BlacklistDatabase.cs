﻿using DN.DataAccess.ConnectionFactory;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Blacklist
{
    /// <summary>
    /// Enables interaction in checking ean existence in blacklist
    /// </summary>
    public class BlacklistDatabase : IBlacklistDatabase
    {
        /// <summary>
        /// A delegate in communicating to the ebay categories collection.
        /// </summary>
        private readonly IDbConnection dbConnection;

        private List<string> blacklist = new List<string>();

        /// <summary>
        /// The construction of this class
        /// </summary>
        /// <param name="dbConnection">A delegate in communicating to the ebay categories collection.</param>
        public BlacklistDatabase(IDbConnection dbConnection)
        {
            this.dbConnection = dbConnection;
        }

        /// <summary>
        /// checks ean if exists in blacklist
        /// </summary>
        /// <param name="ean">13 digit number</param>
        /// <returns>returns boolean</returns>
        public bool IsExists(string ean)
        {
            if (blacklist.Count == 0)
            {
                blacklist = dbConnection.DataAccess.Queries.Find<BsonDocument>("{ ean : {$exists:true}}", new string[] { "ean" }).Select(x => x["ean"].AsString).ToList();
            }

            bool exists = false;

            if (blacklist.Contains(ean))
            {
                exists = true;
            }

            return exists;
        }
    }
}
