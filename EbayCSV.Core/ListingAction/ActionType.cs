﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.ListingAction
{
    /// <summary>
    /// Action types
    /// </summary>
    public enum ActionType
    {
        /// <summary>
        /// a. sku does not exists in inventory database
        /// OR
        /// b. if sku exists, it doesn't have ItemID
        /// </summary>
        Add,

        /// <summary>
        /// a. inventory StartPrice is not equal to wanted price
        /// OR
        /// b. inventory Quantity is not equal to wanted quantity
        /// </summary>
        Revise,

        Delete,

        /// <summary>
        /// not Add and not Revise
        /// </summary>
        None
    }
}
