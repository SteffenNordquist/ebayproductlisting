﻿using DN_Classes.Logger;
using EbayCSV.Core.CsvItem;
using EbayCSV.Core.FileWriter;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.Inventory.InventorySource;
using EbayCSV.Core.Inventory.InventorySource.ListingInventory;
using EbayCSV.Core.Inventory.InventorySource.ListingInventory.BK;
using EbayCSV.Core.ListingAction;
using EbayCSV.Core.Mapper;
using EbayCSV.Core.RawData;
using EbayCSV.Core.ResultDirectory;
using EbayCSV.Core.Settings;
using ProcuctDB;
using ProcuctDB.JTL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvGenerator
{
    /// <summary>
    /// A csv generator class that delegates csv generation works.
    /// </summary>
    public class BKCsvGenerator : ICsvGenerator
    {
        /// <summary>
        /// Contains raw items from any source that will be converted into a csv file
        /// </summary>
        private readonly IRawItems rawItems;

        /// <summary>
        /// A mapper from a raw item in a csv item
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Writes csv items into a file form
        /// </summary>
        private readonly IFileWriter fileWriter;

        /// <summary>
        /// The settings/configuration container
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// Container of mapped raw items into csv items
        /// </summary>
        private List<ICsvItem> csvItems = new List<ICsvItem>();

        /// <summary>
        /// Container of generated csv paths
        /// </summary>
        private List<string> generatedCsvPaths = new List<string>();

        /// <summary>
        /// uset to get all eans of a supplier
        /// </summary>
        private readonly ISupplierEans supplierEansInterface;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="rawItems">Contains raw items from any source that will be converted into a csv file</param>
        /// <param name="mapper">A mapper from a raw item in a csv item</param>
        /// <param name="fileWriter">Writes csv items into a file form</param>
        /// <param name="settings">The settings/configuration container</param>
        public BKCsvGenerator(IRawItems rawItems, IMapper mapper, IFileWriter fileWriter, ISettings settings, ISupplierEans supplierEansInterface)
        {
            this.rawItems = rawItems;
            this.mapper = mapper;
            this.fileWriter = fileWriter;
            this.settings = settings;
            this.supplierEansInterface = supplierEansInterface;
        }

        /// <summary>
        /// Manages the csv generation
        /// Delegates the work on getting raw item, mapping from raw to csv and writing to a file
        /// </summary>
        /// <seealso cref="EbayCSV.Core.CsvGenerator.ICsvGenerator.Generate"/>
        public void Generate()
        {
            
            int actionlimit = int.Parse(settings.GetValue("actionlimit"));
            int itemsPerFile = int.Parse(settings.GetValue("itemsPerFile"));

            int processedCounter = 0;

            string supplierName = "";

            AppLogger.Log("getting supplier products");
            bool hasMore = true;
            int limit = 10000;
            int skip = 0;
            while (hasMore)
            {
                hasMore = false;

                foreach (var bkProduct in rawItems.GetItems<IProductSalesInformation>(skip, limit).ToList())
                {
                    if (supplierName == "")
                    {
                        supplierName = bkProduct.getSupplierName();

                        //not to worry on jtl different products
                        //because all products of the implemented 'IRawItems' is one supplier
                        //ie : BKRawItemsWithJtlJpcSupplier
                        if (bkProduct.GetType().Name.ToLower().StartsWith("jtl"))
                        {
                            JTLEntity bkJtlProduct = (JTLEntity)bkProduct;
                            supplierName = bkJtlProduct.product.supplier;
                        }
                    }

                    var mappedItem = mapper.MapItem<ICsvItem, IProductSalesInformation>(bkProduct);

                    if (mappedItem != null)
                    {
                        processedCounter += 1;
                        Console.WriteLine(processedCounter);

                        csvItems.Add(mappedItem);

                        //writing all revisions
                        if (csvItems.FindAll(x => x.GetActionType() == ActionType.Revise).Count() == itemsPerFile)
                        {
                            fileWriter.Write(csvItems.FindAll(x => x.GetActionType() == ActionType.Revise), supplierName);
                            csvItems.RemoveAll(x => x.GetActionType() == ActionType.Revise);
                        }

                        //writing all new items
                        if (csvItems.FindAll(x => x.GetActionType() == ActionType.Add).Count() == itemsPerFile)
                        {
                            fileWriter.Write(csvItems.FindAll(x => x.GetActionType() == ActionType.Add), supplierName);
                            csvItems.RemoveAll(x => x.GetActionType() == ActionType.Add);

                        }

                    }

                    hasMore = true;

                    if (processedCounter >= actionlimit)
                    {
                        hasMore = false;
                        break;
                    }
                }

                skip += limit;
            }

            AppLogger.Log("products processed : " + (skip - limit));

            if (csvItems.Count() > 0)
            {
                fileWriter.Write(csvItems, supplierName);
            }

            AppLogger.Log("**********************generating end listings*********************************");

            if (supplierName != "")
            {
                List<string> supplierEans = supplierEansInterface.GetEans().ToList();

                List<string> endListingLines = new List<string>();
                endListingLines.Add("Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID\tCustomLabel");

                endListingLines.AddRange(EbayInventory.InventoryItems().Where(x => !supplierEans.Contains(x.Ean) && x.ItemID != null && x.SupplierName != null && x.SupplierName.ToLower().StartsWith(supplierName.ToLower()) && (x.ListingStatus != null && x.ListingStatus == "Active"))
                     .Select(x => "EndItem" + "\t" + x.ItemID + "\t" + x.Ean).ToList());

                if (endListingLines.Count() > 1)
                {
                    AppLogger.Log("listings to end : " + endListingLines.Count());
                    fileWriter.Write(endListingLines, supplierName);
                }
            }
        }

        /// <summary>
        /// Returns the list of generated csv data ( ie : list of csv files )
        /// </summary>
        /// <returns>Returns the list of generated csv data</returns>
        /// <seealso cref="EbayCSV.Core.CsvGenerator.ICsvGenerator.GeneratedCsvData"/>
        public IEnumerable<string> GeneratedCsvData()
        {
            return fileWriter.GeneratedData();
        }
    }
}
