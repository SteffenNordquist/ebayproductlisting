﻿using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.RawData
{
    public class GallayEntity
    {
        public ObjectId id { get; set; }
        public Product product { get; set; }
        public BsonDocument plus { get; set; }
    }

    public class Product
    {
        public string ean { get; set; }
        public List<string> asin { get; set; }
        public string itemno { get; set; }
        public string title { get; set; }
        public string addtext { get; set; }
        public string unit { get; set; }
        public double lstpr { get; set; }
        public string pic1 { get; set; }
        public string pic1url { get; set; }
        public string pic2 { get; set; }
        public string pic2url { get; set; }
        public string shopcat { get; set; }
        public string cat1 { get; set; }
        public string cat2 { get; set; }
        public string cat3 { get; set; }
        public int stock { get; set; }
        public string spec1 { get; set; }
        public string spec1txt { get; set; }
        public string spec2 { get; set; }
        public string spec2txt { get; set; }
        public string spec3 { get; set; }
        public string spec3txt { get; set; }
        public string size { get; set; }
        public string cross { get; set; }
        public string alloy { get; set; }
        public string clasp { get; set; }
        public string newItem { get; set; }
        public bool done { get; set; }
        public int status { get; set; }
        public string htmlbody { get; set; }
    }
}
