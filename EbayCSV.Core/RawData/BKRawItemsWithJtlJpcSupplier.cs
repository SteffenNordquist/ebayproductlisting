﻿using ProcuctDB;
using ProcuctDB.JTL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.RawData
{
    public class BKRawItemsWithJtlJpcSupplier : IRawItems
    {
        private readonly IDatabase database;

        public BKRawItemsWithJtlJpcSupplier(IDatabase database)
        {
            this.database = database;
        }

        public IEnumerable<T> GetItems<T>(int skip, int limit)
        {
            List<IProductSalesInformation> bkProducts = new List<IProductSalesInformation>();

            foreach (var product in database.GetEbayReady(skip, limit))
            {
                bkProducts.Add(product);
            }

            if (database.GetType().Name.ToLower().StartsWith("jtl"))
            {
                List<JTLEntity> bkJtlProducts = new List<JTLEntity>();
                bkProducts.ForEach(x => bkJtlProducts.Add((JTLEntity)x));
                return (IEnumerable<T>)(IEnumerable<object>)bkJtlProducts.Where(x => x.product.supplier.ToLower().StartsWith("jpc")).Select(x => x).ToList();
                //return (IEnumerable<T>)(IEnumerable<object>)bkProducts.Where(x => x.getSupplierName().ToLower().StartsWith("jpc")).Select(x => x).ToList();
            }

            return (IEnumerable<T>)(IEnumerable<object>)bkProducts;
        }
    }
}
