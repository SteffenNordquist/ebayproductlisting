﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.RawData
{
    public interface IRawItems
    {
        IEnumerable<T> GetItems<T>(int skip, int limit);
        //IEnumerable<T> GetHeader<T>(char delimiter);
    }
}
