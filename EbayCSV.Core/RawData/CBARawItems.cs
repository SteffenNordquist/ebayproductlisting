﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.RawData
{
    public class CBARawItems : IRawItems
    {
        private readonly ISettings settings;

        public CBARawItems(ISettings settings)
        {
            this.settings = settings;
        }

        public IEnumerable<T> GetItems<T>(int skip, int limit)
        {
            string filename = settings.GetValue("InputFilePath");

            List<string> lines = File.ReadAllLines(filename, Encoding.GetEncoding("windows-1252")).ToList();

            //removing header
            if (lines[0].ToLower().Contains("category") && lines[0].ToLower().Contains("title") || lines[0].ToLower().Contains("price"))
            {
                lines.RemoveAt(0);
            }

            //removing empty lines
            lines = lines.Where(x => x.Trim() != string.Empty).Select(x => x).ToList();
            
            return (IEnumerable<T>)lines;
        }


        //public IEnumerable<T> GetHeader<T>(char delimiter)
        //{
        //    List<string> lines = File.ReadAllLines(filename, Encoding.GetEncoding("windows-1252")).ToList();
        //    return (IEnumerable<T>)lines.First().Split(delimiter).ToList(); 
        //}
    }
}
