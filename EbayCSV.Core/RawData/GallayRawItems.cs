﻿using MongoDB.Driver;
using ProcuctDB;
using ProcuctDB.Gallay;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.RawData
{
    public class GallayRawItems : IRawItems
    {
        private readonly IDatabase gallayDB;

        public GallayRawItems()
        {
            gallayDB = new GallayDB();
        }

        public IEnumerable<T> GetItems<T>(int skip, int limit)
        {
            return (IEnumerable<T>)gallayDB.GetEbayReady(skip, limit);
        }

    }
}
