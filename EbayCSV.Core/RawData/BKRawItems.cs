﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.RawData
{
    public class BkRawItems : IRawItems
    {
        public IDatabase database;

        public BkRawItems(IDatabase database)
        {
            this.database = database;
        }

        public IEnumerable<T> GetItems<T>(int skip, int limit)
        {
            List<IProductSalesInformation> bkProducts = new List<IProductSalesInformation>();

            #region old version
            //bool hasMore = true;
            //limit = 10000;
            //while (hasMore)
            //{
            //    hasMore = false;

            //    foreach (var product in database.GetEbayReady(skip, limit))
            //    {
            //        bkProducts.Add(product);
            //        hasMore = true;
            //    }

            //    skip += limit;
            //}
            #endregion

            
                foreach (var product in database.GetEbayReady(skip, limit))
                {
                    bkProducts.Add(product);
                }


            return (IEnumerable<T>)(IEnumerable<object>)bkProducts;
        }
    }
}
