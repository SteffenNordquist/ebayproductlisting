﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.ResultDirectory
{
    public class BKResultDirectory : IResultDirectory
    {
        public string GetResultDirectory()
        {
            string bkDirectory = @"C:\DevNetworks\EBAY\EbayListing\GeneratedEbayListingFiles\";
            if (!Directory.Exists(bkDirectory))
            {
                Directory.CreateDirectory(bkDirectory);
            }

            return bkDirectory;
        }
    }
}
