﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Filter
{
    /// <summary>
    /// Responsible for filtering invalid category items
    /// </summary>
    public class InvalidCategoryFilter : IListingFilter
    {
        /// <summary>
        /// Checks if an item is okay to be included
        /// </summary>
        /// <typeparam name="T1">Parameter type</typeparam>
        /// <typeparam name="T2">Parameter type</typeparam>
        /// <typeparam name="T3">Parameter type</typeparam>
        /// <param name="value1">Indicator of an item</param>
        /// <param name="value2">Indicator of an item</param>
        /// <param name="value3">Indicator of an item</param>
        /// <returns>Returns boolean value</returns>
        public bool IsOkay<T1, T2, T3>(T1 value1, T2 value2, T3 value3)
        {
            string ean = (string)(object)value1;

            bool okay = true;

            if (InvalidCategoryList.Exists(ean))
            {
                okay = false;
                DN_Classes.Logger.AppLogger.Warn("filtered : " + ean + " : invalid category");
            }

            return okay;
        }
    }
}
