﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Filter
{
    /// <summary>
    /// An static class
    /// A container of list of eans having invalid category
    /// </summary>
    public static class InvalidCategoryList
    {
        /// <summary>
        /// list of eans having invalid category
        /// </summary>
        private static List<string> eans = new List<string>();

        /// <summary>
        /// Checks if an ean exists in the list having invalid category
        /// </summary>
        /// <param name="ean">13-digit european article number</param>
        /// <returns>Returns boolean value</returns>
        public static bool Exists(string ean)
        {
            if (eans.Count() == 0)
            {
                if (File.Exists(@"D:\InvalidCategories\invalidCategoriesEans.txt"))
                {
                    eans = File.ReadAllLines(@"D:\InvalidCategories\invalidCategoriesEans.txt").ToList();
                }
            }

            if (eans.Contains(ean))
            {
                return true;
            }

            return false;
        }
    }
}
