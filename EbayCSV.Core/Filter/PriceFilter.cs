﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Logger;
using EbayCSV.Core.Inventory;
using ProcuctDB;

namespace EbayCSV.Core.Filter
{
    public class PriceFilter : IListingFilter
    {
        /// <summary>
        /// Checks if an item is okay to be included
        /// </summary>
        /// <typeparam name="T1">Parameter type</typeparam>
        /// <typeparam name="T2">Parameter type</typeparam>
        /// <typeparam name="T3">Parameter type</typeparam>
        /// <param name="value1">Indicator of an item</param>
        /// <param name="value2">Indicator of an item</param>
        /// <param name="value3">Indicator of an item</param>
        /// <returns>Returns boolean value</returns>
        public bool IsOkay<T1, T2, T3>(T1 value1, T2 value2, T3 value3)
        {
            if (value1 == null || value2 == null || value3 == null)
            {
                throw new ArgumentNullException("null params : either value1, value2, value3");
            }

            string ean = (string)(object)value1;
            string supplierName = (string)(object)value2;
            double sellPrice = (double)(object)value3;

            bool okay = true;

            var inventoryItem = InventoryStorage.InventoryItems().Find(x => x.Ean == ean && x.SupplierName.ToLower().StartsWith(supplierName.ToLower()));

            if (inventoryItem == null)
            {
                okay = false;
                AppLogger.Warn("filtered : " + ean + " on " + supplierName + " not in inventory");
            }
            else if (sellPrice < inventoryItem.Wanted.Price)
            {
                AppLogger.Warn("filtered : " + ean + " : calculated price lesser than wanted price");
                okay = false;
            }

            return okay;
        }
    }
}
