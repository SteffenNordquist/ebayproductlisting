﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Filter
{
    /// <summary>
    /// Responsible for filtering items
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Filter.InvalidCategoryFilter"/>
    /// <seealso cref="EbayCSV.Core.Filter.BlacklistFilter"/>
    /// <seealso cref="EbayCSV.Core.Filter.PriceFilter"/>
    public interface IListingFilter
    {
        /// <summary>
        /// Checks if an item is okay to be included
        /// </summary>
        /// <typeparam name="T1">Parameter type</typeparam>
        /// <typeparam name="T2">Parameter type</typeparam>
        /// <typeparam name="T3">Parameter type</typeparam>
        /// <param name="value1">Indicator of an item</param>
        /// <param name="value2">Indicator of an item</param>
        /// <param name="value3">Indicator of an item</param>
        /// <returns>Returns boolean value</returns>
        bool IsOkay<T1, T2, T3>(T1 value1, T2 value2, T3 value3);
    }
}
