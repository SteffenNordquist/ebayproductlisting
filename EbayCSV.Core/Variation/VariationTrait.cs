﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Variation
{
    public class VariationTrait
    {
        public string trait { get; set; }
        public string value { get; set; }

        public override string ToString()
        {
            return this.trait + "=" + this.value;
        }
    }
}
