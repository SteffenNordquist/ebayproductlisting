﻿using EbayCSV.Core.ListingAction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Variation
{
    public interface ICsvVariation
    {
        ActionType Action();

        IEnumerable<TResult> VariationTraits<TResult>();

        string GetLineFormat();
    }
}
