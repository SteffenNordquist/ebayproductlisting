﻿using EbayCSV.Core.ListingAction;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Variation
{
    public class DefaultVariation : ICsvVariation
    {
        public string Sku { get; set; }
        public ActionType action { get; set; }
        public string Relationship { get; set; }
        public List<TraitValue> RelationshipDetails { get; set; }
        public string CustomLabel { get; set; }
        public int Quantity { get; set; }
        public double StartPrice { get; set; }
        public string PicURL { get; set; }

        public DefaultVariation(string sku, ActionType action, string relationship, List<TraitValue> relationshipDetails, string customLabel, int quantity, double startPrice, string picUrl)
        {
            this.Sku = sku;
            this.action = action;
            this.Relationship = relationship;
            this.RelationshipDetails = relationshipDetails;
            this.CustomLabel = customLabel;
            this.Quantity = quantity;
            this.StartPrice = startPrice;
            this.PicURL = picUrl;
        }

        public string GetLineFormat()
        {
            if (action == ActionType.Revise)
            {
                return GetReviseActionFormat();

            }
            else
            {
                return GetNewActionFormat();
            }
        }

        private string GetNewActionFormat()
        {
            string relationshipDetailsFormat = "";

            if (this.RelationshipDetails.Count() > 0)
            {
                relationshipDetailsFormat = string.Join("|", RelationshipDetails.Select(x => x.ToString()).ToList());
            }

            return String.Format(
                            "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}\t{20}\t{21}\t{22}\t{23}\t{24}\t{25}\t{26}\t{27}\t{28}\t{29}\t{30}\t{31}\t{32}\t{33}\t{34}\t{35}\t{36}\t{37}\t{38}\t{39}\t{40}\t{41}",
                            "",
                            "",
                            "",
                            "",
                            this.Relationship,
                            relationshipDetailsFormat,
                            "",
                            "",
                            this.PicURL,
                            this.Quantity,
                            "",
                            this.StartPrice.ToString(CultureInfo.InvariantCulture),
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            this.CustomLabel,
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            "",
                            ""
                        );
        }

        private string GetReviseActionFormat()
        {
            string relationshipDetailsFormat = "";

            if (this.RelationshipDetails.Count() > 0)
            {
                relationshipDetailsFormat = string.Join("|", RelationshipDetails.Select(x => x.ToString()).ToList());
            }

            string lines = String.Format(
                            "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}",
                            "Delete",
                            this.CustomLabel,
                            this.Relationship,
                            relationshipDetailsFormat,
                            "",
                            "",
                            "",
                            this.StartPrice.ToString(CultureInfo.InvariantCulture),
                            "",
                            this.Quantity,
                            this.Sku,
                            this.PicURL,
                            ""
                        );

            lines += "\n" + lines.Replace("Delete", "");

            return lines;
        }

        public IEnumerable<TResult> VariationTraits<TResult>()
        {
            return (IEnumerable<TResult>)RelationshipDetails;
        }

        public ActionType Action()
        {
            return this.action;
        }
    }
}
