﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.BrandFactory
{
    public class GallayBrandFactory : IBrandFactory
    {
        public string GetBrand()
        {
            return "Gallay";
        }
    }
}
