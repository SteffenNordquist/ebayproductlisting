﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.DispatchTimeFactory
{
    /// <summary>
    /// The dispatch (or handling) time is the number of working days the seller will take to post the item after receiving your cleared payment.
    /// Processes maximum dispathc time value
    /// </summary>
    public interface IDispatchTimeFactory
    {
        /// <summary>
        /// Returns the dispatch time value
        /// </summary>
        /// <typeparam name="TSource">Type of the parameter</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns the dispatch time value</returns>
        string GetDispatchTime<TSource>(TSource tSource);
    }
}
