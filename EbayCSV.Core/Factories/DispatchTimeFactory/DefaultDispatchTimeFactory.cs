﻿using EbayCSV.Core.Settings;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.DispatchTimeFactory
{
    /// <summary>
    /// The dispatch (or handling) time is the number of working days the seller will take to post the item after receiving your cleared payment.
    /// Processes maximum dispathc time value
    /// </summary>
    public class DefaultDispatchTimeFactory : IDispatchTimeFactory
    {
        /// <summary>
        /// Container of settings/configuration
        /// </summary>
        private readonly ISettings csvSettings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="csvSettings">Container of settings/configuration</param>
        public DefaultDispatchTimeFactory(ISettings csvSettings)
        {
            this.csvSettings = csvSettings;
        }

        /// <summary>
        /// Returns the dispatch time value
        /// </summary>
        /// <typeparam name="TSource">Type of the parameter</typeparam>
        /// <param name="tSource">Treated as IProductSalesInformation</param>
        /// <returns>Returns the dispatch time value</returns>
        public string GetDispatchTime<TSource>(TSource tSource)
        {
            IProductSalesInformation product = (IProductSalesInformation)(object)tSource;

            string supplier = product.getSupplierName().ToLower();

            return csvSettings.GetValue(supplier + "DispatchTimeMax");
        }
    }
}
