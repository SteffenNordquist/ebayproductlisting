﻿using EbayCSV.Core.ListingAction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ListingActionFactory
{
    /// <summary>
    /// Processes the listing action of an item variation. ( ie : Add, Revise )
    /// </summary>
    public class DefaultVariationListingActionFactory : IVariationListingActionFactory
    {
        /// <summary>
        /// Processes on returning the listing action of an item variation
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Extra parameter placeholder</param>
        /// <param name="sku">Stock Keeping unit</param>
        /// <param name="price">Item price</param>
        /// <param name="quantity">Item quantity</param>
        /// <returns>Returns the listing action of an item variation</returns>
        public ActionType ListingActionType<TSource>(TSource tSource, string sku, double price, int quantity)
        {
            throw new NotImplementedException();
        }
    }
}
