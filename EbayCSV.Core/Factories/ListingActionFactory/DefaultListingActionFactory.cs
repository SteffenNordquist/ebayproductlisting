﻿
using EbayCSV.Core.Inventory;
using EbayCSV.Core.ListingAction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ListingActionFactory
{
    /// <summary>
    /// Processes the listing action of an item. ( ie : Add, Revise )
    /// </summary>
    public class DefaultListingActionFactory : IListingActionFactory
    {
        /// <summary>
        /// Processes on returning the listing action of an item
        /// </summary>
        /// <param name="sku">Stock Keeping unit</param>
        /// <param name="wantedPrice">Item price</param>
        /// <param name="wantedQuantity">Item quantity</param>
        /// <param name="supplierName">supplier name</param>
        /// <returns>Returns the listing action of an item</returns>
        public ActionType ListingActionType(string sku, double wantedPrice, int wantedQuantity, string supplierName)
        {
            var inventoryItem = InventoryStorage.InventoryItems().Find(x => x.Ean == sku);

            if (inventoryItem == null)
            {
                return ActionType.Add;
            }
            else if (Math.Round(inventoryItem.StartPrice, 2) != Math.Round(wantedPrice, 2) || inventoryItem.Quantity != wantedQuantity)
            {
                return ActionType.Revise;
            }

            return ActionType.None;
        }
    }
}
