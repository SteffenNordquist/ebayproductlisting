﻿using DN_Classes.Logger;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.ListingAction;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using EbayCSV.Core.Settings;

namespace EbayCSV.Core.Factories.ListingActionFactory
{
    /// <summary>
    /// Processes the listing action of an item. ( ie : Add, Revise )
    /// </summary>
    public class BkListingActionFactory : IListingActionFactory
    {
        /// <summary>
        /// Container of settings/configuration
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of settings/configuration</param>
        public BkListingActionFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Processes on returning the listing action of an item
        /// </summary>
        /// <param name="sku">Stock Keeping unit</param>
        /// <param name="wantedPrice">Item price</param>
        /// <param name="wantedQuantity">Item quantity</param>
        /// <param name="supplierName">supplier name</param>
        /// <returns>Returns the listing action of an item</returns>
        public ActionType ListingActionType(string sku, double wantedPrice, int wantedQuantity, string supplierName)
        {
            if (sku == null || supplierName == null)
            {
                throw new ArgumentNullException();
            }

            var inventoryItem = InventoryStorage.InventoryItems().Find(x => x.Ean == sku && x.SupplierName.ToLower().StartsWith(supplierName.ToLower()));

            ActionType actionType = ActionType.None;

            string logMessage = "filtered : " + sku + " : ActionType.None";

            if (inventoryItem == null || (inventoryItem.ItemID == null))
            {
                actionType = ActionType.Add;

                if (wantedQuantity == 0)
                {
                    logMessage = "filtered : " + sku + " : ActionType.Add with no stock";
                    actionType = ActionType.None;
                }

                if (Boolean.Parse(settings.GetValue("revisionOnly")))
                {
                    logMessage = "filtered : " + sku + " : only revisions set to be processed";
                    actionType = ActionType.None;
                }

            }
            else if (Math.Abs(inventoryItem.StartPrice - wantedPrice) > 0
                || inventoryItem.Quantity > wantedQuantity
                || (inventoryItem.Quantity == 0 && wantedQuantity > 0)
                || inventoryItem.ForceRevision)
            {
                ThreadStart updateThread = delegate
                {
                    string query = "{$and:[{Ean:\"" + sku + "\"},{SupplierName:\"" + supplierName + "\"}]}";
                    InventoryStorage.InventorySource.InventoryDbConnection()
                        .DataAccess.Commands.Update(query, "ForceRevision", false);
                };
                new Thread(updateThread).Start();

                actionType = ActionType.Revise;
            }

            AppLogger.Info(logMessage);

            return actionType;
        }
    }
}
