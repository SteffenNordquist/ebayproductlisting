﻿//using EbayCSV.Core.Factories.BrandFactory;
using EbayCSV.Core.Factories.CategoryFactory;
using EbayCSV.Core.Factories.CustomLabelFactory;
using EbayCSV.Core.Factories.ImageFactory;
using EbayCSV.Core.Factories.ListingActionFactory;
using EbayCSV.Core.Factories.PaypalEmailFactory;
using EbayCSV.Core.Factories.SellpriceFactory;
using EbayCSV.Core.Factories.TitleFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories
{
    public interface ICsvFieldsFactory
    {
        IListingActionFactory ListingActionFactory();

        ICustomLabelFactory CustomLabelFactory();

        ITitleFactory TitleFactory();

        ICategoryFactory CategoryFactory();

        IImageFactory ImageFactory();

        ISellpriceFactory SellpriceFactory();

        IPaypalEmailFactory PaypalEmailFactory();

        //IBrandFactory BrandFactory();
    }
}
