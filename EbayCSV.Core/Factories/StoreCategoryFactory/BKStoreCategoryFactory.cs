﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.StoreCategoryFactory
{
    /// <summary>
    /// Processes the store category of an item
    /// </summary>
    public class BKStoreCategoryFactory : IStoreCategoryFactory
    {
        /// <summary>
        /// Processes the work on returning the store category of an item
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Additional property placeholder</param>
        /// <returns>Returns the store category</returns>
        /// <seealso cref="EbayCSV.Core.Factories.StoreCategoryFactory.IStoreCategoryFactory.GetStoreCategory"/>
        public string GetStoreCategory<TSource>(TSource tSource)
        {
            return "";
        }
    }
}
