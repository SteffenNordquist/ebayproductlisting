﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.StoreCategoryFactory
{
    /// <summary>
    /// Processes the store category of an item
    /// </summary>
    public class DefaultStoreCategoryFactory : IStoreCategoryFactory
    {
        /// <summary>
        /// Container of settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of settings/configurations</param>
        public DefaultStoreCategoryFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Processes the work on returning the store category of an item
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Additional property placeholder</param>
        /// <returns>Returns the store category</returns>
        /// <seealso cref="EbayCSV.Core.Factories.StoreCategoryFactory.IStoreCategoryFactory.GetStoreCategory"/>
        public string GetStoreCategory<TSource>(TSource tSource)
        {
            string source = (string)(object)tSource;

            string delimiter = settings.GetValue("InputFileDelimiter");
            #region finalizing delimeter
            if (delimiter.Contains("tab"))
            {
                delimiter = "\t";
            }
            #endregion

            string storeCategory = "";

            string[] splits = source.Split(delimiter.ToCharArray());

            if (splits.Count() > 2)
            {
                storeCategory = splits[2];
            }

            return storeCategory;

        }
    }
}
