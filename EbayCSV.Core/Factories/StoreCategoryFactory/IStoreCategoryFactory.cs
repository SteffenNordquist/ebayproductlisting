﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.StoreCategoryFactory
{
    /// <summary>
    /// Processes the store category of an item
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Factories.StoreCategoryFactory.BKStoreCategoryFactory"/>
    /// <seealso cref="EbayCSV.Core.Factories.StoreCategoryFactory.DefaultStoreCategoryFactory"/>
    public interface IStoreCategoryFactory
    {
        /// <summary>
        /// Processes the work on returning the store category of an item
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Additional property placeholder</param>
        /// <returns>Returns the store category</returns>
        string GetStoreCategory<TSource>(TSource tSource);
    }
}
