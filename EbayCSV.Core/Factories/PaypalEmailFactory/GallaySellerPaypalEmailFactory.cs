﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.PaypalEmailFactory
{
    public class GallaySellerPaypalEmailFactory : IPaypalEmailFactory
    {
        public string GetPaypalEmail()
        {
            return "gallayseller@gmail.com";
        }
    }
}
