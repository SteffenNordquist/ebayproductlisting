﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.PaypalEmailFactory
{
    /// <summary>
    /// PaypalEmail - paypal email of the seller
    /// Processes the paypal email value
    /// </summary>
    public class DefaultPaypalEmailFactory : IPaypalEmailFactory
    {
        /// <summary>
        /// Container of the settings/configurations
        /// </summary>
        private ISettings csvSettings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="csvSettings">Container of the settings/configurations</param>
        public DefaultPaypalEmailFactory(ISettings csvSettings)
        {
            this.csvSettings = csvSettings;
        }

        /// <summary>
        /// Delegates the work on returning paypal email value
        /// </summary>
        /// <returns>Returns the paypal email</returns>
        public string GetPaypalEmail()
        {
            return csvSettings.GetValue("PaypalEmail");
        }
    }
}
