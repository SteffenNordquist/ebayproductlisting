﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.PaypalEmailFactory
{
    public interface IPaypalEmailFactory
    {
        string GetPaypalEmail();
    }
}
