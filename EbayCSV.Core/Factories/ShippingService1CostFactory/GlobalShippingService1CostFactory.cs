﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ShippingService1CostFactory
{
    /// <summary>
    /// ShippingService-1:Cost - Cost to ship the item with the selected shipping service
    /// Processes the shipping service cost
    /// </summary>
    public class GlobalShippingService1CostFactory : IShippingService1CostFactory
    {
        /// <summary>
        /// Container of the settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of the settings/configurations</param>
        public GlobalShippingService1CostFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Delegates the work on returning the shipping service cost
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Extra parameter placeholder</param>
        /// <returns>Returns the shipping service cost</returns>
        public double GetShippingService1Cost<TSource>(TSource tSource)
        {
            double shippingServiceCost = 0;

            Double.TryParse(settings.GetValue("ShippingService1Cost"), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out shippingServiceCost);

            return shippingServiceCost;
        }
    }
}
