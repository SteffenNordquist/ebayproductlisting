﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ShippingService1CostFactory
{
    /// <summary>
    /// ShippingService-1:Cost - Cost to ship the item with the selected shipping service
    /// Processes the shipping service cost
    /// </summary>
    public interface IShippingService1CostFactory
    {
        /// <summary>
        /// Delegates the work on returning the shipping service cost
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Additional parameter placeholder</param>
        /// <returns>Returns the shipping service cost</returns>
        double GetShippingService1Cost<TSource>(TSource tSource);
    }
}
