﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ShippingService1FreeShippingFactory
{
    /// <summary>
    /// ShippingService-1:FreeShipping - Determines whether shipping is free for the first domestic shipping option.
    /// Processes the free shipping value
    /// </summary>
    public class BKShippingService1FreeShippingFactory : IShippingService1FreeShippingFactory
    {
        /// <summary>
        /// Container of settings/configuraions
        /// </summary>
        private readonly ISettings csvSettings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="csvSettings">Container of settings/configuraions</param>
        public BKShippingService1FreeShippingFactory(ISettings csvSettings)
        {
            this.csvSettings = csvSettings;
        }

        /// <summary>
        /// Processes on returning the free shipping value
        /// </summary>
        /// <typeparam name="TSource">Type parameter</typeparam>
        /// <param name="tSource">Additional parameter placeholder</param>
        /// <returns>Returns the free shipping value</returns>
        public string GetShippingService1FreeShipping<TSource>(TSource tSource)
        {
            return csvSettings.GetValue("ShippingService1FreeShipping");
        }
    }
}
