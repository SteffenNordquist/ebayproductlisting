﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ShippingService1FreeShippingFactory
{
    /// <summary>
    /// ShippingService-1:FreeShipping - Determines whether shipping is free for the first domestic shipping option.
    /// Processes the free shipping value
    /// </summary>
    public interface IShippingService1FreeShippingFactory
    {
        /// <summary>
        /// Processes on returning the free shipping value
        /// </summary>
        /// <typeparam name="TSource">Type parameter</typeparam>
        /// <param name="tSource">Additional parameter placeholder</param>
        /// <returns>Returns the free shipping value</returns>
        string GetShippingService1FreeShipping<TSource>(TSource tSource);
    }
}
