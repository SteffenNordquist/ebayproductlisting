﻿using EbayCSV.Core.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.SellpriceFactory
{
    /// <summary>
    /// Sellprice - price of the item
    /// Processes sellprice value
    /// </summary>
    public class BKSellpriceFactory : ISellpriceFactory
    {
        /// <summary>
        /// Processes the works on returning the sellprice
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <param name="supplierName">Parameter value</param>
        /// <returns>Returns the sellprice</returns>
        public double GetSellprice<TSource>(TSource tSource, string supplierName)
        {
            string ean = (string)(object)tSource;

            var item = InventoryStorage.InventoryItems().Find(x => x.Ean == ean && x.SupplierName.ToLower().StartsWith(supplierName.ToLower()));

            if (item == null)
            {
                return 0;
            }

            return Math.Max(1, item.Wanted.Price);
        }
    }
}
