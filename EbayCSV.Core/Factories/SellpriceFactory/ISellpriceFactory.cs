﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.SellpriceFactory
{
    /// <summary>
    /// Sellprice - price of the item
    /// Processes sellprice value
    /// </summary>
    public interface ISellpriceFactory
    {
        /// <summary>
        /// Works on returning the processed sellprice
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns the processed sellprice</returns>
        double GetSellprice<TSource>(TSource tSource, string supplierName);
    }
}
