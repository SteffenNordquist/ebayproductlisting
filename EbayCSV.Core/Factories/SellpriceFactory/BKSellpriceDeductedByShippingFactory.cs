﻿using EbayCSV.Core.Factories.ShippingService1CostFactory;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.SellpriceFactory
{
    /// <summary>
    /// Sellprice - price of the item
    /// Processes sellprice value
    /// </summary>
    public class BKSellpriceDeductedByShippingFactory : ISellpriceFactory
    {
        /// <summary>
        /// returns shipping service1 cost
        /// </summary>
        private readonly IShippingService1CostFactory shippingSerivice1CostFactory;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="shippingSerivice1CostFactory">shipping service1 cost</param>
        public BKSellpriceDeductedByShippingFactory(IShippingService1CostFactory shippingSerivice1CostFactory)
        {
            this.shippingSerivice1CostFactory = shippingSerivice1CostFactory;
        }

        /// <summary>
        /// Processes the works on returning the sellprice
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <param name="supplierName">Parameter value</param>
        /// <returns>Returns the sellprice</returns>
        public double GetSellprice<TSource>(TSource tSource, string supplierName)
        {
            string ean = (string)(object)tSource;

            var item = InventoryStorage.InventoryItems().Find(x => x.Ean == ean && x.SupplierName.ToLower().StartsWith(supplierName.ToLower()));

            if (item == null)
            {
                return 0;
            }

            double shippingServiceCost = shippingSerivice1CostFactory.GetShippingService1Cost<string>("");

            return Math.Max(1, (item.Wanted.Price - shippingServiceCost));
        }
    }
}
