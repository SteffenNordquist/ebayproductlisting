﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.SellpriceFactory
{
    /// <summary>
    /// Sellprice - price of the item
    /// Processes sellprice value
    /// </summary>
    public class DefaultSellpriceFactory : ISellpriceFactory
    {
        /// <summary>
        /// Container of settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of settings/configurations</param>
        public DefaultSellpriceFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Works on returning the processed sellprice
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns the processed sellprice</returns>
        public double GetSellprice<TSource>(TSource tSource, string supplierName)
        {
            string sourceText = (string)(object)tSource;

            #region finalizing delimeter
            string settingsDelimiter = settings.GetValue("InputFileDelimiter");

            if (settingsDelimiter.Contains("tab"))
            {
                settingsDelimiter = "\t";
            }
            #endregion

            double sellPrice = 0;

            Double.TryParse(sourceText.Split(settingsDelimiter.ToCharArray())[10].Trim().Replace(",","."), NumberStyles.AllowDecimalPoint, CultureInfo.InvariantCulture, out sellPrice);
            
            return Math.Max(1, sellPrice);
        }
    }
}
