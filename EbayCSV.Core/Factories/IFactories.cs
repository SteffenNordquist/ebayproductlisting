﻿
using EbayCSV.Core.Factories.CategoryFactory;
using EbayCSV.Core.Factories.CustomLabelFactory;
using EbayCSV.Core.Factories.DispatchTimeFactory;
using EbayCSV.Core.Factories.DurationFactory;
using EbayCSV.Core.Factories.FormatFactory;
using EbayCSV.Core.Factories.ImageFactory;
using EbayCSV.Core.Factories.ListingActionFactory;
using EbayCSV.Core.Factories.LocationFactory;
using EbayCSV.Core.Factories.PaypalAcceptFactory;
using EbayCSV.Core.Factories.PaypalEmailFactory;
using EbayCSV.Core.Factories.RelationshipDetailsFactory;
using EbayCSV.Core.Factories.RelationshipFactory;
using EbayCSV.Core.Factories.ReturnsOptionFactory;
using EbayCSV.Core.Factories.SellpriceFactory;
using EbayCSV.Core.Factories.ShippingService1CostFactory;
using EbayCSV.Core.Factories.ShippingService1FreeShippingFactory;
using EbayCSV.Core.Factories.ShippingService1OptionFactory;
using EbayCSV.Core.Factories.StockFactory;
using EbayCSV.Core.Factories.StoreCategoryFactory;
using EbayCSV.Core.Factories.TitleFactory;
using EbayCSV.Core.Factories.TitleFactory.SubTitleFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Factories.PaymentProfileName;
using EbayCSV.Core.Factories.ReturnProfileName;
using EbayCSV.Core.Factories.SalesTaxPercent;
using EbayCSV.Core.Factories.ShippingProfileName;
using EbayCSV.Core.Factories.Supplier;

namespace EbayCSV.Core.Factories
{
    /// <summary>
    /// Processes the interaction for the different attribute factories
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Factories.Factories"/>
    public interface IFactories
    {
        IListingActionFactory ListingActionFactory();

        ICustomLabelFactory CustomLabelFactory();

        ITitleFactory TitleFactory();

        ISubTitleFactory SubTitleFactory();

        ICategoryFactory CategoryFactory();

        IStoreCategoryFactory StoreCategoryFactory();

        IImageFactory ImageFactory();

        ISellpriceFactory SellpriceFactory();

        IPaypalEmailFactory PaypalEmailFactory();

        IFormatFactory FormatFactory();

        IDurationFactory DuractionFactory();

        IPaypalAcceptFactory PaypalAcceptFactory();

        ILocationFactory LocationFactory();

        IDispatchTimeFactory DispatchTimeFactory();

        IReturnsOptionFactory ReturnsOptionFactory();

        IVariationRowRelationshipFactory RelationshipFactory();

        IRelationshipDetailsFactory RelationshipDetailsFactory();

        IStockFactory StockFactory();

        IShippingService1OptionFactory ShippingService1OptionFactory();

        IShippingService1CostFactory ShippingService1CostFactory();

        IShippingService1FreeShippingFactory ShippingService1FreeShippingFactory();

        IShippingProfileName ShippingProfileName();

        IReturnProfileName ReturnProfileName();

        IPaymentProfileName PaymentProfileName();

        ISalesTaxPercent SalesTaxPercent();

        ISupplierFactory SupplierFactory();
    }
}
