﻿
using EbayCSV.Core.Factories.CategoryFactory;
using EbayCSV.Core.Factories.CustomLabelFactory;
using EbayCSV.Core.Factories.DispatchTimeFactory;
using EbayCSV.Core.Factories.DurationFactory;
using EbayCSV.Core.Factories.FormatFactory;
using EbayCSV.Core.Factories.ImageFactory;
using EbayCSV.Core.Factories.ListingActionFactory;
using EbayCSV.Core.Factories.LocationFactory;
using EbayCSV.Core.Factories.PaypalAcceptFactory;
using EbayCSV.Core.Factories.PaypalEmailFactory;
using EbayCSV.Core.Factories.RelationshipDetailsFactory;
using EbayCSV.Core.Factories.RelationshipFactory;
using EbayCSV.Core.Factories.ReturnsOptionFactory;
using EbayCSV.Core.Factories.SellpriceFactory;
using EbayCSV.Core.Factories.ShippingService1CostFactory;
using EbayCSV.Core.Factories.ShippingService1FreeShippingFactory;
using EbayCSV.Core.Factories.ShippingService1OptionFactory;
using EbayCSV.Core.Factories.StockFactory;
using EbayCSV.Core.Factories.StoreCategoryFactory;
using EbayCSV.Core.Factories.Supplier;
using EbayCSV.Core.Factories.TitleFactory;
using EbayCSV.Core.Factories.TitleFactory.SubTitleFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Factories.PaymentProfileName;
using EbayCSV.Core.Factories.ReturnProfileName;
using EbayCSV.Core.Factories.SalesTaxPercent;
using EbayCSV.Core.Factories.ShippingProfileName;

namespace EbayCSV.Core.Factories
{
    /// <summary>
    /// Processes the interaction for the different attribute factories
    /// </summary>
    public class Factories : IFactories
    {
        public Factories(
            IListingActionFactory listingFactory,
            ICustomLabelFactory customLabelFactory,
            ITitleFactory titleFactory,
            ISubTitleFactory subTitleFactory,
            ICategoryFactory categoryFactory,
            IImageFactory imageFactory,
            ISellpriceFactory sellPriceFactory,
            IPaypalEmailFactory paypalEmailFactory,
            IFormatFactory formatFactory,
            IDurationFactory duractionFactory,
            IPaypalAcceptFactory paypalAcceptFactory,
            ILocationFactory locationFactory,
            IDispatchTimeFactory dispatchTimeFactory,
            IReturnsOptionFactory returnsOptionFactory,
            IVariationRowRelationshipFactory relationshipFactory,
            IRelationshipDetailsFactory relationshipDetailsFactory,
            IStoreCategoryFactory storeCategoryFactory,
            IStockFactory stockFactory,
            IShippingService1OptionFactory shippingService1OptionFactory,
            IShippingService1CostFactory shippingService1CostFactory,
            IShippingService1FreeShippingFactory shippingService1FreeShippingFactory,
            IShippingProfileName shippingProfileName,
            IReturnProfileName returnProfileName,
            IPaymentProfileName paymentProfileName,
            ISalesTaxPercent salesTaxPercent,
            ISupplierFactory supplierFactory
            )
        {
            this.listingFactory = listingFactory;
            this.customLabelFactory = customLabelFactory;
            this.titleFactory = titleFactory;
            this.subTitleFactory = subTitleFactory;
            this.categoryFactory = categoryFactory;
            this.imageFactory = imageFactory;
            this.sellPriceFactory = sellPriceFactory;
            this.paypalEmailFactory = paypalEmailFactory;
            this.formatFactory = formatFactory;
            this.duractionFactory = duractionFactory;
            this.paypalAcceptFactory = paypalAcceptFactory;
            this.locationFactory = locationFactory;
            this.dispatchTimeFactory = dispatchTimeFactory;
            this.returnsOptionFactory = returnsOptionFactory;
            this.relationshipFactory = relationshipFactory;
            this.relationshipDetailsFactory = relationshipDetailsFactory;
            this.storeCategoryFactory = storeCategoryFactory;
            this.stockFactory = stockFactory;
            this.shippingService1OptionFactory = shippingService1OptionFactory;
            this.shippingService1CostFactory = shippingService1CostFactory;
            this.shippingService1FreeShippingFactory = shippingService1FreeShippingFactory;
            this._shippingProfileName = shippingProfileName;
            this._returnProfileName = returnProfileName;
            this._paymentProfileName = paymentProfileName;
            this._salesTaxPercent = salesTaxPercent;
            this.supplierFactory = supplierFactory;
        }

        #region listing factory
        private readonly IListingActionFactory listingFactory;
        public IListingActionFactory ListingActionFactory()
        {
            return listingFactory;
        }
        #endregion

        #region cutom label factory
        private readonly ICustomLabelFactory customLabelFactory;
        public ICustomLabelFactory CustomLabelFactory()
        {
            return customLabelFactory;
        }
        #endregion

        #region title factory
        private readonly ITitleFactory titleFactory;
        public ITitleFactory TitleFactory()
        {
            return titleFactory;
        }
        #endregion

        #region sub title factory
        private readonly ISubTitleFactory subTitleFactory;
        public ISubTitleFactory SubTitleFactory()
        {
            return subTitleFactory;
        }
        #endregion

        #region category factory
        private readonly ICategoryFactory categoryFactory;
        public ICategoryFactory CategoryFactory()
        {
            return categoryFactory;
        }
        #endregion

        #region store category factory
        private readonly IStoreCategoryFactory storeCategoryFactory;
        public IStoreCategoryFactory StoreCategoryFactory()
        {
            return storeCategoryFactory;
        }
        #endregion

        #region store category factory
        private readonly IImageFactory imageFactory;
        public IImageFactory ImageFactory()
        {
            return imageFactory;
        }
        #endregion

        #region sell price factory
        private readonly ISellpriceFactory sellPriceFactory;
        public ISellpriceFactory SellpriceFactory()
        {
            return sellPriceFactory;
        }
        #endregion

        #region paypal email factory
        private readonly IPaypalEmailFactory paypalEmailFactory;
        public IPaypalEmailFactory PaypalEmailFactory()
        {
            return paypalEmailFactory;
        }
        #endregion

        #region format factory
        private readonly IFormatFactory formatFactory;
        public IFormatFactory FormatFactory()
        {
            return formatFactory;
        }
        #endregion

        #region duration factory
        private readonly IDurationFactory duractionFactory;
        public IDurationFactory DuractionFactory()
        {
            return duractionFactory;
        }
        #endregion

        #region paypal accept factory
        private readonly IPaypalAcceptFactory paypalAcceptFactory;
        public IPaypalAcceptFactory PaypalAcceptFactory()
        {
            return paypalAcceptFactory;
        }
        #endregion

        #region location factory
        private readonly ILocationFactory locationFactory;
        public ILocationFactory LocationFactory()
        {
            return locationFactory;
        }
        #endregion

        #region dispatch time factory
        private readonly IDispatchTimeFactory dispatchTimeFactory;
        public IDispatchTimeFactory DispatchTimeFactory()
        {
            return dispatchTimeFactory;
        }
        #endregion

        #region returns option factory
        private readonly IReturnsOptionFactory returnsOptionFactory;
        public IReturnsOptionFactory ReturnsOptionFactory()
        {
            return returnsOptionFactory;
        }
        #endregion

        #region relationship factory
        private readonly IVariationRowRelationshipFactory relationshipFactory;
        public IVariationRowRelationshipFactory RelationshipFactory()
        {
            return relationshipFactory;
        }
        #endregion

        #region relationship details factory
        private readonly IRelationshipDetailsFactory relationshipDetailsFactory;
        public IRelationshipDetailsFactory RelationshipDetailsFactory()
        {
            return relationshipDetailsFactory;
        }
        #endregion

        #region stock factory
        private readonly IStockFactory stockFactory;
        public IStockFactory StockFactory()
        {
            return stockFactory;
        }
        #endregion

        #region shipping service1 option factory
        private readonly IShippingService1OptionFactory shippingService1OptionFactory;
        public IShippingService1OptionFactory ShippingService1OptionFactory()
        {
            return shippingService1OptionFactory;
        }
        #endregion

        #region shipping service1 cost factory
        private readonly IShippingService1CostFactory shippingService1CostFactory;
        public IShippingService1CostFactory ShippingService1CostFactory()
        {
            return shippingService1CostFactory;
        }
        #endregion

        #region shipping service1 free shipping  factory
        private readonly IShippingService1FreeShippingFactory shippingService1FreeShippingFactory;
        public IShippingService1FreeShippingFactory ShippingService1FreeShippingFactory()
        {
            return shippingService1FreeShippingFactory;
        }
        #endregion

        #region supplier factory
        private readonly ISupplierFactory supplierFactory;
        public ISupplierFactory SupplierFactory()
        {
            return supplierFactory;
        }
        #endregion

        #region shipping profile name factory
        private readonly IShippingProfileName _shippingProfileName;
        public IShippingProfileName ShippingProfileName()
        {
            return _shippingProfileName;
        }
        #endregion

        #region return profile name factory
        private readonly IReturnProfileName _returnProfileName;
        public IReturnProfileName ReturnProfileName()
        {
            return _returnProfileName;
        }
        #endregion

        #region payment profile name factory
        private readonly IPaymentProfileName _paymentProfileName;
        public IPaymentProfileName PaymentProfileName()
        {
            return _paymentProfileName;
        }
        #endregion

        #region sales tax percent factory
        private readonly ISalesTaxPercent _salesTaxPercent;
        public ISalesTaxPercent SalesTaxPercent()
        {
            return _salesTaxPercent;
        }
        #endregion
    }
}
