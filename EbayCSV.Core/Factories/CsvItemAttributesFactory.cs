﻿
using EbayCSV.Core.Factories.CategoryFactory;
using EbayCSV.Core.Factories.CustomLabelFactory;
using EbayCSV.Core.Factories.DispatchTimeFactory;
using EbayCSV.Core.Factories.DurationFactory;
using EbayCSV.Core.Factories.FormatFactory;
using EbayCSV.Core.Factories.ImageFactory;
using EbayCSV.Core.Factories.ListingActionFactory;
using EbayCSV.Core.Factories.LocationFactory;
using EbayCSV.Core.Factories.PaypalAcceptFactory;
using EbayCSV.Core.Factories.PaypalEmailFactory;
using EbayCSV.Core.Factories.RelationshipDetailsFactory;
using EbayCSV.Core.Factories.RelationshipFactory;
using EbayCSV.Core.Factories.ReturnsOptionFactory;
using EbayCSV.Core.Factories.SellpriceFactory;
using EbayCSV.Core.Factories.ShippingService1CostFactory;
using EbayCSV.Core.Factories.ShippingService1FreeShippingFactory;
using EbayCSV.Core.Factories.ShippingService1OptionFactory;
using EbayCSV.Core.Factories.StockFactory;
using EbayCSV.Core.Factories.StoreCategoryFactory;
using EbayCSV.Core.Factories.TitleFactory;
using EbayCSV.Core.Factories.TitleFactory.SubTitleFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories
{
    /// <summary>
    /// Processes the interaction for the different attribute factories
    /// </summary>
    public class CsvItemAttributesFactory : ICsvItemAttributesFactory
    {
        private readonly IListingActionFactory listingFactory;
        private readonly ICustomLabelFactory customLabelFactory;
        private readonly ITitleFactory titleFactory;
        private readonly ISubTitleFactory subTitleFactory;
        private readonly ICategoryFactory categoryFactory;
        private readonly IImageFactory imageFactory;
        private readonly ISellpriceFactory sellPriceFactory;
        private readonly IPaypalEmailFactory paypalEmailFactory;
        private readonly IFormatFactory formatFactory;
        private readonly IDurationFactory duractionFactory;
        private readonly IPaypalAcceptFactory paypalAcceptFactory;
        private readonly ILocationFactory locationFactory;
        private readonly IDispatchTimeFactory dispatchTimeFactory;
        private readonly IReturnsOptionFactory returnsOptionFactory;
        private readonly IVariationRowRelationshipFactory relationshipFactory;
        private readonly IRelationshipDetailsFactory relationshipDetailsFactory;
        private readonly IStoreCategoryFactory storeCategoryFactory;
        private readonly IStockFactory stockFactory;
        private readonly IShippingService1OptionFactory shippingService1OptionFactory;
        private readonly IShippingService1CostFactory shippingService1CostFactory;
        private readonly IShippingService1FreeShippingFactory shippingService1FreeShippingFactory;

        public CsvItemAttributesFactory(IListingActionFactory listingFactory,
            ICustomLabelFactory customLabelFactory,
            ITitleFactory titleFactory,
            ISubTitleFactory subTitleFactory,
            ICategoryFactory categoryFactory,
            IImageFactory imageFactory,
            ISellpriceFactory sellPriceFactory,
            IPaypalEmailFactory paypalEmailFactory,
            IFormatFactory formatFactory,
            IDurationFactory duractionFactory,
            IPaypalAcceptFactory paypalAcceptFactory,
            ILocationFactory locationFactory,
            IDispatchTimeFactory dispatchTimeFactory,
            IReturnsOptionFactory returnsOptionFactory,
            IVariationRowRelationshipFactory relationshipFactory,
            IRelationshipDetailsFactory relationshipDetailsFactory,
            IStoreCategoryFactory storeCategoryFactory,
            IStockFactory stockFactory,
            IShippingService1OptionFactory shippingService1OptionFactory,
            IShippingService1CostFactory shippingService1CostFactory,
            IShippingService1FreeShippingFactory shippingService1FreeShippingFactory
            )
        {
            this.listingFactory = listingFactory;
            this.customLabelFactory = customLabelFactory;
            this.titleFactory = titleFactory;
            this.subTitleFactory = subTitleFactory;
            this.categoryFactory = categoryFactory;
            this.imageFactory = imageFactory;
            this.sellPriceFactory = sellPriceFactory;
            this.paypalEmailFactory = paypalEmailFactory;
            this.formatFactory = formatFactory;
            this.duractionFactory = duractionFactory;
            this.paypalAcceptFactory = paypalAcceptFactory;
            this.locationFactory = locationFactory;
            this.dispatchTimeFactory = dispatchTimeFactory;
            this.returnsOptionFactory = returnsOptionFactory;
            this.relationshipFactory = relationshipFactory;
            this.relationshipDetailsFactory = relationshipDetailsFactory;
            this.storeCategoryFactory = storeCategoryFactory;
            this.stockFactory = stockFactory;
            this.shippingService1OptionFactory = shippingService1OptionFactory;
            this.shippingService1CostFactory = shippingService1CostFactory;
            this.shippingService1FreeShippingFactory = shippingService1FreeShippingFactory;
        }

        public IListingActionFactory ListingActionFactory()
        {
            return listingFactory;
        }

        public ICustomLabelFactory CustomLabelFactory()
        {
            return customLabelFactory;
        }

        public ITitleFactory TitleFactory()
        {
            return titleFactory;
        }

        public ISubTitleFactory SubTitleFactory()
        {
            return subTitleFactory;
        }

        public ICategoryFactory CategoryFactory()
        {
            return categoryFactory;
        }

        public IStoreCategoryFactory StoreCategoryFactory()
        {
            return storeCategoryFactory;
        }

        public IImageFactory ImageFactory()
        {
            return imageFactory;
        }

        public ISellpriceFactory SellpriceFactory()
        {
            return sellPriceFactory;
        }

        public IPaypalEmailFactory PaypalEmailFactory()
        {
            return paypalEmailFactory;
        }

        public IFormatFactory FormatFactory()
        {
            return formatFactory;
        }

        public IDurationFactory DuractionFactory()
        {
            return duractionFactory;
        }

        public IPaypalAcceptFactory PaypalAcceptFactory()
        {
            return paypalAcceptFactory;
        }

        public ILocationFactory LocationFactory()
        {
            return locationFactory;
        }

        public IDispatchTimeFactory DispatchTimeFactory()
        {
            return dispatchTimeFactory;
        }

        public IReturnsOptionFactory ReturnsOptionFactory()
        {
            return returnsOptionFactory;
        }

        public IVariationRowRelationshipFactory RelationshipFactory()
        {
            return relationshipFactory;
        }

        public IRelationshipDetailsFactory RelationshipDetailsFactory()
        {
            return relationshipDetailsFactory;
        }

        public IStockFactory StockFactory()
        {
            return stockFactory;
        }


        public IShippingService1OptionFactory ShippingService1OptionFactory()
        {
            return shippingService1OptionFactory;
        }

        public IShippingService1CostFactory ShippingService1CostFactory()
        {
            return shippingService1CostFactory;
        }

        public IShippingService1FreeShippingFactory ShippingService1FreeShippingFactory()
        {
            return shippingService1FreeShippingFactory;
        }
    }
}
