﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories
{
    public interface IFactoryDataSource
    {
        Dictionary<string, string> GetFactoryData();
    }
}
