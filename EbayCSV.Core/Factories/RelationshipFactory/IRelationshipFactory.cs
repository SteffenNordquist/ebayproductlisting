﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.RelationshipFactory
{
    /// <summary>
    /// Relationship - "Variation" in the item row indicates that an item has variation rows
    /// Processes the relationship value of the item row
    /// </summary>
    public interface IRelationshipFactory
    {
        /// <summary>
        /// Processes works on returning the relationship value of the item row
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns relationship value of the item row</returns>
        string GetRelationship<TSource>(TSource tSource);
    }
}
