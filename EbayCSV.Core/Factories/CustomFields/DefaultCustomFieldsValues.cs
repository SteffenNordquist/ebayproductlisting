﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.CustomFields
{
    /// <summary>
    /// Process extra custom fields values
    /// </summary>
    public class DefaultCustomFieldsValues : ICustomFieldsValues
    {
        /// <summary>
        /// A delegate that works on the custom fields headers
        /// </summary>
        private readonly ICustomFieldsHeaders customFieldsHeaders;

        /// <summary>
        /// A container for settings/configuration
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="customFieldsHeaders">A delegate that works on the custom fields headers</param>
        /// <param name="settings">A container for settings/configuration</param>
        public DefaultCustomFieldsValues(ICustomFieldsHeaders customFieldsHeaders, ISettings settings)
        {
            this.customFieldsHeaders = customFieldsHeaders;
            this.settings = settings;
        }

        /// <summary>
        /// Splits the source by the delimiter. 
        /// Uses the custom field headers indices as indicator of the location of the custom fields values.
        /// </summary>
        /// <typeparam name="TSource">Type of the source parameter</typeparam>
        /// <param name="tSource">Line source in a form of a string</param>
        /// <returns>Returns custom fields values</returns>
        public string GetCustomFieldsValuesFormat<TSource>(TSource tSource)
        {
            string itemSource = (string)(object)tSource;

            List<int> customFieldsIndices = customFieldsHeaders.GetHeaders().Select(x => x.Key).ToList();

            List<string> customFieldsValues = new List<string>();

            string settingsDelimiter = settings.GetValue("InputFileDelimiter");
            #region finalizing delimeter
            if (settingsDelimiter.Contains("tab"))
            {
                settingsDelimiter = "\t";
            }
            #endregion

            string[] itemSourceSplits = itemSource.Split(settingsDelimiter.ToCharArray());

            foreach (int index in customFieldsIndices)
            {
                if (itemSourceSplits.Count() > index)
                {
                    customFieldsValues.Add(itemSourceSplits[index]);
                }
            }

            return string.Join("\t", customFieldsValues);
        }
    }
}
