﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.CustomFields
{
    public interface ICustomFieldsHeaders
    {
        Dictionary<int, string> GetHeaders();
    }
}
