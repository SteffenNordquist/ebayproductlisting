﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.CustomFields
{
    /// <summary>
    /// Processes extra custom fields headers
    /// </summary>
    public class BKCustomFieldsHeaders : ICustomFieldsHeaders
    {
        /// <summary>
        /// Returns custom fields headers
        /// </summary>
        /// <returns>Returns custom fields headers</returns>
        public Dictionary<int, string> GetHeaders()
        {
            return new Dictionary<int, string>();
        }
    }
}
