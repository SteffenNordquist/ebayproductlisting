﻿using EbayCSV.Core.RawData;
using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.CustomFields
{
    /// <summary>
    /// Processes extra custom fields headers
    /// </summary>
    public class DefaultCustomFieldsHeaders : ICustomFieldsHeaders
    {
        /// <summary>
        /// A container for settings/configuration
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">A container for settings/configuration</param>
        public DefaultCustomFieldsHeaders(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Splits the header row by the delimeter.
        /// Gets the custom fields headers using the indicator
        /// "C:" indicator of a custom fields
        /// </summary>
        /// <returns>Returns index-value pair of the input file colums</returns>
        public Dictionary<int, string> GetHeaders()
        {
            string delimeter = settings.GetValue("InputFileDelimiter");
            #region finalizing delimeter
            if (delimeter.Contains("tab"))
            {
                delimeter = "\t";
            }
            #endregion

            string filename = settings.GetValue("InputFilePath");

            List<string> headers = File.ReadAllLines(filename, Encoding.GetEncoding("windows-1252"))
                .ToList()
                .First()
                .Split(delimeter.ToCharArray())
                .ToList();

            List<string> cFields = headers.Where(x => x.StartsWith("C:")).ToList().Distinct().ToList();

            Dictionary<int, string> cFieldIndices = cFields.ToDictionary(y => headers.IndexOf(y), x => x);

            return cFieldIndices;
        }
    }
}
