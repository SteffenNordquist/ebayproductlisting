﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.CustomFields
{
    /// <summary>
    /// Process extra custom fields values
    /// </summary>
    public class BKCustomFieldsValues : ICustomFieldsValues
    {
        /// <summary>
        /// Returns custom fields values
        /// </summary>
        /// <typeparam name="TSource">Type of the source parameter</typeparam>
        /// <param name="tSource">Line source in a form of a string</param>
        /// <returns>Returns custom fields values</returns>
        public string GetCustomFieldsValuesFormat<TSource>(TSource tSource)
        {
            return "";
        }
    }
}
