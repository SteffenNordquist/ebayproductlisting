﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.SalesTaxPercent
{
    public class EmptySalesTaxPercent : ISalesTaxPercent
    {
        public int SalesTax<T>(T obj)
        {
            return 0;
        }
    }
}
