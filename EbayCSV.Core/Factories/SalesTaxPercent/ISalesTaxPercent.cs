﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.SalesTaxPercent
{
    /// <summary>
    /// Item's sales tax 
    /// </summary>
    public interface ISalesTaxPercent
    {
        /// <summary>
        /// returns item's sales tax percent in integer
        /// </summary>
        /// <typeparam name="T">param type</typeparam>
        /// <param name="obj">T instance</param>
        /// <returns>sales tax percent</returns>
        int SalesTax<T>(T obj);
    }
}
