﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProcuctDB;

namespace EbayCSV.Core.Factories.SalesTaxPercent
{
    /// <summary>
    /// Item's sales tax 
    /// </summary>
    public class ProductSalesTaxPercent : ISalesTaxPercent
    {
        /// <summary>
        /// returns item's sales tax percent in integer
        /// </summary>
        /// <typeparam name="T">type of IProduct</typeparam>
        /// <param name="obj">IProduct instance</param>
        /// <returns>sales tax percent</returns>
        public int SalesTax<T>(T obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            if (!typeof(IProduct).IsAssignableFrom(obj.GetType()))
            {
                throw new FormatException("'obj' expected type of 'IProduct'");
            }

            IProduct product = (IProduct) (object) obj;

            return product.getTax();
        }
    }
}
