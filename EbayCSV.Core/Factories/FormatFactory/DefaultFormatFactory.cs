﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.FormatFactory
{
    /// <summary>
    /// Selling format of an item. ( ie : FixedPrice, Auction )
    /// Process the format of an item
    /// </summary>
    public class DefaultFormatFactory : IFormatFactory
    {
        /// <summary>
        /// Container of the settings/configuration
        /// </summary>
        private readonly ISettings csvSettings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="csvSettings">Container of the settings/configuration</param>
        public DefaultFormatFactory(ISettings csvSettings)
        {
            this.csvSettings = csvSettings;
        }

        /// <summary>
        /// Processes the returning of the listing format of the item
        /// </summary>
        /// <typeparam name="TSource">Type of the parameter</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns listing format of the item</returns>
        public string GetListingFormat<TSource>(TSource tSource)
        {
            return csvSettings.GetValue("Format");
        }
    }
}
