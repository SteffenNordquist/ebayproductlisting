﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.FormatFactory
{
    /// <summary>
    /// Selling format of an item. ( ie : FixedPrice, Auction )
    /// Process the format of an item
    /// </summary>
    public interface IFormatFactory
    {
        /// <summary>
        /// Processes the returning of the listing format of the item
        /// </summary>
        /// <typeparam name="TSource">Type of the parameter</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns listing format of the item</returns>
        string GetListingFormat<TSource>(TSource tSource);
    }
}
