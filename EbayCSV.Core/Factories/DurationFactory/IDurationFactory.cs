﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.DurationFactory
{
    /// <summary>
    /// Duration is how long be an item active as listing before it will be ended
    /// Processes the duration of an item
    /// </summary>
    public interface IDurationFactory
    {
        /// <summary>
        /// Processes the work of returning the duration value of an item
        /// </summary>
        /// <typeparam name="TSource">Type of the parameter</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns duration value of an item</returns>
        string GetDuration<TSource>(TSource tSource);
    }
}
