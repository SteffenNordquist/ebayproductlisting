﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.DurationFactory
{
    /// <summary>
    /// Duration is how long be an item active as listing before it will be ended
    /// Processes the duration of an item
    /// </summary>
    public class DefaultDurationFactory : IDurationFactory
    {
        /// <summary>
        /// Container of the settings/configuration
        /// </summary>
        private readonly ISettings csvSettings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="csvSettings">Container of the settings/configuration</param>
        public DefaultDurationFactory(ISettings csvSettings)
        {
            this.csvSettings = csvSettings;
        }

        /// <summary>
        /// Delegates the work of returning the duration value of an item
        /// </summary>
        /// <typeparam name="TSource">Type of the parameter</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns duration value of an item</returns>
        public string GetDuration<TSource>(TSource tSource)
        {
            return csvSettings.GetValue("Duration");
        }
    }
}
