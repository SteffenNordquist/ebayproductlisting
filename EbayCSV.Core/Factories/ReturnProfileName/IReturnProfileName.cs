﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ReturnProfileName
{
    /// <summary>
    /// Returns return profile name
    /// </summary>
    public interface IReturnProfileName
    {
        /// <summary>
        /// retrieves and returns return profile name
        /// </summary>
        /// <returns>return profile name</returns>
        string GetReturnProfileName();
    }
}
