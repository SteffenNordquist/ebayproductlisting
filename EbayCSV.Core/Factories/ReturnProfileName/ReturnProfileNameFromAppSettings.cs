﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Settings;

namespace EbayCSV.Core.Factories.ReturnProfileName
{
    /// <summary>
    /// Returns return profile name from app settings
    /// </summary>
    public class ReturnProfileNameFromAppSettings : IReturnProfileName
    {
        /// <summary>
        /// app settings container
        /// </summary>
        private readonly ISettings _settings;

        /// <summary>
        /// consturctor
        /// </summary>
        /// <param name="settings">app settings container</param>
        public ReturnProfileNameFromAppSettings(ISettings settings)
        {
            this._settings = settings;
        }

        /// <summary>
        /// retrieves return profile name from app settings
        /// </summary>
        /// <returns>return profile name</returns>
        public string GetReturnProfileName()
        {
            return _settings.GetValue("returnProfileName");
        }
    }
}
