﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.LocationFactory
{
    /// <summary>
    /// Location - location of an item
    /// Process the location of an item
    /// </summary>
    public class DefaultLocationFactory : ILocationFactory
    {
        /// <summary>
        /// Container of the settings/configuration
        /// </summary>
        private readonly ISettings csvSettings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="csvSettings">Container of the settings/configuration</param>
        public DefaultLocationFactory(ISettings csvSettings)
        {
            this.csvSettings = csvSettings;
        }

        /// <summary>
        /// Processes on returning the location of an item
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns the location of an item</returns>
        public string GetLocation<TSource>(TSource tSource)
        {
            return csvSettings.GetValue("Location");
        }
    }
}
