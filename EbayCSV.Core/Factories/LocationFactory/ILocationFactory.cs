﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.LocationFactory
{
    /// <summary>
    /// Location - location of an item
    /// Process the location of an item
    /// </summary>
    public interface ILocationFactory
    {
        /// <summary>
        /// Processes on returning the location of an item
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns the location of an item</returns>
        string GetLocation<TSource>(TSource tSource);
    }
}
