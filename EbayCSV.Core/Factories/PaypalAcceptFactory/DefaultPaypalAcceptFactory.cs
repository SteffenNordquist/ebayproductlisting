﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.PaypalAcceptFactory
{
    /// <summary>
    /// PaypalAccept indicates if a seller accepts paypal payments
    /// Processes PaypalAccept value
    /// </summary>
    public class DefaultPaypalAcceptFactory : IPaypalAcceptFactory
    {
        /// <summary>
        /// Container of the settings/configuration
        /// </summary>
        private readonly ISettings csvSettings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="csvSettings">Container of the settings/configuration</param>
        public DefaultPaypalAcceptFactory(ISettings csvSettings)
        {
            this.csvSettings = csvSettings;
        }

        /// <summary>
        /// value will be either '0' or '1'
        /// 1 => accept
        /// Processes on returning PaypalAccept value
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns PaypalAccept value</returns>
        public string AcceptPaypal<TSource>(TSource tSource)
        {
            string settingsValue = csvSettings.GetValue("AcceptPaypal");

            try
            {
                Int32.Parse(settingsValue.Trim());
                return settingsValue;
            }
            catch { }

            try
            {
                bool acceptPaypal = Boolean.Parse(settingsValue.Trim());
                if (acceptPaypal)
                {
                    return "1";
                }
            }
            catch { }

            return "0";
        }
    }
}
