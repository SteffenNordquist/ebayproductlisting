﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.PaypalAcceptFactory
{
    /// <summary>
    /// PaypalAccept indicates if a seller accepts paypal payments
    /// Processes PaypalAccept value
    /// </summary>
    public interface IPaypalAcceptFactory
    {
        /// <summary>
        /// value will be either '0' or '1'
        /// 1 => accept
        /// Processes on returning PaypalAccept value
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns PaypalAccept value</returns>
        string AcceptPaypal<TSource>(TSource tSource);
    }
}
