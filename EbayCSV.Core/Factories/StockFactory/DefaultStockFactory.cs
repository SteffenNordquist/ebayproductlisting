﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.StockFactory
{
    /// <summary>
    /// Processes stock value
    /// </summary>
    public class DefaultStockFactory : IStockFactory
    {
        /// <summary>
        /// Container of settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of settings/configurations</param>
        public DefaultStockFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Delegates the work on returning stock value
        /// </summary>
        /// <typeparam name="T">Paremeter type</typeparam>
        /// <param name="obj">Additional parameter placeholder</param>
        /// <returns>Returns stock value</returns>
        public int GetStock<T>(T obj)
        {
            string sourceText = (string)(object)obj;

            string settingsDelimiter = settings.GetValue("InputFileDelimiter");
            #region finalizing delimeter
            if (settingsDelimiter.Contains("tab"))
            {
                settingsDelimiter = "\t";
            }
            #endregion

            int stock = 0;

            try
            {
                stock = Int32.Parse(sourceText.Split(settingsDelimiter.ToCharArray())[9].Trim()); 
            }
            catch { }

            return stock;
        }
    }
}
