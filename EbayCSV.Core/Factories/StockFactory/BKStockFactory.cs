﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProcuctDB;

namespace EbayCSV.Core.Factories.StockFactory
{
    /// <summary>
    /// Processes stock value
    /// </summary>
    public class BKStockFactory : IStockFactory
    {
        /// <summary>
        /// Processes on returning stock value
        /// </summary>
        /// <typeparam name="T">Type expected to be IProductSalesInformation</typeparam>
        /// <param name="obj">IProductSalesInformation object</param>
        /// <returns>Returns stock value</returns>
        public int GetStock<T>(T obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            if (!typeof(IProductSalesInformation).IsAssignableFrom(obj.GetType()))
            {
                throw new FormatException("'obj' expected type of 'IProductSalesInformation'");
            }

            IProductSalesInformation product = (IProductSalesInformation)(object)obj;

            return Math.Min(3, product.getStock());
        }
    }
}
