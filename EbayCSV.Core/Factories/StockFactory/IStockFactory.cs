﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.StockFactory
{
    /// <summary>
    /// Processes stock value
    /// </summary>
    public interface IStockFactory
    {
        /// <summary>
        /// Processes on returning stock value
        /// </summary>
        /// <typeparam name="T">Paremeter type</typeparam>
        /// <param name="tSource">Additional parameter placeholder</param>
        /// <returns>Returns stock value</returns>
        int GetStock<T>(T obj);
    }
}
