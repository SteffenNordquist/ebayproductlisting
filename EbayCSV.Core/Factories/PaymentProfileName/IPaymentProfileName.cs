﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.PaymentProfileName
{
    /// <summary>
    /// Returns payment profile name
    /// </summary>
    public interface IPaymentProfileName
    {
        /// <summary>
        /// retrieves and returns payment profile name
        /// </summary>
        /// <returns>payment profile name</returns>
        string GetPaymentProfileName();
    }
}
