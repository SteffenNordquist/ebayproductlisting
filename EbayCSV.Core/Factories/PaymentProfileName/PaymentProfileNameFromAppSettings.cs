﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Settings;

namespace EbayCSV.Core.Factories.PaymentProfileName
{
    /// <summary>
    /// Returns payment profile name from app settings
    /// </summary>
    public class PaymentProfileNameFromAppSettings : IPaymentProfileName
    {
        /// <summary>
        /// app settings container
        /// </summary>
        private readonly ISettings _settings;

        /// <summary>
        /// consturctor
        /// </summary>
        /// <param name="settings">app settings container</param>
        public PaymentProfileNameFromAppSettings(ISettings settings)
        {
            this._settings = settings;
        }

        /// <summary>
        /// retrieves payment profile name from app settings
        /// </summary>
        /// <returns>payment profile name</returns>
        public string GetPaymentProfileName()
        {
            return _settings.GetValue("paymentProfileName");
        }
    }
}
