﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.CategoryFactory
{
    /// <summary>
    /// Responsible for giving the ebay category by ean
    /// </summary>
    public interface ICategoryFactory 
    {
        /// <summary>
        /// Delegates the work of getting ebay category by ean
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="tSource">Additional property</param>
        /// <param name="ean">13 digit European Article Number</param>
        /// <returns>Returns ebay category for an ean</returns>
        string GetCategory<TSource>(TSource tSource, string ean);
    }
}
