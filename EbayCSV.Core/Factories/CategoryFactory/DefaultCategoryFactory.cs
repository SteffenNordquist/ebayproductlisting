﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.CategoryFactory
{
    /// <summary>
    /// A class responsible for giving the ebay category by ean
    /// </summary>
    public class DefaultCategoryFactory : ICategoryFactory
    {
        /// <summary>
        /// Container of settings/configuration
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of settings/configuration</param>
        public DefaultCategoryFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Delegates the work of getting ebay category by ean
        /// </summary>
        /// <typeparam name="TSource">Type of the parameter</typeparam>
        /// <param name="tSource">A line of string</param>
        /// <param name="ean">13 digit European Article Number</param>
        /// <returns>Returns ebay category for an ean</returns>
        public string GetCategory<TSource>(TSource tSource, string ean)
        {
            string source = (string)(object)tSource;

            string delimiter = settings.GetValue("InputFileDelimiter");
            #region finalizing delimeter
            if (delimiter.Contains("tab"))
            {
                delimiter = "\t";
            }
            #endregion

            string category = "";

            string[] splits = source.Split(delimiter.ToCharArray());

            if (splits.Count() > 1)
            {
                category = splits[1];
            }

            return category;
        }
    }
}
