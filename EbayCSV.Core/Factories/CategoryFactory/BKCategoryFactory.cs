﻿using DN_Classes.Logger;
using EbayCSV.Core.Category;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.CategoryFactory
{
    /// <summary>
    /// A class responsible for giving the ebay category by ean
    /// </summary>
    public class BKCategoryFactory : ICategoryFactory
    {
        /// <summary>
        /// A delegate database allows for retrieving the ebay category by ean
        /// </summary>
        private readonly ICategoriesDatabase categoriesDatabase;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="categoriesDatabase">A delegate database allows for retrieving the ebay category by ean</param>
        public BKCategoryFactory(ICategoriesDatabase categoriesDatabase)
        {
            this.categoriesDatabase = categoriesDatabase;
        }

        /// <summary>
        /// Delegates the work of getting ebay category by ean
        /// </summary>
        /// <typeparam name="TSource"></typeparam>
        /// <param name="tSource">Additional property</param>
        /// <param name="ean">13 digit European Article Number</param>
        /// <returns>Returns ebay category for an ean</returns>
        public string GetCategory<TSource>(TSource tSource, string ean)
        {
            string category = categoriesDatabase.GetCategory(ean);

            if (String.IsNullOrEmpty(category))
            {
                AppLogger.Warn("filtered : " + ean + " : no category");
            }

            return category;
        }
    }
}
