﻿using EbayCSV.Core.Settings;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.CsvItem.Variation;

namespace EbayCSV.Core.Factories.RelationshipDetailsFactory
{
    /// <summary>
    /// RelationshipDetails (item row) contains all trait and values of an item. ( ie : Size=S;M;L;XL|Color=Black;Pink;Red;Blue )
    /// RelationshipDetails (variation row) contains trait and value of the variation. ( Color=Black|Size=XL  )
    /// Processes RelationshipDetails value
    /// </summary>
    public class CBAVariationRelationshipDetailsFactory : IRelationshipDetailsFactory
    {
        /// <summary>
        /// Container of settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of settings/configurations</param>
        public CBAVariationRelationshipDetailsFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Processes the works on returning the relationship details of an item ( item row and variation rows )
        /// </summary>
        /// <typeparam name="TResult">Return type</typeparam>
        /// <typeparam name="TSource">Type parameter</typeparam>
        /// <param name="tSource">Additional parameter placeholder</param>
        /// <returns>Returns relationship details</returns>
        public IEnumerable<TResult> GetRelationshipDetails<TResult, TSource>(TSource tSource)
        {
            string sourceText = (string)(object)tSource;

            List<TraitValue> variationTraits = new List<TraitValue>();

            
            string settingsDelimiter = settings.GetValue("InputFileDelimiter");
            #region finalizing delimeter
            if (settingsDelimiter.Contains("tab"))
            {
                settingsDelimiter = "\t";
            }
            #endregion

            string[] lineColumns = sourceText.Split(settingsDelimiter.ToCharArray());

            string variation1 = "";
            if (lineColumns.Count() > 6) { variation1 = lineColumns[6]; }

            string variation2 = "";
            if (lineColumns.Count() > 7) { variation2 = lineColumns[7]; }

            string variation3 = "";
            if (lineColumns.Count() > 8) { variation3 = lineColumns[8]; }


            if (variation1.Trim() != string.Empty)
            {
                string[] colonSplit = variation1.Split(':');
                if (colonSplit.Count() > 1)
                {
                    variationTraits.Add(new TraitValue(colonSplit[0], colonSplit[1]));
                }
            }
            if (variation2.Trim() != string.Empty)
            {
                string[] colonSplit = variation2.Split(':');
                if (colonSplit.Count() > 1)
                {
                    variationTraits.Add(new TraitValue(colonSplit[0], colonSplit[1]));
                }
            }
            if (variation3.Trim() != string.Empty)
            {
                string[] colonSplit = variation3.Split(':');
                if (colonSplit.Count() > 1)
                {
                    variationTraits.Add(new TraitValue(colonSplit[0], colonSplit[1]));
                }
            }

            return (IEnumerable<TResult>)variationTraits;
        }
    }
}
