﻿using EbayCSV.Core.Settings;
using EbayCSV.Core.Variation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.RelationshipDetailsFactory
{
    public class BasicRelationshipDetailsFactory : IRelationshipDetailsFactory
    {
        private readonly ISettings settings;

        public BasicRelationshipDetailsFactory(ISettings settings)
        {
            this.settings = settings;
        }

        public IEnumerable<TResult> GetRelationshipDetails<TResult, TSource>(TSource tSource)
        {
            string source = (string)(object)tSource;

            List<VariationTrait> variationTraits = new List<VariationTrait>();


            string settingsDelimiter = settings.GetValue("InputFileDelimiter");

            if (settingsDelimiter.Contains("tab"))
            {
                settingsDelimiter = "\t";
            }

            string variation1 = source.Split(settingsDelimiter.ToCharArray())[6];
            string variation2 = source.Split(settingsDelimiter.ToCharArray())[7];
            string variation3 = source.Split(settingsDelimiter.ToCharArray())[8];

            if (variation1.Trim() != string.Empty)
            {
                variationTraits.Add(new VariationTrait { trait = variation1.Split(':')[0], value = variation1.Split(':')[1] });
            }
            if (variation2.Trim() != string.Empty)
            {
                variationTraits.Add(new VariationTrait { trait = variation2.Split(':')[0], value = variation2.Split(':')[1] });
            }
            if (variation3.Trim() != string.Empty)
            {
                variationTraits.Add(new VariationTrait { trait = variation3.Split(':')[0], value = variation3.Split(':')[1] });
            }

            return (IEnumerable<TResult>)variationTraits;
        }
    }
}
