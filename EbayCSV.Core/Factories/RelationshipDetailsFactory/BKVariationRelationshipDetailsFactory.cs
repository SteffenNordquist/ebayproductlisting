﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.CsvItem.Variation;

namespace EbayCSV.Core.Factories.RelationshipDetailsFactory
{
    /// <summary>
    /// RelationshipDetails (item row) contains all trait and values of an item. ( ie : Size=S;M;L;XL|Color=Black;Pink;Red;Blue )
    /// RelationshipDetails (variation row) contains trait and value of the variation. ( Color=Black|Size=XL  )
    /// Processes RelationshipDetails value
    /// </summary>
    public class BKVariationRelationshipDetailsFactory : IRelationshipDetailsFactory
    {
        /// <summary>
        /// Processes the works on returning the relationship details of an item ( item row and variation rows )
        /// </summary>
        /// <typeparam name="TResult">Return type</typeparam>
        /// <typeparam name="TSource">Type parameter</typeparam>
        /// <param name="tSource">Additional parameter placeholder</param>
        /// <returns>Returns relationship details</returns>
        public IEnumerable<TResult> GetRelationshipDetails<TResult, TSource>(TSource tSource)
        {
            List<TraitValue> variationTraits = new List<TraitValue>();

            return (IEnumerable<TResult>)variationTraits;
        }
    }
}
