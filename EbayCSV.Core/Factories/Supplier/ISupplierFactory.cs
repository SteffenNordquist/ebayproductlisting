﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProcuctDB;

namespace EbayCSV.Core.Factories.Supplier
{
    public interface ISupplierFactory
    {
        string GetSupplierName(IProductSalesInformation product);
    }
}
