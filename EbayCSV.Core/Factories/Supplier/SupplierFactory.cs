﻿using ProcuctDB.JTL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.Supplier
{
    public class SupplierFactory : ISupplierFactory
    {
        public string GetSupplierName(ProcuctDB.IProductSalesInformation product)
        {
            string supplierName = product.getSupplierName();

            if (product.GetType().Name.ToLower().StartsWith("jtl"))
            {
                JTLEntity bkJtlProduct = (JTLEntity)product;
                supplierName = bkJtlProduct.product.supplier;
            }

            return supplierName;

        }
    }
}
