﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ImageFactory
{
    /// <summary>
    /// Processes the work on returning the list of image urls
    /// </summary>
    public interface IImageFactory
    {
        /// <summary>
        /// Processes on returning the image urls
        /// </summary>
        /// <typeparam name="T">Parameter type</typeparam>
        /// <param name="obj">Parameter value</param>
        /// <returns>Returns image urls</returns>
        IEnumerable<string> GetPicUrls<T>(T obj);
    }
}
