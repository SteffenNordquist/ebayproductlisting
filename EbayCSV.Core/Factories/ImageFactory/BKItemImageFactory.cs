﻿
using DN_Classes.Logger;
using EbayCSV.Core.Settings;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ImageFactory
{
    /// <summary>
    /// Processes the work on returning the list of image urls
    /// </summary>
    public class BkItemImageFactory : IImageFactory
    {
        /// <summary>
        /// Container of settings/configuration
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of settings/configuration</param>
        public BkItemImageFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Processes on returning the image urls
        /// </summary>
        /// <typeparam name="T">type of IProductSalesInformation</typeparam>
        /// <param name="obj">An instance of IProductSalesInformation</param>
        /// <returns>Returns image urls</returns>
        public IEnumerable<string> GetPicUrls<T>(T obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("'obj' must not be null");
            }

            if (!typeof(IProductSalesInformation).IsAssignableFrom(obj.GetType()))
            {
                throw new FormatException("'obj' expected type of 'IProductSalesInformation'");
            }

            IProductSalesInformation rawItem = (IProductSalesInformation)(object)obj;

            string imageForwarderLink = settings.GetValue("imageforwarderlink").Replace("Home", "");

            List<string> imagelinks = new List<string>();
            rawItem.GetMainImage().ForEach(x => imagelinks.Add(String.Format("{0}/images/{1}", imageForwarderLink, x)));
            if (imagelinks.Any(x => x.Contains("_1")))
            {
                imagelinks = imagelinks.Where(x => x.Contains("_1")).ToList();
            }

            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;


            if (imagelinks.Any())
            {
                //string link = String.Format("{0}/images/{1}", imageForwarderLink, imagelinks.First());

                try
                {
                    var htmlString = webClient.DownloadString(imagelinks.First());
                }
                catch(Exception ex)
                {
                    AppLogger.Error("Image error : " + imagelinks.First() + "\n" + ex.Message);
                    imagelinks.Clear();
                }
            }
            else
            {
                AppLogger.Warn("filtered : " + rawItem.GetEan() + " : no images");
            }
            

            return imagelinks;
        }
    }
}
