﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ImageFactory
{
    /// <summary>
    /// Processes the work on returning the list of variation image urls
    /// </summary>
    public interface IVariationImageFactory
    {
        /// <summary>
        /// Processes on returning the variation image urls
        /// </summary>
        /// <typeparam name="TResult">Return type</typeparam>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <typeparam name="TVariation">Paramter type</typeparam>
        /// <param name="tSource">Raw item source value</param>
        /// <param name="tVariation">Variation trait-value pair</param>
        /// <returns>Returns variation image urls</returns>
        IEnumerable<TResult> GetImage<TResult, TSource, TVariation>(TSource tSource, TVariation tVariation);
    }
}
