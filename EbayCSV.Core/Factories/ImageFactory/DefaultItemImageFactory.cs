﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ImageFactory
{
    /// <summary>
    /// Processes the work on returning the list of image urls
    /// </summary>
    public class DefaultItemImageFactory : IImageFactory
    {
        /// <summary>
        /// Container of settings/configuration
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of settings/configuration</param>
        public DefaultItemImageFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Processes on returning the image urls
        /// </summary>
        /// <typeparam name="T">Parameter type</typeparam>
        /// <param name="obj">Parameter value</param>
        /// <returns>Returns image urls</returns>
        public IEnumerable<string> GetPicUrls<T>(T obj)
        {
            string sourceText = (string)(object)obj;

            string settingsDelimiter = settings.GetValue("InputFileDelimiter");
            #region finalizing delimeter
            if (settingsDelimiter.Contains("tab"))
            {
                settingsDelimiter = "\t";
            }
            #endregion

            List<string> images = new List<string>();

            string[] columnSplit = sourceText.Split(settingsDelimiter.ToCharArray());

            if (columnSplit.Count() > 11)
            {
                images.Add(columnSplit[11]);
            }
            else
            {
                images.Add("");
            }

            return images;
        }
    }
}
