﻿using EbayCSV.Core.RawData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ImageFactory
{
    public class GallayImageFactory : IImageFactory
    {

        public IEnumerable<TResult> GetImage<TResult, TSource>(TSource tSource)
        {
            GallayEntity gallay = (GallayEntity)(object)tSource;

            List<string> images = new List<string>();

            images.Add(gallay.product.pic1url);

            return (IEnumerable<TResult>)images;
        }
    }
}
