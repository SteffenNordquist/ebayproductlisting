﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.CsvItem.Variation;

namespace EbayCSV.Core.Factories.ImageFactory
{
    /// <summary>
    /// Processes the work on returning the list of variation image urls
    /// </summary>
    public class DefaultVariationImageFactory : IVariationImageFactory
    {
        /// <summary>
        /// Container of settings/configuration
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of settings/configuration</param>
        public DefaultVariationImageFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Processes on returning the variation image urls
        /// </summary>
        /// <typeparam name="TResult">Return type</typeparam>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <typeparam name="TVariation">Paramter type</typeparam>
        /// <param name="tSource">Raw item source value</param>
        /// <param name="tVariation">Variation trait-value pair</param>
        /// <returns>Returns variation image urls</returns>
        public IEnumerable<TResult> GetImage<TResult, TSource, TVariation>(TSource tSource, TVariation tVariation)
        {
            string sourceText = (string)(object)tSource;
            TraitValue variationTrait = (TraitValue)(object)tVariation;
            
            string settingsDelimiter = settings.GetValue("InputFileDelimiter");
            #region finalizing delimeter
            if (settingsDelimiter.Contains("tab"))
            {
                settingsDelimiter = "\t";
            }
            #endregion

            List<string> images = new List<string>();

            string[] columnSplit = sourceText.Split(settingsDelimiter.ToCharArray());

            if (columnSplit.Count() > 11 && variationTrait.Trait != "")
            {
                images.Add(variationTrait.Value + "=" + columnSplit[11]);
            }
            else
            {
                images.Add("");
            }

            return (IEnumerable<TResult>)images;
        }
    }
}
