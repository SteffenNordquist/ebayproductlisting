﻿//using EbayCSV.Core.Factories.BrandFactory;
using EbayCSV.Core.Factories.CategoryFactory;
using EbayCSV.Core.Factories.CustomLabelFactory;
using EbayCSV.Core.Factories.ImageFactory;
using EbayCSV.Core.Factories.ListingActionFactory;
using EbayCSV.Core.Factories.PaypalEmailFactory;
using EbayCSV.Core.Factories.SellpriceFactory;
using EbayCSV.Core.Factories.TitleFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories
{
    public class DefaultCsvFieldsFactory : ICsvFieldsFactory
    {
        private readonly IListingActionFactory listingFactory;
        private readonly ICustomLabelFactory customLabelFactory;
        private readonly ITitleFactory titleFactory;
        private readonly ICategoryFactory categoryFactory;
        private readonly IImageFactory imageFactory;
        private readonly ISellpriceFactory sellPriceFactory;
        private readonly IPaypalEmailFactory paypalEmailFactory;
        //private readonly IBrandFactory brandFactory;

        public DefaultCsvFieldsFactory(IListingActionFactory listingFactory,
            ICustomLabelFactory customLabelFactory,
            ITitleFactory titleFactory,
            ICategoryFactory categoryFactory,
            IImageFactory imageFactory,
            ISellpriceFactory sellPriceFactory,
            IPaypalEmailFactory paypalEmailFactory
            //IBrandFactory brandFactory
            )
        {
            this.listingFactory = listingFactory;
            this.customLabelFactory = customLabelFactory;
            this.titleFactory = titleFactory;
            this.categoryFactory = categoryFactory;
            this.imageFactory = imageFactory;
            this.sellPriceFactory = sellPriceFactory;
            this.paypalEmailFactory = paypalEmailFactory;
            //this.brandFactory = brandFactory;
        }

        public IListingActionFactory ListingActionFactory()
        {
            return listingFactory;
        }

        public ICustomLabelFactory CustomLabelFactory()
        {
            return customLabelFactory;
        }

        public ITitleFactory TitleFactory()
        {
            return titleFactory;
        }

        public ICategoryFactory CategoryFactory()
        {
            return categoryFactory;
        }

        public IImageFactory ImageFactory()
        {
            return imageFactory;
        }

        public ISellpriceFactory SellpriceFactory()
        {
            return sellPriceFactory;
        }

        public IPaypalEmailFactory PaypalEmailFactory()
        {
            return paypalEmailFactory;
        }

        //public IBrandFactory BrandFactory()
        //{
        //    return brandFactory;
        //}
    }
}
