﻿using EbayCSV.Core.ListingAction;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.CustomLabelFactory
{
    /// <summary>
    /// The Custom Label field is an internal view-only field within Selling Manager Pro, 
    /// Turbo Lister and File Exchange used by sellers to add a special text field to assign "descriptors" to the items they list. 
    /// These descriptors, commonly known as Custom SKUs (stock keeping units), 
    /// can be any text that makes it easier for you to manage your inventory.
    /// They're only visible to you as a seller, 
    /// and don't affect the eBay-generated item numbers that buyers see when they bid on or buy your items.
    /// 
    /// Processes custom label to use by an ean and action type
    /// </summary>
    public interface ICustomLabelFactory
    {
        /// <summary>
        /// Returns custom label
        /// </summary>
        /// <param name="actionType">Listing action type</param>
        /// <param name="ean">13 digit European Article Number</param>
        /// <returns>Returns custom label</returns>
        string GetCustomLabel(ActionType actionType, string ean, string supplierName);
    }
}
