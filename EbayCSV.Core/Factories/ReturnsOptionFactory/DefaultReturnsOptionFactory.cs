﻿using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ReturnsOptionFactory
{
    /// <summary>
    /// ReturnsAcceptedOption - indicates if a seller accepts returns ( ie : ReturnsAccepted/ReturnsNotAccepted )
    /// Processes ReturnsAcceptedOption value
    /// </summary>
    public class DefaultReturnsOptionFactory : IReturnsOptionFactory
    {
        /// <summary>
        /// Container of settings/configurations
        /// </summary>
        private readonly ISettings csvSettings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="csvSettings">Container of settings/configurations</param>
        public DefaultReturnsOptionFactory(ISettings csvSettings)
        {
            this.csvSettings = csvSettings;
        }

        /// <summary>
        /// Delegates the works on returning the ReturnsAcceptedOption value
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns ReturnsAcceptedOption</returns>
        public string GetReturnsOption<TSource>(TSource tSource)
        {
            return csvSettings.GetValue("ReturnsAcceptedOption");
        }
    }
}
