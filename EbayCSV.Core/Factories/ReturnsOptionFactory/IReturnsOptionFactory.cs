﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ReturnsOptionFactory
{
    /// <summary>
    /// ReturnsAcceptedOption - indicates if a seller accepts returns ( ie : ReturnsAccepted/ReturnsNotAccepted )
    /// Processes ReturnsAcceptedOption value
    /// </summary>
    public interface IReturnsOptionFactory
    {
        /// <summary>
        /// Works on returning the ReturnsAcceptedOption value
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Parameter value</param>
        /// <returns>Returns ReturnsAcceptedOption</returns>
        string GetReturnsOption<TSource>(TSource tSource);
    }
}
