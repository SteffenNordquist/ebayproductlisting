﻿using EbayCSV.Core.Filter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.TemplateFactory
{
    /// <summary>
    /// Processes the work on returning the template type 
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Factories.TemplateFactory.BkTemplateFactory"/>
    public interface ITemplateFactory
    {
        /// <summary>
        /// Processes on returning the template type
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="identifier">Additional parameter placeholder</param>
        /// <returns>Returns the template type</returns>
        TemplateType GetMapper<TSource>(TSource identifier);
    }
}
