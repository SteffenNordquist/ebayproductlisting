﻿using EbayCSV.Core.Filter;
using EbayCSV.Core.Settings;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.TemplateFactory
{
    /// <summary>
    /// Processes the work on returning the template type 
    /// </summary>
    public class BkTemplateFactory : ITemplateFactory
    {
        /// <summary>
        /// Container of the settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="csvSettings">Container of the settings/configurations</param>
        public BkTemplateFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Processes on returning the template type
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="identifier">Additional parameter placeholder</param>
        /// <returns>Returns the template type</returns>
        /// <seealso cref="EbayCSV.Core.Factories.TemplateFactory.ITemplateFactory.GetMapper"/>
        public TemplateType GetMapper<TSource>(TSource identifier)
        {
            string sourceIdentifier = (string)(object)identifier;

            List<string> catalogSuppliers = settings.GetValue("catalogSuppliers").Split('|').ToList();

            if (catalogSuppliers.Any(x=>x.ToLower().Contains(sourceIdentifier.ToLower())))
            {
                return TemplateType.catalog;
            }
            else
            {
                return TemplateType.basic;
            }
        }
    }
}
