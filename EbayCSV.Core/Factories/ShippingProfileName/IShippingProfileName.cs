﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ShippingProfileName
{
    /// <summary>
    /// Returns shipping profile name
    /// </summary>
    public interface IShippingProfileName
    {
        /// <summary>
        /// retrieves and returns shipping profile name
        /// </summary>
        /// <returns>shipping profile name</returns>
        string GetShippingProfileName();
    }
}
