﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ShippingProfileName
{
    public class EmptyShippingProfileName : IShippingProfileName
    {
        public string GetShippingProfileName()
        {
            return "";
        }
    }
}
