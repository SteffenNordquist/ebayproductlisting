﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Settings;

namespace EbayCSV.Core.Factories.ShippingProfileName
{
    /// <summary>
    /// Returns shipping profile name from app settings
    /// </summary>
    public class ShippingProfileNameFromAppSettings : IShippingProfileName
    {
        /// <summary>
        /// app settings container
        /// </summary>
        private readonly ISettings _settings;

        /// <summary>
        /// consturctor
        /// </summary>
        /// <param name="settings">app settings container</param>
        public ShippingProfileNameFromAppSettings(ISettings settings)
        {
            this._settings = settings;
        }

        /// <summary>
        /// retrieves shipping profile name from app settings
        /// </summary>
        /// <returns>shipping profile name</returns>
        public string GetShippingProfileName()
        {
            return _settings.GetValue("shippingProfileName");
        }
    }
}
