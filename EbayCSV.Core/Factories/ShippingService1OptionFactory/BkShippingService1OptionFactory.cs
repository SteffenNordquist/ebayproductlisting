﻿using EbayCSV.Core.Inventory;
using EbayCSV.Core.Settings;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ShippingService1OptionFactory
{
    /// <summary>
    /// ShippingService-1:Option - A domestic shipping service that can be selected by the buyer.
    /// Processes the shipping service option value
    /// </summary>
    public class BkShippingService1OptionFactory : IShippingService1OptionFactory
    {
        /// <summary>
        /// Processes the work on returning the shipping service option
        /// </summary>
        /// <typeparam name="TSource">Type parameter</typeparam>
        /// <param name="tSource">Additional parameter placeholder</param>
        /// <returns>Returns shipping service option</returns>
        public string GetShippingService1Option<TSource>(TSource tSource)
        {
            IProductSalesInformation product = (IProductSalesInformation)(object)tSource;

            string ean = product.GetEan();

            string shippingServiceOption = "DE_DeutschePostBrief";

            var inventoryItem = InventoryStorage.InventoryItems().Find(x => x.Ean == ean);
            if (inventoryItem != null)
            {
                double shippingCosts = inventoryItem.plus["shippingCosts"].AsDouble;

                if (shippingCosts > 2.40)
                {
                    shippingServiceOption = "DE_DHLPaket";
                }
            }

            return shippingServiceOption;
        }
    }
}
