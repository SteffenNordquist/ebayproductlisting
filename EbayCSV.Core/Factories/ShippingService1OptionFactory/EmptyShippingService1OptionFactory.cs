﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ShippingService1OptionFactory
{
    /// <summary>
    /// Returns an empty value as shipping service option 1
    /// </summary>
    public class EmptyShippingService1OptionFactory : IShippingService1OptionFactory
    {
        /// <summary>
        /// Returns an empty value as shipping service option 1
        /// </summary>
        public string GetShippingService1Option<TSource>(TSource tSource)
        {
            return string.Empty;
        }
    }
}
