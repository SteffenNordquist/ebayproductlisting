﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.ShippingService1OptionFactory
{
    /// <summary>
    /// ShippingService-1:Option - A domestic shipping service that can be selected by the buyer.
    /// Processes the shipping service option value
    /// </summary>
    public interface IShippingService1OptionFactory
    {
        /// <summary>
        /// Processes the work on returning the shipping service option
        /// </summary>
        /// <typeparam name="TSource">Type parameter</typeparam>
        /// <param name="tSource">Additional parameter placeholder</param>
        /// <returns>Returns shipping service option</returns>
        string GetShippingService1Option<TSource>(TSource tSource);
    }
}
