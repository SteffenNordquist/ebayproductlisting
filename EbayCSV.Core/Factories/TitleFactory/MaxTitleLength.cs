﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.TitleFactory
{
    /// <summary>
    /// Processes the work on returning the maximum title length
    /// </summary>
    public class MaxTitleLength : IMaxTitleLength
    {
        /// <summary>
        /// Processes on returning the maximum title length
        /// Hardcoded value
        /// </summary>
        /// <returns>Returns the maximum title length</returns>
        /// <seealso cref="EbayCSV.Core.Factories.TitleFactory.IMaxTitleLength.GetMaxTitleLength"/>
        public int GetMaxTitleLength()
        {
            return 75;
        }
    }
}
