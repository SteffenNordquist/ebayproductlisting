﻿using EbayCSV.Core.Decorators;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.TitleFactory
{
    /// <summary>
    /// Processes the work on returning the product title
    /// </summary>
    public class BKTitleFactory : ITitleFactory
    {
        /// <summary>
        /// Container of the maximum title length
        /// </summary>
        private readonly IMaxTitleLength maxTitleLength;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="maxTitleLength">Container of the maximum title length</param>
        public BKTitleFactory(IMaxTitleLength maxTitleLength)
        {
            this.maxTitleLength = maxTitleLength;
        }

        /// <summary>
        /// Processes on returning the product title
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as IProductSalesInformation</param>
        /// <returns>Returns the product title</returns>
        /// <seealso cref="EbayCSV.Core.Factories.TitleFactory.ITitleFactory.GetTitle"/>
        public string GetTitle<TSource>(TSource tSource)
        {
            IProductSalesInformation rawproduct = (IProductSalesInformation)(object)tSource;
            
            string rawTitle = rawproduct.getTitle()[0].Replace("*", "").Replace("\"", "").Replace("(", "'").Replace(")", "'").Replace("{", "'").Replace("}", "'").Replace("“", "'").Replace("„", "'");
            string ean = rawproduct.GetEan();

            ITitleDecorator titleDecorator = new TitleLengthDecorator(maxTitleLength);

            string title = titleDecorator.GetDecoratedItem<string, string>(rawTitle);

            if (title.Length + ean.Length + 1 <= maxTitleLength.GetMaxTitleLength())
            {
                title = title + " " + ean;
            }

            return title;
        }
    }
}
