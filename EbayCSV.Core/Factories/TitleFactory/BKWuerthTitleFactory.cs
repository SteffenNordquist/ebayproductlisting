﻿using EbayCSV.Core.Decorators;
using EbayCSV.Core.Factories.SellpriceFactory;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes;
using ProcuctDB.JTL;
namespace EbayCSV.Core.Factories.TitleFactory
{
    /// <summary>
    /// Processes the work on returning the product title
    /// </summary>
    public class BKWuerthTitleFactory : ITitleFactory
    {
        /// <summary>
        /// Container of the maximum title length
        /// </summary>
        private readonly IMaxTitleLength maxTitleLength;

        /// <summary>
        /// A delegate returning sellprice
        /// </summary>
        private readonly ISellpriceFactory sellPriceFactory;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="maxTitleLength">Container of the maximum title length</param>
        /// <param name="sellPriceFactory">A delegate returning sellprice</param>
        public BKWuerthTitleFactory(IMaxTitleLength maxTitleLength, ISellpriceFactory sellPriceFactory)
        {
            this.maxTitleLength = maxTitleLength;
            this.sellPriceFactory = sellPriceFactory;
        }

        /// <summary>
        /// Processes on returning the product title
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as IProductSalesInformation</param>
        /// <returns>Returns the product title</returns>
        /// <seealso cref="EbayCSV.Core.Factories.TitleFactory.ITitleFactory.GetTitle"/>
        public string GetTitle<TSource>(TSource tSource)
        {
            IProductSalesInformation rawproduct = (IProductSalesInformation)(object)tSource;
            var contentWeight = rawproduct.GetContentWeight();

            string ean = rawproduct.GetEan();

            string rawTitle = rawproduct.getTitle()[0].Replace("*", "").Replace("\"", "").Replace("(", "'").Replace(")", "'").Replace("{", "'").Replace("}", "'").Replace("“", "'").Replace("„", "'");

            string listingDbRecordSupplierName = rawproduct.getSupplierName();
            if (rawproduct.GetType().Name.ToLower().StartsWith("jtl"))
            {
                JTLEntity bkJtlProduct = (JTLEntity)rawproduct;
                listingDbRecordSupplierName = bkJtlProduct.product.supplier;
            }
            double initialSellPrice = sellPriceFactory.GetSellprice<string>(ean, listingDbRecordSupplierName);

            //getting formatted title with price calculation
            rawTitle = getGroundPrice((int)contentWeight.amount, contentWeight.unit, initialSellPrice) + " Würth " + rawTitle;

            ITitleDecorator titleDecorator = new TitleLengthDecorator(maxTitleLength);

            string title = titleDecorator.GetDecoratedItem<string, string>(rawTitle);

            if (title.Length + ean.Length + 1 <= maxTitleLength.GetMaxTitleLength())
            {
                title = title + " " + ean;
            }

            return title;
        }

        /// <summary>
        /// Processes the work returning the price per unit of the item
        /// </summary>
        /// <param name="itemCapacity">capacity value</param>
        /// <param name="unit">unit of measurement</param>
        /// <param name="sellprice">sellprice value</param>
        /// <returns>returns the price per unit of the item</returns>
        private string getGroundPrice(int itemCapacity, string unit, double sellprice)
        {
            if (unit == "g")
            {
                if (itemCapacity > 250)
                {
                    return "[" + Math.Round((sellprice / itemCapacity * 1000), 2).ToString(CultureInfo.CreateSpecificCulture("de-DE")) + "&#8364;/kg]";
                }
                else
                {
                    return "[" + Math.Round((sellprice / itemCapacity * 100), 2).ToString(CultureInfo.CreateSpecificCulture("de-DE")) + "&#8364;/100g]";
                }
            }
            else if (unit == "ml")
            {
                if (itemCapacity > 250)
                {
                    return "[" + Math.Round((sellprice / itemCapacity * 1000), 2).ToString(CultureInfo.CreateSpecificCulture("de-DE")) + "&#8364;/l]";
                }
                else
                {
                    return "[" + Math.Round((sellprice / itemCapacity * 100), 2).ToString(CultureInfo.CreateSpecificCulture("de-DE")) + "&#8364;/100ml]";
                }
            }

            return "";
        }
    }
}
