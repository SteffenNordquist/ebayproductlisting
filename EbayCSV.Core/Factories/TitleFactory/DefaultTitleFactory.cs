﻿using EbayCSV.Core.Decorators;
using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.TitleFactory
{
    /// <summary>
    /// Processes the work on returning the product title
    /// </summary>
    public class DefaultTitleFactory : ITitleFactory
    {
        /// <summary>
        /// Container of the settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// A delegate finalizing the title
        /// </summary>
        private readonly ITitleDecorator titleDecorator;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of the settings/configurations</param>
        /// <param name="titleDecorator">A delegate finalizing the title</param>
        public DefaultTitleFactory(ISettings settings, ITitleDecorator titleDecorator)
        {
            this.settings = settings;
            this.titleDecorator = titleDecorator;
        }

        /// <summary>
        /// Processes on returning the product title
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as IProductSalesInformation</param>
        /// <returns>Returns the product title</returns>
        /// <seealso cref="EbayCSV.Core.Factories.TitleFactory.ITitleFactory.GetTitle"/>
        public string GetTitle<TSource>(TSource tSource)
        {
            string sourceText = (string)(object)tSource;

            string delimiter = settings.GetValue("InputFileDelimiter");
            #region finalizing delimeter
            if (delimiter.Contains("tab"))
            {
                delimiter = "\t";
            }
            #endregion

            string[] splits = sourceText.Split(delimiter.ToCharArray());

            string title = "";

            if (splits.Count() > 3) { title = splits[3]; }

            //remvoving 'quote' at the beginning
            if (title.Trim().Length > 0 && title.Trim().ElementAt(0) == '\"')
            {
                title = title.Trim().Remove(0, 1);
            }
            //removing 'quote' at the end
            if (title.Trim().Length > 0 && title.Trim().ElementAt(title.IndexOf(title.Last())) == '\"')
            {
                title = title.Trim().Remove(title.Length - 1, 1);
            }

            title = titleDecorator.GetDecoratedItem<string, string>(title);

            return title;
        }

        
    }
}
