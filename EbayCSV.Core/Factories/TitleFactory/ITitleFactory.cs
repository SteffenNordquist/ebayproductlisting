﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.TitleFactory
{
    /// <summary>
    /// Processes the work on returning the product title
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Factories.TitleFactory.BKTitleFactory"/>
    /// <seealso cref="EbayCSV.Core.Factories.TitleFactory.BKTitleWuerthFactory"/>
    /// <seealso cref="EbayCSV.Core.Factories.TitleFactory.DefaultTitleFactory"/>
    public interface ITitleFactory
    {
        /// <summary>
        /// Processes on returning the product title
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Addtional property placeholder</param>
        /// <returns>Returns the product title</returns>
        string GetTitle<TSource>(TSource tSource);
    }
}
