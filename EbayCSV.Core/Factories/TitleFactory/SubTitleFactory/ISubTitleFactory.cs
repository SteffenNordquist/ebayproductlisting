﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.TitleFactory.SubTitleFactory
{
    /// <summary>
    /// Processes the work on returning the subtitle of the item
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Factories.TitleFactory.SubTitleFactory.BKSubTitleFactory"/>
    /// <seealso cref="EbayCSV.Core.Factories.TitleFactory.SubTitleFactory.DefaultSubTitleFactory"/>
    public interface ISubTitleFactory
    {
        /// <summary>
        /// Processes on returning the subtitle of the item
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Additional property placeholder</param>
        /// <returns>Returns the subtitle of the item</returns>
        string GetSubTitle<TSource>(TSource tSource);
    }
}
