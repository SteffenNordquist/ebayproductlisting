﻿using EbayCSV.Core.Decorators;
using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.TitleFactory.SubTitleFactory
{
    /// <summary>
    /// Processes the work on returning the subtitle of the item
    /// </summary>
    public class DefaultSubTitleFactory : ISubTitleFactory
    {
        /// <summary>
        /// Container of the settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// A delegate for finalized title
        /// </summary>
        private readonly ITitleDecorator titleDecorator;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container of the settings/configurations</param>
        /// <param name="titleDecorator">A delegate for finalized title</param>
        public DefaultSubTitleFactory(ISettings settings, ITitleDecorator titleDecorator)
        {
            this.settings = settings;
            this.titleDecorator = titleDecorator;
        }

        /// <summary>
        /// Processes on returning the subtitle of the item
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Additional property placeholder</param>
        /// <returns>Returns the subtitle of the item</returns>
        /// <seealso cref="EbayCSV.Core.Factories.TitleFactory.SubTitleFactory.ISubTitleFactory.GetSubTitle"/>
        public string GetSubTitle<TSource>(TSource tSource)
        {
            string sourceText = (string)(object)tSource;

            string delimiter = settings.GetValue("InputFileDelimiter");

            #region finalizing delimeter
            if (delimiter.Contains("tab"))
            {
                delimiter = "\t";
            }
            #endregion

            string[] splits = sourceText.Split(delimiter.ToCharArray());

            string title = "";
            if (splits.Count() > 4) { title = splits[4]; }

            //remvoving 'quote' at the beginning
            if (title.Trim().Length > 0 && title.Trim().ElementAt(0) == '\"')
            {
                title = title.Trim().Remove(0, 1);
            }
            //removing 'quote' at the end
            if (title.Trim().Length > 0 && title.Trim().ElementAt(title.IndexOf(title.Last())) == '\"')
            {
                title = title.Trim().Remove(title.Length - 1, 1);
            }

            title = titleDecorator.GetDecoratedItem<string, string>(title);

            return title;
        }

    }
}
