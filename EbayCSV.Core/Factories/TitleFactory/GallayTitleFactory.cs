﻿using EbayCSV.Core.RawData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.TitleFactory
{
    public class GallayTitleFactory : ITitleFactory
    {
        public string GetTitle<TSource>(TSource tSource)
        {
            GallayEntity gallay = (GallayEntity)(object)tSource;

            return gallay.product.title;
        }
    }
}
