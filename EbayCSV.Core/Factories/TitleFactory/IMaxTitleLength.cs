﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Factories.TitleFactory
{
    /// <summary>
    /// Processes the work on returning the maximum title length
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Factories.TitleFactory.MaxTitleLength"/>
    public interface IMaxTitleLength
    {
        /// <summary>
        /// Processes on returning the maximum title length
        /// </summary>
        /// <returns>Returns the maximum title length</returns>
        int GetMaxTitleLength();
    }
}
