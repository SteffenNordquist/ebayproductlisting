﻿
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Upload.UploadResponse
{
    public class MessageParser : IMessageParser
    {
        public string Parse<TSource>(TSource tSource)
        {
            string response = (string)(object)tSource;
            HtmlDocument doc = new HtmlDocument();
            doc.LoadHtml(response);

            string documentText = doc.DocumentNode.InnerText;

            return documentText;
        }
    }
}
