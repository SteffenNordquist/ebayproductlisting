﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Upload.UploadResponse
{
    public interface IReferenceParser
    {
        string Parse<TSource>(TSource tSource);
    }
}
