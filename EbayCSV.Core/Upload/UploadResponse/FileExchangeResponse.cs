﻿using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Upload.UploadResponse
{
    [BsonIgnoreExtraElements]
    public class FileExchangeResponse
    {
        public string id { get; set; }
        public string UploadMessage { get; set; }
        public string ReferenceNumber { get; set; }
        public DateTime UploadTime { get; set; }
        public string SupplierName { get; set; }
        public int AgentId { get; set; }
    }
}
