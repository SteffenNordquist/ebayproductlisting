﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Upload.UploadResponse
{
    public interface IUploadResponseManager
    {
        void Manage(string response);
    }
}
