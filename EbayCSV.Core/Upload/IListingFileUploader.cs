﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Upload
{
    public interface IListingFileUploader
    {
        string BeginUpload<TPath>(TPath tValue);
    }
}
