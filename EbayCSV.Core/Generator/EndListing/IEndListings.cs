﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Generator.EndListing
{
    public interface IEndListings
    {
        void Generate<T1, T2, T3>(T1 obj1, T2 obj2, T3 obj3);
    }
}
