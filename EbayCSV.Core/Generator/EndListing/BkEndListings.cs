﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Logger;
using EbayCSV.Core.FileWriter;
using ProcuctDB;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.Settings;

namespace EbayCSV.Core.Generator.EndListing
{
    /// <summary>
    /// A generator of items to be ended
    /// </summary>
    public class BkEndListings : IEndListings
    {
        // <summary>
        /// uset to get all eans of a supplier
        /// </summary>
        private readonly ISupplierEans _supplierEansInterface;

        /// <summary>
        /// The settings/configuration container
        /// </summary>
        private readonly ISettings _settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="supplierEansInterface">used to get the eans of a supplier</param>
        /// <param name="settings">The settings/configuration container</param>
        public BkEndListings(ISupplierEans supplierEansInterface, ISettings settings)
        {
            _supplierEansInterface = supplierEansInterface;
            _settings = settings;
        }

        /// <summary>
        /// will generate a file with items to be ended
        /// </summary>
        /// <typeparam name="T1"></typeparam>
        /// <typeparam name="T2"></typeparam>
        /// <typeparam name="T3"></typeparam>
        /// <param name="obj1"></param>
        /// <param name="obj2"></param>
        /// <param name="obj3"></param>
        public void Generate<T1, T2, T3>(T1 obj1, T2 obj2, T3 obj3)
        {
            if (Boolean.Parse(_settings.GetValue("endUnexistingListings")))
            {
                if (obj1 == null)
                {
                    throw new ArgumentNullException("obj1");
                }

                if (obj2 == null)
                {
                    throw new ArgumentNullException("obj2");
                }

                IFileWriter fileWriter = (IFileWriter) obj1;
                string supplierName = obj2 as string;

                if (supplierName != "")
                {
                    AppLogger.Warn("**********************generating end listings*********************************");

                    List<string> supplierEans = _supplierEansInterface.GetEans().ToList();

                    List<string> endListingLines = new List<string>();
                    endListingLines.Add(
                        "Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID\tCustomLabel");

                    endListingLines.AddRange(
                        InventoryStorage.InventoryItems()
                            .Where(
                                x =>
                                    !supplierEans.Contains(x.Ean) && x.ItemID != null && x.SupplierName != null &&
                                    x.SupplierName.ToLower().StartsWith(supplierName.ToLower()) &&
                                    (x.ListingStatus != null && x.ListingStatus == "Active"))
                            .Select(x => "EndItem" + "\t" + x.ItemID + "\t" + x.Ean).ToList());

                    if (endListingLines.Count() > 1)
                    {
                        AppLogger.Warn("listings to end : " + endListingLines.Count());
                        fileWriter.Write(endListingLines, supplierName);
                    }
                }
            }
        }
    }
}
