﻿
using EbayCSV.Core.CsvItem;
using EbayCSV.Core.FileWriter;
using EbayCSV.Core.HtmlData;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.Mapper;
using EbayCSV.Core.RawData;
using EbayCSV.Core.ResultDirectory;
using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Generator
{
    /// <summary>
    /// A csv generator class that delegates csv generation works.
    /// </summary>
    public class CBACsvGenerator : IGenerator
    {
        /// <summary>
        /// Contains raw items from any source that will be converted into a csv file
        /// </summary>
        private readonly IRawItems rawItems;
        /// <summary>
        /// A mapper from a raw item in a csv item
        /// </summary>
        private readonly IMapper mapper;
        /// <summary>
        /// Writes csv items into a file form
        /// </summary>
        private readonly IFileWriter fileWriter;
        /// <summary>
        /// The settings/configuration container
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// Container of mapped raw items into csv items
        /// </summary>
        private List<ICsvItem> csvItems = new List<ICsvItem>();

        /// <summary>
        /// Container of generated csv paths
        /// </summary>
        private List<string> generatedCsvPaths = new List<string>();

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="rawItems">Contains raw items from any source that will be converted into a csv file</param>
        /// <param name="mapper">A mapper from a raw item in a csv item</param>
        /// <param name="fileWriter">Writes csv items into a file form</param>
        /// <param name="settings">The settings/configuration container</param>
        public CBACsvGenerator(IRawItems rawItems, IMapper mapper, IFileWriter fileWriter, ISettings settings)
        {
            this.rawItems = rawItems;
            this.mapper = mapper;
            this.fileWriter = fileWriter;
            this.settings = settings;
        }

        /// <summary>
        /// Manages the csv generation
        /// Delegates the work on getting raw item, mapping from raw to csv and writing to a file
        /// </summary>
        /// <seealso cref="EbayCSV.Core.Generator.ICsvGenerator.Generate"/>
        public void Generate()
        {
            string delimeter = settings.GetValue("InputFileDelimiter");
            #region finalizing delimeter
            if (delimeter.Contains("tab"))
            {
                delimeter = "\t";
            }
            #endregion

            //List<string> rawItemsHeader = rawItems.GetHeader<string>(delimeter.ToArray().First()).ToList(); 
            List<string> fileItems = rawItems.GetItems<string>(0, Int32.MaxValue).ToList();

            //removing empty lines
            fileItems = fileItems.Where(x => x.Trim() != string.Empty).Select(x => x).ToList();

            for(int x = 0; x < fileItems.Count(); x++)
            {
                string itemLines = fileItems[x];

                #region getting variation lines
                int nextIndices = x + 1;
                while (nextIndices < fileItems.Count() && !IsParent(fileItems[nextIndices], delimeter))
                {
                    itemLines += "\n" + fileItems[nextIndices];
                    x = nextIndices;
                    nextIndices++;
                }
                #endregion

                csvItems.Add(mapper.MapItem<ICsvItem, string>(itemLines));
            }

            fileWriter.Write(csvItems);
        }

        /// <summary>
        /// Returns the list of generated csv data ( ie : list of csv files )
        /// </summary>
        /// <returns>Returns the list of generated csv data</returns>
        /// <seealso cref="EbayCSV.Core.Generator.ICsvGenerator.GeneratedCsvData"/>
        public IEnumerable<string> GeneratedCsvData()
        {
            return fileWriter.GeneratedData();
        }

        /// <summary>
        /// The purpose is to identify if a line is a parent or not
        /// Parent line indicates having value on the column[1] or category column
        /// Lines under a parent line are variations of an item (ie : color variations of a product)
        /// </summary>
        /// <param name="line">A line from a file</param>
        /// <param name="delimeter">A seperator for columns of a line(s)</param>
        /// <returns>Returns true if a line is a parent else false</returns>
        private bool IsParent(string line, string delimeter)
        {
            //column[1] or CategoryId column is not empty then its a parent
            return line.Split(delimeter.ToCharArray())[1].Trim() != string.Empty;
        }

        
    }
}
