﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Generator
{
    /// <summary>
    /// Csv generator that delegates csv generation works.
    /// </summary>
    /// <seealso cref="BkCsvGenerator"/>
    /// <seealso cref="EbayCSV.Core.Generator.CBACsvGenerator"/>
    public interface IGenerator
    {
        /// <summary>
        /// Manages the csv generation
        /// Delegates the work on getting raw item, mapping from raw to csv and writing to a file
        /// </summary>
        void Generate();

        /// <summary>
        /// Returns the list of generated csv data ( ie : list of csv files )
        /// </summary>
        /// <returns>Returns the list of generated csv data</returns>
        IEnumerable<string> GeneratedCsvData();
    }
}
