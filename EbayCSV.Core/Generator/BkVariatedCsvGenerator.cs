﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Logger;
using EbayCSV.Core.CsvItem;
using EbayCSV.Core.CsvItem.Variation;
using EbayCSV.Core.CsvItem.Variation.Grouping;
using EbayCSV.Core.Factories.Supplier;
using EbayCSV.Core.FileWriter;
using EbayCSV.Core.Generator.EndListing;
using EbayCSV.Core.ListingAction;
using EbayCSV.Core.Mapper;
using EbayCSV.Core.Mapper.VariationMapper;
using EbayCSV.Core.RawData;
using EbayCSV.Core.Settings;
using ProcuctDB;

namespace EbayCSV.Core.Generator
{
    public class BkVariatedCsvGenerator : IGenerator
    {
        /// <summary>
        /// Contains raw items from any source that will be converted into a csv file
        /// </summary>
        private readonly IRawItems _rawItems;

        /// <summary>
        /// A mapper from a raw item in a csv item
        /// </summary>
        private readonly IMapper _mapper;

        /// <summary>
        /// A mapper from a raw item in a csv variation item
        /// </summary>
        private readonly IVariationMapper _variationMapper;

        /// <summary>
        /// Writes csv items into a file form
        /// </summary>
        private readonly IFileWriter _fileWriter;

        /// <summary>
        /// The settings/configuration container
        /// </summary>
        private readonly ISettings _settings;

        /// <summary>
        /// Container of mapped raw items into csv items
        /// </summary>
        private List<ICsvItem> csvItems = new List<ICsvItem>();

        /// <summary>
        /// Container of generated csv paths
        /// </summary>
        private List<string> generatedCsvPaths = new List<string>();

        /// <summary>
        /// used to generate items to be ended
        /// </summary>
        private readonly IEndListings _endListings;

        /// <summary>
        /// supplier name worker
        /// </summary>
        private readonly ISupplierFactory _supplierFactory;

        /// <summary>
        /// Group parent identifier
        /// </summary>
        private readonly IGroupParent _groupParentIndicator;

        /// <summary>
        /// Group indicator
        /// </summary>
        private readonly IGroupIndicator _groupIndicator;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="rawItems">Contains raw items from any source that will be converted into a csv file</param>
        /// <param name="mapper">A mapper from a raw item in a csv item</param>
        /// <param name="variationMapper">A mapper from a raw item in a csv variation item</param>
        /// <param name="fileWriter">Writes csv items into a file form</param>
        /// <param name="settings">The settings/configuration container</param>
        /// <param name="supplierFactory">supplier name worker</param>
        /// <param name="endListings">used to generate items to be ended</param>
        /// <param name="groupParentIdentifier">Group parent identifier</param>
        /// <param name="groupIndicator">Group indicator</param>
        public BkVariatedCsvGenerator(IRawItems rawItems, IMapper mapper, IVariationMapper variationMapper, IFileWriter fileWriter, ISettings settings, ISupplierFactory supplierFactory, IEndListings endListings, IGroupParent groupParentIdentifier, IGroupIndicator groupIndicator)
        {
            _rawItems = rawItems;
            _mapper = mapper;
            _variationMapper = variationMapper;
            _fileWriter = fileWriter;
            _settings = settings;
            _supplierFactory = supplierFactory;
            _endListings = endListings;
            _groupParentIndicator = groupParentIdentifier;
            _groupIndicator = groupIndicator;
        }

        /// <summary>
        /// Manages the csv generation
        /// Delegates the work on getting raw item, mapping from raw to csv and writing to a file
        /// </summary>
        /// <seealso cref="EbayCSV.Core.Generator.IGenerator.Generate"/>
        public void Generate()
        {
            int actionlimit = int.Parse(_settings.GetValue("actionlimit"));
            int itemsPerFile = int.Parse(_settings.GetValue("itemsPerFile"));

            int processedCounter = 0;

            string supplierName = "";

            Dictionary<string, Tuple<ICsvItem, List<ICsvVariationItem>>> mappedToCsvItemWithVarationsGroup =
                new Dictionary<string, Tuple<ICsvItem, List<ICsvVariationItem>>>();

            AppLogger.Info("getting supplier products");
            bool hasMore = true;
            int limit = 10000;
            int skip = 0;
            while (hasMore)
            {
                hasMore = false;

                foreach (var bkProduct in _rawItems.GetItems<IProductSalesInformation>(skip, limit).ToList())
                {
                    if (supplierName == "") supplierName = _supplierFactory.GetSupplierName(bkProduct);

                    string groupCode = _groupIndicator.GetGroupCode((IProduct) bkProduct);
                    bool isGroupParent = _groupParentIndicator.IsGroupParent((IProduct) bkProduct);

                    ICsvItem mappedItem = null;
                    ICsvVariationItem mappedVariationItem = null;

                    if (isGroupParent) mappedItem = _mapper.MapItem<ICsvItem, IProductSalesInformation>(bkProduct);
                    else {mappedVariationItem = _variationMapper.MapItem<ICsvVariationItem, IProductSalesInformation>(bkProduct);}

                    if (mappedToCsvItemWithVarationsGroup.ContainsKey(groupCode))
                    {
                        if (mappedItem != null)
                        {
                            Tuple<ICsvItem, List<ICsvVariationItem>> value = new Tuple<ICsvItem, List<ICsvVariationItem>>(mappedItem, mappedToCsvItemWithVarationsGroup[groupCode].Item2);
                            mappedToCsvItemWithVarationsGroup[groupCode] = value;
                        }
                        else if (mappedVariationItem != null)
                        {
                            List<ICsvVariationItem> variations = new List<ICsvVariationItem>();
                            variations.AddRange(mappedToCsvItemWithVarationsGroup[groupCode].Item2);
                            variations.Add(mappedVariationItem);

                            Tuple<ICsvItem, List<ICsvVariationItem>> value = new Tuple<ICsvItem, List<ICsvVariationItem>>(mappedToCsvItemWithVarationsGroup[groupCode].Item1, variations);
                            mappedToCsvItemWithVarationsGroup[groupCode] = value;
                        }
                    }
                    else
                    {
                        if (mappedItem != null)
                        {
                            mappedToCsvItemWithVarationsGroup.Add(groupCode,
                                new Tuple<ICsvItem, List<ICsvVariationItem>>(mappedItem, new List<ICsvVariationItem>()));
                        }
                        else if (mappedVariationItem != null)
                        {
                            mappedToCsvItemWithVarationsGroup.Add(groupCode,
                                new Tuple<ICsvItem, List<ICsvVariationItem>>(null , new List<ICsvVariationItem>{mappedVariationItem}));
                        }
                    }

                    if (mappedItem != null || mappedVariationItem != null)
                    {
                        processedCounter += 1;
                        AppLogger.Info("mapped items : " + processedCounter);
                    }

                    hasMore = true;

                    if (processedCounter >= actionlimit)
                    {
                        hasMore = false;
                        break;
                    }
                }

                skip += limit;
            }

            ////writing all revisions
            //if (csvItems.FindAll(x => x.GetActionType() == ActionType.Revise).Count() == itemsPerFile)
            //{
            //    _fileWriter.Write(csvItems.FindAll(x => x.GetActionType() == ActionType.Revise), supplierName);
            //    csvItems.RemoveAll(x => x.GetActionType() == ActionType.Revise);
            //}

            ////writing all new items
            //if (csvItems.FindAll(x => x.GetActionType() == ActionType.Add).Count() == itemsPerFile)
            //{
            //    _fileWriter.Write(csvItems.FindAll(x => x.GetActionType() == ActionType.Add), supplierName);
            //    csvItems.RemoveAll(x => x.GetActionType() == ActionType.Add);

            //}

            AppLogger.Info("products processed : " + (skip - limit));

            if (csvItems.Any())
            {
                _fileWriter.Write(csvItems, supplierName);
            }


            _endListings.Generate(_fileWriter, supplierName, "");
        }

        /// <summary>
        /// Returns the list of generated csv data ( ie : list of csv files )
        /// </summary>
        /// <returns>Returns the list of generated csv data</returns>
        /// <seealso cref="EbayCSV.Core.Generator.IGenerator.GeneratedCsvData"/>
        public IEnumerable<string> GeneratedCsvData()
        {
            return _fileWriter.GeneratedData();
        }
    }
}
