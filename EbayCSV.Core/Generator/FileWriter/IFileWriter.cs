﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvGenerator.FileWriter
{
    public interface IFileWriter
    {
        void Write<TSource>(IEnumerable<TSource> tSources);

        IEnumerable<string> GeneratedFilePaths();
    }
}
