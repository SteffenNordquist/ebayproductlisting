﻿using EbayCSV.Core.CsvHeader;
using EbayCSV.Core.CsvItem;
using EbayCSV.Core.ResultDirectory;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.CsvGenerator.FileWriter
{
    public class BasicFileWriter : IFileWriter
    {
        private List<string> generatedCsvPaths = new List<string>();

        private readonly IResultDirectory resultDirectory;
        private readonly ICsvHeader csvHeader;

        public BasicFileWriter(IResultDirectory resultDirectory, ICsvHeader csvHeader)
        {
            this.resultDirectory = resultDirectory;
            this.csvHeader = csvHeader;
        }

        public void Write<TSource>(IEnumerable<TSource> tSources)
        {
            List<BasicItem> basicItems = (List<BasicItem>)(IEnumerable<object>)tSources;

            WriteNewItems(basicItems.Where(x => x.Action == ListingAction.ActionType.AddItem).ToList());

            WriteReviseItems(basicItems.Where(x => x.Action == ListingAction.ActionType.Revise).ToList());

        }

        public IEnumerable<string> GeneratedFilePaths()
        {
            return generatedCsvPaths;
        }

        private void WriteNewItems(List<BasicItem> newItems)
        {
            string path = resultDirectory.GetResultDirectory() + DateTime.Now.Ticks + "newcsv.txt";

            StringBuilder csvlines = new StringBuilder();
            csvlines.AppendLine(csvHeader.GetNewActionHeader());

            foreach (var item in newItems)
            {
                foreach (string line in item.GetNewActionFormat())
                {
                    csvlines.AppendLine(line);
                }
            }

            File.WriteAllText(path, csvlines.ToString());
        }

        private void WriteReviseItems(List<BasicItem> reviseItems)
        {
            string path = resultDirectory.GetResultDirectory() + DateTime.Now.Ticks + "revisecsv.txt";

            StringBuilder csvlines = new StringBuilder();
            csvlines.AppendLine(csvHeader.GetReviseActionHeader());

            foreach (var item in reviseItems)
            {
                foreach (string line in item.GetReviseActionFormat())
                {
                    csvlines.AppendLine(line);
                }
            }

            File.WriteAllText(path, csvlines.ToString());
        }
    }
}
