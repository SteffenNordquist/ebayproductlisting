﻿using DN_Classes.Logger;
using EbayCSV.Core.CsvItem;
using EbayCSV.Core.FileWriter;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.ListingAction;
using EbayCSV.Core.Mapper;
using EbayCSV.Core.RawData;
using EbayCSV.Core.ResultDirectory;
using EbayCSV.Core.Settings;
using ProcuctDB;
using ProcuctDB.JTL;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Factories.Supplier;
using EbayCSV.Core.Generator.EndListing;

namespace EbayCSV.Core.Generator
{
    /// <summary>
    /// A csv generator class that delegates csv generation works.
    /// </summary>
    public class BkCsvGenerator : IGenerator
    {
        /// <summary>
        /// Contains raw items from any source that will be converted into a csv file
        /// </summary>
        private readonly IRawItems rawItems;

        /// <summary>
        /// A mapper from a raw item in a csv item
        /// </summary>
        private readonly IMapper mapper;

        /// <summary>
        /// Writes csv items into a file form
        /// </summary>
        private readonly IFileWriter fileWriter;

        /// <summary>
        /// The settings/configuration container
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// Container of mapped raw items into csv items
        /// </summary>
        private List<ICsvItem> csvItems = new List<ICsvItem>();

        /// <summary>
        /// Container of generated csv paths
        /// </summary>
        private List<string> generatedCsvPaths = new List<string>();

        /// <summary>
        /// used to generate items to be ended
        /// </summary>
        private readonly IEndListings endListings;

        /// <summary>
        /// supplier name worker
        /// </summary>
        private readonly ISupplierFactory supplierFactory;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="rawItems">Contains raw items from any source that will be converted into a csv file</param>
        /// <param name="mapper">A mapper from a raw item in a csv item</param>
        /// <param name="fileWriter">Writes csv items into a file form</param>
        /// <param name="settings">The settings/configuration container</param>
        /// <param name="supplierFactory">supplier name worker</param>
        /// <param name="endListings">used to generate items to be ended</param>
        public BkCsvGenerator(IRawItems rawItems, IMapper mapper, IFileWriter fileWriter, ISettings settings, ISupplierFactory supplierFactory, IEndListings endListings)
        {
            this.rawItems = rawItems;
            this.mapper = mapper;
            this.fileWriter = fileWriter;
            this.settings = settings;
            this.supplierFactory = supplierFactory;
            this.endListings = endListings;
        }

        /// <summary>
        /// Manages the csv generation
        /// Delegates the work on getting raw item, mapping from raw to csv and writing to a file
        /// </summary>
        /// <seealso cref="EbayCSV.Core.Generator.IGenerator.Generate"/>
        public void Generate()
        {
            
            int actionlimit = int.Parse(settings.GetValue("actionlimit"));
            int itemsPerFile = int.Parse(settings.GetValue("itemsPerFile"));

            int processedCounter = 0;

            string supplierName = "";

            AppLogger.Info("getting supplier products");
            bool hasMore = true;
            int limit = 10000;
            int skip = 0;
            while (hasMore)
            {
                hasMore = false;

                foreach (var bkProduct in rawItems.GetItems<IProductSalesInformation>(skip, limit).ToList())
                {
                    if (supplierName == "")
                    {
                        supplierName = supplierFactory.GetSupplierName(bkProduct);
                    }

                    var mappedItem = mapper.MapItem<ICsvItem, IProductSalesInformation>(bkProduct);

                    if (mappedItem != null)
                    {
                        processedCounter += 1;
                        AppLogger.Info("mapped items : " + processedCounter);

                        csvItems.Add(mappedItem);

                        //writing all revisions
                        if (csvItems.FindAll(x => x.GetActionType() == ActionType.Revise).Count() == itemsPerFile)
                        {
                            fileWriter.Write(csvItems.FindAll(x => x.GetActionType() == ActionType.Revise), supplierName);
                            csvItems.RemoveAll(x => x.GetActionType() == ActionType.Revise);
                        }

                        //writing all new items
                        if (csvItems.FindAll(x => x.GetActionType() == ActionType.Add).Count() == itemsPerFile)
                        {
                            fileWriter.Write(csvItems.FindAll(x => x.GetActionType() == ActionType.Add), supplierName);
                            csvItems.RemoveAll(x => x.GetActionType() == ActionType.Add);

                        }

                    }

                    hasMore = true;

                    if (processedCounter >= actionlimit)
                    {
                        hasMore = false;
                        break;
                    }
                }

                skip += limit;
            }

            AppLogger.Info("products processed : " + (skip - limit));

            if (csvItems.Any())
            {
                fileWriter.Write(csvItems, supplierName);
            }

            
            endListings.Generate(fileWriter, supplierName,"");
                
            
        }

        /// <summary>
        /// Returns the list of generated csv data ( ie : list of csv files )
        /// </summary>
        /// <returns>Returns the list of generated csv data</returns>
        /// <seealso cref="EbayCSV.Core.Generator.IGenerator.GeneratedCsvData"/>
        public IEnumerable<string> GeneratedCsvData()
        {
            return fileWriter.GeneratedData();
        }
    }
}
