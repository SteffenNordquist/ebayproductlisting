﻿using EbayCSV.Core.Factories;
using EbayCSV.Core.Factories.ImageFactory;
using EbayCSV.Core.ListingAction;
using EbayCSV.Core.Settings;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.CsvItem.Variation;

namespace EbayCSV.Core.Mapper.VariationMapper
{
    public class DefaultVariationMapper : IVariationMapper
    {
        private readonly IFactories itemAttributesFactory;
        private readonly ISettings settings;
        private readonly IVariationImageFactory variationImageFactory;

        public DefaultVariationMapper(IFactories itemAttributesFactory, ISettings settings, IVariationImageFactory variationImageFactory)
        {
            this.itemAttributesFactory = itemAttributesFactory;
            this.settings = settings;
            this.variationImageFactory = variationImageFactory;
        }

        public TResult MapItem<TResult, TSource>(TSource sourceItem)
        {
            string variationLine = (string)(object)sourceItem;

            string settingsDelimiter = settings.GetValue("InputFileDelimiter");
            #region finalizing delimeter
            if (settingsDelimiter.Contains("tab"))
            {
                settingsDelimiter = "\t";
            }
            #endregion

            string sku = variationLine.Split(settingsDelimiter.ToCharArray())[0];

            int stock = itemAttributesFactory.StockFactory().GetStock<string>(variationLine);

            double sellPrice = itemAttributesFactory.SellpriceFactory().GetSellprice<string>(variationLine, "");

            ActionType action = itemAttributesFactory.ListingActionFactory().ListingActionType(sku, sellPrice, stock, "");

            string relationship = itemAttributesFactory.RelationshipFactory().GetRelationship<string>(variationLine);

            List<TraitValue> relationshipDetails = itemAttributesFactory.RelationshipDetailsFactory().GetRelationshipDetails<TraitValue, string>(variationLine).ToList();

            string customLabel = itemAttributesFactory.CustomLabelFactory().GetCustomLabel(action, sku, "");

            TraitValue firstVariationTrait = null;
            if (relationshipDetails.Any())
            {
                firstVariationTrait = relationshipDetails[0];
            }
            string picUrl = variationImageFactory.GetImage<string, string, TraitValue>(variationLine, firstVariationTrait).ToList()[0];

            VariationItem basicVariation = new VariationItem(
                                                            sku,
                                                            action,
                                                            relationship,
                                                            relationshipDetails,
                                                            customLabel,
                                                            stock,
                                                            sellPrice,
                                                            picUrl);

            return (TResult)(object)(basicVariation);
        }
    }
}
