﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Factories;
using EbayCSV.Core.Factories.ImageFactory;
using EbayCSV.Core.Settings;

namespace EbayCSV.Core.Mapper.VariationMapper
{
    public class BkVariationMapper : IVariationMapper
    {
        private readonly IFactories _itemAttributesFactory;
        private readonly ISettings _settings;
        private readonly IVariationImageFactory _variationImageFactory;

        public BkVariationMapper(IFactories itemAttributesFactory, ISettings settings, IVariationImageFactory variationImageFactory)
        {
            this._itemAttributesFactory = itemAttributesFactory;
            this._settings = settings;
            this._variationImageFactory = variationImageFactory;
        }


        public TResult MapItem<TResult, TSource>(TSource sourceItem)
        {
            throw new NotImplementedException();
        }
    }
}
