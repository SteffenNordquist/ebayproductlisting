﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Mapper.VariationMapper
{
    public interface IVariationMapper
    {
        TResult MapItem<TResult, TSource>(TSource sourceItem);
    }
}
