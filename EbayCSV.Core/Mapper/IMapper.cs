﻿using EbayCSV.Core.CsvItem;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Mapper
{
    /// <summary>
    /// Maps raw items into csv items
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Mapper.CBAToBasicItemMapper"/>
    /// <seealso cref="EbayCSV.Core.Mapper.BktoBasicItemMapper"/>
    /// <seealso cref="EbayCSV.Core.Mapper.BktoCatalogItemMapper"/>
    public interface IMapper
    {
        /// <summary>
        /// Processes the returning of mapped csv item
        /// </summary>
        /// <typeparam name="TResult">Return type</typeparam>
        /// <typeparam name="TSource">Additional property placeholder</typeparam>
        /// <param name="sourceItem">Treated as the raw item to be mapped</param>
        /// <returns>Returns the mapped csv item</returns>
        TResult MapItem<TResult, TSource>(TSource sourceItem);
    }
}
