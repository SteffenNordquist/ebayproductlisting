﻿using DN_Classes.Logger;
using EbayCSV.Core.CsvItem;
using EbayCSV.Core.CsvItem.ActionFormats;
using EbayCSV.Core.Factories;
using EbayCSV.Core.Factories.CustomFields;
using EbayCSV.Core.Filter;
using EbayCSV.Core.HtmlData;
using EbayCSV.Core.ListingAction;
using EbayCSV.Core.Mapper.VariationMapper;
using EbayCSV.Core.Settings;
using ProcuctDB;
using ProcuctDB.JTL;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.CsvItem.Variation;

namespace EbayCSV.Core.Mapper
{
    public class BktoCatalogItemMapper : IMapper
    {
        private readonly IHtml html;
        private readonly IFactories itemAttributesFactory;
        private readonly ISettings settings;
        private readonly IVariationMapper variationMapper;
        private readonly ICustomFieldsValues basicCustomFieldsValues;
        private readonly IListingFilter[] listingFilters;
        private readonly IRevisionFormat revisionFormat;
        private readonly IAdditionFormat additionFormat;

        public BktoCatalogItemMapper(
            IHtml html, 
            IFactories itemAttributesFactory, 
            ISettings settings, 
            IVariationMapper variationMapper, 
            ICustomFieldsValues basicCustomFieldsValues, 
            IListingFilter[] listingFilters,
            IRevisionFormat revisionFormat,
            IAdditionFormat additionFormat)
        {
            this.html = html;
            this.itemAttributesFactory = itemAttributesFactory;
            this.settings = settings;
            this.variationMapper = variationMapper;
            this.basicCustomFieldsValues = basicCustomFieldsValues;
            this.listingFilters = listingFilters;
            this.revisionFormat = revisionFormat;
            this.additionFormat = additionFormat;
        }

        public TResult MapItem<TResult, TSource>(TSource sourceItem)
        {
            IProductSalesInformation rawSourceItem = (IProductSalesInformation)(object)sourceItem;
            
            string ean = rawSourceItem.GetEan();

            string category = itemAttributesFactory.CategoryFactory().GetCategory<string>("", ean);

            string listingDbRecordSupplierName = itemAttributesFactory.SupplierFactory().GetSupplierName(rawSourceItem);

            double startprice = itemAttributesFactory.SellpriceFactory().GetSellprice<string>(ean, listingDbRecordSupplierName);

            double actionTypeIndicatorStartPrice = startprice;

            double shippingService1Costs = itemAttributesFactory.ShippingService1CostFactory().GetShippingService1Cost<IProductSalesInformation>(rawSourceItem);

            if (itemAttributesFactory.SellpriceFactory().GetType().Name == "BKSellpriceDeductedByShippingFactory")
            {
                actionTypeIndicatorStartPrice = startprice + shippingService1Costs;
            }

            if (listingFilters.Any(x => !x.IsOkay<string, string, double>(ean, listingDbRecordSupplierName, actionTypeIndicatorStartPrice)))
            {
                return (TResult)(object)null;
            }

            int quantity = itemAttributesFactory.StockFactory().GetStock<IProductSalesInformation>(rawSourceItem);

            ActionType actionType = itemAttributesFactory.ListingActionFactory().ListingActionType(ean, startprice, quantity, listingDbRecordSupplierName);

            if (actionType == ActionType.None)
            {
                return (TResult)(object)null;
            }

            if (actionType == ActionType.Add && String.IsNullOrEmpty(category) && Boolean.Parse(settings.GetValue("catalogTemplateRequireCategory")))
            {
                return (TResult)(object)null;
            }

            List<string> picUrls = itemAttributesFactory.ImageFactory().GetPicUrls<IProductSalesInformation>(rawSourceItem).ToList();
            if (!picUrls.Any())
            {
                return (TResult)(object)null;
            }
            string picUrl = picUrls.First();

            string description = "";

            if (!Boolean.Parse(settings.GetValue("excludeDescription")))
            {
                description = html.GetHtmlString<IProductSalesInformation>(rawSourceItem);

                if (description == "")
                {
                    return (TResult)(object)null;
                }
            }
            else if (actionType == ActionType.Revise)
            {
                description = "";
            }

            string customLabel = itemAttributesFactory.CustomLabelFactory().GetCustomLabel(actionType, ean, listingDbRecordSupplierName);

            string title = itemAttributesFactory.TitleFactory().GetTitle<IProductSalesInformation>(rawSourceItem);

            string subTitle = "";

            string storeCategory = "";

            string paypalEmail = itemAttributesFactory.PaypalEmailFactory().GetPaypalEmail();

            string dispatchTimeMax = itemAttributesFactory.DispatchTimeFactory().GetDispatchTime<IProductSalesInformation>(rawSourceItem);

            string duration = itemAttributesFactory.DuractionFactory().GetDuration<IProductSalesInformation>(rawSourceItem);

            string format = itemAttributesFactory.FormatFactory().GetListingFormat<IProductSalesInformation>(rawSourceItem);

            string location = itemAttributesFactory.LocationFactory().GetLocation<IProductSalesInformation>(rawSourceItem);

            string paypalAccept = itemAttributesFactory.PaypalAcceptFactory().AcceptPaypal<IProductSalesInformation>(rawSourceItem);

            string returnsOption = itemAttributesFactory.ReturnsOptionFactory().GetReturnsOption<IProductSalesInformation>(rawSourceItem);

            string shippingService1Option = itemAttributesFactory.ShippingService1OptionFactory().GetShippingService1Option<IProductSalesInformation>(rawSourceItem);

            string shippingService1FreeShipping = itemAttributesFactory.ShippingService1FreeShippingFactory().GetShippingService1FreeShipping<IProductSalesInformation>(rawSourceItem);

            string shippingProfileName = itemAttributesFactory.ShippingProfileName().GetShippingProfileName();

            string returnProfileName = itemAttributesFactory.ReturnProfileName().GetReturnProfileName();

            string paymentProfileName = itemAttributesFactory.PaymentProfileName().GetPaymentProfileName();

            int salesTaxPercent = itemAttributesFactory.SalesTaxPercent().SalesTax((IProduct)rawSourceItem);
            string salesTaxPercentText = "";
            if (salesTaxPercent > 0)
            {
                salesTaxPercentText = salesTaxPercent.ToString();
            }

            List<ICsvVariationItem> variations = new List<ICsvVariationItem>();

            string relationship = "";

            string relationshipDetails = "";

            string cFieldsValuesInALine = basicCustomFieldsValues.GetCustomFieldsValuesFormat<IProductSalesInformation>(rawSourceItem);

            CatalogItem catalogItem = new CatalogItem(
                ean, 
                customLabel, 
                actionType, 
                category, 
                title, 
                subTitle, 
                storeCategory, 
                picUrl, 
                description, 
                Math.Round(startprice, 2), 
                quantity.ToString(), 
                paypalEmail, 
                dispatchTimeMax, 
                duration, 
                format, 
                location, 
                paypalAccept, 
                returnsOption, 
                variations, 
                relationship, 
                relationshipDetails, 
                cFieldsValuesInALine,
                shippingService1Option,
                Math.Round(shippingService1Costs, 2).ToString(CultureInfo.InvariantCulture),
                shippingService1FreeShipping,
                returnProfileName,
                paymentProfileName,
                shippingProfileName,
                salesTaxPercentText,
                revisionFormat,
                additionFormat
                );

            return (TResult)(object)catalogItem;
        }
    }
}
