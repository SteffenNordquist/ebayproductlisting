﻿using EbayCSV.Core.CsvItem;
using EbayCSV.Core.CsvItem.ActionFormats;
using EbayCSV.Core.Factories;
using EbayCSV.Core.Factories.CustomFields;
using EbayCSV.Core.HtmlData;
using EbayCSV.Core.ListingAction;
using EbayCSV.Core.Mapper.VariationMapper;
using EbayCSV.Core.Settings;

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.CsvItem.Variation;

namespace EbayCSV.Core.Mapper
{
    /// <summary>
    /// Maps raw items into csv items
    /// </summary>
    public class CBAToBasicItemMapper : IMapper
    {
        /// <summary>
        /// 
        /// </summary>
        private readonly IHtml html;
        private readonly IFactories itemAttributesFactory;
        private readonly ISettings settings;
        private readonly IVariationMapper variationMapper;
        private readonly ICustomFieldsValues basicCustomFieldsValues;
        private readonly IRevisionFormat revisionFormat;
        private readonly IAdditionFormat additionFormat;

        public CBAToBasicItemMapper(IHtml html, IFactories itemAttributesFactory, ISettings settings, IVariationMapper variationMapper, ICustomFieldsValues basicCustomFieldsValues
            , IRevisionFormat revisionFormat
            , IAdditionFormat additionFormat)
        {
            this.html = html;
            this.itemAttributesFactory = itemAttributesFactory;
            this.settings = settings;
            this.variationMapper = variationMapper;
            this.basicCustomFieldsValues = basicCustomFieldsValues;
            this.revisionFormat = revisionFormat;
            this.additionFormat = additionFormat;
        }

        public TResult MapItem<TResult, TSource>(TSource sourceItem)
        {
            string itemLines = (string)(object)sourceItem;
            string parentLine = itemLines;

            string settingsDelimiter = settings.GetValue("InputFileDelimiter");
            #region finalizing delimeter
            if (settingsDelimiter.Contains("tab"))
            {
                settingsDelimiter = "\t";
            }
            #endregion

            string[] linesSplit = itemLines.Split('\n');

            List<ICsvVariationItem> variations = new List<ICsvVariationItem>();
            #region populating 'variations' using 'variationMapper' if 'linesSplit' count is > 0
            if (linesSplit.Count() > 1)
            {
                parentLine = linesSplit[0];

                List<string> lines = linesSplit.ToList();
                //removing parent line
                lines.RemoveAt(0);

                foreach (string line in lines)
                {
                    if (line.Trim() != string.Empty)
                    {
                        variations.Add(variationMapper.MapItem<VariationItem, string>(line));
                    }
                }


            }
            #endregion

            //List<string> cFields = header.Where(x => x.StartsWith("C:")).ToList();
            // var cFieldIndices = cFields.ToDictionary(x => x, y => header.IndexOf(y));
             var splittedParentLine = parentLine.Split(settingsDelimiter.ToCharArray());
          
             //var cFieldMappingList = cFields.Select(x => new KeyValuePair<string, string>(x, splittedParentLine[cFieldIndices[x]])).ToList();
             //string cFieldsValuesInALine = string.Join("\t",cFieldMappingList.Select(x => x.Value));
             string cFieldsValuesInALine = basicCustomFieldsValues.GetCustomFieldsValuesFormat<string>(parentLine);

             string sku = splittedParentLine[0];

            int stock = itemAttributesFactory.StockFactory().GetStock<string>(parentLine);
            string stockText = stock.ToString();

            double sellPrice = itemAttributesFactory.SellpriceFactory().GetSellprice<string>(parentLine, "");
            //string sellPriceText = sellPrice.ToString();

            string relationship = "";
            string relationshipDetails = "";

            ActionType actionType = itemAttributesFactory.ListingActionFactory().ListingActionType(sku, sellPrice, stock, "");

            if (actionType == ActionType.None)
            {
                return (TResult)(object)null;
            }

            #region finalizing 'item row' variables if 'variations' is not empty
            if (variations.Count() > 0)
            {
                stockText = "";
                //sellPriceText = "";
                sellPrice = 0;

                #region relationship details format
                var traits = new List<TraitValue>();
                variations.ForEach(x => x.VariationTraits<TraitValue>().ToList().ForEach(y => traits.Add(y)));
                var traitsGroups = traits.GroupBy(x => x.Trait);

                int counter = 0;
                foreach (var traitsGroup in traitsGroups)
                {
                    relationshipDetails += traitsGroup.Key + "=" + String.Join(";", traitsGroup.ToList().Select(x => x.Value).ToList());

                    counter++;
                    if (counter != traitsGroups.Count())
                    {
                        relationshipDetails += "|";
                    }
                }
                #endregion

                #region action type
                if (variations.FindAll(x => x.ActionType() == ActionType.Revise || x.ActionType() == ActionType.Delete).Count() > 0)
                {
                    actionType = ActionType.Revise;
                }
                #endregion
            }
            #endregion

            string customLabel = itemAttributesFactory.CustomLabelFactory().GetCustomLabel(actionType, sku, "");

            string title = itemAttributesFactory.TitleFactory().GetTitle<string>(parentLine);

            string subTitle = itemAttributesFactory.SubTitleFactory().GetSubTitle<string>(parentLine);

            string description = html.GetHtmlString<string>(parentLine);

            string category = itemAttributesFactory.CategoryFactory().GetCategory<string>(parentLine, sku);

            string storeCategory = itemAttributesFactory.StoreCategoryFactory().GetStoreCategory<string>(parentLine);

            string image = itemAttributesFactory.ImageFactory().GetPicUrls<string>(parentLine).ToList()[0];

            string paypalEmail = itemAttributesFactory.PaypalEmailFactory().GetPaypalEmail();

            string dispatchTimeMax = itemAttributesFactory.DispatchTimeFactory().GetDispatchTime<string>(parentLine);

            string duration = itemAttributesFactory.DuractionFactory().GetDuration<string>(parentLine);

            string format = itemAttributesFactory.FormatFactory().GetListingFormat<string>(parentLine);

            string location = itemAttributesFactory.LocationFactory().GetLocation<string>(parentLine);

            string paypalAccept = itemAttributesFactory.PaypalAcceptFactory().AcceptPaypal<string>(parentLine);

            string returnsOption = itemAttributesFactory.ReturnsOptionFactory().GetReturnsOption<string>(parentLine);

            string shippingProfileName = itemAttributesFactory.ShippingProfileName().GetShippingProfileName();

            string returnProfileName = itemAttributesFactory.ReturnProfileName().GetReturnProfileName();

            string paymentProfileName = itemAttributesFactory.PaymentProfileName().GetPaymentProfileName();

            int salesTaxPercent = 7;
            string salesTaxPercentText = "";
            if (salesTaxPercent > 0)
            {
                salesTaxPercentText = salesTaxPercent.ToString();
            }

            BasicItem csvItem = new BasicItem(
                sku, 
                customLabel, 
                actionType, 
                title, 
                subTitle,
                category,
                "",
                "",
                storeCategory,
                image, 
                description,
                sellPrice,
                stockText, 
                paypalEmail,
                dispatchTimeMax,
                duration,
                format,
                location,
                paypalAccept,
                returnsOption,
                variations,
                relationship,
                relationshipDetails,
                cFieldsValuesInALine,
                "",
                "",
                "",
                returnProfileName,
                paymentProfileName,
                shippingProfileName,
                salesTaxPercentText,
                revisionFormat,
                additionFormat
                );

            

            return (TResult)(object)csvItem;
        }
    }
}
