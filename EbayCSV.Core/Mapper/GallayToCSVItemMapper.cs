﻿using EbayCSV.Core.ListingAction;
using EbayCSV.Core.CsvItem;
using EbayCSV.Core.HtmlData;
using EbayCSV.Core.RawData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Factories;
using EbayCSV.Core.Factories.ListingActionFactory;
using EbayCSV.Core.Factories.CustomLabelFactory;

namespace EbayCSV.Core.Mapper
{
    public class GallayToCSVItemMapper : IMapper
    {
        private readonly IHtml html;
        private readonly ICsvFieldsFactory csvFieldsFactory;

        public GallayToCSVItemMapper(IHtml html, ICsvFieldsFactory csvFieldsFactory)
        {
            this.html = html;
            this.csvFieldsFactory = csvFieldsFactory;
        }

        public TResult MapItem<TResult, TSource>(TSource sourceItem)
        {
            GallayEntity gallay = (GallayEntity)(object)sourceItem;

            string description = html.GetHtmlString<GallayEntity>(gallay);

            string ean = gallay.product.ean;

            ActionType actionType = csvFieldsFactory.ListingActionFactory().ListingActionType(ean, 90.0, gallay.product.stock);

            string customLabel = csvFieldsFactory.CustomLabelFactory().GetCustomLabel(actionType, ean);

            string title = csvFieldsFactory.TitleFactory().GetTitle<GallayEntity>(gallay);

            string category = csvFieldsFactory.CategoryFactory().GetCategory(ean);

            string image = csvFieldsFactory.ImageFactory().GetImage<string, GallayEntity>(gallay).ToList()[0];

            double sellPrice = csvFieldsFactory.SellpriceFactory().GetSellprice<GallayEntity>(gallay);

            int stock = gallay.product.stock;

            string paypalEmail = csvFieldsFactory.PaypalEmailFactory().GetPaypalEmail();

            //string brand = csvFieldsFactory.BrandFactory().GetBrand();

            BasicItem csvItem = new BasicItem(ean, customLabel, actionType, title, category, image, description, sellPrice, stock, paypalEmail);

            return (TResult)(object)csvItem;
        }
    }
}
