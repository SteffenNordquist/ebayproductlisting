﻿using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Logger;

namespace EbayCSV.Core.HtmlData.BK
{
    /// <summary>
    /// Processes item's description in a form of html
    /// </summary>
    public class BKHtml : IHtml
    {
        /// <summary>
        /// A delegate for processing html data and returns compressed html
        /// </summary>
        private readonly IHtmlDataFeeder htmlDataFeeder;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="htmlDataFeeder">A delegate for processing html data and returns compressed html</param>
        public BKHtml(IHtmlDataFeeder htmlDataFeeder)
        {
            this.htmlDataFeeder = htmlDataFeeder;
        }

        /// <summary>
        /// Processes and delegates the work on returning item's description in a form of html
        /// </summary>
        /// <typeparam name="T">Parameter type</typeparam>
        /// <param name="obj">Treated as IProductSalesInformation</param>
        /// <returns>Returns item's description in a form of html</returns>
        /// <seealso cref="EbayCSV.Core.HtmlData.IHtml.GetHtmlString"/>
        public string GetHtmlString<T>(T obj)
        {
            IProductSalesInformation bkProduct = (IProductSalesInformation)(object)obj;

            htmlDataFeeder.Feed<IProductSalesInformation>(bkProduct);

            string compressedHtml = htmlDataFeeder.GetCompressedHtml();

            if (compressedHtml == String.Empty)
            {
                AppLogger.Warn("filtered : " + bkProduct.GetEan() + " : empty description");
            }

            return compressedHtml;
        }
    }
}
