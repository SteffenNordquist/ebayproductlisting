﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.HtmlData.BK
{
    /// <summary>
    /// Processes html data and returns compressed html
    /// </summary>
    /// <seealso cref="EbayCSV.Core.HtmlData.BK.HtmlDataFeeder"/>
    public interface IHtmlDataFeeder
    {
        /// <summary>
        /// Processes the html data
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as html data</param>
        void Feed<TSource>(TSource tSource);

        /// <summary>
        /// Processes on returning the compressed html
        /// </summary>
        /// <returns>Returns the compressed html</returns>
        string GetCompressedHtml();
    }
}
