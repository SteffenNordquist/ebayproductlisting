﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.HtmlData.BK
{
    /// <summary>
    /// Allows for compressing downloaded html
    /// </summary>
    /// <seealso cref="EbayCSV.Core.HtmlData.BK.HtmlCompressor"/>
    public interface IHtmlCompressor
    {
        /// <summary>
        /// Compresses downloaded html
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as additional property placeholder</param>
        /// <returns>Returns compressed html</returns>
        string GetCompressedHtml<TSource>(TSource tSource);
    }
}
