﻿using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.HtmlData.BK
{
    /// <summary>
    /// Allows for compressing downloaded html
    /// </summary>
    public class HtmlCompressor : IHtmlCompressor
    {
        /// <summary>
        /// Compresses downloaded html
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as uncompressed html string</param>
        /// <returns>Returns compressed html</returns>
        /// <seealso cref="EbayCSV.Core.HtmlData.BK.IHtmlCompressor.GetCompressedHtml"/>
        public string GetCompressedHtml<TSource>(TSource tSource)
        {
            string htmlString = (string)(object)tSource;

            string htmlBody = "";

            if (htmlString != "")
            {
                HtmlDocument htmlDocument = new HtmlDocument();

                htmlDocument.LoadHtml(htmlString);

                htmlBody = htmlDocument.DocumentNode.SelectSingleNode("//body").InnerHtml.Replace("\r\n", "").Replace("\t", "").Replace("\n", "").Trim().TrimStart().TrimEnd();

                //eBay error 353: Java script not allowed.
                htmlBody = htmlBody.Replace("cookie", "C ookie").Replace("Cookie", "C ookie");
                //.Replace("(", "'").Replace(")", "'").Replace("{", "'").Replace("}", "'");

                htmlBody = htmlBody.Replace("\"", "\"\"")
                    .Replace("”", "\"\"")
                    .Replace("“", "\"\"")
                    .Replace("″", "\"\"")
                    .Replace("‴", "\"\"")
                    .Replace("„", "\"\"");

                htmlBody = "\"" + "<meta charset=\"\"utf-8\"\">" + htmlBody + "\"";
            }

            return htmlBody;
        }
    }
}
