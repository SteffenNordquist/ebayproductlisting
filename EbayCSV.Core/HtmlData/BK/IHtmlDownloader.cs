﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.HtmlData.BK
{
    /// <summary>
    /// Allows for downloading the html
    /// </summary>
    /// <seealso cref="EbayCSV.Core.HtmlData.BK.HtmlDownloader"/>
    public interface IHtmlDownloader
    {
        /// <summary>
        /// Processes on returning the downloaded html
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as additional property placeholder</param>
        /// <returns>Returns downloaded html</returns>
        string GetHtml<TSource>(TSource tSource);
    }
}
