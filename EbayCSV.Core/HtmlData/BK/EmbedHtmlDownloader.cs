﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Logger;

namespace EbayCSV.Core.HtmlData.BK
{
    /// <summary>
    /// Allows for downloading the html
    /// </summary>
    public class EmbedHtmlDownloader : IHtmlDownloader
    {
        /// <summary>
        /// Processes on returning the downloaded html
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as the link of the html to be downloaded</param>
        /// <returns>Returns downloaded html</returns>
        /// <seealso cref="EbayCSV.Core.HtmlData.BK.IHtmlDownloader.GetHtml"/>
        public string GetHtml<TSource>(TSource tSource)
        {
            string link = (string)(object)tSource;

            string htmlString = "<!DOCTYPE html><html style=\"margin:0;width:100%;height:100%;overflow:hidden;\"><body style=\"margin:0;width:100%;height:100%;\"><embed src=\"" + link + "\"  style=\"width: 100%; height:100%;\" ></body></html>";

            return htmlString;
        }
    }
}
