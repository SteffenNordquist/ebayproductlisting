﻿using DN_Classes.Logger;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.Settings;
using HtmlAgilityPack;
using ProcuctDB;
using ProcuctDB.JTL;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EbayCSV.Core.HtmlData.BK
{
    /// <summary>
    /// Processes html data and returns compressed html
    /// </summary>
    public class HtmlDataFeeder : IHtmlDataFeeder
    {
        /// <summary>
        /// A delegate allows interaction with the html data entries collection
        /// </summary>
        private readonly IHtmlDataEntries htmlDataEntries;

        /// <summary>
        /// A delegate allows for downloading the html
        /// </summary>
        private readonly IHtmlDownloader htmlDownloader;

        /// <summary>
        /// A delegate allows for compressing downloaded html
        /// </summary>
        private readonly IHtmlCompressor htmlCompressor;

        /// <summary>
        /// Container of settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// An id of the current html entry ( an entry to the collection )
        /// </summary>
        private string htmlEntryId;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="htmlDataEntries">A delegate allows interaction with the html data entries collection</param>
        /// <param name="settings">Container of settings/configurations</param>
        /// <param name="htmlDownloader">A delegate allows for downloading the html</param>
        /// <param name="htmlCompressor">A delegate allows for compressing downloaded html</param>
        public HtmlDataFeeder(IHtmlDataEntries htmlDataEntries, ISettings settings, IHtmlDownloader htmlDownloader, IHtmlCompressor htmlCompressor)
        {
            this.htmlDataEntries = htmlDataEntries;
            this.htmlDownloader = htmlDownloader;
            this.htmlCompressor = htmlCompressor;
            this.settings = settings;
        }

        /// <summary>
        /// Processes the html data
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as IProductSalesInformation</param>
        /// <seealso cref="EbayCSV.Core.HtmlData.BK.IHtmlDataFeeder.Feed"/>
        public void Feed<TSource>(TSource tSource)
        {
            IProductSalesInformation bkProduct = (IProductSalesInformation)(object)tSource;

            string imagePathSupplierFolder = bkProduct.getSupplierName();
            if (bkProduct.GetType().Name.ToLower().StartsWith("jtl"))
            {
                JTLEntity bkJtlProduct = (JTLEntity)bkProduct;
                imagePathSupplierFolder = bkJtlProduct.product.supplier;
            }

            if (bkProduct != null && bkProduct.GetMainImage() != null && bkProduct.GetMainImage().Count() > 0 && bkProduct.getSupplierName() != "" && imagePathSupplierFolder != "")
            {
                string imagelinkformat = settings.GetValue("imagelinkformat");
                string ebayname = settings.GetValue("ebayname").Replace(".", "");

                List<string> images = new List<string>();
                bkProduct.GetMainImage().ForEach(imageName => images.Add(string.Format(imagelinkformat, imagePathSupplierFolder, imageName, ebayname, ProductBrowsing.IsLocal)));

                string itemId = null;
                double price = 0;
                var inventoryItem = InventoryStorage.InventoryItems().Find(x => x.Ean == bkProduct.GetEan());
                if (inventoryItem != null)
                {
                    itemId = inventoryItem.ItemID;
                    price = inventoryItem.Wanted.Price;
                }

                HtmlDataEntity htmlData = new HtmlDataEntity();
                htmlData.id = settings.GetValue("agentid") + "_" + bkProduct.getSupplierName() + bkProduct.GetEan();
                htmlData.ItemId = itemId;
                htmlData.attributeList = bkProduct.GetAttributeList();
                htmlData.mainimages = images;
                htmlData.title = bkProduct.getTitle()[0];
                htmlData.logoLink = settings.GetValue("logolink");
                htmlData.Price = price;

                this.htmlEntryId = htmlData.id;

                //htmlDataEntries.Upsert<HtmlDataEntity>(htmlData);

                ThreadStart threadStart = delegate
                {
                    bool upsertOp = htmlDataEntries.Upsert<HtmlDataEntity>(htmlData);
                    AppLogger.Info(htmlEntryId + " upsert : " + upsertOp);
                };
                new Thread(threadStart).Start();

                
                
            }
        }

        /// <summary>
        /// Processes on returning the compressed html
        /// </summary>
        /// <returns>Returns the compressed html</returns>
        /// <seealso cref="EbayCSV.Core.HtmlData.BK.IHtmlDataFeeder.GetCompressedHtml"/>
        public string GetCompressedHtml()
        {
            string mvclink = String.Format("{0}id={1}", settings.GetValue("mvcviewlink"), this.htmlEntryId);

            AppLogger.Info("mvc link : " + mvclink);

            if (ProductBrowsing.IsLocal) { Process.Start(mvclink); }

            string htmlString = htmlDownloader.GetHtml<string>(mvclink);

            string compressedHtml = htmlCompressor.GetCompressedHtml<string>(htmlString);

            //ProcessHtml();

            //return this.htmlString;

            return compressedHtml;
        }

        #region old codes
        //private void ProcessHtml()
        //{
        //    HtmlDocument htmlDocument = new HtmlDocument();

        //    string htmlString = DownloadHtmlString();

        //    string htmlBody = "";

        //    if (htmlString != "")
        //    {
        //        htmlDocument.LoadHtml(htmlString);

        //        htmlBody = htmlDocument.DocumentNode.SelectSingleNode("//body").InnerHtml.Replace("\r\n", "").Replace("\t", "").Replace("\n", "").Trim().TrimStart().TrimEnd();

        //        htmlBody = htmlBody.Replace("\"", "\"\"");
        //        htmlBody = "\"" + "<meta charset=\"\"utf-8\"\">" + htmlBody + "\"";
        //    }

        //    this.htmlString = htmlBody;
        //}

        //private string DownloadHtmlString()
        //{

        //    string mvclink = String.Format("{0}id={1}", settings.GetValue("mvcviewlink") , this.htmlEntryId);

        //    Console.WriteLine("Mvc Link " + mvclink);

        //    if (ProductBrowsing.IsLocal) { Process.Start(mvclink); }

        //    WebClient webClient = new WebClient();
        //    webClient.Encoding = Encoding.UTF8;

        //    string htmlString = "";

        //    try { htmlString = webClient.DownloadString(mvclink); }
        //    catch { }

        //    return htmlString;
        //}
        #endregion
    }
}
