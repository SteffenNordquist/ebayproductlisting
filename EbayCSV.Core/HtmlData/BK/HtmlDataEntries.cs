﻿using DN.DataAccess;
using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.HtmlData.BK
{
    /// <summary>
    /// Allows interaction with the html data entries collection
    /// </summary>
    public class HtmlDataEntries : IHtmlDataEntries
    {
        /// <summary>
        /// A delegate allows interaction with the collection
        /// </summary>
        private readonly IDbConnection dbConnection;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        public HtmlDataEntries()
        {
            #region dn data access
            IDbConnectionFactory connectionFactory = new MongoConnectionFactory(new MongoConnectionProperties());
            connectionFactory.ConnectionProperties.SetProperty("connectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/HtmlGenerationEntries");
            connectionFactory.ConnectionProperties.SetProperty("databasename", "HtmlGenerationEntries");
            connectionFactory.ConnectionProperties.SetProperty("collectionname", "entries");
            #endregion

            dbConnection = connectionFactory.CreateConnection();
        }

        /// <summary>
        /// Upserts data into htmldataentries collection
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as HtmlDataEntity</param>
        /// <seealso cref="EbayCSV.Core.HtmlData.BK.IHtmlDataEntries.Upsert"/>
        public bool Upsert<TSource>(TSource tSource)
        {
            HtmlDataEntity htmlData = (HtmlDataEntity)(object)tSource;
            string query = " { _id : \"" + htmlData.id + "\" } ";
            bool response1 = dbConnection.DataAccess.Commands.Update(query, "mainimages", htmlData.mainimages, false, true);
            bool response2 = dbConnection.DataAccess.Commands.Update(query, "mainimage", htmlData.mainimages[0], false, true);
            bool response3 = dbConnection.DataAccess.Commands.Update(query, "attributeList", htmlData.attributeList, false, true);
            bool response4 = dbConnection.DataAccess.Commands.Update(query, "title", htmlData.title, false, true);
            bool response5 = dbConnection.DataAccess.Commands.Update(query, "logoLink", htmlData.logoLink, false, true);
            bool response6 = dbConnection.DataAccess.Commands.Update(query, "ItemId", htmlData.ItemId, false, true);
            bool response7 = dbConnection.DataAccess.Commands.Update(query, "Price", htmlData.Price, false, true);

            List<bool> responses = new List<bool>();
            responses.Add(response1);
            responses.Add(response2);
            responses.Add(response3);
            responses.Add(response4);
            responses.Add(response5);
            responses.Add(response6);
            responses.Add(response7);

            bool upserted = responses.All(x => x);

            return upserted;
        }

        /// <summary>
        /// Queries html data entry using the id
        /// </summary>
        /// <typeparam name="TResult">Return type</typeparam>
        /// <param name="id">Id of html data entry</param>
        /// <returns>Returns html data entry</returns>
        /// <seealso cref="EbayCSV.Core.HtmlData.BK.IHtmlDataEntries.Find"/>
        public TResult Find<TResult>(string id)
        {
            return (TResult)(object)dbConnection.DataAccess.Queries.FindByKey<TResult>("_id", id);
        }

        /// <summary>
        /// Updates entry's price
        /// </summary>
        /// <param name="ean">13-digit European Article Number</param>
        /// <param name="supplierName">supplier name of the product</param>
        /// <param name="price">calculated price of the product</param>
        /// <seealso cref="EbayCSV.Core.HtmlData.BK.IHtmlDataEntries.UpdatePrice"/>
        public void UpdatePrice(string ean, string supplierName, double price)
        {
            string query = " { _id : /" + supplierName + ean + " /} ";
            dbConnection.DataAccess.Commands.Update<double>(query, "Price", price, true);
            //EntriesCollection.Update(Query.Matches("_id", ean), Update<HtmlData>.Set(x => x.Price, price), UpdateFlags.Multi);
        }

        /// <summary>
        /// Updates entry's price
        /// </summary>
        /// <param name="id">id of the entry</param>
        /// <param name="price">calculated price of the product</param>
        /// <seealso cref="EbayCSV.Core.HtmlData.BK.IHtmlDataEntries.UpdatePrice"/>
        public void UpdatePrice(string id, double price)
        {
            string query = " { _id : \"" + id + "\"} ";
            dbConnection.DataAccess.Commands.Update<double>(query, "Price", price, true);
            //EntriesCollection.Update(Query.Matches("_id", ean), Update<HtmlData>.Set(x => x.Price, price), UpdateFlags.Multi);
        }
    }
}
