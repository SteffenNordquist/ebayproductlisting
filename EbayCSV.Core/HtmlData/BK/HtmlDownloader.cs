﻿using DN_Classes.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.HtmlData.BK
{
    /// <summary>
    /// Allows for downloading the html
    /// </summary>
    public class HtmlDownloader : IHtmlDownloader
    {
        /// <summary>
        /// Processes on returning the downloaded html
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as the link of the html to be downloaded</param>
        /// <returns>Returns downloaded html</returns>
        /// <seealso cref="EbayCSV.Core.HtmlData.BK.IHtmlDownloader.GetHtml"/>
        public string GetHtml<TSource>(TSource tSource)
        {
            string link = (string)(object)tSource;

            WebClient webClient = new WebClient();
            webClient.Encoding = Encoding.UTF8;

            string htmlString = "";
            string errorMessage = "";
            int errorCount = 0;
            while (errorCount < 2)
            {
                try { 
                    htmlString = webClient.DownloadString(link);
                    errorCount = 2;
                }
                catch (Exception ex)
                {
                    errorMessage = "html download error : " + link + "\n" + ex.Message + "\n" + ex.StackTrace;
                    errorCount++;
                }
            }

            if (htmlString == "")
            {
                AppLogger.Error(errorMessage);
            }

            return htmlString;
        }
    }
}
