﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.HtmlData.BK
{
    /// <summary>
    /// Allows interaction with the html data entries collection
    /// </summary>
    /// <seealso cref="EbayCSV.Core.HtmlData.BK.HtmlDataEntries"/>
    public interface IHtmlDataEntries
    {
        /// <summary>
        /// Upserts data into htmldataentries collection
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as the date to be upserted</param>
        bool Upsert<TSource>(TSource tSource);

        /// <summary>
        /// Queries html data entry using the id
        /// </summary>
        /// <typeparam name="TResult">Return type</typeparam>
        /// <param name="id">Id of html data entry</param>
        /// <returns>Returns html data entry</returns>
        TResult Find<TResult>(string id);

        /// <summary>
        /// Updates entry's price
        /// </summary>
        /// <param name="ean">13-digit European Article Number</param>
        /// <param name="supplierName">supplier name of the product</param>
        /// <param name="price">calculated price of the product</param>
        void UpdatePrice(string ean, string supplierName, double price);

        /// <summary>
        /// Updates entry's price
        /// </summary>
        /// <param name="id">id of the entry</param>
        /// <param name="price">calculated price of the product</param>
        void UpdatePrice(string id, double price);
    }
}
