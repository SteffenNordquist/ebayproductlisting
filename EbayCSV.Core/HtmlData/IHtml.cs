﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.HtmlData
{
    /// <summary>
    /// Processes item's description in a form of html
    /// </summary>
    /// <seealso cref="EbayCSV.Core.HtmlData.DefaultHtml"/>
    /// <seealso cref="EbayCSV.Core.HtmlData.BK.BKHtml"/>
    public interface IHtml
    {
        /// <summary>
        /// Processes on returning item's description in a form of html
        /// </summary>
        /// <typeparam name="T">Parameter type</typeparam>
        /// <param name="obj">A property placeholder</param>
        /// <returns>Returns item's description in a form of html</returns>
        string GetHtmlString<T>(T obj);
    }
}
