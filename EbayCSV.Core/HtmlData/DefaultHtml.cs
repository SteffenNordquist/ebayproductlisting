﻿using EbayCSV.Core.Factories;
using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.HtmlData
{
    /// <summary>
    /// Processes item's description in a form of html
    /// </summary>
    public class DefaultHtml : IHtml
    {
        /// <summary>
        /// Container of all attributes factory
        /// </summary>
        private readonly IFactories csvItemAttributesFactory;

        /// <summary>
        /// Container of settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="csvItemAttributesFactory">Container of all attributes factory</param>
        /// <param name="settings">Container of settings/configurations</param>
        public DefaultHtml(IFactories csvItemAttributesFactory, ISettings settings)
        {
            this.csvItemAttributesFactory = csvItemAttributesFactory;
            this.settings = settings;
        }

        /// <summary>
        /// Processes on returning item's description in a form of html
        /// </summary>
        /// <typeparam name="T">Parameter type</typeparam>
        /// <param name="obj">A property placeholder</param>
        /// <returns>Returns item's description in a form of html</returns>
        /// <seealso cref="EbayCSV.Core.HtmlData.IHtml.GetHtmlString"/>
        public string GetHtmlString<T>(T obj)
        {
            string source = (string)(object)(obj);

            //reading html template text from file
            string htmlTemplateText = File.ReadAllText(settings.GetValue("HtmlTemplateFilePath"));

            //replacing all double quote by singe quote
            htmlTemplateText = htmlTemplateText.Replace("\"", "'");

            //adding meta data at the beginning of the template text
            htmlTemplateText = "<meta charset='utf-8'>" + htmlTemplateText;

            //adding double quote at the start and end of the entire template text
            htmlTemplateText = "\"" + htmlTemplateText + "\"";

            string image = csvItemAttributesFactory.ImageFactory().GetPicUrls<string>(source).ToList()[0];

            string title = csvItemAttributesFactory.TitleFactory().GetTitle<string>(source);

            string settingsDelimeter = settings.GetValue("InputFileDelimiter");
            #region finalizing delimeter
            if (settingsDelimeter.Contains("tab"))
            {
                settingsDelimeter = "\t";
            }
            #endregion

            string[] splits = source.Split(settingsDelimeter.ToCharArray());
            string properties = "";
            if(splits.Count() > 5){properties = splits[5];}

            //removing 'quote' at the beginning
            if (properties.Trim().Length > 0 && properties.Trim().ElementAt(0) == '\"')
            {
                properties = properties.Trim().Remove(0, 1);
            }
            //removing 'quote' at the end
            if (properties.Trim().Length > 0 && properties.Trim().ElementAt(properties.IndexOf(properties.Last())) == '\"')
            {
                properties = properties.Trim().Remove(properties.Length - 1, 1);
            }

            htmlTemplateText = htmlTemplateText.Replace("@ImageHere", image);
            htmlTemplateText = htmlTemplateText.Replace("@ProductTitleHere", title);
            htmlTemplateText = htmlTemplateText.Replace("@ProductPropertiesHere", properties);
            htmlTemplateText = htmlTemplateText.Replace("\r\n", "").Replace("\t", "").Replace("\n", "").Trim().TrimStart().TrimEnd();

            //return "<p>test</p>";
            return htmlTemplateText;
        }
    }
}
