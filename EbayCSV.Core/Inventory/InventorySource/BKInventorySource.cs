﻿using EbayCSV.Core.Inventory.InventorySource.ListingInventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory.InventorySource
{
    /// <summary>
    /// Allows interaction with ebay inventories
    /// </summary>
    public class BKInventorySource : IInventorySource
    {
        /// <summary>
        /// A delegate that allows reading interaction with the inventory collection
        /// </summary>
        private readonly IListingDataReader dataReader;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="dataReader">A delegate that allows reading interaction with the inventory collection</param>
        public BKInventorySource(IListingDataReader dataReader)
        {
            this.dataReader = dataReader;
        }

        /// <summary>
        /// Processes the returning of ebay inventories
        /// </summary>
        /// <returns>Returns ebay inventories</returns>
        /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.IInventorySource.GetInventoryItems"/>
        public IEnumerable<InventoryItem> GetInventoryItems()
        {
            //true to all
            string query = "{ _id : { $exists: true} }";

            return dataReader.Find<InventoryItem, string>(query).ToList();
        }
    }
}
