﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using EbayCSV.Core.Settings;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory.InventorySource.ListingInventory.BK
{
    /// <summary>
    /// A factory for listing inventory database
    /// </summary>
    public class BKListingDatabaseFactory : IListingDatabaseFactory
    {
        /// <summary>
        /// Container for settings/configurations
        /// </summary>
        private readonly ISettings settings;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container for settings/configurations</param>
        public BKListingDatabaseFactory(ISettings settings)
        {
            this.settings = settings;
        }

        /// <summary>
        /// Processes the work on returning listing inventory database
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="identifier">Treated as ebay name</param>
        /// <returns>Returns listing inventory database</returns>
        /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.IListingDatabaseFactory.GetListingDatabase"/>
        public IListingDatabase GetListingDatabase<TSource>(TSource identifier)
        {
            string ebayName = (string)(object)identifier;

            #region dn data access
            IDbConnectionFactory connectionFactory = new MongoConnectionFactory(new MongoConnectionProperties());
            connectionFactory.ConnectionProperties.SetProperty("connectionstring", settings.GetValue("listingdbconnectionstring") + ebayName);
            connectionFactory.ConnectionProperties.SetProperty("databasename", settings.GetValue("listingdbdatabasename") + ebayName);
            connectionFactory.ConnectionProperties.SetProperty("collectionname", settings.GetValue("listingdbcollectionname"));
            #endregion

            IDbConnection dbConnection = connectionFactory.CreateConnection();

            IListingDatabase bkDatabase = new BKListingDatabase(dbConnection);

            return bkDatabase;
        }
    }
}
