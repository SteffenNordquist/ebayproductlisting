﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory.InventorySource
{
    /// <summary>
    /// Allows interaction with ebay inventories
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.BKInventorySource"/>
    /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.DefaultInventorySource"/>
    public interface IInventorySource
    {
        /// <summary>
        /// Processes the returning of ebay inventories
        /// </summary>
        /// <returns>Returns ebay inventories</returns>
        IEnumerable<InventoryItem> GetInventoryItems();
    }
}
