﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory.InventorySource
{
    /// <summary>
    /// Allows writing interaction with listing inventory collection
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.ListingInventory.BK.BKListingDataWriter"/>
    public interface IListingDataWriter
    {
        /// <summary>
        /// Inserts new record in the collection
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as the object to be inserted</param>
        /// <returns>Returns insert response</returns>
        string Insert<TSource>(TSource tSource);

        /// <summary>
        /// Updates record in the collection
        /// </summary>
        /// <typeparam name="TValue">Parameter type</typeparam>
        /// <param name="query">query string</param>
        /// <param name="updateField">field to be updated</param>
        /// <param name="updateValue">new value</param>
        /// <param name="multi">Indicates if update process if multiple</param>
        /// <param name="upsert">Indicates if update process upserts</param>
        /// <returns>Returns update response</returns>
        string Update<TValue>(string query, string updateField, TValue updateValue, bool multi = false, bool upsert = false);

        /// <summary>
        /// Deletes record in the collection
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as the object to be deleted</param>
        /// <returns>Returns delete response</returns>
        string Delete<TSource>(TSource tSource);
    }
}
