﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory.InventorySource
{
    /// <summary>
    /// A factory for listing inventory database
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.ListingInventory.BK.BKListingDatabaseFactory"/>
    public interface IListingDatabaseFactory
    {
        /// <summary>
        /// Processes the work on returning listing inventory database
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="identifier">Identifier property placeholder</param>
        /// <returns>Returns listing inventory database</returns>
        IListingDatabase GetListingDatabase<TSource>(TSource identifier);
    }
}
