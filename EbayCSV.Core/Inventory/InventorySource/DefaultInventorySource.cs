﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory.InventorySource
{
    /// <summary>
    /// Allows interaction with ebay inventories
    /// </summary>
    public class DefaultInventorySource : IInventorySource
    {
        /// <summary>
        /// Processes the returning of ebay inventories
        /// Returns empty 
        /// </summary>
        /// <returns>Returns ebay inventories</returns>
        /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.IInventorySource.GetInventoryItems"/>
        public IEnumerable<InventoryItem> GetInventoryItems()
        {
            return new List<InventoryItem>();
        }
    }
}
