﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory
{
    [BsonIgnoreExtraElements]
    public class Inventory
    {
        public string id { get; set; }
        public string ItemID { get; set; }
        public string Ean { get; set; }
        public double BuyItNowPrice { get; set; }
        public double StartPrice { get; set; }
        public int Quantity { get; set; }
        public int QuantityAvailable { get; set; }
        public string ListingStatus { get; set; }
        public int Views { get; set; }
        public string SupplierName { get; set; }
        public bool UpdateDescription { get; set; }
        public bool UpdateTitle { get; set; }
        public bool ForceRevision { get; set; }
        public WantedClass Wanted { get; set; }
        public BsonDocument plus { get; set; }
        public BsonDocument ItemStatusData { get; set; }


    }

    public class WantedClass
    {
        public double Price { get; set; }
        public int Quantity { get; set; }
    }
}
