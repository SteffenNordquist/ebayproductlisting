﻿using EbayCSV.Core.Inventory.InventorySource;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory
{
    /// <summary>
    /// Allows interaction with ebay inventory
    /// </summary>
    public static class EbayInventory
    {
        /// <summary>
        /// Container for ebay inventories
        /// </summary>
        private static List<InventoryItem> inventoryItems = new List<InventoryItem>();

        /// <summary>
        /// Processes the returning of ebay inventories
        /// </summary>
        /// <returns>Returns ebay inventories</returns>
        public static List<InventoryItem> InventoryItems()
        {
            if (inventoryItems.Count == 0)
            {
                inventoryItems = InventorySource.GetInventoryItems().ToList();
            }

            return inventoryItems;
        }

        /// <summary>
        /// A delegate for the ebay inventories
        /// </summary>
        public static IInventorySource InventorySource;

        /// <summary>
        /// A reading delegate for interaction with the collection
        /// </summary>
        public static IListingDataReader DataReader;

        /// <summary>
        /// A writing delegate for interaction with the collection
        /// </summary>
        public static IListingDataWriter DataWriter;
    }
}
