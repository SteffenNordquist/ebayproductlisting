﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConnectionFactory;

namespace EbayCSV.Core.Inventory.Source
{
    /// <summary>
    /// A factory for inventory database connection
    /// </summary>
    public interface IInventoryDbFactory
    {
        /// <summary>
        /// Processes the work on returning inventory database connection
        /// </summary>
        /// <returns>IDbConnection</returns>
        IDbConnection InventoryDbConnection<T>(T obj);
    }
}
