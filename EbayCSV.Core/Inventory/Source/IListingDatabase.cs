﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory.InventorySource
{
    /// <summary>
    /// Allows interaction for the listing inventory database
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.ListingInventory.BK.BKListingDatabase"/>
    public interface IListingDatabase
    {
        /// <summary>
        /// Works on returning the collection data reader
        /// </summary>
        /// <returns>Returns collection data reader</returns>
        IListingDataReader dataReader();

        /// <summary>
        /// Works on returning the collection data writer
        /// </summary>
        /// <returns>Returns collection data writer</returns>
        IListingDataWriter dataWriter();
    }
}
