﻿using DN.DataAccess.ConnectionFactory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory.InventorySource.ListingInventory.BK
{
    /// <summary>
    /// Allows interaction for the listing inventory database
    /// </summary>
    public class BKListingDatabase : IListingDatabase
    {
        /// <summary>
        /// A delegate that allows interaction with listing inventory datbase
        /// </summary>
        private readonly IDbConnection dbConnection;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="dbConnection">A delegate that allows interaction with listing inventory datbase</param>
        public BKListingDatabase(IDbConnection dbConnection)
        {
            this.dbConnection = dbConnection;
        }

        /// <summary>
        /// Works on returning the collection data reader
        /// </summary>
        /// <returns>Returns collection data reader</returns>
        /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.IListingDatabase.dataReader"/>
        public IListingDataReader dataReader()
        {
            return new BKListingDataReader(dbConnection.DataAccess.Queries);
        }

        /// <summary>
        /// Works on returning the collection data writer
        /// </summary>
        /// <returns>Returns collection data writer</returns>
        /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.IListingDatabase.dataWriter"/>
        public IListingDataWriter dataWriter()
        {
            return new BKListingDataWriter(dbConnection.DataAccess.Commands);
        }
    }
}
