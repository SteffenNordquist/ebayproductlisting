﻿using DN.DataAccess.DataCommand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory.InventorySource.ListingInventory.BK
{
    /// <summary>
    /// Allows writing interaction with listing inventory collection
    /// </summary>
    public class BKListingDataWriter : IListingDataWriter
    {
        /// <summary>
        /// A delegate that allows data commands to the colection
        /// </summary>
        private readonly IDataCommand dataCommand;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="dataCommand">A delegate that allows data commands to the colection</param>
        public BKListingDataWriter(IDataCommand dataCommand)
        {
            this.dataCommand = dataCommand;
        }

        /// <summary>
        /// Inserts new record in the collection
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as InventoryItem</param>
        /// <returns>Returns insert response</returns>
        /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.IListingDataWriter.Insert"/>
        public string Insert<TSource>(TSource tSource)
        {
            Inventory listing = (Inventory)(object)tSource;

            return dataCommand.Insert<Inventory>(listing).ToString();
        }

        /// <summary>
        /// Updates record in the collection
        /// </summary>
        /// <typeparam name="TValue">Parameter type</typeparam>
        /// <param name="query">query string</param>
        /// <param name="updateField">field to be updated</param>
        /// <param name="updateValue">new value</param>
        /// <param name="multi">Indicates if update process if multiple</param>
        /// <param name="upsert">Indicates if update process upserts</param>
        /// <returns>Returns update response</returns>
        /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.IListingDataWriter.Update"/>
        public string Update<TValue>(string query, string updateField, TValue updateValue, bool multi = false, bool upsert = false)
        {
            return dataCommand.Update<TValue>(query, updateField, updateValue, multi, upsert).ToString();
        }

        /// <summary>
        /// Deletes record in the collection
        /// </summary>
        /// <typeparam name="TSource">Parameter type</typeparam>
        /// <param name="tSource">Treated as the query for the delete operation</param>
        /// <returns>Returns delete response</returns>
        /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.IListingDataWriter.Delete"/>
        public string Delete<TSource>(TSource tSource)
        {
            string query = (string)(object)tSource;

            return dataCommand.Delete(query).ToString();
        }
    }
}
