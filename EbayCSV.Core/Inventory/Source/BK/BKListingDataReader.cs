﻿using DN.DataAccess.DataQuery;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory.InventorySource.ListingInventory.BK
{
    /// <summary>
    /// Allows reading interaction with listing inventory collection
    /// </summary>
    public class BKListingDataReader : IListingDataReader
    {
        /// <summary>
        /// A delegate for querying from the collection
        /// </summary>
        private readonly IDataQuery dataQuery;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="dataQuery">A delegate for querying from the collection</param>
        public BKListingDataReader(IDataQuery dataQuery)
        {
            this.dataQuery = dataQuery;
        }

        /// <summary>
        /// Used to query from listing inventory collection
        /// </summary>
        /// <typeparam name="TResult">Return type</typeparam>
        /// <typeparam name="TQuery">Query type</typeparam>
        /// <param name="tQuery">Treated as query string</param>
        /// <returns>Returns list of liting inventories</returns>
        /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.IListingDataReader.Find"/>
        public IEnumerable<TResult> Find<TResult, TQuery>(TQuery tQuery)
        {
            string query = (string)(object)tQuery;

            return dataQuery.Find<TResult>(query);
        }
    }
}
