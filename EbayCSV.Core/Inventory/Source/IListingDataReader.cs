﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory.InventorySource
{
    /// <summary>
    /// Allows reading interaction with listing inventory collection
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Inventory.InventorySource.ListingInventory.BK.BKListingDataReader"/>
    public interface IListingDataReader
    {
        /// <summary>
        /// Used to query from listing inventory collection
        /// </summary>
        /// <typeparam name="TResult">Return type</typeparam>
        /// <typeparam name="TQuery">Query type</typeparam>
        /// <param name="tQuery">Treated as query property</param>
        /// <returns>Returns list of liting inventories</returns>
        IEnumerable<TResult> Find<TResult, TQuery>(TQuery tQuery);
    }
}
