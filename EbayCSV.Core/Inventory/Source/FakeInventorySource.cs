﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConnectionFactory.Fakes;

namespace EbayCSV.Core.Inventory.Source
{
    public class FakeInventorySource : IInventorySource
    {
        public IEnumerable<Inventory> InventoryItems()
        {
            return new List<Inventory>();
        }

        public DN.DataAccess.ConnectionFactory.IDbConnection InventoryDbConnection()
        {
            return new FakeDbConnection();
        }
    }
}
