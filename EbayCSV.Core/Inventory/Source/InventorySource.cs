﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConnectionFactory;

namespace EbayCSV.Core.Inventory.Source
{
    /// <summary>
    /// Allows interaction with ebay inventories
    /// </summary>
    public class InventorySource : IInventorySource
    {
        /// <summary>
        /// inventory db connection
        /// </summary>
        private readonly IDbConnection _inventoryDbConnection;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="inventoryDbConnection">inventory db connection</param>
        public InventorySource(IDbConnection inventoryDbConnection)
        {
            this._inventoryDbConnection = inventoryDbConnection;
        }

        /// <summary>
        /// Processes the returning of ebay inventories
        /// </summary>
        /// <returns>Returns ebay inventories</returns>
        /// <seealso cref="EbayCSV.Core.Inventory.Source.IInventorySource.InventoryItems"/>
        public IEnumerable<Inventory> InventoryItems()
        {
            //true to all
            string query = "{ _id : { $exists: true} }";

            List<Inventory> inventoryList = new List<Inventory>();

            var inventoryInArray = _inventoryDbConnection.DataAccess.Queries.Find<Inventory>(query);

            if (inventoryInArray != null)
            {
                inventoryList = inventoryInArray.ToList();
            }

            return inventoryList;
        }

        /// <summary>
        /// Processes the work on returning listing inventory database connection
        /// </summary>
        /// <returns></returns>
        public IDbConnection InventoryDbConnection()
        {
            return _inventoryDbConnection;
        }
    }
}
