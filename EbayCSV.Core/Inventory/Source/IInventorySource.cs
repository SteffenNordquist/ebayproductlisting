﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConnectionFactory;

namespace EbayCSV.Core.Inventory.Source
{
    /// <summary>
    /// Allows interaction with ebay inventories
    /// </summary>
    /// <seealso cref="EbayCSV.Core.Inventory.Source.InventorySource"/>
    public interface IInventorySource
    {
        /// <summary>
        /// Processes the returning of ebay inventories
        /// </summary>
        /// <returns>Returns ebay inventories</returns>
        IEnumerable<Inventory> InventoryItems();

        /// <summary>
        /// Processes the work on returning listing inventory database connection
        /// </summary>
        /// <returns></returns>
        IDbConnection InventoryDbConnection();
    }
}
