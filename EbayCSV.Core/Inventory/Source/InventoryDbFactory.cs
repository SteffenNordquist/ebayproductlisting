﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using EbayCSV.Core.Settings;

namespace EbayCSV.Core.Inventory.Source
{
    /// <summary>
    /// A factory for inventory database connection
    /// </summary>
    public class InventoryDbFactory : IInventoryDbFactory
    {
        /// <summary>
        /// Container for settings/configurations
        /// </summary>
        private readonly ISettings _settings;

        /// <summary>
        /// creates db connection
        /// </summary>
        private readonly IDbConnectionFactory _dbConnectionFactory;

        /// <summary>
        /// The constructor of this class
        /// </summary>
        /// <param name="settings">Container for settings/configurations</param>
        /// <param name="dbConnectionFactory">creates db connection</param>
        public InventoryDbFactory(ISettings settings, IDbConnectionFactory dbConnectionFactory)
        {
            this._settings = settings;
            this._dbConnectionFactory = dbConnectionFactory;
        }

        /// <summary>
        /// Processes the work on returning listing inventory database connection
        /// </summary>
        /// <typeparam name="T">expectype type for this implementation : type of string</typeparam>
        /// <param name="obj">ebay name</param>
        /// <returns></returns>
        public IDbConnection InventoryDbConnection<T>(T obj)
        {
            if (obj == null)
            {
                throw new ArgumentNullException("obj");
            }

            if (obj.GetType() != typeof (string))
            {
                throw new FormatException("'obj' must be a string");
            }

            string ebayName = (string)(object)obj;

            _dbConnectionFactory.ConnectionProperties.SetProperty("connectionstring", _settings.GetValue("listingdbconnectionstring") + ebayName);
            _dbConnectionFactory.ConnectionProperties.SetProperty("databasename", _settings.GetValue("listingdbdatabasename") + ebayName);
            _dbConnectionFactory.ConnectionProperties.SetProperty("collectionname", _settings.GetValue("listingdbcollectionname"));

            return _dbConnectionFactory.CreateConnection();
        }
    }
}
