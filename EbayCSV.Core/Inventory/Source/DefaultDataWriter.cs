﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Inventory.InventorySource
{
    public class DefaultDataWriter : IListingDataWriter
    {
        public string Insert<TSource>(TSource tSource)
        {
            return "";
        }

        public string Update(string query, string updateField, object updateValue, bool multi = false, bool upsert = false)
        {
            return "";
        }

        public string Delete<TSource>(TSource tSource)
        {
            return "";
        }
    }
}
