﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Inventory.Source;

namespace EbayCSV.Core.Inventory
{
    /// <summary>
    /// Allows interaction with ebay inventory
    /// </summary>
    public static class InventoryStorage
    {
        /// <summary>
        /// Container for ebay inventories
        /// </summary>
        private static List<Inventory> _inventoryItems = new List<Inventory>();

        /// <summary>
        /// Processes the returning of ebay inventories
        /// </summary>
        /// <returns>Returns ebay inventories</returns>
        public static List<Inventory> InventoryItems()
        {
            if (_inventoryItems.Count == 0)
            {
                _inventoryItems = InventorySource.InventoryItems().ToList();
            }

            return _inventoryItems;
        }

        /// <summary>
        /// inventories source delegate
        /// </summary>
        public static IInventorySource InventorySource { get; set; }
    }
}
