﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace InvalidCategories
{
    class Program
    {
        static void Main(string[] args)
        {
            List<string> invalidCategoriesEansWriter = new List<string>();
            File.ReadAllLines("invalidCategoriesEans.txt").ToList().ForEach(x => invalidCategoriesEansWriter.Add(x.Trim()));


            DirectoryInfo currentDirectory = new DirectoryInfo(AppDomain.CurrentDomain.BaseDirectory);

            foreach (string filename in currentDirectory.GetFiles().Select(x => x.FullName).Where(x => x.ToLower().Contains("fileexchange")))
            {
                if (File.Exists(filename))
                {
                    File.ReadAllLines(filename).ToList().Where(x => x.Contains("Failure") && x.Contains("Error - Die Kategorie ist nicht gültig.")).ToList()
                        .ForEach(x => 
                            {
                                if (GetEan(x) != "" && !invalidCategoriesEansWriter.Contains(GetEan(x)))
                                {
                                    invalidCategoriesEansWriter.Add(GetEan(x));
                                }
                            });
                }
            }

            File.WriteAllLines("invalidCategoriesEans.txt", invalidCategoriesEansWriter);
        }

        static string GetEan(string line)
        {
            string pattern = "[0-9]{13}";

            var matches = Regex.Matches(line, pattern);

            if (matches.Count > 0)
            {
                return matches[0].Value;
            }

            return "";

        }
    }
}
