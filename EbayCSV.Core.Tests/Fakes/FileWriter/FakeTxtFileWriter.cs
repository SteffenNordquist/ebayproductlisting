﻿using EbayCSV.Core.FileWriter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Tests.Fakes.FileWriter
{
    public class FakeTxtFileWriter : IFileWriter
    {
        private List<string> generatedData = new List<string>();
 
        public void Write(IEnumerable<CsvItem.ICsvItem> csvItems, string factor1 = "", string factor2 = "")
        {
            generatedData.Add("");
        }

        public void Write(IEnumerable<string> csvLines, string supplierName)
        {
            generatedData.Add("");
        }

        public IEnumerable<string> GeneratedData()
        {
            return generatedData;
        }
    }
}
