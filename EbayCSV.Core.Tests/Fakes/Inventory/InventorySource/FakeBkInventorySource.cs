﻿using EbayCSV.Core.Inventory;
using EbayCSV.Core.Inventory.Source;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConnectionFactory.Fakes;

namespace EbayCSV.Core.Tests.Fakes.Inventory.InventorySource
{
    public class FakeBkInventorySource : IInventorySource
    {
        private List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();

        public FakeBkInventorySource(List<Core.Inventory.Inventory> inventoryItems)
        {
            this.inventoryItems = inventoryItems;
        }

        public IEnumerable<Core.Inventory.Inventory> InventoryItems()
        {
            return inventoryItems;
        }

        public DN.DataAccess.ConnectionFactory.IDbConnection InventoryDbConnection()
        {
            return new FakeDbConnection();
        }
    }
}
