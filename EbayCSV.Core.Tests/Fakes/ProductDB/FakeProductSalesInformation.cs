﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Products.Price;
using ProcuctDB;

namespace EbayCSV.Core.Tests.Fakes.ProductDB
{
    public class FakeProductSalesInformation : IProductSalesInformation
    {
        public string getSupplierName()
        {
            return "Fake";
        }

        public double getPrice()
        {
            return 9999;
        }

        public int stock = 0;
        public int getStock()
        {
            return stock;
        }

        public string GetTitle()
        {
            return "fake product";
        }

        public List<string> getTitle()
        {
            List<string> titles = new List<string>();
            titles.Add(GetTitle());
            return titles;
        }

        public List<string> GetImageUrlList()
        {
            List<string> titles = new List<string>();
            titles.Add(GetTitle());
            return titles;
        }

        public List<string> images = new List<string>(); 
        public List<string> GetMainImage()
        {
            return images;
        }

        public string GetEan()
        {
            return "12345";
        }

        public DN_Classes.Products.Price.PriceType getPriceType()
        {
            return PriceType.fixedPrice;
        }

        public Dictionary<string, List<string>> GetAttributeList()
        {
            throw new NotImplementedException();
        }

        public List<string> getmanufacturer()
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, string> GetTextDictionary()
        {
            throw new NotImplementedException();
        }

        public string getArticleNumberforEbay()
        {
            return "art123";
        }

        public DN_Classes.Classes.ContentWeight GetContentWeight()
        {
            throw new NotImplementedException();
        }
    }
}
