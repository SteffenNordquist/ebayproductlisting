﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ProcuctDB;

namespace EbayCSV.Core.Tests.Fakes.ProductDB
{
    public class FakeSupplierEans : ISupplierEans
    {
        public IEnumerable<string> GetEans()
        {
            return eans;
        }

        public List<string> eans = new List<string>(); 
    }
}
