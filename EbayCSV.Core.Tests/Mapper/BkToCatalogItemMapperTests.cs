﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConfigurationData;
using DN_Classes.Agent;
using DN_Classes.Logger;
using EbayCSV.Core.CsvItem;
using EbayCSV.Core.Mapper;
using EbayListingV2.IOCConfigurations;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProcuctDB;
using ProcuctDB.Jpc;

namespace EbayCSV.Core.Tests.Mapper
{
    [TestClass]
    public class BkToCatalogItemMapperTests
    {
        [TestMethod]
        public void RandomTests()
        {
            string configFile = AppDomain.CurrentDomain.BaseDirectory + @"\AppConfiguration.config";
            IConfigurationSource configSource = new ConfigurationSource(configFile);
            IConfigurationManager appConfiguration = new AppConfigurationManager(configSource);

            var jpcDb = new JpcDB();

            EbayCsvConfiguration csvConfiguration = new EbayCsvConfiguration(jpcDb, new Agent(), appConfiguration);

            var jpcProduct = jpcDb.GetMongoProductSalesInformationByEan("4029759040729");

            ILogFileLocation logFileLocation = new DefaultLogFileLocation { LogDirectory = appConfiguration.GetValue("LogDirectory"), ToolName = "EbayListing", SubToolName = "EbayListing" };
            LoggingConfig loggingConfig = new LoggingConfig();
            loggingConfig.Register(logFileLocation);

            using (ILogger logger = loggingConfig.Resolve<ILogger>())
            {
                var mapper = csvConfiguration.Resolve<IMapper>();

                mapper.MapItem<ICsvItem, IProductSalesInformation>(jpcProduct);
            }
        }
    }
}
