﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EbayCSV.Core.Category;
using EbayCSV.Core.Category.Factory;
using EbayCSV.Core.Factories.CategoryFactory;
using EbayCSV.Core.Settings;

namespace EbayCSV.Core.Tests.Factories.Category
{
    [TestClass]
    public class BKCategoryFactoryTests
    {
        private ICategoriesDatabase categoriesDatabase;

        [TestInitialize]
        public void Init()
        {
            var settings = new DefaultSettings();
            settings.SetValue("ebaycategoriesconnectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/EbayCategories");
            settings.SetValue("ebaycategoriesdatabasename", "EbayCategories");
            settings.SetValue("ebaycategoriescollectionname", "ebaycategoriespaths");

            var dbFactory = new CategoriesDatabaseFactory(settings);
            this.categoriesDatabase = dbFactory.GetCategoriesDatabase();
        }

        public void GetCategory_WithSetEan_HasCategory()
        {
            var categoryFactory = new BKCategoryFactory(categoriesDatabase);

            Assert.IsTrue(!String.IsNullOrEmpty(categoryFactory.GetCategory<string>("", "4010168030630")));
        }

        public void GetCategory_WithSetUnknownEan_HasNoCategory()
        {
            var categoryFactory = new BKCategoryFactory(categoriesDatabase);

            Assert.IsTrue(String.IsNullOrEmpty(categoryFactory.GetCategory<string>("", "this_is_unknown_ean")));
        }
    }
}
