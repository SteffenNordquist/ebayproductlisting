﻿using EbayCSV.Core.Factories.TemplateFactory;
using EbayCSV.Core.Settings;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCSV.Core.Tests.Factories.TemplateFactory
{
    [TestClass]
    public class BKTemplateFactoryTests
    {
        [TestMethod]
        public void GetMapper_WithCatalogSupplier_ReturnsCatalogTemplate()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("catalogSuppliers", "jpc|knv|jtl");
            BkTemplateFactory templateFactory = new BkTemplateFactory(settings);

            TemplateType templateType = templateFactory.GetMapper<string>("jpc");

            Assert.AreEqual(TemplateType.catalog, templateType);
        }
    }
}
