﻿using System;
using EbayCSV.Core.Factories.Supplier;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProcuctDB.Haba;
using ProcuctDB.JTL;

namespace EbayCSV.Core.Tests.Factories.Supplier
{
    [TestClass]
    public class SupplierFactoryTests
    {
        [TestMethod]
        public void GetSupplierName_WithSetHabaSupplier_ReturnsHaba()
        {
            var product = new HabaEntity();
            var supplierFactory = new SupplierFactory();

            string supplierName = supplierFactory.GetSupplierName(product);

            Assert.IsTrue(supplierName.ToLower().Contains("haba"));
        }

        [TestMethod]
        public void GetSupplierName_WithSetJtlSupplierWithSpecificSupplier_ReturnsSetSpecificSupplier()
        {
            var innertproduct = new JTLEntity.Product { supplier = "specific supplier" };
            var product = new JTLEntity { product = innertproduct};

            var supplierFactory = new SupplierFactory();

            string supplierName = supplierFactory.GetSupplierName(product);

            Assert.IsTrue(supplierName.ToLower().Contains("specific supplier"));
        }
    }
}
