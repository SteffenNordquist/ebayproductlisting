﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Logger;
using DN_Classes.Logger.Fakes;
using EbayCSV.Core.Factories.ImageFactory;
using EbayCSV.Core.Settings;
using EbayCSV.Core.Tests.Fakes.ProductDB;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EbayCSV.Core.Tests.Factories.Image
{
    [TestClass]
    public class BkItemImageFactoryTests
    {
        private ISettings settings;

        [TestInitialize]
        public void Init()
        {
            settings = new DefaultSettings();
            settings.SetValue("imageforwarderlink", "http://78.31.64.165/ImageForwarder/");//francesco forwarder link
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetImage_WithNullParam_ThrowsArgumentNullException()
        {
            BkItemImageFactory imageFactory = new BkItemImageFactory(settings);
            imageFactory.GetPicUrls<string>(null);
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void GetImage_WithInvalidParamType_ThrowsFormatException()
        {
            BkItemImageFactory imageFactory = new BkItemImageFactory(settings);
            imageFactory.GetPicUrls("");

        }

        [TestMethod]
        public void GetImage_WithLiveImage_ReturnsListGreaterZero()
        {
            BkItemImageFactory imageFactory = new BkItemImageFactory(settings);

            FakeProductSalesInformation fakeProductSalesInformation = new FakeProductSalesInformation();
            fakeProductSalesInformation.images.Add("4010168203959_1.jpg");

            Assert.IsTrue(imageFactory.GetPicUrls(fakeProductSalesInformation).Any());

        }

        [TestMethod]
        public void GetImage_WithUnknownImage_ReturnsEmptyList()
        {
            AppLogger.SetLogger(new FakeLogger());

            BkItemImageFactory imageFactory = new BkItemImageFactory(settings);

            FakeProductSalesInformation fakeProductSalesInformation = new FakeProductSalesInformation();
            fakeProductSalesInformation.images.Add("randomSampleImage.jpg");

            Assert.IsTrue(!imageFactory.GetPicUrls(fakeProductSalesInformation).Any());

        }
    }
}
