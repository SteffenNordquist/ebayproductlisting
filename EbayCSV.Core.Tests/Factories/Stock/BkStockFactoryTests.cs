﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Factories.StockFactory;
using EbayCSV.Core.Tests.Fakes.ProductDB;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProcuctDB;

namespace EbayCSV.Core.Tests.Factories.Stock
{
    [TestClass]
    public class BkStockFactoryTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void GetStock_WithNullParam_ThrowsArgumentNullException()
        {
            BKStockFactory bkStockFactory = new BKStockFactory();
            bkStockFactory.GetStock<IProductSalesInformation>(null);
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void GetStock_WithWrongParam_ThrowsFormatException()
        {
            BKStockFactory bkStockFactory = new BKStockFactory();
            bkStockFactory.GetStock("");
        }

        [TestMethod]
        public void GetStock_WithSetData_ReturnsExpectedStock()
        {
            FakeProductSalesInformation fakseProductSalesInformation = new FakeProductSalesInformation();
            fakseProductSalesInformation.stock = 4;

            BKStockFactory bkStockFactory = new BKStockFactory();
            int stock = bkStockFactory.GetStock(fakseProductSalesInformation);

            Assert.IsTrue(stock == 3);
        }
    }
}
