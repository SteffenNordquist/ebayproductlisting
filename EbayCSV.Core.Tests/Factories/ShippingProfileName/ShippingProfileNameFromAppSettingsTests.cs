﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Factories.ShippingProfileName;
using EbayCSV.Core.Settings;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EbayCSV.Core.Tests.Factories.ShippingProfileName
{
    [TestClass]
    public class ShippingProfileNameFromAppSettingsTests
    {
        [TestMethod]
        public void GetShippingProfileName_WithUnsetSettingValue_ReturnsEmpty()
        {
            ISettings settings = new DefaultSettings();
            ShippingProfileNameFromAppSettings shippingProfileNameFact = new ShippingProfileNameFromAppSettings(settings);

            Assert.IsTrue(shippingProfileNameFact.GetShippingProfileName() == string.Empty);

        }

        [TestMethod]
        public void GetShippingProfileName_WtihSetSettingsValue_ReturnsSetValue()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("shippingProfileName","testShippingProfileName");
            ShippingProfileNameFromAppSettings shippingProfileNameFact = new ShippingProfileNameFromAppSettings(settings);

            Assert.IsTrue(shippingProfileNameFact.GetShippingProfileName() == "testShippingProfileName");
        }
    }
}
