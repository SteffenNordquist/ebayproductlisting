﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Factories.SalesTaxPercent;
using EbayCSV.Core.Tests.Fakes.ProductDB;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProcuctDB;

namespace EbayCSV.Core.Tests.Factories.SalesTaxPercent
{
    [TestClass]
    public class ProductSalesTaxPercentTests
    {
        [TestMethod]
        [ExpectedException(typeof (ArgumentNullException))]
        public void SalesTax_WithNullParam_ThrowsArgumentNullExeption()
        {
            ISalesTaxPercent salesTaxPercent = new ProductSalesTaxPercent();
            salesTaxPercent.SalesTax<string>(null);
        }

        [TestMethod]
        [ExpectedException(typeof(FormatException))]
        public void SalesTax_WithInvalidParamType_ThrowsFormatException()
        {
            ISalesTaxPercent salesTaxPercent = new ProductSalesTaxPercent();
            salesTaxPercent.SalesTax<double>(0);
        }

        [TestMethod]
        public void SalesTax_WithSetProductTax_ReturnsSame()
        {
            FakeProduct fakseProduct = new FakeProduct();
            fakseProduct.Tax = 19;
            ISalesTaxPercent salesTaxPercent = new ProductSalesTaxPercent();
            int tax = salesTaxPercent.SalesTax<IProduct>(fakseProduct);

            Assert.AreEqual(tax, 19);
        }
    }
}
