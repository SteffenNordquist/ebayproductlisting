﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EbayCSV.Core.Factories.SellpriceFactory;
using EbayCSV.Core.Factories.ShippingService1CostFactory;
using EbayCSV.Core.Settings;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.Tests.Fakes.Inventory.InventorySource;
using System.Collections.Generic;

namespace EbayCSV.Core.Tests.Factories.SellpriceFactory
{
    [TestClass]
    public class BKSellpriceDeductedByShippingFactoryTests
    {
        [TestMethod]
        public void GetSellprice_WithSetCostValue_ReturnsExpectedValue()
        {
            List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();
            inventoryItems.Add(new Core.Inventory.Inventory
            {
                Wanted = new WantedClass{ Price = 18.45},
                Ean = "12345",
                SupplierName = "TestSupplier"
            });
            InventoryStorage.InventorySource = new FakeBkInventorySource(inventoryItems);

            ISettings settings = new DefaultSettings();
            settings.SetValue("ShippingService1Cost", "2.9");
            IShippingService1CostFactory shippingService1CostFactory = new GlobalShippingService1CostFactory(settings);

            ISellpriceFactory sellpriceDeductedByShippingFactory = new BKSellpriceDeductedByShippingFactory(shippingService1CostFactory);

            double sellPrice = sellpriceDeductedByShippingFactory.GetSellprice<string>("12345", "TestSupplier");
        
            //18.45 - 2.9 = 15.55
            Assert.AreEqual(15.55, Math.Round(sellPrice, 2));

        }
    }
}
