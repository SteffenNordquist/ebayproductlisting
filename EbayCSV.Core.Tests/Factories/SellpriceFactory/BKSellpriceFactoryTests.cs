﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EbayCSV.Core.Inventory;
using System.Collections.Generic;
using EbayCSV.Core.Tests.Fakes.Inventory.InventorySource;
using EbayCSV.Core.Factories.SellpriceFactory;

namespace EbayCSV.Core.Tests.Factories.SellpriceFactory
{
    [TestClass]
    public class BKSellpriceFactoryTests
    {
        [TestMethod]
        public void GetSellprice_WithSetPrice_ReturnsExpectedValue()
        {
            List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();
            inventoryItems.Add(new Core.Inventory.Inventory
            {
                Wanted = new WantedClass { Price = 18.45 },
                Ean = "12345",
                SupplierName = "TestSupplier"
            });
            InventoryStorage.InventorySource = new FakeBkInventorySource(inventoryItems);

            ISellpriceFactory sellpriceFactory = new BKSellpriceFactory();

            double sellPrice = sellpriceFactory.GetSellprice<string>("12345", "TestSupplier");


            Assert.AreEqual(18.45, Math.Round(sellPrice, 2));

        }
    }
}
