﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Factories.ReturnProfileName;
using EbayCSV.Core.Factories.ShippingProfileName;
using EbayCSV.Core.Settings;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EbayCSV.Core.Tests.Factories.ReturnProfileName
{
    [TestClass]
    public class ReturnProfileNameFromAppSettingsTests
    {
        [TestMethod]
        public void GetReturnProfileName_WithUnsetSettingValue_ReturnsEmpty()
        {
            ISettings settings = new DefaultSettings();
            ReturnProfileNameFromAppSettings profileNameFact = new ReturnProfileNameFromAppSettings(settings);

            Assert.IsTrue(profileNameFact.GetReturnProfileName() == string.Empty);

        }

        [TestMethod]
        public void GetReturnProfileName_WtihSetSettingsValue_ReturnsSetValue()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("returnProfileName", "testReturnProfileName");
            ReturnProfileNameFromAppSettings profileNameFact = new ReturnProfileNameFromAppSettings(settings);

            Assert.IsTrue(profileNameFact.GetReturnProfileName() == "testReturnProfileName");
        }
    }
}
