﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Factories.PaymentProfileName;
using EbayCSV.Core.Factories.ReturnProfileName;
using EbayCSV.Core.Settings;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EbayCSV.Core.Tests.Factories.PaymentProfileName
{
    [TestClass]
    public class PaymentProfileNameFromAppSettingsTests
    {
        [TestMethod]
        public void GetPaymentProfileName_WithUnsetSettingValue_ReturnsEmpty()
        {
            ISettings settings = new DefaultSettings();
            PaymentProfileNameFromAppSettings profileNameFact = new PaymentProfileNameFromAppSettings(settings);

            Assert.IsTrue(profileNameFact.GetPaymentProfileName() == string.Empty);

        }

        [TestMethod]
        public void GetPaymentProfileName_WtihSetSettingsValue_ReturnsSetValue()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("paymentProfileName", "testPaymentProfileName");
            PaymentProfileNameFromAppSettings profileNameFact = new PaymentProfileNameFromAppSettings(settings);

            Assert.IsTrue(profileNameFact.GetPaymentProfileName() == "testPaymentProfileName");
        }
    }
}
