﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EbayCSV.Core.Settings;
using EbayCSV.Core.Factories.ShippingService1CostFactory;

namespace EbayCSV.Core.Tests.Factories.ShippingService1CostFactory
{
    [TestClass]
    public class GlobalShippingService1CostFactoryTests
    {
        [TestMethod]
        public void GetShippingService1Cost_WithSetCostValue_ReturnsSameValue()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("ShippingService1Cost", "2.9");
            IShippingService1CostFactory shippingService1CostFactory = new GlobalShippingService1CostFactory(settings);

            double shippingCost = shippingService1CostFactory.GetShippingService1Cost<string>("");

            Assert.AreEqual(2.9, shippingCost);

        }
    }
}
