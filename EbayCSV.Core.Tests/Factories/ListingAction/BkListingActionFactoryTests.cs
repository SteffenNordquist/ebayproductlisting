﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Logger;
using DN_Classes.Logger.Fakes;
using EbayCSV.Core.Factories.ListingActionFactory;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.ListingAction;
using EbayCSV.Core.Settings;
using EbayCSV.Core.Tests.Fakes.Inventory.InventorySource;

namespace EbayCSV.Core.Tests.Factories.ListingAction
{
    [TestClass]
    public class BkListingActionFactoryTests
    {
        [TestInitialize]
        public void Init()
        {
            AppLogger.SetLogger(new FakeLogger());
        }

        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void ListingActionType_WithNullParams_ThrowsArgumentNullException()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("revisionOnly", "false");

            IListingActionFactory listingActionFactory = new BkListingActionFactory(settings);
            listingActionFactory.ListingActionType(null, 0, 0, null);
        }

        [TestMethod]
        public void ListingActionType_ListingStartPriceNotEqualToWantedPrice_ReturnsReviseActionType()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("revisionOnly", "false");

            List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();
            inventoryItems.Add(new Core.Inventory.Inventory
            {
                Ean = "12345",
                SupplierName = "TestSupplier",
                ItemID = "54321",
                StartPrice = 15.43,
                Quantity = 3
            });
            InventoryStorage.InventorySource = new FakeBkInventorySource(inventoryItems);

            IListingActionFactory listingActionFactory = new BkListingActionFactory(settings);
            ActionType actionType = listingActionFactory.ListingActionType("12345", 2.8, 3, "TestSupplier");

            Assert.IsTrue(actionType == ActionType.Revise);
        }

        [TestMethod]
        public void ListingActionType_ListingQuantityNotEqualToWantedQuantity_ReturnsReviseActionType()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("revisionOnly", "false");

            List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();
            inventoryItems.Add(new Core.Inventory.Inventory
            {
                Ean = "12345",
                SupplierName = "TestSupplier",
                ItemID = "54321",
                StartPrice = 15.43,
                Quantity = 3
            });
            InventoryStorage.InventorySource = new FakeBkInventorySource(inventoryItems);

            IListingActionFactory listingActionFactory = new BkListingActionFactory(settings);
            ActionType actionType = listingActionFactory.ListingActionType("12345", 15.43, 1, "TestSupplier");

            Assert.IsTrue(actionType == ActionType.Revise);
        }

        [TestMethod]
        public void ListingActionType_InventoryItemDontHaveItemID_ReturnsAddActionType()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("revisionOnly", "false");

            List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();
            inventoryItems.Add(new Core.Inventory.Inventory
            {
                Wanted = new WantedClass { Price = 15.43, Quantity = 3 },
                Ean = "12345",
                SupplierName = "TestSupplier"
            });
            InventoryStorage.InventorySource = new FakeBkInventorySource(inventoryItems);

            IListingActionFactory listingActionFactory = new BkListingActionFactory(settings);
            ActionType actionType = listingActionFactory.ListingActionType("12345", 2.8, 4, "TestSupplier");

            Assert.IsTrue(actionType == ActionType.Add);
        }

        [TestMethod]
        public void ListingActionType_InventoryItemDontHaveItemIDWithRevisionOnlySet_ReturnsNoneActionType()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("revisionOnly", "true");

            List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();
            inventoryItems.Add(new Core.Inventory.Inventory
            {
                Wanted = new WantedClass { Price = 15.43, Quantity = 3 },
                Ean = "12345",
                SupplierName = "TestSupplier"
            });
            InventoryStorage.InventorySource = new FakeBkInventorySource(inventoryItems);

            IListingActionFactory listingActionFactory = new BkListingActionFactory(settings);
            ActionType actionType = listingActionFactory.ListingActionType("12345", 2.8, 4, "TestSupplier");

            Assert.IsTrue(actionType == ActionType.None);
        }

        [TestMethod]
        public void ListingActionType_DoesNotExistsInInventoryDB_ReturnsAddActionType()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("revisionOnly", "false");

            List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();
            InventoryStorage.InventorySource = new FakeBkInventorySource(inventoryItems);

            IListingActionFactory listingActionFactory = new BkListingActionFactory(settings);
            ActionType actionType = listingActionFactory.ListingActionType("12345", 2.8, 4, "TestSupplier");

            Assert.IsTrue(actionType == ActionType.Add);
        }

        [TestMethod]
        public void ListingActionType_DoesNotExistsInInventoryDBWithRevisionOnlySet_ReturnsNoneActionType()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("revisionOnly", "true");

            List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();
            InventoryStorage.InventorySource = new FakeBkInventorySource(inventoryItems);

            IListingActionFactory listingActionFactory = new BkListingActionFactory(settings);
            ActionType actionType = listingActionFactory.ListingActionType("12345", 2.8, 4, "TestSupplier");

            Assert.IsTrue(actionType == ActionType.None);
        }

        [TestMethod]
        public void ListingActionType_ListingStartPriceAndQuantityEqualToWantedPriceAndQuantity_ReturnsNoneActionType()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("revisionOnly", "false");

            List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();
            inventoryItems.Add(new Core.Inventory.Inventory
            {
                Ean = "12345",
                SupplierName = "TestSupplier",
                ItemID = "54321",
                StartPrice = 15.43,
                Quantity = 3
            });
            InventoryStorage.InventorySource = new FakeBkInventorySource(inventoryItems);

            IListingActionFactory listingActionFactory = new BkListingActionFactory(settings);
            ActionType actionType = listingActionFactory.ListingActionType("12345", 15.43, 3, "TestSupplier");

            Assert.IsTrue(actionType == ActionType.None);
        }

        [TestMethod]
        public void ListingActionType_ListingStartPriceAndQuantityEqualToWantedPriceAndQuantityWithForceRevision_ReturnsReviseActionType()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("revisionOnly", "false");

            List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();
            inventoryItems.Add(new Core.Inventory.Inventory
            {
                Ean = "12345",
                SupplierName = "TestSupplier",
                ItemID = "54321",
                StartPrice = 15.43,
                Quantity = 3,
                ForceRevision = true
            });
            InventoryStorage.InventorySource = new FakeBkInventorySource(inventoryItems);

            IListingActionFactory listingActionFactory = new BkListingActionFactory(settings);
            ActionType actionType = listingActionFactory.ListingActionType("12345", 15.43, 3, "TestSupplier");

            Assert.IsTrue(actionType == ActionType.Revise);
        }

        [TestMethod]
        public void ListingActionType_ListingStartPriceAndQuantityEqualToWantedPriceAndQuantityWithForceRevisionFalse_ReturnsNoneActionType()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("revisionOnly", "false");

            List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();
            inventoryItems.Add(new Core.Inventory.Inventory
            {
                Ean = "12345",
                SupplierName = "TestSupplier",
                ItemID = "54321",
                StartPrice = 15.43,
                Quantity = 3,
                ForceRevision = false
            });
            InventoryStorage.InventorySource = new FakeBkInventorySource(inventoryItems);

            IListingActionFactory listingActionFactory = new BkListingActionFactory(settings);
            ActionType actionType = listingActionFactory.ListingActionType("12345", 15.43, 3, "TestSupplier");

            Assert.IsTrue(actionType == ActionType.None);
        }
    }
}
