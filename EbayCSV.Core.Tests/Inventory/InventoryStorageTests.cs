﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Inventory;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EbayCSV.Core.Tests.Inventory
{
    [TestClass]
    public class InventoryStorageTests
    {
        [TestMethod]
        [ExpectedException(typeof (NullReferenceException))]
        public void InventoryItems_WithInventorySourceNotSet_ThrowsNullReferenceException()
        {
            InventoryStorage.InventoryItems();

        }
    }
}
