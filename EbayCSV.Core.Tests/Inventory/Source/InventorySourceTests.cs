﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConnectionFactory.Fakes;
using EbayCSV.Core.Inventory.Source;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EbayCSV.Core.Tests.Inventory.Source
{
    [TestClass]
    public class InventorySourceTests
    {
        [TestMethod]
        public void InventoryItems_ReturnsSomethingOrNoException()
        {
            IInventorySource inventorySource = new InventorySource(new FakeDbConnection());
            var something = inventorySource.InventoryItems();
        }
    }
}
