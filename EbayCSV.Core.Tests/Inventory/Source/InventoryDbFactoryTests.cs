﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConnectionFactory.Fakes;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.Inventory.Source;
using EbayCSV.Core.Settings;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace EbayCSV.Core.Tests.Inventory.Source
{
    [TestClass]
    public class InventoryDbFactoryTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void InventoryDbConnection_WithNullParam_ThrowsArgumentNullException()
        {
            IInventoryDbFactory inventoryDbFactory = new InventoryDbFactory(new DefaultSettings(), new FakeConnectionFactory());
            inventoryDbFactory.InventoryDbConnection<string>(null);
        }

        [TestMethod]
        [ExpectedException(typeof (FormatException))]
        public void InventoryDbConnection_WithInvalidTypeParam_ThrowsFormatException()
        {
            IInventoryDbFactory inventoryDbFactory = new InventoryDbFactory(new DefaultSettings(), new FakeConnectionFactory());
            inventoryDbFactory.InventoryDbConnection<double>(0);
        }

        [TestMethod]
        public void InventoryDbConnection_WithValidParamAndType_NoException()
        {
            IInventoryDbFactory inventoryDbFactory = new InventoryDbFactory(new DefaultSettings(), new FakeConnectionFactory());

            try
            {
                inventoryDbFactory.InventoryDbConnection<string>("");
                Assert.IsTrue(true);
            }catch
            {
                Assert.Fail();
            }
        
        }
    }
}
