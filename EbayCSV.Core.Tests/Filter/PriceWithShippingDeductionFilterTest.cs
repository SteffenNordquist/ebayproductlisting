﻿using System;
using EbayCSV.Core.Blacklist;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using ProcuctDB;
using ProcuctDB.Knv;
using EbayCSV.Core.Inventory;
using System.Collections.Generic;
using EbayCSV.Core.Settings;
using EbayCSV.Core.Tests.Fakes.Inventory.InventorySource;
using EbayCSV.Core.Factories.ShippingService1CostFactory;
using EbayCSV.Core.Factories.SellpriceFactory;
using EbayCSV.Core.Filter;

namespace EbayCSV.Core.Tests.Filter
{
    [TestClass]
    public class PriceWithShippingDeductionFilterTest
    {
        [TestMethod]
        public void IsOkay_WithSetValues_CalculatedSellPrice_EqualToWantedPrice()
        {
            List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();
            inventoryItems.Add(new Core.Inventory.Inventory
            {
                Wanted = new WantedClass { Price = 15.43 },
                Ean = "12345",
                SupplierName = "TestSupplier"
            });
            InventoryStorage.InventorySource = new FakeBkInventorySource(inventoryItems);

            ISettings settings = new DefaultSettings();
            settings.SetValue("ShippingService1Cost", "2.9");
            IShippingService1CostFactory shippingService1CostFactory = new GlobalShippingService1CostFactory(settings);

            ISellpriceFactory sellpriceDeductedByShippingFactory = new BKSellpriceDeductedByShippingFactory(shippingService1CostFactory);

            double sellPrice = sellpriceDeductedByShippingFactory.GetSellprice<string>("12345", "TestSupplier");

            double finalSellprice = sellPrice +
                                    shippingService1CostFactory.GetShippingService1Cost<string>("");


            PriceFilter priceFilter = new PriceFilter();
            bool okay = priceFilter.IsOkay<string, string, double>("12345", "TestSupplier", finalSellprice);

            Assert.IsTrue(okay);
        }
    }
}
