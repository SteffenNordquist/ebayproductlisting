﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using EbayCSV.Core.Generator.EndListing;
using EbayCSV.Core.Tests.Fakes.ProductDB;
using EbayCSV.Core.Inventory;
using System.Collections.Generic;
using System.Linq;
using DN_Classes.Logger;
using DN_Classes.Logger.Fakes;
using EbayCSV.Core.FileWriter;
using EbayCSV.Core.Settings;
using EbayCSV.Core.Tests.Fakes.FileWriter;
using EbayCSV.Core.Tests.Fakes.Inventory.InventorySource;

namespace EbayCSV.Core.Tests.Generator.EndListing
{
    [TestClass]
    public class BkEndListingsTests
    {
        [TestMethod]
        [ExpectedException(typeof(ArgumentNullException))]
        public void Generate_WithNullParam_ThrowsArgumentNullException()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("endUnexistingListings", "true");
            IEndListings endListings = new BkEndListings(new FakeSupplierEans(), settings);
            
            endListings.Generate<string,string,string>(null,null,"");

        }

        [TestMethod]
        public void Generate_WithSetToGenerateData_GeneratedDataCountGreaterZero()
        {
            AppLogger.SetLogger(new FakeLogger());

            FakeSupplierEans fakeSupplierEans = new FakeSupplierEans();
            fakeSupplierEans.eans.Add("54321");
            ISettings settings = new DefaultSettings();
            settings.SetValue("endUnexistingListings", "true");
            IEndListings endListings = new BkEndListings(fakeSupplierEans, settings);

            List<Core.Inventory.Inventory> inventoryItems = new List<Core.Inventory.Inventory>();
            inventoryItems.Add(new Core.Inventory.Inventory
            {
                Ean = "12345",
                SupplierName = "TestSupplier",
                ItemID = "6789",
                ListingStatus = "Active"
            });
            InventoryStorage.InventorySource = new FakeBkInventorySource(inventoryItems);

            FakeTxtFileWriter fakeTxtFileWriter = new FakeTxtFileWriter();
            endListings.Generate<IFileWriter, string, string>(fakeTxtFileWriter, "TestSupplier", "");

            Assert.IsTrue(fakeTxtFileWriter.GeneratedData().Any());

            
        }
    }
}
