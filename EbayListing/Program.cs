﻿
using DN_Classes.Agent;
using DN_Classes.AppStatus;
using DNEbaySystem.EbayAgentSuppliers;
using DNEbaySystem.Lister;
using DNEbaySystem.ListingUploader;
using DNEbaySystem.ListingUploader.UploadResponseManager;
using DNEbaySystem.Logging;
using Microsoft.Practices.Unity;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayListing
{
    class Program
    {
        static void Main(string[] args)
        {
            ///sampleids
            ///liefer 14
            ///berliner 12
            ///protect 9
            ///allesauflager 0
            ///
            //using (ApplicationStatus appStatus = new ApplicationStatus("EbayListing-LieferLucas"))
            //using (ApplicationStatus appStatus = new ApplicationStatus("EbayListing-ProtectThisHouse"))
            //using (ApplicationStatus appStatus = new ApplicationStatus("EbayListing-AllesAufLager"))
            //using (ApplicationStatus appStatus = new ApplicationStatus("EbayListing-Berliner"))
            using (ApplicationStatus appStatus = new ApplicationStatus("EbayListing_Mediendo"))
            {

                //uncomment when testing
                //ProductBrowsing.SetLocalBrowsing();
                try
                {
                    Agent agent = new Agent(15);
                    AgentSuppliers agentSuppliers = new AgentSuppliers(agent);
                    List<IDatabase> supplierDatabaseList = agentSuppliers.GetList();

                    foreach (IDatabase supplierDatabase in supplierDatabaseList)
                    {
                        EbayLister ebayLister = new EbayLister(supplierDatabase, agent);
                        //ebayLister.Upload();

                        IListingUploader listingUploader = new FileExchangeListingUploader(agent.agentEntity.agent.keys.ebay.token);

                        string supplierName = supplierDatabase.GetType().Name.Replace("DB", "");
                        IUploadRespManager uploadRespManager = new BasicUploadRespManager(supplierName, agent.agentEntity.agent.agentID);


                        EbayUploader ebayUploader = new EbayUploader(ebayLister, listingUploader, uploadRespManager);
                        ebayUploader.BeginUpload();

                        appStatus.AddMessagLine(supplierDatabase.GetType().Name + "</br>" + SystemLogging.Error.GetLogs() + "</br>");
                        File.WriteAllText("log.txt", SystemLogging.Normal.GetLogs());

                    }

                    appStatus.Successful();
                    appStatus.AddMessagLine("Success");
                }
                catch(Exception ex) {
                    appStatus.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
                }

            }
        }
    }
}
