﻿using EbayCSV.Core.Inventory;
using ProcuctDB;
using ProcuctDB.JTL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using DN_Classes.Logger;

namespace EbaySellpricesPrepare
{
    public class SellpricePersistent : ISellpricePersistent
    {
        public void Save<T>(T productInfo, double calculatedValue)
        {
            IProductSalesInformation product = (IProductSalesInformation)productInfo;

            WantedClass wanted = new WantedClass { Price = calculatedValue, Quantity = product.getStock() };

            string updateQuery = " { _id : \"" + product.GetEan() + "\" } ";

            string supplierName = product.getSupplierName();
            if (product.GetType().Name.ToLower().StartsWith("jtl"))
            {
                JTLEntity bkJtlProduct = (JTLEntity)product;
                supplierName = bkJtlProduct.product.supplier;
            }
            //ThreadStart updateThread = delegate 
            //{ 
            InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<WantedClass>(updateQuery, "Wanted", wanted, false, true);
            InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<string>(updateQuery, "Ean", product.GetEan());
            InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<string>(updateQuery, "SupplierName", supplierName);
            InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<DateTime>(updateQuery, "plus.PriceCalculation", DateTime.Now);
            //};
            //new Thread(updateThread).Start();

            AppLogger.Info("Persisted price " + calculatedValue + ", " + product.GetEan());

        }
    }
}
