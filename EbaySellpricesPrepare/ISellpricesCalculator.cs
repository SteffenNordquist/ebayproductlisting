﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbaySellpricesPrepare
{
    public interface ISellpricesCalculator
    {
        double Calculate<T>(T value);
    }
}
