﻿
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN_Classes.Logger;
using EbayCSV.Core.Inventory;

namespace EbaySellpricesPrepare
{
    /// <summary>
    /// updates shipping cost to inventory
    /// </summary>
    public class ShippingCostsPersistent : IShippingCostsPersistent
    {
        /// <summary>
        /// updates shipping cost to inventory
        /// </summary>
        /// <typeparam name="T">type of IProductSalesInformation</typeparam>
        /// <param name="productInfo">IProductSalesInformation instance</param>
        /// <param name="shippingCost">shipping cost</param>
        public void Save<T>(T productInfo, double shippingCost)
        {
            IProductSalesInformation product = (IProductSalesInformation)productInfo;

            string updateQuery = " { _id : \"" + product.GetEan() + "\" } ";

            //ThreadStart updateThread = delegate 
            //{ 
            InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<double>(updateQuery, "plus.shippingCosts", shippingCost);
            //};
            //new Thread(updateThread).Start();

            AppLogger.Info("Persisted price " + shippingCost + ", " + product.GetEan());
        }
    }
}
