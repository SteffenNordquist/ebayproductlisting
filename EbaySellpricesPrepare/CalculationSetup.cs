﻿using DN_Classes.Agent;
using EbayCSV.Core.HtmlData.BK;
using EbayCSV.Core.Settings;
using EbaySellpricesPrepare.DefaultPriceCalculation;
using Microsoft.Practices.Unity;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory.Mongo;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.Inventory.Source;

namespace EbaySellpricesPrepare
{
    public class CalculationSetup
    {
        private readonly IUnityContainer _container;
        private readonly IDatabase _supplierDb;
        private readonly Agent _agent;
        private readonly IConfigurationManager _appConfiguration;

        public CalculationSetup(Agent agent, IDatabase database, IConfigurationManager appConfigurationManager)
        {
            this._container = new UnityContainer();
            this._agent = agent;
            this._supplierDb = database;
            this._appConfiguration = appConfigurationManager;

            RegisterDependencies();
        }

        private void RegisterDependencies()
        {
            _container.RegisterInstance(_agent);
            _container.RegisterInstance(_supplierDb);
            _container.RegisterType<IHtmlDataEntries, HtmlDataEntries>();
            _container.RegisterType<ISellpricesCalculator, SellpricesCalculator>();

            ISettings settings = new DefaultSettings();
            settings.SetValue("ebayname", _agent.agentEntity.agent.ebayname);
            settings.SetValue("listingdbconnectionstring", _appConfiguration.GetValue("ListingDBConnectionStringFormat"));
            settings.SetValue("listingdbdatabasename", _appConfiguration.GetValue("ListingDBNameFormat"));
            settings.SetValue("listingdbcollectionname", _appConfiguration.GetValue("ListingCollectionName"));
            IInventoryDbFactory inventoryDbFactory = new InventoryDbFactory(settings, new MongoConnectionFactory(new MongoConnectionProperties()));
            InventoryStorage.InventorySource = new InventorySource(inventoryDbFactory.InventoryDbConnection(settings.GetValue("ebayname")));

            _container.RegisterType<ISellpricePersistent, SellpricePersistent>();
            _container.RegisterType<IShippingCostsPersistent, ShippingCostsPersistent>();
        }

        public TResult Resolve<TResult>()
        {
            return _container.Resolve<TResult>();
        }
    }
}
