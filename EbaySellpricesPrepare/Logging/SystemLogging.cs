﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbaySellpricesPrepare.Logging
{
    public class SystemLogging
    {
        public static ILogger logger;

        public static void Log(string log)
        {
            if (logger == null)
            {
                throw new ArgumentNullException();
            }

            logger.Log(log);
        }

        public static void Save()
        {
            logger.Save();
        }
    }
}
