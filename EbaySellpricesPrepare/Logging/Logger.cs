﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbaySellpricesPrepare.Logging
{
    public class Logger : ILogger
    {
        private StringBuilder logs;

        public Logger()
        {
            logs = new StringBuilder();
        }

        public void Log(string log)
        {
            logs.AppendLine(log);
            Console.WriteLine(log);
        }


        public void Save()
        {
            File.WriteAllText("logs.txt", logs.ToString());
        }
    }
}
