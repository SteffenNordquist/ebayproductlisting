﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbaySellpricesPrepare.Logging
{
    public interface ILogger
    {
        void Log(string log);

        void Save();
    }
}
