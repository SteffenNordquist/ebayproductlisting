﻿using DN_Classes.Agent;
using DN_Classes.Conditions;
using DN_Classes.Supplier;
using MongoDB.Bson;
using PriceCalculation;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbaySellpricesPrepare.DefaultPriceCalculation
{
    public class ActualCalculator
    {
        public static Agent agent;
        public static IDatabase database;

        private static double shippingCosts = 0;


        private static double GetSellPrice(IProduct iproduct, string supplierName)
        {
            if (agent == null || database == null)
            {
               
                throw new ArgumentNullException();
            }

            double sellPrice = 0;

            string path = AppDomain.CurrentDomain.BaseDirectory + @"\Templates\Packings.txt";

            PackingList packingList = new PackingList();

            SupplierDB supplierDB = new SupplierDB();
            SupplierEntity supplierEntity = supplierDB.GetSupplier(supplierName.ToUpper());


            BsonDocument profitConditionBson = agent.agentEntity.agent.platforms.Find(x => x.name == "ebay").suppliers.Find(x => x.name.ToUpper() == supplierName.ToUpper()).ToBsonDocument();
            ProfitConditions profitCondition = new ProfitConditions(profitConditionBson["ProfitWanted"].AsDouble, profitConditionBson["RiskFactor"].AsDouble, profitConditionBson["ItemHandlingCosts"].AsDouble);

            PlatformConditions platformConsditions = new PlatformConditions(9.5, 1.9, 0.35);
            platformConsditions.PlatformCategory = PlatformCategory.ebayGeneral;

            ContractConditions contractcondition = supplierEntity.contractCondition;

            Product product = new Product(iproduct, contractcondition, profitCondition, platformConsditions, packingList);

            shippingCosts = new ShippingType(product).price;

            sellPrice = PriceCalc.getSellPrice(product, profitCondition, supplierEntity.contractCondition, platformConsditions);

            return Math.Round(sellPrice, 2);
        }

        public static double GetSellPrice(IProductSalesInformation ebayproduct)
        {
            double sellPrice = 0;

            if (ebayproduct.GetEan().Length > 0)
            {
                IProduct product = database.GetMongoProductByEan(ebayproduct.GetEan());

                sellPrice = GetSellPrice(product, ebayproduct.getSupplierName());
            }

            return Math.Round(sellPrice, 2);
        }

        public static double GetShippingCosts()
        {
            return shippingCosts;
        }
    }
}
