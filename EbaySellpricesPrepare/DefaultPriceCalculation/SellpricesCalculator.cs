﻿using DN_Classes.Agent;
using EbayCSV.Core.HtmlData.BK;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EbaySellpricesPrepare.DefaultPriceCalculation
{
    public class SellpricesCalculator : ISellpricesCalculator
    {

        private readonly IHtmlDataEntries htmlDataEntries;
        private readonly ISellpricePersistent sellPricePersistent;
        private readonly IShippingCostsPersistent shippingCostsPersistent;
        private readonly Agent agent;

        public SellpricesCalculator(IDatabase database, Agent agent, IHtmlDataEntries htmlDataEntries, ISellpricePersistent sellPricePersistent, IShippingCostsPersistent shippingCostsPersistent)
        {
            ActualCalculator.agent = agent;
            this.agent = agent;
            ActualCalculator.database = database;
            this.htmlDataEntries = htmlDataEntries;
            this.sellPricePersistent = sellPricePersistent;
            this.shippingCostsPersistent = shippingCostsPersistent;
        }

        public double Calculate<T>(T value)
        {
            IProductSalesInformation product = (IProductSalesInformation)value;
            //SystemLogging.Log(product.GetEan());

            double calculatedPrice = ActualCalculator.GetSellPrice(product);

            if (calculatedPrice > 0)
            {
                //requiste : ActualCalculator.GetSellPrice
                double shippingCosts = ActualCalculator.GetShippingCosts();

                //SystemLogging.Log(calculatedPrice.ToString());

                sellPricePersistent.Save<IProductSalesInformation>(product, calculatedPrice);
                shippingCostsPersistent.Save<IProductSalesInformation>(product, shippingCosts);

                //ThreadStart htmlUpdateThread = delegate { 
                string id = agent.agentEntity.agent.agentID + "_" + product.getSupplierName() + product.GetEan();
                htmlDataEntries.UpdatePrice(id, calculatedPrice);
                //};
                //new Thread(htmlUpdateThread).Start();
            }

            return calculatedPrice;
        }
    }
}
