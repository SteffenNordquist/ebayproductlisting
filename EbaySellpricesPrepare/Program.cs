﻿using DN_Classes.Agent;
using DN_Classes.AppStatus;
using EbayCSV.Core.Suppliers;
using ProcuctDB;
using ProcuctDB.Knv;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConfigurationData;
using DN_Classes.Logger;

namespace EbaySellpricesPrepare
{
    class Program
    {
        static void Main(string[] args)
        {
            string configFile = AppDomain.CurrentDomain.BaseDirectory + @"\AppConfiguration.config";
            IConfigurationSource configSource = new ConfigurationSource(configFile);
            IConfigurationManager appConfiguration = new AppConfigurationManager(configSource);

            ILogFileLocation logFileLocation = new DefaultLogFileLocation { LogDirectory = appConfiguration.GetValue("LogDirectory"), ToolName = "EbaySellpricesPrepare", SubToolName = "EbaySellpricesPrepare" };
            LoggingConfig loggingConfig = new LoggingConfig();
            loggingConfig.Register(logFileLocation);

            using (ILogger logger = loggingConfig.Resolve<ILogger>())
            {

                List<Agent> agents = new List<Agent>();
                agents.Add(new Agent(13));
                agents.Add(new Agent(4));

                using (ApplicationStatus appStatus = new ApplicationStatus("EbaySellpricesPrepare_148"))
                {
                    try
                    {
                        foreach (Agent agent in agents)
                        {

                            List<string> ebaySupplierNames =
                                agent.agentEntity.agent.platforms.Where(x => x.name == "ebay")
                                    .First()
                                    .suppliers.Select(x => x.name)
                                    .ToList();
                            //ebaySupplierNames.RemoveAll(x => !x.ToLower().Contains("knv"));
                            IAgentEbaySuppliers ebaySuppliers = new AgentEbaySuppliers();

                            List<IDatabase> supplierDatabaseList =
                                ebaySuppliers.GetSuppliersDatabases<IDatabase, string>(ebaySupplierNames).ToList();

                            foreach (IDatabase supplierDb in supplierDatabaseList)
                            {
                                CalculationSetup calculationSetup = new CalculationSetup(agent, supplierDb,
                                    appConfiguration);
                                ISellpricesCalculator sellPricesCalculator =
                                    calculationSetup.Resolve<ISellpricesCalculator>();
                                int counter = 0;
                                int skipped = 0;
                                bool hasItems = true;
                                while (hasItems)
                                {
                                    hasItems = false;
                                    int amount = 1000;

                                    //ParallelOptions parOptions = new ParallelOptions { MaxDegreeOfParallelism = 8 };
                                    foreach (
                                        IProductSalesInformation product in supplierDb.GetEbayReady(skipped, amount))
                                        //Parallel.ForEach(supplierDb.GetEbayReady(skipped, amount), parOptions, product =>
                                    {
                                        double calculatedSellPrice =
                                            sellPricesCalculator.Calculate<IProductSalesInformation>(product);

                                        Console.WriteLine(skipped);
                                        hasItems = true;

                                    }
                                    //);

                                    skipped += amount;
                                }
                            }
                        }

                        appStatus.AddMessagLine("Success");
                        appStatus.Successful();
                    }
                    catch (Exception ex)
                    {
                        appStatus.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
                    }
                }
            }
        }
    }
}
