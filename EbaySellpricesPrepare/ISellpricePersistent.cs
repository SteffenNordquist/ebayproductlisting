﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbaySellpricesPrepare
{
    public interface ISellpricePersistent
    {
        void Save<T>(T productInfo, double calculatedValue);

    }
}
