﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Logging
{
    public class ErrorLogging : ILogger
    {
        private StringBuilder logs = new StringBuilder();

        public void AddLog(string log)
        {
            logs.AppendLine(log + "</br>");
            Console.WriteLine(log);
        }

        public string GetLogs()
        {
            return logs.ToString();
        }

        public void ClearLogs()
        {
            logs.Clear();
        }
    }
}
