﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Logging
{
    public class SystemLogging
    {
        public static ILogger Error = new ErrorLogging();
        public static ILogger Normal = new NormalLogging();
    }
}
