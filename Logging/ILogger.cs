﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Logging
{
    public interface ILogger
    {
        /// <summary>
        /// adds log item
        /// </summary>
        /// <param name="log"></param>
        void AddLog(string log);

        /// <summary>
        /// returns concatinated logs in pretty format
        /// </summary>
        /// <returns>string</returns>
        string GetLogs();

        /// <summary>
        /// clears the logs initially saved
        /// </summary>
        void ClearLogs();
    }
}
