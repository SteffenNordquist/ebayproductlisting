﻿
using DNEbaySystem.Lister.Interfaces;
using HtmlAgilityPack;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace DNEbaySystem.FileExchange
{
    public class FileExchangeUploader
    {
        private string SupplierName { get; set; }
        private int AgentId { get; set; }
        private string  Action { get; set; }

        private string UploadStatusReferenceNumber { get; set; }
        private string UploadStatusMessage { get; set; }


        public FileExchangeUploader(IFile csvFile,int agentID, string agentToken, string supplierName)
        {
            if (csvFile.HasRevisedItems())
            {

                foreach (string filename in csvFile.GetRevisedItemsFilenames())
                {
                    Console.WriteLine("Save path : " + filename);
                    Upload(filename, agentToken, supplierName, "revise", agentID);
                }

            }


            if (csvFile.HasNewItems())
            {


                foreach (string filename in csvFile.GetNewItemsFilenames())
                {
                    Console.WriteLine("Save path : " + filename);
                    Upload(filename, agentToken, supplierName, "new", agentID);
                }

            }
        }

        public FileExchangeUploader(DirectoryInfo newCsvs, DirectoryInfo reviseCsvs, int agentID, string agentToken)
        {

            foreach (var csvFile in reviseCsvs.GetFiles())
            {
                string supplierName = Path.GetFileNameWithoutExtension(csvFile.FullName).Replace("revise_ebaycsv", "");
                supplierName = supplierName.Remove(supplierName.Length - 1);
                Console.WriteLine("Save path : " + csvFile.FullName);
                Upload(csvFile.FullName, agentToken, supplierName, "revise", agentID);
            }

            foreach (var csvFile in newCsvs.GetFiles())
            {
                string supplierName = Path.GetFileNameWithoutExtension(csvFile.FullName).Replace("new_ebaycsv", "");
                supplierName = supplierName.Remove(supplierName.Length - 1);
                Console.WriteLine("Save path : " + csvFile.FullName);
                Upload(csvFile.FullName, agentToken, supplierName, "new", agentID);
            }
            


        }

        private void Upload(string path, string token, string supplierName, string action, int agentId)
        {
            this.SupplierName = supplierName;
            this.Action = action;
            this.AgentId = agentId;

            string responseHtml = UploadToFileExchange(path, token);

            SetUploadStatusMessage(responseHtml);

            SetStatusReferenceNumber(UploadStatusMessage);

            Insert();

        }

        private string UploadToFileExchange(string path, string token)
        {
            Encoding encoding = System.Text.Encoding.UTF8;

            FileInfo fileInfo = new FileInfo(path);

            string boundary = "THIS_STRING_SEPARATES";
            string newLine = "\r\n";

            StringBuilder dataStream = new StringBuilder();
            dataStream.Append("--" + boundary + newLine);
            dataStream.Append("Content-Disposition: form-data; name=\"token\"" + newLine + newLine);
            dataStream.Append(token + newLine);
            dataStream.Append("--" + boundary + newLine);
            dataStream.Append("Content-disposition: form-data; name=\"file\"; filename=\"" + fileInfo.Name + "\"" + newLine);
            dataStream.Append(@"Content-type: text/plain;" + newLine + newLine);

            using (StreamReader sr = new StreamReader(fileInfo.FullName))
            {
                String line;
                while ((line = sr.ReadLine()) != null)
                {
                    dataStream.Append(line + "\n"); // read entire file into "dataStream"
                    //Console.WriteLine(line);
                }
            }
            dataStream.Append(newLine);
            dataStream.Append("--" + boundary + newLine);

            // now build the WebRequest...
            HttpWebRequest webReq = (HttpWebRequest)WebRequest.Create(@"https://bulksell.ebay.de/ws/eBayISAPI.dll?FileExchangeUpload");
            webReq.Method = "POST";
            webReq.ContentType = @"multipart/form-data; boundary=" + boundary;
            webReq.UserAgent = "V.01";
            webReq.KeepAlive = true;
            webReq.ProtocolVersion = HttpVersion.Version10;

            byte[] postBuffer = encoding.GetBytes(dataStream.ToString());
            webReq.ContentLength = postBuffer.Length;

            // post the data
            Stream postDataStream = webReq.GetRequestStream();
            postDataStream.Write(postBuffer, 0, postBuffer.Length);
            postDataStream.Flush();
            postDataStream.Close();

            // Get the response
            HttpWebResponse response = (HttpWebResponse)webReq.GetResponse();

            // Read the response from the stream
            StreamReader responseStream = new StreamReader(response.GetResponseStream(), encoding, true);
            string responseHtml = "";
            responseHtml = responseStream.ReadToEnd();
            Console.WriteLine(responseHtml);
            response.Close();
            responseStream.Close();

            return responseHtml;
        }

        private void SetUploadStatusMessage(string responseHtml)
        {
            string logPath = AppDomain.CurrentDomain.BaseDirectory + @"\" + SupplierName + "_" + Action + "_log.html";
            File.WriteAllText(logPath, responseHtml);

            HtmlDocument doc = new HtmlDocument();
            doc.Load(logPath);

            string documentText = doc.DocumentNode.InnerText;
            this.UploadStatusMessage = documentText;
        }

        private void SetStatusReferenceNumber(string htmlString)
        {
            Match match = Regex.Match(htmlString, @"lautet (\d+)");
            UploadStatusReferenceNumber = match.Value.Replace("lautet","");
        }

        private void Insert()
        {
            FileExchangeDB fileExchangeDB = new FileExchangeDB();

            FileExchangeEntity fe = new FileExchangeEntity
            {
                id = UploadStatusReferenceNumber, 
                ReferenceNumber = UploadStatusReferenceNumber,
                SupplierName = this.SupplierName,
                UploadMessage = UploadStatusMessage,
                UploadTime = DateTime.Now,
                AgentId = this.AgentId

            };

            fileExchangeDB.Insert(fe);
        }

    }
}
