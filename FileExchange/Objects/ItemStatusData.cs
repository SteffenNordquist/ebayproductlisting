﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.FileExchange.Objects
{
    public class ItemStatusData
    {
        public string Action { get; set; }
        public string Status { get; set; }
        public string ErrorCode { get; set; }
        public string ErrorMessage { get; set; }
        public string Ean { get; set; }
        public string ReferenceNumber { get; set; }

        public ItemStatusData(string line, string refId)
        {
            string[] tabSplits = line.Split('\t');
            this.Action = tabSplits[1];
            this.Status = tabSplits[2];
            this.ErrorCode = tabSplits[3];
            this.ErrorMessage = tabSplits[4];
            this.Ean = tabSplits[34];
            this.ReferenceNumber = refId;
        }
    }
}
