﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.FileExchange
{
    public class FileExchangeDownloader
    {
        public static void DownloadUploadsStatusFile(string refId, string token, string path)
        {
            string tokenUrlEncoded = WebUtility.UrlEncode(token);

            WebClient wc = new WebClient();
            string downloadUrl = "https://bulksell.ebay.com/ws/eBayISAPI.dll?FileExchangeProgrammaticDownload&jobId=" + refId + "&token=" + tokenUrlEncoded;
            wc.DownloadFile(downloadUrl, path);
        }
    }
}
