﻿using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.FileExchange
{
    public class FileExchangeDB
    {
        public MongoCollection<FileExchangeEntity> Collection = null;

        public FileExchangeDB()
        {
            Collection = new MongoClient("mongodb://client148:client148devnetworks@148.251.0.235/FileExchangeResponses")
                .GetServer()
                .GetDatabase("FileExchangeResponses")
                .GetCollection<FileExchangeEntity>("responses");
        }

        public void Insert(FileExchangeEntity fileExchangeData)
        {
            if (Collection.Find(Query.EQ("_id", fileExchangeData.id)).Count() == 0)
            {
                Collection.Insert(fileExchangeData);
            }
        }

        public FileExchangeEntity GetResponseByReferenceNumber(string referenceNumber)
        {
            return Collection.FindOne(Query.EQ("_id", referenceNumber));
        }

        public List<FileExchangeEntity> GetResponsesByTimeFilters(DateTime from)
        {
            return Collection.Find(Query.And(Query.GT("UploadTime", from))).ToList();
        }
    }
}
