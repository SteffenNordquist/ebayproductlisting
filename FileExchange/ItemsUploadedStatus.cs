﻿
using DN_Classes.Agent;
using DNEbaySystem.FileExchange.Objects;
using DNEbaySystem.ListingDatabase;
using DNEbaySystem.ListingDatabase.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.FileExchange
{
    public class ItemsUploadedStatus
    {
        private string ResponseFilename;
        private List<ItemStatusData> Items = new List<ItemStatusData>();
        private IListingDatabaseUploadStatuses ListingDatabase = null;
        private string ReferenceNumber;

        public ItemsUploadedStatus(string responseFileName, Agent Agent, string referenceNumber)
        {
            this.ResponseFilename = responseFileName;
            this.ReferenceNumber = referenceNumber;

            ListingDatabaseFactory listingDatabaseFactory = new ListingDatabaseFactory();
            this.ListingDatabase = listingDatabaseFactory.GetListingDatabaseUploadStatuses(Agent.agentEntity.agent.ebayname);
        }

        public void ProcessFile()
        {
            List<string> lines = File.ReadAllLines(ResponseFilename).ToList();
            lines.RemoveAt(0);

            foreach (string line in lines)
            {
                ItemStatusData i = new ItemStatusData(line, ReferenceNumber);
                Items.Add(i);
            }
        }

        public void UpdateDB()
        {
            foreach (ItemStatusData i in Items)
            {
                ListingDatabase.UpdateStatuses(i);
            }
        }


    }
}
