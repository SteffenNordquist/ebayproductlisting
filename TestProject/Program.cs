﻿using DN_Classes.Agent;
using DN_Classes.Logger;
using EbayCSV.Core;
using EbayCSV.Core.HtmlData.BK;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.Settings;
using ProcuctDB;
using ProcuctDB.Haba;
using ProcuctDB.Jpc;
using ProcuctDB.Knv;
using ProcuctDB.Loqi;
using ProcuctDB.Ravensburger;
using ProcuctDB.Suppliers.TFA;
using ProcuctDB.Wuerth;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using EbayCSV.Core.Inventory.Source;
using DN.DataAccess.ConnectionFactory.Mongo;

namespace TestProject
{
    class Program
    {
        static void Main(string[] args)
        {
            ProductBrowsing.SetLocalBrowsing();

            ILogFileLocation logFileLocation = new DefaultLogFileLocation { LogDirectory = "C:\\DevNetworks\\Logs\\EbayListing", ToolName = "HtmlDisplayTest", SubToolName = "HtmlDisplayTest" };
            LoggingConfig loggingConfig = new LoggingConfig();
            loggingConfig.Register(logFileLocation);

            AppLogger.SetLogger(loggingConfig.Resolve<ILogger>());

            DefaultSettings settings = new DefaultSettings();
            settings.SetValue("listingdbconnectionstring", "");
            settings.SetValue("listingdbdatabasename", "");
            settings.SetValue("listingdbcollectionname", "");

            //var listingDatabaseFactory = new BKListingDatabaseFactory();
            //IListingDatabase listingDatabase = listingDatabaseFactory.GetListingDatabase<string>(settings.GetValue("ebayname"));

            //EbayInventory.DataReader = listingDatabase.dataReader();
            //EbayInventory.DataWriter = listingDatabase.dataWriter();
            //EbayInventory.InventorySource = new BKInventorySource(listingDatabase.dataReader());


            IInventoryDbFactory inventoryDbFactory = new InventoryDbFactory(settings, new MongoConnectionFactory(new MongoConnectionProperties()));
            InventoryStorage.InventorySource = new InventorySource(inventoryDbFactory.InventoryDbConnection(""));

            IDatabase jpcDB = new TfaDB();
            Agent agent = new Agent(4);
            IProductSalesInformation product = jpcDB.GetMongoProductSalesInformationByEan("4009816319012");

            HtmlDataEntries htmlDataEntries = new HtmlDataEntries();
            
            settings.SetValue("imageforwarderlink", agent.agentEntity.agent.htmlgeneratorproperties.imageforwarderlink);
            settings.SetValue("imagelinkformat", agent.agentEntity.agent.htmlgeneratorproperties.imagelinkformat);
            settings.SetValue("ebayname", agent.agentEntity.agent.ebayname);
            settings.SetValue("agentid", agent.agentEntity.agent.agentID.ToString());
            settings.SetValue("logolink", agent.agentEntity.agent.htmlgeneratorproperties.logolink);
            settings.SetValue("mvcviewlink", agent.agentEntity.agent.htmlgeneratorproperties.mvcviewlink);
            settings.SetValue("PaypalEmail", agent.agentEntity.agent.keys.ebay.email);

            HtmlDownloader htmlDownloader = new HtmlDownloader();
            HtmlCompressor htmlCompressor = new HtmlCompressor();

            HtmlDataFeeder htmlDataFeeder =new HtmlDataFeeder(htmlDataEntries, settings, htmlDownloader, htmlCompressor);

            BKHtml bkHtml = new BKHtml(htmlDataFeeder);

            string htmlstring = bkHtml.GetHtmlString<IProductSalesInformation>(product);
        }
    }
}
