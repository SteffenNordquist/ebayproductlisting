﻿
using DN_Classes.Agent;
using DNEbaySystem.Lister.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister
{
    public class PrepareAgent
    {
        public static EbayAgent GetAgent(Agent agent)
        {
            EbayAgent seller = new EbayAgent
            {
                Id = agent.agentEntity.agent.agentID,
                Email = agent.agentEntity.agent.keys.ebay.email,
                Name = agent.agentEntity.agent.ebayname,
                Token = agent.agentEntity.agent.keys.ebay.token,
                MvcView = agent.agentEntity.agent.htmlgeneratorproperties.mvcviewlink
            };

            return seller;
        }
    }
}
