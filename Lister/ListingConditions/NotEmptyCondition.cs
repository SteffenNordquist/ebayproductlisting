﻿
using DNEbaySystem.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ListingConditions
{
    public class NotEmptyCondition : ICondition
    {
        private string supplierName;

        public NotEmptyCondition(string supplierName)
        {
            this.supplierName = supplierName;
        }

        public bool Passed()
        {
            if (this.supplierName != string.Empty)
            {
                return true;
            }

            SystemLogging.Error.AddLog("Suppliername is empty");

            return false;

        }
    }
}
