﻿using DNEbaySystem.ListingDatabase.Objects;
using DNEbaySystem.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ListingConditions
{
    public class EbayProductFoundCondition : ICondition
    {
        private ListingEntity Listing;

        public EbayProductFoundCondition(ListingEntity listing)
        {
            this.Listing = listing;
        }

        public bool Passed()
        {
            if (Listing.ItemStatusData != null && Listing.ItemStatusData.Status.Contains("Failure") && Listing.ItemStatusData.ErrorMessage.Contains("Es wurde kein Posten gefunden für ProductListingDetails"))
            {
                SystemLogging.Error.AddLog(Listing.Ean + " : no ebay product found");
                return false;
            }
            else
            {
                return true;
            }
        }

    }
}
