﻿
using DNEbaySystem.Lister.CsvTemplates;
using DNEbaySystem.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ListingConditions
{
    public class CategoryCondition : ICondition
    {
        private string Id;

        public CategoryCondition(string id)
        {
            this.Id = id;
        }

        public bool Passed()
        {
            return HasCategory();
        }

        private bool HasCategory()
        {
            if (IsCategoryRequired())
            {
                List<string> ids = new List<string>();
                ids.Add(Id);

                if (EbayCategories.HasCategory(ids))
                {
                    return true;
                }
            }
            else
            {
                return true;
            }

            SystemLogging.Error.AddLog(Id + " : no category");

            return false;
        }

        private bool IsCategoryRequired()
        {
            if (Template.TemplateType != TemplateType.Catalog)
            {
                return true;
            }

            return false;
        }


    }
}
