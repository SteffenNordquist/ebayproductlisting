﻿
using DNEbaySystem.Lister.Objects;
using DNEbaySystem.Logging;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ListingConditions
{
    public class ImageCondition: ICondition
    {
        private RawItem rawItem;

        public ImageCondition(RawItem product)
        {
            this.rawItem = product;
        }

        public bool Passed()
        {
            if (rawItem.HtmlProperties.mainimages == null)
            {
                SystemLogging.Error.AddLog(rawItem.Ean + " : no image");
                return false;
            }
            else if (rawItem.HtmlProperties.mainimages.Count() == 0)
            {
                SystemLogging.Error.AddLog(rawItem.Ean + " : no image");
                return false;
            }
            else
            {
                return true;
            }
            
        }

    }
}
