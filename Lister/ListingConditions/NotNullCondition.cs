﻿
using DNEbaySystem.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ListingConditions
{
    public class NotNullCondition : ICondition
    {
        private Object obj;

        public NotNullCondition(Object obj)
        {
            this.obj = obj;
        }

        public bool Passed()
        {
            if (this.obj != null)
            {
                return true;
            }

            SystemLogging.Error.AddLog(this.obj.GetType().Name + " : null");

            return false;
        }
    }
}
