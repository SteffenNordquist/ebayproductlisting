﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ListingConditions
{
    public interface ICondition
    {
        bool Passed();
    }
}
