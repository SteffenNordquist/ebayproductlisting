﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ListingConditions
{
    public class ConditionsProcessor
    {
        private List<ICondition> Conditions = new List<ICondition>();

        public void AddCondition(ICondition condition)
        {
            Conditions.Add(condition);
        }

        public bool HavePassed()
        {
            bool pass = false;

            foreach (var condition in Conditions)
            {
                pass = condition.Passed();
                if (!pass) { break; }
            }

            return pass;
        }
    }
}
