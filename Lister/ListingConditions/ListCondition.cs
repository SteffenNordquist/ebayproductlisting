﻿
using DNEbaySystem.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ListingConditions
{
    public class ListCondition : ICondition
    {
        private List<Object> list = null;

        public ListCondition(List<Object> list)
        {
            this.list = list;
        }

        public bool Passed()
        {
            if (list != null && list.Count() > 0)
            {
                return true;
            }

            SystemLogging.Error.AddLog("List is null or empty");

            return false;
        }
    }
}
