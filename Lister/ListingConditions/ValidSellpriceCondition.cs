﻿
using DNEbaySystem.ListingDatabase.Objects;
using DNEbaySystem.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ListingConditions
{
    public class ValidSellpriceCondition : ICondition
    {

        private ListingEntity Listing;

        public ValidSellpriceCondition(ListingEntity listing)
        {
            this.Listing = listing;
        }

        public bool Passed()
        {
            if (Listing.Wanted != null && Listing.Wanted.Price != null)
            {
                if (Listing.Wanted != null && Listing.Wanted.Price != 0 && Listing.Wanted.Price >= Listing.StartPrice)
                {
                    return true;
                }

                SystemLogging.Error.AddLog(Listing.Wanted.Price + " : sellPrice not allowed");
            }
            return false;
        }
    }
}
