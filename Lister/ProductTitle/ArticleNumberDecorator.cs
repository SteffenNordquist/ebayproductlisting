﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ProductTitle
{
    public class ArticleNumberDecorator : ITitleDecorator
    {
        private string ArticleNumber;
        private ITitleDecorator Decorator;

        public ArticleNumberDecorator(string artNumber, ITitleDecorator decorator)
        {
            this.ArticleNumber = artNumber;
            this.Decorator = decorator;
        }

        public string GetTitle()
        {
            return Decorator.GetTitle() + " " + ArticleNumber;
        }
    }
}
