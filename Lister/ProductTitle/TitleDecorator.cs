﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ProductTitle
{
    public class TitleDecorator : ITitleDecorator
    {
        private string Title;
        private ITitleDecorator Decorator;

        public TitleDecorator(string title, ITitleDecorator decorator)
        {
            this.Title = title;
            this.Decorator = decorator;
        }

        public string GetTitle()
        {
            return Decorator.GetTitle() + " " + Title;
        }
    }
}
