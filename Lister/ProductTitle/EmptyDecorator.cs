﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ProductTitle
{
    public class EmptyDecorator : ITitleDecorator
    {
        public string GetTitle()
        {
            return "";
        }
    }
}
