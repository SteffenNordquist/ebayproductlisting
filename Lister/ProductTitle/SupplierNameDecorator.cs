﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ProductTitle
{
    public class SupplierNameDecorator : ITitleDecorator
    {
        private string supplierName;
        private ITitleDecorator Decorator;

        public SupplierNameDecorator(string supplierName, ITitleDecorator decorator)
        {
            this.supplierName = supplierName;
            this.Decorator = decorator;
        }

        public string GetTitle()
        {
            return Decorator.GetTitle() + " " + supplierName;
        }
    }
}
