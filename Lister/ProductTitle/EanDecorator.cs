﻿
using DNEbaySystem.Lister.Modifiers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.ProductTitle
{
    public class EanDecorator : ITitleDecorator
    {
        private string Ean;
        private ITitleDecorator Decorator;

        public EanDecorator(string ean, ITitleDecorator decorator)
        {
            this.Ean = ean;
            this.Decorator = decorator;
        }

        public string GetTitle()
        {
            string preTitle = Decorator.GetTitle();
            preTitle = WebUtility.HtmlEncode(preTitle);
            string eanSuffix = " " + Ean;
            return StringModifier.Truncate(preTitle, 80, eanSuffix);//80 is max
        }
    }
}
