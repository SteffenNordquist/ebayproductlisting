﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.Interfaces
{
    public interface IGenerator
    {
        /// <summary>
        /// Generate csv
        /// </summary>
        /// <returns></returns>
        void Generate();

        /// <summary>
        /// Returns csv file
        /// </summary>
        /// <returns>IFile</returns>
        IFile GetFile();
    }
}
