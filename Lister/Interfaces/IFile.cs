﻿using DNEbaySystem.Lister.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.Interfaces
{
    public interface IFile
    {
        /// <summary>
        /// adds item for listing whether revision or new
        /// </summary>
        /// <param name="product"></param>
        /// <param name="actionType"></param>
        /// <param name="itemId"></param>
        /// <param name="sellPrice"></param>
        /// <param name="quantity"></param>
        /// <param name="updateTitle"></param>
        /// <param name="updateDescription"></param>
        void AddItem(RawItem product, ActionType actionType, string itemId, double sellPrice, int quantity, bool updateTitle, bool updateDescription);

        bool AdditionalActionAllowed();



        bool HasNewItems();

        List<string> GetNewItemsFilenames();



        bool HasRevisedItems();

        List<string> GetRevisedItemsFilenames();
    }
}
