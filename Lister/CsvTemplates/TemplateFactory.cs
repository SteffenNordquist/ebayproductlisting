﻿using DNEbaySystem.Lister.Implementations;
using DNEbaySystem.Lister.Interfaces;
using DNEbaySystem.Lister.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.CsvTemplates
{
    public class TemplateFactory
    {
        public static IFile GetCSVTemplate(EbayAgent agent, TemplateType templateType, int actionLimit)
        {
            Dictionary<TemplateType, IFile> templateDictionary = TemplateList.GetList(agent, actionLimit);

            IFile templateCsv = templateDictionary.Where(x => x.Key == templateType).First().Value;

            if (templateCsv != null)
            {
                return templateCsv;
            }
            else
            {
                return new NullCsvFile();
            }
        }

        public static TemplateType GetType(string supplierName)
        {
            if (supplierName.ToUpper().Contains("JPC"))
            {
                return TemplateType.Catalog;
            }
            else
            {
                return TemplateType.Basic;
            }
        }
    }
}
