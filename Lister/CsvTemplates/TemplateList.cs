﻿using DNEbaySystem.Lister.Implementations;
using DNEbaySystem.Lister.Interfaces;
using DNEbaySystem.Lister.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.CsvTemplates
{
    public class TemplateList
    {
        public static Dictionary<TemplateType, IFile> GetList(EbayAgent agent, int actionLimit)
        {
            Dictionary<TemplateType, IFile> templateDictionary = new Dictionary<TemplateType, IFile>();

            templateDictionary.Add(TemplateType.Basic, new BasicCsvFile(agent, actionLimit));
            templateDictionary.Add(TemplateType.Catalog, new CatalogCsvFile(agent, actionLimit));

            return templateDictionary;
        }
    }
}
