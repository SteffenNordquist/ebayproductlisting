﻿using DNEbaySystem.HtmlManager;
using DNEbaySystem.Lister.Interfaces;
using DNEbaySystem.Lister.Objects;
using DNEbaySystem.Lister.ProductTitle;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.Implementations
{
    public class BasicCsvFile : IFile
    {
        private List<BasicItem> items = new List<BasicItem>();
        private EbayAgent Agent;
        private int ActionLimit;
        private string SupplierName;

        public BasicCsvFile(EbayAgent agent, int actionLimit)
        {
            this.Agent = agent;
            this.ActionLimit = actionLimit;
        }

        public void AddItem(RawItem rawItem, ActionType actionType, string itemId, double sellPrice, int quantity, bool updateTitle, bool updateDescription)
        {
            this.SupplierName = rawItem.Supplier;

            ///<comment> get ebay category
            List<string> ids = new List<string>();
            ids.Add(rawItem.Ean);
            string category = EbayCategories.GetCategory(ids);
            ///</comment>


            ITitleDecorator emptyDecorator = new EmptyDecorator();
            ITitleDecorator supplierNameDecorator = new SupplierNameDecorator(rawItem.Supplier, emptyDecorator);
            ITitleDecorator articleNumberDecorator = new ArticleNumberDecorator(rawItem.ArticleNo, supplierNameDecorator);
            ITitleDecorator titleDecorator = new TitleDecorator(rawItem.Title, articleNumberDecorator);
            ITitleDecorator eanDecorator = new EanDecorator(rawItem.Ean, titleDecorator);

            HtmlDataFeeder htmlData = new HtmlDataFeeder();

            string htmlString = "";
            string title = "";
            string ean = itemId;
            if (actionType == ActionType.AddItem)
            {
                htmlData.Feed(rawItem, Agent);
                htmlString = htmlData.GetHtmlString();

                title = eanDecorator.GetTitle();

                ean = rawItem.Ean;
            }

            if (updateTitle)
            {
                title = eanDecorator.GetTitle();
            }

            if (updateDescription)
            {
                htmlData.Feed(rawItem, Agent);
                htmlString = htmlData.GetHtmlString();
            }

            //if (htmlString != "")
            //{
                BasicItem item = new BasicItem(
                    ean,
                    actionType,
                    title,
                    category,
                    rawItem.PicUrl,
                    htmlString,
                    sellPrice,
                    quantity,
                    Agent.Email,
                    rawItem.Supplier
                    );

                items.Add(item);
                ActionLimit--;
            //}

        }

        public bool AdditionalActionAllowed()
        {
            if (this.ActionLimit > 0)
            {
                return true;
            }

            return false;
        }

        /// <summary>
        /// Returns a line string from an EbayItem passed
        /// </summary>
        /// <returns>string</returns>
        private string AddItemLine(BasicItem item)
        {

            return String.Format(
                            "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}\t{12}\t{13}\t{14}\t{15}\t{16}\t{17}\t{18}\t{19}\t{20}\t{21}\t{22}\t{23}\t{24}\t{25}\t{26}\t{27}\t{28}\t{29}\t{30}\t{31}\t{32}\t{33}\t{34}\t{35}\t{36}\t{37}\t{38}\t{39}\t{40}",
                            item.Action,
                            item.Category,
                            item.Title,
                            item.SubTitle,
                            item.Description,
                            item.ConditionID,
                            item.PicUrl,
                            item.Quantity,
                            item.Format,
                            item.StartPrice.ToString(CultureInfo.InvariantCulture),
                            item.BuyItNowPrice,
                            item.Duration,
                            item.ImmediatePayRequired,
                            item.Location,
                            item.GalleryType,
                            item.PayPalAccepted,
                            item.PayPalEmailAddress,
                            item.PaymentInstructions,
                            item.StoreCategory,
                            item.ShippingDiscountProfileID,
                            item.DomesticRateTable,
                            item.ShippingType,
                            item.ShippingService_1Option,
                            item.ShippingService_1Cost,
                            item.ShippingService_1Priority,
                            item.ShippingService_1ShippingSurcharge,
                            item.ShippingService_2Option,
                            item.ShippingService_2Cost,
                            item.ShippingService_2Priority,
                            item.ShippingService_2ShippingSurcharge,
                            item.DispatchTimeMax,
                            item.CustomLabel,
                            item.ReturnsAcceptedOption,
                            item.RefundOption,
                            item.ReturnsWithinOption,
                            item.ShippingCostPaidByOption,
                            item.AdditionalDetails,
                            item.ShippingProfileName,
                            item.ReturnProfileName,
                            item.PaymentProfileName,
                            item.ProductBrand
                        );
        }

        /// <summary>
        /// Returns a line string from an EbayItem passed
        /// </summary>
        /// <returns>string</returns>
        private string ReviseItemLine(BasicItem item)
        {
            return String.Format(
                            "{0}\t{1}\t{2}\t{3}\t{4}\t{5}\t{6}\t{7}\t{8}\t{9}\t{10}\t{11}",
                            item.Action,
                            item.CustomLabel,
                            item.Title,
                            "Germany",
                            "EUR",
                            item.StartPrice.ToString(CultureInfo.InvariantCulture),
                            item.BuyItNowPrice,
                            item.Quantity,
                            item.PicUrl.Split(new string[] { "/images/" }, StringSplitOptions.None)[1].Split(new string[] { "_1." }, StringSplitOptions.None)[0],
                            item.PicUrl,
                            item.Description,
                            item.ProductBrand
                        );
        }

        /// <summary>
        /// Returns csv new header
        /// </summary>
        /// <returns>string</returns>
        private string GetNewHeader()
        {
            return "Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tCategory\tTitle\tSubtitle\tDescription\tConditionID\tPicURL\tQuantity\tFormat\tStartPrice\tBuyItNowPrice\tDuration\tImmediatePayRequired\tLocation\tGalleryType\tPayPalAccepted\tPayPalEmailAddress\tPaymentInstructions\tStoreCategory\tShippingDiscountProfileID\tDomesticRateTable\tShippingType\tShippingService-1:Option\tShippingService-1:Cost\tShippingService-1:Priority\tShippingService-1:ShippingSurcharge\tShippingService-2:Option\tShippingService-2:Cost\tShippingService-2:Priority\tShippingService-2:ShippingSurcharge\tDispatchTimeMax\tCustomLabel\tReturnsAcceptedOption\tRefundOption\tReturnsWithinOption\tShippingCostPaidByOption\tAdditionalDetails\tShippingProfileName\tReturnProfileName\tPaymentProfileName\tProductBrand";
        }

        /// <summary>
        /// Returns csv revised header
        /// </summary>
        /// <returns>string</returns>
        private string GetRevisedHeader()
        {
            return "Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID\tTitle\tSiteID\tCurrency\tStartPrice\tBuyItNowPrice\tQuantity\tCustomLabel\tPicURL\tDescription\tProductBrand";
        }

        public bool HasNewItems()
        {
            if (items.Where(x => x.Action == ActionType.AddItem).ToList().Count() == 0)
            {
                return false;
            }

            return true;
        }

        public List<string> GetNewItemsFilenames()
        {

            DirectoryInfo newCsvDirectory = new DirectoryInfo("C:\\EbayNewCsvFiles\\");
            if (!newCsvDirectory.Exists)
            {
                newCsvDirectory.Create();
            }

            List<string> filenames = new List<string>();

            int counter = 0;
            //we put 1000 items each file to ensure that each file will not exceed 15mb
            for (int x = 0; x <= 100000; x += 1000)//assuming there would be 100 files containing 1000 items each
            {

                if (items.Where(y => y.Action == ActionType.AddItem).ToList().Skip(x).Take(1000).Count() == 0)
                {
                    break;
                }

                string path = newCsvDirectory + SupplierName + counter + "new_ebaycsv.txt";

                using (var file = File.Create(path))
                {
                    using (StreamWriter writer = new StreamWriter(file, Encoding.UTF8))
                    {
                        writer.WriteLine(GetNewHeader());

                        foreach (var item in items.OrderBy(y => y.StartPrice).Where(y => y.Action == ActionType.AddItem).ToList().Skip(x).Take(1000))
                        {
                            writer.WriteLine(AddItemLine(item));
                        }
                    }
                }

                counter++;

                filenames.Add(path);
            }

            return filenames;
        }

        public List<string> GetRevisedItemsFilenames()
        {
            List<string> filenames = new List<string>();

            DirectoryInfo reviseCsvDirectory = new DirectoryInfo("C:\\EbayReviseCsvFiles\\");
            if (!reviseCsvDirectory.Exists)
            {
                reviseCsvDirectory.Create();
            }

            int counter = 0;
            //we put 1000 items each file to ensure that each file will not exceed 15mb
            for (int x = 0; x <= 100000; x += 1000)//assuming there would be 100 files containing 1000 items each
            {

                if (items.Where(y => y.Action == ActionType.Revise).ToList().Skip(x).Take(1000).Count() == 0)
                {
                    break;
                }

                string path = reviseCsvDirectory + SupplierName + counter + "revise_ebaycsv.txt";

                using (var file = File.Create(path))
                {
                    using (StreamWriter writer = new StreamWriter(file, Encoding.UTF8))
                    {
                        writer.WriteLine(GetRevisedHeader());

                        foreach (var item in items.OrderBy(y => y.StartPrice).Where(y => y.Action == ActionType.Revise).ToList().Skip(x).Take(1000))
                        {
                            writer.WriteLine(ReviseItemLine(item));
                        }
                    }
                }

                counter++;

                filenames.Add(path);
            }

            return filenames;
        }

        public bool HasRevisedItems()
        {
            if (items.Where(x => x.Action == ActionType.Revise).ToList().Count() == 0)
            {
                return false;
            }

            return true;
        }
    }
}
