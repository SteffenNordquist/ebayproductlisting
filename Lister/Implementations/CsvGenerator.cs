﻿using DNEbaySystem.Lister.CsvTemplates;
using DNEbaySystem.Lister.Interfaces;
using DNEbaySystem.Lister.ListingConditions;
using DNEbaySystem.Lister.Objects;
using DNEbaySystem.ListingDatabase.Interfaces;
using DNEbaySystem.ListingDatabase.Objects;
using DNEbaySystem.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.Implementations
{
    public class CsvGenerator : IGenerator
    {
        private List<RawItem> RawItems;
        private EbayAgent Agent;
        private IListingDatabase ListingDatabase;
        private IFile CsvFile;

        public CsvGenerator(EbayAgent agent, List<RawItem> rawItems, IListingDatabase listingDatabase, int actionLimit)
        {
            this.Agent = agent;
            this.RawItems = rawItems;
            this.ListingDatabase = listingDatabase;
            this.CsvFile = TemplateFactory.GetCSVTemplate(this.Agent, Template.TemplateType, actionLimit);
        }

        public void Generate()
        {
            foreach(RawItem rawItem in RawItems)
            {
                ListingEntity listingSavedFromDB = ListingDatabase.Find(rawItem.Ean);

                ConditionsProcessor conditions = new ConditionsProcessor();
                ICondition categoryCondition = new CategoryCondition(rawItem.Ean);
                conditions.AddCondition(categoryCondition);
                ICondition ebayProductFoundCondition = new EbayProductFoundCondition(listingSavedFromDB);
                conditions.AddCondition(ebayProductFoundCondition);
                ICondition validSellPriceCondition = new ValidSellpriceCondition(listingSavedFromDB);
                conditions.AddCondition(validSellPriceCondition);
                ICondition imageCondition = new ImageCondition(rawItem);
                conditions.AddCondition(imageCondition);

                if (conditions.HavePassed())
                {
                    //Thread updateListingFlagsThread = new Thread(() => UpdateListingFlags(listingSavedFromDB.Ean, listingSavedFromDB.SupplierName));
                    //updateListingFlagsThread.Start();

                    double calculatedSellPrice = listingSavedFromDB.Wanted.Price;
                    int ebayStock = Math.Min(rawItem.Stock, 3);

                    SystemLogging.Normal.AddLog("Conditions passed : " + rawItem.Ean);

                    if (!ListingDatabase.IsListingExists(rawItem.Ean, rawItem.Supplier))
                    {
                        SystemLogging.Normal.AddLog("AddItem : " + rawItem.Ean);
                        CsvFile.AddItem(rawItem, ActionType.AddItem, "", calculatedSellPrice, ebayStock, false, false);
                    }
                    else if (ListingDatabase.IsSubjectForDeletion(rawItem.Ean))
                    {
                        if (listingSavedFromDB.ItemID != null)
                        {
                            SystemLogging.Normal.AddLog("Revise : " + rawItem.Ean);
                            CsvFile.AddItem(rawItem, ActionType.Revise, listingSavedFromDB.ItemID, calculatedSellPrice, 0, listingSavedFromDB.UpdateTitle, listingSavedFromDB.UpdateDescription);
                        }
                    }
                    else if (ListingDatabase.NeedsUpdate(rawItem.Ean, ebayStock))
                    {
                        if (listingSavedFromDB.ItemID != null)
                        {
                            SystemLogging.Normal.AddLog("Revise : " + rawItem.Ean);
                            CsvFile.AddItem(rawItem, ActionType.Revise, listingSavedFromDB.ItemID, calculatedSellPrice, ebayStock, listingSavedFromDB.UpdateTitle, listingSavedFromDB.UpdateDescription);
                        }
                    }

                    Thread upsertThread = new Thread(() => Upsert(rawItem, calculatedSellPrice));
                    upsertThread.Start();


                    if (!CsvFile.AdditionalActionAllowed())
                    {
                        break;
                    }
                }


            }

            
        }

        public IFile GetFile()
        {
            return this.CsvFile;
        }

        //set update flags to true
        private void UpdateListingFlags(string ean, string supplierName)
        {
            ListingDatabase.SetDescriptionUpdateFlag(ean, supplierName, false);
            ListingDatabase.SetTitleUpdateFlag(ean, supplierName, false);
        }

        private void Upsert(RawItem rawItem, double sellPrice)
        {
            WantedClass wanted = new WantedClass();
            wanted.Price = sellPrice;
            wanted.Quantity = rawItem.Stock;

            ListingEntity listing = new ListingEntity
            {
                id = rawItem.Ean,
                Ean = rawItem.Ean,
                SupplierName = rawItem.Supplier,
                Wanted = wanted
            };

            ListingDatabase.Upsert(listing);
        }
    }
}
