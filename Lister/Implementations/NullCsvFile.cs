﻿using DNEbaySystem.Lister.Interfaces;
using DNEbaySystem.Lister.Objects;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.Implementations
{
    public class NullCsvFile : IFile
    {
        public void AddItem(RawItem product, ActionType actionType, string itemId, double sellPrice, int quantity, bool updateTitle, bool updateDescription)
        {

        }

        public bool AdditionalActionAllowed()
        {
            return false;
        }

        public string GetNewHeader()
        {
            return "Action(SiteID=Germany|Country=DE|Currency=EUR|Version=585|CC=UTF-8)\tCategory\tTitle\tSubtitle\tDescription\tConditionID\tPicURL\tQuantity\tFormat\tStartPrice\tBuyItNowPrice\tDuration\tImmediatePayRequired\tLocation\tGalleryType\tPayPalAccepted\tPayPalEmailAddress\tPaymentInstructions\tStoreCategory\tShippingDiscountProfileID\tDomesticRateTable\tShippingType\tShippingService-1:Option\tShippingService-1:Cost\tShippingService-1:Priority\tShippingService-1:ShippingSurcharge\tShippingService-2:Option\tShippingService-2:Cost\tShippingService-2:Priority\tShippingService-2:ShippingSurcharge\tDispatchTimeMax\tCustomLabel\tReturnsAcceptedOption\tRefundOption\tReturnsWithinOption\tShippingCostPaidByOption\tAdditionalDetails\tShippingProfileName\tReturnProfileName\tPaymentProfileName\tProductBrand";
        }

        public List<string> GetNewItemsFilenames()
        {
            return new List<string>();
        }

        public bool HasNewItems()
        {
            return false;
        }

        public string GetRevisedHeader()
        {
            return "Action(SiteID=Germany|Country=DE|Currency=EUR|Version=585|CC=UTF-8)\tItemID\tTitle\tSiteID\tCurrency\tStartPrice\tBuyItNowPrice\tQuantity\tCustomLabel\tPicURL\tDescription\tProductBrand";
        }


        public List<string> GetRevisedItemsFilenames()
        {
            return new List<string>();
        }

        public bool HasRevisedItems()
        {
            return false;
        }

        public string GetCsvString()
        {
            return String.Empty;
        }

        public string GetErrorMessages()
        {
            return String.Empty;
        }
    }
}
