﻿
using DN_Classes.Agent;
using DN_Classes.Conditions;
using DN_Classes.Supplier;
using DNEbaySystem.HtmlManager;
using MongoDB.Bson;
using PriceCalculation;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.SellPriceCalc
{
    public class SellPriceCalculator
    {
        private static double GetSellPrice(IProduct iproduct, Agent agent, string supplierName)
        {
            double sellPrice = 0;

            string path = AppDomain.CurrentDomain.BaseDirectory + @"\Templates\Packings.txt";

            PackingList packingList = new PackingList();

            SupplierDB supplierDB = new SupplierDB();
            SupplierEntity supplierEntity = supplierDB.GetSupplier(supplierName.ToUpper());


            BsonDocument profitConditionBson = agent.agentEntity.agent.platforms.Find(x => x.name == "ebay").suppliers.Find(x => x.name.ToUpper() == supplierName.ToUpper()).ToBsonDocument();
            ProfitConditions profitCondition = new ProfitConditions(profitConditionBson["ProfitWanted"].AsDouble, profitConditionBson["RiskFactor"].AsDouble, profitConditionBson["ItemHandlingCosts"].AsDouble);

            PlatformConditions platformConsditions = new PlatformConditions(9.5, 1.9, 0.35);
            platformConsditions.PlatformCategory = PlatformCategory.ebayGeneral;

            ContractConditions contractcondition = supplierEntity.contractCondition;

            if (iproduct.GetEanList()[0] == "3417761527047")
            {
                string s;
            }

            Product product = new Product(iproduct, contractcondition, profitCondition, platformConsditions, packingList);

            sellPrice = PriceCalc.getSellPrice(product, profitCondition, supplierEntity.contractCondition, platformConsditions);

            return Math.Round(sellPrice, 2);
        }

        public static double GetSellPrice(IDatabase Database, Agent agent, string ean, string supplierName)
        {
            double sellPrice = 0;

            IProduct product = Database.GetMongoProductByEan(ean);

            sellPrice = GetSellPrice(product, agent, supplierName);

            
                HtmlDataEntries htmlDataEntries = new HtmlDataEntries();
                htmlDataEntries.UpdatePrice(ean, supplierName, sellPrice);
          

            return Math.Round(sellPrice, 2);
        }
    }
}
