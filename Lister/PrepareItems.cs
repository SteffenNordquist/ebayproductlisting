﻿
using DN_Classes.Agent;
using DNEbaySystem.HtmlManager;
using DNEbaySystem.Lister.Objects;
using DNEbaySystem.ListingDatabase;
using DNEbaySystem.ListingDatabase.Interfaces;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister
{
    public class PrepareItems
    {
        public static List<RawItem> GetItems(IDatabase database, Agent agent, IListingDatabase sellerDatabase)
        {
            List<RawItem> items = new List<RawItem>();

            int skip = 0;
            bool hasMoreItems = true;
            while (hasMoreItems)
            {
                hasMoreItems = false;
                int limit = 1000;

                foreach (IProductSalesInformation product in database.GetEbayReady(skip, limit))
                {

                    RawItem item = new RawItem();

                    item.Ean = product.GetEan();
                    item.ArticleNo = product.getArticleNumberforEbay();
                    item.Stock = product.getStock();
                    item.Title = product.getTitle()[0].Replace("*", "");
                    item.Supplier = product.getSupplierName();
                    item.PicUrl = String.Format("{0}/images/{1}", agent.agentEntity.agent.htmlgeneratorproperties.imageforwarderlink.Replace("Home", ""), product.GetMainImage()[0]);

                    List<string> imageNames = new List<string>();
                    product.GetMainImage().ForEach(imageName => imageNames.Add(string.Format(agent.agentEntity.agent.htmlgeneratorproperties.imagelinkformat, product.getSupplierName(), imageName, agent.agentEntity.agent.ebayname.Replace(".", ""), ProductBrowsing.IsLocal)));

                    HtmlData htmlData = new HtmlData();
                    htmlData.ItemId = sellerDatabase.Find(product.GetEan()).ItemID;
                    htmlData.id = agent.agentEntity.agent.agentID + "_" + product.getSupplierName() + product.GetEan();
                    htmlData.attributeList = product.GetAttributeList();
                    htmlData.mainimages = imageNames;
                    htmlData.title = product.getTitle()[0];
                    htmlData.logoLink = agent.agentEntity.agent.htmlgeneratorproperties.logolink;

                    item.HtmlProperties = htmlData;

                    items.Add(item);

                    hasMoreItems = true;
                }

                skip += limit;
            }

            return items;
        }
    }
}
