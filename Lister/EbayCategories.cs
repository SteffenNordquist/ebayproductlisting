﻿using DNEbaySystem.Lister.CsvTemplates;
using DNEbaySystem.Lister.Objects;
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DNEbaySystem.DatabaseConstructor;
namespace DNEbaySystem.Lister
{
    public class EbayCategories
    {
        //private static MongoCollection<EbayCategoryEntity> Collection = new MongoClient("mongodb://client148:client148devnetworks@148.251.0.235/EbayCategories")
        //        .GetServer()
        //        .GetDatabase("EbayCategories")
        //        .GetCollection<EbayCategoryEntity>("ebaycategories");
        private static MongoCollection<EbayCategoryEntity> Collection = DBConstructor<EbayCategoryEntity>.GetCollection("mongodb://client148:client148devnetworks@148.251.0.235/EbayCategories", "EbayCategories", "ebaycategories");

        /// <summary>
        /// Returns the ebay category
        /// </summary>
        /// <param name="ids"></param>
        /// <returns>string</returns>
        public static string GetCategory(List<string> ids)
        {
            if (Collection != null)
            {
                List<EbayCategoryEntity> ebayCat = new List<EbayCategoryEntity>();
                foreach (string id in ids)
                {
                    ebayCat = Collection.Find(Query.EQ("eans", id)).SetFields("category").ToList();
                    if (ebayCat.Count() > 0)
                    {
                        break;
                    }
                }

                if (ebayCat.Count() > 0)
                {
                    return ebayCat.First().category;
                }

                return String.Empty;
            }

            return String.Empty;
        }

        /// <summary>
        /// Check if it has category
        /// </summary>
        /// <param name="ids"></param>
        /// <returns>boolean</returns>
        public static bool HasCategory(List<string> ids)
        {
            if (Collection != null)
            {
                foreach (string id in ids)
                {
                    var ebayCat = Collection.Find(Query.EQ("eans", id)).SetFields("category");
                    if (ebayCat.Count() > 0)
                    {
                        return true;
                    }
                }

            }

            return false;
        }

        /// <summary>
        /// Check if category is required
        /// </summary>
        /// <param name="templateType"></param>
        /// <returns>boolean</returns>
        public static bool CategoryRequired(TemplateType templateType)
        {
            if (templateType == TemplateType.Catalog)
            {
                return false;
            }

            return true;
        }
    }
}
