﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister
{
    public class ProductBrowsing
    {
        public static bool IsLocal
        {
            private set;
            get;
        }

        public static void SetLocalBrowsing()
        {
            IsLocal = true;
        }
    }
}
