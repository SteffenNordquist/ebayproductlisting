﻿using DNEbaySystem.Lister.CsvTemplates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister
{
    public class Template
    {
        public static TemplateType TemplateType
        {
            private set;
            get;
        }

        public static void SetTemplateType(TemplateType templateType)
        {
            TemplateType = templateType;
        }
    }
}
