﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.Modifiers
{
    public class StringModifier
    {
        public static string Truncate(string value, int maxLength, string suffix = "")
        {
            maxLength -= suffix.Length;

            if (!string.IsNullOrEmpty(value) && value.Length > maxLength)
            {
                return value.Substring(0, maxLength) + suffix;
            }

            return value + suffix;
        }
    }
}
