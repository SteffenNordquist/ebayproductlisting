﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.Objects
{
    public class EbayAgent
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Token { get; set; }
        public string Email { get; set; }

        public string MvcView { get; set; }
    }
}
