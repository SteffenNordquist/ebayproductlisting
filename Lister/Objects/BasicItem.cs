﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister.Objects
{
    public class BasicItem
    {
        public ActionType Action { get; set; }
        public string Category { get; set; }
        public string Title { get; set; }
        public string SubTitle { get; set; }
        public string Description { get; set; }
        public string ConditionID { get; set; }
        public string PicUrl { get; set; }
        public string Quantity { get; set; }
        public string Format { get; set; }
        public double StartPrice { get; set; }
        public string BuyItNowPrice { get; set; }
        public string Duration { get; set; }
        public string ImmediatePayRequired { get; set; }
        public string Location { get; set; }
        public string GalleryType { get; set; }
        public string PayPalAccepted { get; set; }
        public string PayPalEmailAddress { get; set; }
        public string PaymentInstructions { get; set; }
        public string StoreCategory { get; set; }
        public string ShippingDiscountProfileID { get; set; }
        public string DomesticRateTable { get; set; }
        public string ShippingType { get; set; }
        public string ShippingService_1Option { get; set; }
        public string ShippingService_1Cost { get; set; }
        public string ShippingService_1Priority { get; set; }
        public string ShippingService_1ShippingSurcharge { get; set; }
        public string ShippingService_2Option { get; set; }
        public string ShippingService_2Cost { get; set; }
        public string ShippingService_2Priority { get; set; }
        public string ShippingService_2ShippingSurcharge { get; set; }
        public string DispatchTimeMax { get; set; }
        public string CustomLabel { get; set; }
        public string ReturnsAcceptedOption { get; set; }
        public string RefundOption { get; set; }
        public string ReturnsWithinOption { get; set; }
        public string ShippingCostPaidByOption { get; set; }
        public string AdditionalDetails { get; set; }
        public string ShippingProfileName { get; set; }
        public string ReturnProfileName { get; set; }
        public string PaymentProfileName { get; set; }
        public string ProductBrand { get; set; }


        public BasicItem(string ean, ActionType actiontype, string title, string category, string imageLink, string description, double sellprice, int quantity, string paypalEmail, string supplierName)
        {
            this.Action = actiontype;
            this.Quantity = quantity.ToString();
            this.StartPrice = sellprice;
            this.CustomLabel = ean;
            this.Category = category;
            this.StoreCategory = "";
            this.Title = title;
            this.SubTitle = "";
            this.ConditionID = "1000";
            this.PicUrl = imageLink;
            this.GalleryType = "";
            this.Description = description;
            this.Format = "StoresFixedPrice";
            this.Duration = "GTC";
            this.BuyItNowPrice = "";
            this.PayPalAccepted = "1";
            this.PayPalEmailAddress = paypalEmail;
            this.ImmediatePayRequired = "";
            this.PaymentInstructions = "";
            this.Location = "10969";
            this.ShippingType = "Flat";
            this.ShippingService_1Option = "DE_SonstigeDomestic";
            this.ShippingService_1Cost = "0,00";
            this.ShippingService_1Priority = "";
            this.ShippingService_1ShippingSurcharge = "";
            this.ShippingService_2Option = "";
            this.ShippingService_2Cost = "";
            this.ShippingService_2Priority = "";
            this.ShippingService_2ShippingSurcharge = "";
            this.DispatchTimeMax = "3";
            this.ShippingDiscountProfileID = "";
            this.DomesticRateTable = "";
            this.ReturnsAcceptedOption = "ReturnsAccepted";
            this.ReturnsWithinOption = "";
            this.RefundOption = "";
            this.ShippingCostPaidByOption = "";
            this.AdditionalDetails = "";
            this.ShippingProfileName = "";
            this.ReturnProfileName = "";
            this.PaymentProfileName = "";
            this.ProductBrand = supplierName;
        }
    }
}
