﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DNEbaySystem.HtmlManager;
namespace DNEbaySystem.Lister.Objects
{
    public class RawItem
    {
        public string Ean { get; set; }
        public string ArticleNo { get; set; }
        public int Stock { get; set; }
        public string Title { get; set; }
        public string Supplier { get; set; }
        public string PicUrl { get; set; }
        public HtmlData HtmlProperties { get; set; }

    }
}
