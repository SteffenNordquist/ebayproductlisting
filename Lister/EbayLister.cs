﻿
using DN_Classes.Agent;
using DNEbaySystem.FileExchange;
using DNEbaySystem.Lister.CsvTemplates;
using DNEbaySystem.Lister.Implementations;
using DNEbaySystem.Lister.Interfaces;
using DNEbaySystem.Lister.Objects;
using DNEbaySystem.ListingDatabase;
using DNEbaySystem.ListingDatabase.Interfaces;
using DNEbaySystem.Logging;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.Lister
{
    public class EbayLister
    {
        private EbayAgent Agent;
        private List<RawItem> Items;
        private IGenerator FileGenerator;
        //private string SupplierName;

        public EbayLister(IDatabase database, Agent agent, int actionLimit = 5000)
        {
            //initialize logging
            SystemLogging.Error.ClearLogs();
            SystemLogging.Normal.ClearLogs();

            //set supplier name
            //this.SupplierName = database.GetType().Name.Replace("DB", "");

            if (database != null && agent != null)
            {
                

                //setting agent
                this.Agent = PrepareAgent.GetAgent(agent);

                //setting agent listing database
                ListingDatabaseFactory listingDatabaseFactory = new ListingDatabaseFactory();
                IListingDatabase listingDatabase = listingDatabaseFactory.GetListingDatabase(this.Agent.Name);

                //setting items for listing
                this.Items = PrepareItems.GetItems(database, agent, listingDatabase);

                //setting template type
                TemplateType templateType = TemplateFactory.GetType(database.GetType().Name.Replace("DB", ""));
                Template.SetTemplateType(templateType);

                //setting file generator
                FileGenerator = new CsvGenerator(Agent, Items, listingDatabase, actionLimit);
                FileGenerator.Generate();
     
            }
            else
            {
                SystemLogging.Error.AddLog(database.GetType().Name + " OR Agent is null.");
            }
        }

        /// <summary>
        /// Uploads listing to file exchange service
        /// </summary>
        /// <returns></returns>
        //public void Upload()
        //{

        //    Console.WriteLine("Uploading...");

        //    FileExchangeUploader fileExchange = new FileExchangeUploader(FileGenerator.GetFile(), Agent.Id, Agent.Token, SupplierName);

        //    Console.WriteLine("Uploaded!");


        //}

        /// <summary>
        /// returns generated file paths
        /// </summary>
        /// <returns></returns>
        public List<string> GetFilenames()
        {
            List<string> filenames = new List<string>();

            filenames.AddRange(FileGenerator.GetFile().GetRevisedItemsFilenames());

            filenames.AddRange(FileGenerator.GetFile().GetNewItemsFilenames());

            return filenames;
        }
    }
}
