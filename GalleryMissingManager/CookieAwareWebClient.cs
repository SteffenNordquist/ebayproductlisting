﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;

namespace GalleryMissingManager
{
    public class CookieAwareWebClient : WebClient
    {
        public CookieAwareWebClient()
        {
            CookieContainer = new CookieContainer();
        }
        public CookieContainer CookieContainer { get; private set; }

        protected override WebRequest GetWebRequest(Uri address)
        {
            var request = (HttpWebRequest)base.GetWebRequest(address);
            request.KeepAlive = false;
            request.Timeout = System.Threading.Timeout.Infinite;
            request.ProtocolVersion = HttpVersion.Version10;
            request.AllowWriteStreamBuffering = false;
            request.CookieContainer = CookieContainer;
            return request;
        }
    }
}
