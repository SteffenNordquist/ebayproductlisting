﻿using HtmlAgilityPack;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace GalleryMissingManager
{
    public class ScrapedRawGalleryMissingItems : IRawGalleryMissingItems
    {
        public List<string> ItemIds()
        {
            List<string> listingItemIds = File.ReadAllLines("listingItemIds.txt").ToList();

            List<string> itemids = new List<string>();

            IWebDriver driver = new FirefoxDriver();
            driver.Navigate().GoToUrl("https://signin.ebay.de/ws/eBayISAPI.dll");
            driver.FindElement(By.Id("userid")).SendKeys("Mediendo");
            driver.FindElement(By.Id("pass")).SendKeys("Friedrich40+");
            driver.FindElement(By.Id("sgnBt")).Submit();


            foreach (string listingItemId in listingItemIds)
            {
                string ebayLink = "http://www.ebay.de/itm/" + listingItemId;
                driver.Navigate().GoToUrl(ebayLink);
                //Thread.Sleep(2000);

                if (driver.PageSource.Contains("Ihr Angebot ist erfolgreich, allerdings besteht ein Problem bei Ihrem Galeriebild"))
                {
                    using (StreamWriter writer = new StreamWriter("missingGalleryItems.txt", true))
                    {
                        writer.WriteLine(listingItemId);
                        itemids.Add(listingItemId);
                    }
                }

            }

            return itemids;
        }
    }
}
