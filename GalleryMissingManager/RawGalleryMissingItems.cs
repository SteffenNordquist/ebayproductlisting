﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalleryMissingManager
{
    public class RawGalleryMissingItems : IRawGalleryMissingItems
    {
        public List<string> ItemIds()
        {
            return File.ReadAllLines("ebaymissingimages.txt").Select(x => x.Split(',')[0].Replace("\"", "")).ToList();
        }
    }
}
