﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.MongoDataAccess;
using Microsoft.Practices.Unity;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalleryMissingManager
{
    class Program
    {
        static void Main(string[] args)
        {
            #region mongo setup
            IConnectionProperties connectionProperties = new MongoConnectionProperties(new DefaultConfigurationManager(new DefaultConfigurationSource("")));
            connectionProperties.SetProperty("connectionstring","mongodb://client148:client148devnetworks@136.243.44.111/EAD_mediendo");
            connectionProperties.SetProperty("databasename", "EAD_mediendo");
            connectionProperties.SetProperty("collectionname", "listing");

            IDbConnectionFactory dbConnectionFactory = new MongoConnectionFactory(connectionProperties);
            #endregion
            IDbConnection dbConnection = dbConnectionFactory.CreateConnection();

            StartupConfiguration config = new StartupConfiguration(new UnityContainer());

            IRawGalleryMissingItems missingItems = config.Resolve<IRawGalleryMissingItems>();
            var itemsids = missingItems.ItemIds();

            List<string> recentlyEndedItems = File.ReadAllLines("recentlyEndedItems.txt").ToList();

            StringBuilder missingGalleryItems = new StringBuilder();
            missingGalleryItems.AppendLine("Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID\tCustomLabel");

            int counter = 0;
            foreach (string itemid in itemsids)
            {
                
                    string[] setfields = { "Ean", "ItemID" };
                    string query = "{ ItemID : \"" + itemid + "\"}";
                    string ean = dbConnection.DataAccess.Queries.Find<BsonDocument>(query, setfields).FirstOrDefault()["Ean"].AsString;
                
                if (recentlyEndedItems.Find(x => x.Contains(ean)) == null)
                {
                    bool updated = dbConnection.DataAccess.Commands.Update<BsonNull>("Ean", ean, "ItemID", null, true);
                    missingGalleryItems.AppendLine("EndItem\t" + itemid + "\t" + ean);

                    Console.WriteLine(counter++);
                }
            }

            File.WriteAllText("toEndItems.txt", missingGalleryItems.ToString());


        }
    }
}
