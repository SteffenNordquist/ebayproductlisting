﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalleryMissingManager
{
    public class StartupConfiguration
    {
        private readonly IUnityContainer container;

        public StartupConfiguration(IUnityContainer container)
        {
            this.container = container;
            RegisterObjects();
        }

        private void RegisterObjects()
        {
            container.RegisterType<IRawGalleryMissingItems, ScrapedRawGalleryMissingItems>();
        }

        public T Resolve<T>()
        {
            return container.Resolve<T>();
        }
    }
}
