﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace GalleryMissingManager
{
    public interface IRawGalleryMissingItems
    {
        List<string> ItemIds();
    }
}
