# README #

### EbayCSV.Core ###

* A dll for generating the csv
* Version1

### How do I get set up? ###

* Dependencies - [ productdb, dnclasses, dndataaccess, mongodb, htmlagilitypack]


------------------------------------------------------------------------

### EbayUpload.Core ###

* This dll will upload generated files to eBay.
* Version1

### How do I get set up? ###

* Dependencies - [DN.DataAccess, DN_Classes, eBay.Service, EbayCSV.Core, HtmlAgilityPack, Mongodb, ProductDB]

-------------------------------------------------------------------------

### EbayListing ###

* This tool will pull inventory, generate and upload generated files to eBay.
* Version2

### How do I get set up? ###

* Deploy to agent server.
* Configuration - AppConfiguration.cs
* Dependencies - [DN.DataAccess, DN_Classes, eBay.Service, EbayCSV.Core, EbayUpload.Core, HtmlAgilityPack, Mongodb, ProductDB, Unity]

------------------------------------------------------------------------

### EbaySellpricesPrepare ###

* This tool will run daily and calculate sellprices for each item.
* Version1

### How do I get set up? ###

* Set agent ids of the tool.
* Configuration - [ CalculationSetup.cs ]
* Dependencies - [DN.DataAccess, DN_Classes, eBay.Service, EbayCSV.Core, Mongodb, ProductDB, Unity]

### Who do I talk to? ###

* Wylan Osorio