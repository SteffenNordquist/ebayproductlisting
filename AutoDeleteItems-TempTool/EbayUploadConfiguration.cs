﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.MongoDataAccess;
using DN_Classes.Agent;
using EbayCSV.Core.Settings;
using EbayUpload.Core;
using EbayUpload.Core.UploadResponse;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace AutoDeleteItems_TempTool
{
    public class EbayUploadConfiguration
    {
        private readonly IUnityContainer container;
        private readonly Agent agent;

        public EbayUploadConfiguration(Agent agent)
        {
            this.container = new UnityContainer();
            this.agent = agent;

            RegisterDependencies();
        }

        private void RegisterDependencies()
        {
            container.RegisterInstance(GetSettings());
            container.RegisterInstance(GetResponseDBConnection());
            container.RegisterType<IListingFileUploader, FileExchangeCsvFileUploader>();
            container.RegisterType<IReferenceParser, ReferenceParser>();
            container.RegisterType<IMessageParser, MessageParser>();
            container.RegisterType<IUploadResponseManager, UploadResponseManager>();
            container.RegisterType<IEbayUploader, EbayUploader>();
        }

        private ISettings GetSettings()
        {

            ISettings settings = new DefaultSettings();
            settings.SetValue("ebaytoken", agent.agentEntity.agent.keys.ebay.token);
            settings.SetValue("agentid", agent.agentEntity.agent.agentID.ToString());
            settings.SetValue("supplier", "jpc");

            return settings;
        }

        private IDbConnection GetResponseDBConnection()
        {
            #region dn data access
            IConfigurationManager configurationManager = new NullConfigurationManager();

            IConnectionProperties connectionProperties = new MongoConnectionProperties(configurationManager);
            connectionProperties.SetProperty("connectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/FileExchangeResponses");
            connectionProperties.SetProperty("databasename", "FileExchangeResponses");
            connectionProperties.SetProperty("collectionname", "responses");

            IDbConnectionFactory connectionFactory = new MongoConnectionFactory(connectionProperties);
            #endregion

            IDbConnection dbConnection = connectionFactory.CreateConnection();

            return dbConnection;
        }

        public TResult Resolve<TResult>()
        {
            return container.Resolve<TResult>();
        }
    }
}
