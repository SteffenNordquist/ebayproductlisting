﻿using DN.DataAccess;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.MongoDataAccess;
using DN_Classes.Agent;
using DN_Classes.AppStatus;
using EbayUpload.Core;
using MongoDB.Bson;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace AutoDeleteItems_TempTool
{
    class Program
    {
        static void Main(string[] args)
        {
            #region mongo setup
            IConnectionProperties connectionProperties = new MongoConnectionProperties(new DefaultConfigurationManager(new DefaultConfigurationSource("")));
            connectionProperties.SetProperty("connectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/EAD_mediendo");
            connectionProperties.SetProperty("databasename", "EAD_mediendo");
            connectionProperties.SetProperty("collectionname", "listing");

            IDbConnectionFactory dbConnectionFactory = new MongoConnectionFactory(connectionProperties);
            #endregion
            IDbConnection dbConnection = dbConnectionFactory.CreateConnection();

            using (ApplicationStatus appStatus = new ApplicationStatus("EbayAutoDelete_Mediendo"))
            {
                try
                {
                    

                    string query = "{ ItemID : {$ne:null} }";
                    string[] setfields = { "ItemID", "Quantity", "Wanted.Quantity" , "ListingStatus"};
                    var candidateItems = dbConnection.DataAccess.Queries.Find<BsonDocument>(query, setfields);

                    StringBuilder writer = new StringBuilder();
                    writer.AppendLine("Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID");

                    foreach (var item in candidateItems)
                    {
                        int quantity = int.Parse(item["Quantity"].ToString());
                        int wantedQuantity = int.Parse(item["Wanted"]["Quantity"].ToString());
                        string itemId = item["ItemID"].AsString;
                        string listingStatus = "";

                        if (item.Contains("ListingStatus")) { listingStatus = item["ListingStatus"].AsString; }

                        if (wantedQuantity == 0 && quantity > wantedQuantity && listingStatus.ToLower() != "completed")
                        {
                            writer.AppendLine("EndItem\t" + itemId);
                            dbConnection.DataAccess.Commands.Update<string>("{ ItemID : \"" + itemId + "\"}", "ItemID", null);
                        }
                    }

                    string filepath = AppDomain.CurrentDomain.BaseDirectory + "\\toDeleteItems.txt";

                    File.WriteAllText(filepath, writer.ToString());

                    Agent agent = new Agent(15);
                    EbayUploadConfiguration ebayUploadConfiguration = new EbayUploadConfiguration(agent);

                    List<string> generatedData = new List<string>();
                    generatedData.Add(filepath);

                    IEbayUploader ebayUploader = ebayUploadConfiguration.Resolve<IEbayUploader>();
                    ebayUploader.BeginUpload<string>(generatedData);

                    appStatus.AddMessagLine("success");
                    appStatus.Successful();
                }
                catch (Exception ex)
                {
                    appStatus.AddMessagLine(ex.StackTrace);
                }
            }
        }
    }
}
