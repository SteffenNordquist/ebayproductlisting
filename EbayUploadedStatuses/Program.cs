﻿using AgentDLL;
using DN_Classes.AppStatus;
using DNEbaySystem.FileExchange;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayUploadedStatuses
{
    class Program
    {
        static void Main(string[] args)
        {
            using (ApplicationStatus appStatus = new ApplicationStatus("EbayUploadedItemsStatus"))
            {
                try
                {
                    Agent agent = new Agent(0);

                    FileExchangeDB responsesDB = new FileExchangeDB();

                    List<FileExchangeEntity> responses = responsesDB.GetResponsesByTimeFilters(DateTime.Now.AddDays(-1))
                        .Where(x => x.AgentId == agent.agentEntity.agent.agentID)
                        .Where(x => x.UploadMessage.Contains(x.ReferenceNumber)).ToList();

                    foreach (var response in responses)
                    {

                        string path = "response.csv";
                        FileExchangeDownloader.DownloadUploadsStatusFile(response.ReferenceNumber, agent.agentEntity.agent.keys.ebay.token, path);

                        ItemsUploadedStatus uploadsStatus = new ItemsUploadedStatus(path, agent, response.ReferenceNumber);
                        uploadsStatus.ProcessFile();
                        uploadsStatus.UpdateDB();

                    }

                    appStatus.Successful();
                    appStatus.AddMessagLine("Success");
                }
                catch (Exception ex)
                {
                    appStatus.AddMessagLine(ex.Message);
                }
            }
        }
    }
}
