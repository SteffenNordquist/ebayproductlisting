﻿using DN_Classes.AppStatus;
using DNEbaySystem.Inventory;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayInventory
{
    class Program
    {
        static void Main(string[] args)
        {
            ///sampleids
            ///liefer 14
            ///berliner 12
            ///protect 9

            //using (ApplicationStatus appStatus = new ApplicationStatus("EbayInventory-Protect"))
            //using (ApplicationStatus appStatus = new ApplicationStatus("EbayInventory-LieferLucas"))
            //using (ApplicationStatus appStatus = new ApplicationStatus("EbayInventory-AllesAufLager"))
            using (ApplicationStatus appStatus = new ApplicationStatus("EbayInventory_Mediendo"))
            {

                IInventory ebayInventory = new EbayAccountInventory(15);
                InventoryProcessor.Process(ebayInventory);

                appStatus.Successful();

            }
        }
    }
}
