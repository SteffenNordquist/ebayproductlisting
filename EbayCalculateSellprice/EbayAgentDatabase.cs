﻿using DNEbaySystem.ListingDatabase;
using DNEbaySystem.ListingDatabase.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCalculateSellprice
{
    public class EbayAgentDatabase
    {
        public IListingDatabasePriceCalculation Database;

        /// <summary>
        /// initializes agents listing database
        /// </summary>
        /// <param name="ebayname"></param>
        public void SetAccountData(string ebayname)
        {
            ListingDatabaseFactory dbFactory = new ListingDatabaseFactory();
            Database = dbFactory.GetListingDatabasePriceCalculation(ebayname);
        }
    }
}
