﻿
using DN_Classes.Agent;
using DNEbaySystem.Lister.SellPriceCalc;
using DNEbaySystem.ListingDatabase.Objects;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCalculateSellprice
{
    public class SellPriceCalculation
    {
        private IDatabase Database;
        private Agent Agent;
        private IProductSalesInformation Product;
        private EbayAgentDatabase EAD;

        public SellPriceCalculation(IDatabase database, Agent agent, IProductSalesInformation product, EbayAgentDatabase ead)
        {
            this.Database = database;
            this.Agent = agent;
            this.Product = product;
            this.EAD = ead;

            double calculatedSellPrice = Calculate();

            Save(calculatedSellPrice);
        }

        private double Calculate()
        {
            double calculatedSellPrice = SellPriceCalculator.GetSellPrice(Database, Agent, Product.GetEan(), Product.getSupplierName());

            return calculatedSellPrice;
        }

        private void Save(double sellPrice)
        {
            WantedClass wanted = new WantedClass();
            wanted.Price = sellPrice;
            wanted.Quantity = Product.getStock();

            ListingEntity listing = new ListingEntity
            {
                id = Product.GetEan(),
                Ean = Product.GetEan(),
                SupplierName = Product.getSupplierName(),
                Wanted = wanted
            };

            EAD.Database.Upsert(listing);

            Console.WriteLine("Sellprice Calculated\t" + listing.Ean + "\t" + listing.Wanted.Price);

        }
    }
}
