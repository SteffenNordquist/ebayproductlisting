﻿
using DN_Classes.Agent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace EbayCalculateSellprice
{
    class Program
    {
        static void Main(string[] args)
        {
            ThreadStart threadStart = delegate
            {
                Agent agent = new Agent(15);
                AgentProcessor processor = new AgentProcessor(agent, "EbaySellPricesCalculation_Mediendo");
                processor.Work();
            };
            new Thread(threadStart).Start();
        }
    }
}
