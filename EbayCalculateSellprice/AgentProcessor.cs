﻿using DN_Classes.Agent;
using DN_Classes.AppStatus;
using DNEbaySystem.EbayAgentSuppliers;
using DNEbaySystem.Logging;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayCalculateSellprice
{
    class AgentProcessor
    {
        private Agent Agent;
        private string AppName;
        private EbayAgentDatabase EAD;

        public AgentProcessor(Agent agent, string appName)
        {
            this.Agent = agent;
            this.AppName = appName;
            EAD = new EbayAgentDatabase();
            EAD.SetAccountData(Agent.agentEntity.agent.ebayname);
        }

        /// <summary>
        /// process each supplier's each item
        /// </summary>
        public void Work()
        {
            using (ApplicationStatus appStatus = new ApplicationStatus(AppName))
            {
                AgentSuppliers agentSuppliers = new AgentSuppliers(Agent);
                List<IDatabase> supplierDatabaseList = agentSuppliers.GetList();

                foreach (IDatabase supplierDatabase in supplierDatabaseList)
                {
                    int skipped = 0;
                    bool hasItems = true;
                    while (hasItems)
                    {
                        hasItems = false;
                        int amount = 1000;

                        foreach (IProductSalesInformation product in supplierDatabase.GetEbayReady(skipped, amount))
                        //Parallel.ForEach(supplierDatabase.GetEbayReady(skipped, amount), product =>
                        {

                            SellPriceCalculation sellPriceCalculation = new SellPriceCalculation(supplierDatabase, Agent, product, EAD);


                            hasItems = true;

                        };


                        skipped += amount;
                    }



                    appStatus.AddMessagLine(supplierDatabase.GetType().Name + "</br>" + SystemLogging.Error.GetLogs() + "</br>");
                    File.WriteAllText(AppName + "SellPricesLog.txt", SystemLogging.Normal.GetLogs());

                }

                appStatus.Successful();
            }
        }
    }
}
