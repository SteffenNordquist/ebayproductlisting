﻿using DN.DataAccess;
using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using DN_Classes.Agent;
using EbayCSV.Core.Settings;
using EbayUpload.Core;
using EbayUpload.Core.UploadResponse;
using Microsoft.Practices.Unity;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayListingV2
{
    public class EbayUploadConfiguration
    {
        private readonly IUnityContainer container;
        private readonly Agent agent;
        private readonly IDatabase database;

        public EbayUploadConfiguration(Agent agent, IDatabase database)
        {
            this.container = new UnityContainer();
            this.agent = agent;
            this.database = database;

            RegisterDependencies();
        }

        private void RegisterDependencies()
        {
            container.RegisterInstance(GetSettings());
            container.RegisterInstance(GetResponseDBConnection());
            container.RegisterType<IListingFileUploader, FileExchangeCsvFileUploader>();
            container.RegisterType<IReferenceParser, ReferenceParser>();
            container.RegisterType<IMessageParser, MessageParser>();
            container.RegisterType<IUploadResponseManager, UploadResponseManager>();
            container.RegisterType<IEbayUploader, EbayUploader>();
        }

        private ISettings GetSettings()
        {
            AppConfigurationManager appConfiguration = new AppConfigurationManager();

            ISettings settings = new DefaultSettings();
            settings.SetValue("ebaytoken", agent.agentEntity.agent.keys.ebay.token);
            settings.SetValue("agentid", agent.agentEntity.agent.agentID.ToString());
            settings.SetValue("supplier", database.GetType().ToString().ToLower().Replace("db", ""));

            return settings;
        }

        private IDbConnection GetResponseDBConnection()
        {
            #region dn data access
            IDbConnectionFactory connectionFactory = new MongoConnectionFactory(new MongoConnectionProperties());
            connectionFactory.ConnectionProperties.SetProperty("connectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/FileExchangeResponses");
            connectionFactory.ConnectionProperties.SetProperty("databasename", "FileExchangeResponses");
            connectionFactory.ConnectionProperties.SetProperty("collectionname", "responses");
            #endregion

            IDbConnection dbConnection = connectionFactory.CreateConnection();

            return dbConnection;
        }

        public TResult Resolve<TResult>()
        {
            return container.Resolve<TResult>();
        }
    }
}
