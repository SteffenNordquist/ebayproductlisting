﻿using DN_Classes.Agent;
using DN_Classes.AppStatus;
using DN_Classes.Logger;
using EbayCSV.Core;
using EbayCSV.Core.Generator;
using EbayCSV.Core.Factories.SellpriceFactory;
using EbayCSV.Core.Factories.TitleFactory;
using EbayCSV.Core.HtmlData.BK;
using EbayCSV.Core.ResultDirectory;
using EbayCSV.Core.Suppliers;
using EbayListingV2.Inventory;
using EbayListingV2.IOCConfigurations;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConfigurationData;
using EbayCSV.Core.Factories.ShippingService1CostFactory;
using EbayCSV.Core.Settings;

namespace EbayListingV2
{
    class Program
    {
        static void Main(string[] args)
        {
            Agent agent = new Agent();

            string configFile = AppDomain.CurrentDomain.BaseDirectory + @"\AppConfiguration.config";
            IConfigurationSource configSource = new ConfigurationSource(configFile);
            IConfigurationManager appConfiguration = new AppConfigurationManager(configSource);

            ILogFileLocation logFileLocation = new DefaultLogFileLocation { LogDirectory = appConfiguration.GetValue("LogDirectory"), ToolName = "EbayListing", SubToolName = "EbayListing" };
            LoggingConfig loggingConfig = new LoggingConfig();
            loggingConfig.Register(logFileLocation);

            using (ILogger logger = loggingConfig.Resolve<ILogger>())
            {
                logger.Info("Agent id : " + agent.agentEntity.agent.agentID);

                bool inventoryProcessSucceded = true;

                if (Boolean.Parse(appConfiguration.GetValue("PullInventory")) == true)
                {
                    logger.Info("retrieving inventory");
                    //inventory retirieval
                    InventoryProcessor inventoryProcessor = new InventoryProcessor(agent, appConfiguration);
                    inventoryProcessSucceded = inventoryProcessor.Process();
                }

                if (inventoryProcessSucceded)
                {
                    logger.Info("starting listing process");
                    //listing generation and upload
                    ListingProcessor listingProcessor = new ListingProcessor(agent, appConfiguration);
                    listingProcessor.Process();
                }
            }
        }
    }
}
