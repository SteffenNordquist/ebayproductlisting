﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayListingV2.ConfigManager
{
    public class AppConfigurationManager
    {
        private readonly Configuration config;
        private readonly string configFile;

        public AppConfigurationManager()
        {
            configFile = AppDomain.CurrentDomain.BaseDirectory + @"\AppConfiguration.config";
            AppDomain.CurrentDomain.SetData("APP_CONFIG_FILE", configFile);

            config = ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
           
        }

        public string GetValue(string name)
        {
            //return ConfigurationManager.AppSettings[name];
            return config.AppSettings.Settings[name].Value;
        }

        public void SetValue(string name, string value)
        {
            //ConfigurationManager.AppSettings[name] = value;
            config.AppSettings.Settings[name].Value = value;

            //ConfigurationManager.RefreshSection("appSettings");
        }

        public void Save()
        {
            config.Save(ConfigurationSaveMode.Modified);
        }
    }
}
