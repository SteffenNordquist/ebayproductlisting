﻿using DN_Classes.Agent;
using EbayCSV.Core.Settings;
using EbayListingV2.Inventory.DbUpdate;
using EbayListingV2.Inventory.Downloader;
using EbayListingV2.Inventory.EbayAPI;
using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory.Mongo;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.Inventory.Source;

namespace EbayListingV2.Inventory
{
    public class EbayInventoryConfiguration
    {
        private readonly IUnityContainer _container;
        private readonly Agent _agent;
        private readonly IConfigurationManager _appConfiguration;

        public EbayInventoryConfiguration(Agent agent, IConfigurationManager appConfiguration)
        {
            this._container = new UnityContainer();
            this._agent = agent;
            this._appConfiguration = appConfiguration;

            RegisterDependencies();
        }

        private void RegisterDependencies()
        {
            _container.RegisterInstance(GetSettings());

            SetInventoryConfiguration();

            _container.RegisterType<IEbayAPIContext, EbayAPIContext>();
            _container.RegisterType<IIventoryDownloader, InventoryDownloader>();
            _container.RegisterType<IDbUpdater, DbUpdater>();
        }

        private ISettings GetSettings()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("ebaytoken", _agent.agentEntity.agent.keys.ebay.token);
            settings.SetValue("ebayname", _agent.agentEntity.agent.ebayname);
            settings.SetValue("listingdbconnectionstring", _appConfiguration.GetValue("ListingDBConnectionStringFormat"));
            settings.SetValue("listingdbdatabasename", _appConfiguration.GetValue("ListingDBNameFormat"));
            settings.SetValue("listingdbcollectionname", _appConfiguration.GetValue("ListingCollectionName"));

            return settings;
        }

        private void SetInventoryConfiguration()
        {
            ISettings settings = _container.Resolve<ISettings>();

            IInventoryDbFactory inventoryDbFactory = new InventoryDbFactory(settings, new MongoConnectionFactory(new MongoConnectionProperties()));

            InventoryStorage.InventorySource = new InventorySource(inventoryDbFactory.InventoryDbConnection(settings.GetValue("ebayname")));
        }

        public TResult Resolve<TResult>()
        {
            return _container.Resolve<TResult>();
        }
    }
}
