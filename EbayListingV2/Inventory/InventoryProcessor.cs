﻿using DN_Classes.Agent;
using DN_Classes.AppStatus;
using EbayListingV2.Inventory.DbUpdate;
using EbayListingV2.Inventory.Downloader;
using EbayListingV2.Inventory.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConfigurationData;

namespace EbayListingV2.Inventory
{
    public class InventoryProcessor
    {
        private readonly Agent _agent;
        private readonly IConfigurationManager _appConfiguration;

        public InventoryProcessor(Agent agent, IConfigurationManager appConfiguration)
        {
            this._agent = agent;
            this._appConfiguration = appConfiguration;
        }

        public bool Process()
        {
            bool inventoryProcessSucceded = true;

            using (ApplicationStatus appStatus = new ApplicationStatus("EbayInventory_" + _agent.agentEntity.agent.ebayname))
            {
                try
                {
                    EbayInventoryConfiguration inventoryConfiguration = new EbayInventoryConfiguration(_agent, _appConfiguration);

                    IIventoryDownloader inventoryDownloader = inventoryConfiguration.Resolve<IIventoryDownloader>();
                    List<EbayInventoryItem> inventoryItems = inventoryDownloader.DownloadInventory<EbayInventoryItem>().ToList();

                    IDbUpdater inventoryUpdater = inventoryConfiguration.Resolve<IDbUpdater>();
                    inventoryUpdater.Update<EbayInventoryItem>(inventoryItems);


                    appStatus.AddMessagLine("Success");
                    appStatus.Successful();
                }
                catch (Exception ex)
                {
                    appStatus.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
                    inventoryProcessSucceded = false;
                }
            }

            return inventoryProcessSucceded;
        }
    }
}
