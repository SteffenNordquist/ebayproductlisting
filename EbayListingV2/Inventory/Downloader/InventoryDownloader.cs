﻿using eBay.Service.Call;
using eBay.Service.Core.Soap;
using EbayListingV2.Inventory.EbayAPI;
using EbayListingV2.Inventory.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayListingV2.Inventory.Downloader
{
    public class InventoryDownloader : IIventoryDownloader
    {
        private readonly IEbayAPIContext ebayApiContext;

        public InventoryDownloader(IEbayAPIContext ebayApiContext)
        {
            this.ebayApiContext = ebayApiContext;
        }

        public IEnumerable<TResult> DownloadInventory<TResult>()
        {
            StringBuilder sbNoSKU = new StringBuilder();
            //StringBuilder missingGalleryItems = new StringBuilder();
            //missingGalleryItems.AppendLine("Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID\tCustomLabel");

            List<EbayInventoryItem> inventoryItems = new List<EbayInventoryItem>();

            //var getItemCall = new GetItemCall(ebayApiContext.GetApiContext());
            //var itemTest = getItemCall.GetItem("272194318392");
            //var shippingCost = itemTest.ShippingDetails.ShippingServiceOptions[0].ShippingServiceCost.Value;

            //test token validation
            var timeCall = new GeteBayOfficialTimeCall(ebayApiContext.GetApiContext());
            Console.WriteLine(timeCall.GeteBayOfficialTime());
            //

            var sellerlistCall = new GetSellerListCall(ebayApiContext.GetApiContext());

            sellerlistCall.DetailLevelList.Add(DetailLevelCodeType.ReturnAll);
            sellerlistCall.GranularityLevel = GranularityLevelCodeType.Fine;

            bool hasMoreItems = true;
            int pageNumber = 0;

            PaginationType pt = new PaginationType();
            pt.EntriesPerPage = 200;
            //sellerlistCall.Pagination = pt;
            //pt.PageNumber = pageNumber;

            TimeFilter timeFilter = new TimeFilter(DateTime.Now.AddDays(-5), DateTime.Now.AddMonths(3));
            sellerlistCall.EndTimeFilter = timeFilter;
            sellerlistCall.StartTimeFilter = timeFilter;


            int counter = 0;
            while (hasMoreItems)
            {
                pageNumber++;

                pt.PageNumber = pageNumber;
                sellerlistCall.Pagination = pt;
                sellerlistCall.EndTimeFilter = timeFilter;
                sellerlistCall.StartTimeFilter = timeFilter;

                sellerlistCall.Execute();

                foreach (var item in sellerlistCall.ItemList.ToArray())
                {
                    if (item.SKU != null)
                    {

                        double buyItNowPrice = 0, startPrice = 0;
                        double.TryParse(item.BuyItNowPrice.Value.ToString(), out buyItNowPrice);
                        double.TryParse(item.StartPrice.Value.ToString(), out startPrice);

                        inventoryItems.Add(new EbayInventoryItem
                        {
                            ListingStatus = item.SellingStatus.ListingStatus,
                            ItemId = item.ItemID,
                            Ean = item.SKU,
                            Quantity = item.Quantity,
                            QuantityAvailable = item.QuantityAvailable,
                            BuyItNowPrice = buyItNowPrice,
                            StartPrice = startPrice
                        });
                        Console.WriteLine("{0}\t{1}", item.ItemID, counter); counter++;
                    }
                    else { sbNoSKU.AppendLine(item.ItemID); }
                }

                hasMoreItems = sellerlistCall.HasMoreItems;

            }

            if (!Directory.Exists(@"C:\EbayInventory\"))
            {
                Directory.CreateDirectory(@"C:\EbayInventory\");
            }

            File.WriteAllText(@"C:\EbayInventory\Log_NoSKU_Items.txt", sbNoSKU.ToString());
            File.WriteAllText(@"C:\EbayInventory\Log_ItemCount.txt", counter.ToString());
            //File.WriteAllText(@"C:\EbayInventory\Log_GalleryMissing.txt", missingGalleryItems.ToString());

            return (IEnumerable<TResult>)(IEnumerable<object>)inventoryItems;

            //var sellingCall = new GetMyeBaySellingCall(apiContext);

            //sellingCall.DetailLevelList.Add(DetailLevelCodeType.ReturnAll);

            //sellingCall.Execute();


            //counter = 0;

            //foreach (ItemType item in sellingCall.ApiResponse.ActiveList.ItemArray)
            //{
            //    double buyItNowPrice = 0;
            //    double.TryParse(item.BuyItNowPrice.ToString(), out buyItNowPrice);
            //    InventoryItems.Add(new InventoryItem
            //    {

            //        ItemId = item.ItemID,
            //        Ean = item.SKU,
            //        Quantity = item.QuantityAvailable,
            //        BuyItNowPrice = buyItNowPrice
            //    });
            //    Console.WriteLine("{0}\t{1}", item.ItemID, counter); counter++;
            //}
        }
    }
}
