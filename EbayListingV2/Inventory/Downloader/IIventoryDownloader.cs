﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayListingV2.Inventory.Downloader
{
    public interface IIventoryDownloader
    {
        IEnumerable<TResult> DownloadInventory<TResult>();
    }
}
