﻿using eBay.Service.Core.Soap;
using EbayCSV.Core.Inventory;
using EbayListingV2.Inventory.Entities;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayListingV2.Inventory.DbUpdate
{
    public class DbUpdater : IDbUpdater
    {
        public void Update<TSource>(IEnumerable<TSource> tSources)
        {
            List<EbayInventoryItem> inventoryItems = (List<EbayInventoryItem>)(IEnumerable<object>)tSources;

            StringBuilder sbStatusLog = new StringBuilder();
            StringBuilder sbCompletedLog = new StringBuilder();
            StringBuilder sbEndedLog = new StringBuilder();
            StringBuilder sbDups = new StringBuilder();
            sbDups.AppendLine("Action(SiteID=Germany|Country=DE|Currency=EUR|Version=745)\tItemID\tCustomLabel");

            foreach (EbayInventoryItem i in inventoryItems.OrderBy(x => x.Ean))
            {
                if (i.ListingStatus == ListingStatusCodeType.Completed)
                {
                    sbCompletedLog.AppendLine(i.Ean + "\t" + i.ItemId);
                }
                else if ( i.ListingStatus == ListingStatusCodeType.Ended)
                {
                    sbEndedLog.AppendLine(i.Ean + "\t" + i.ItemId);
                }



                var itemsSameEan = inventoryItems.FindAll(x => x.Ean == i.Ean);
                if (itemsSameEan.Count() > 1)
                {
                    if (itemsSameEan.FindAll(x => x.ListingStatus == ListingStatusCodeType.Active).Count() > 1)
                    {
                        foreach (var item in itemsSameEan)
                        {
                            List<EbayCSV.Core.Inventory.Inventory> dbRecords = InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Queries.Find<EbayCSV.Core.Inventory.Inventory>(" { ItemID : \"" + item.ItemId + "\" } ").ToList();

                            if (dbRecords.Count() == 0)
                            {
                                sbDups.AppendLine("EndItem" + "\t" + i.ItemId + "\t" + i.Ean);
                            }
                            else
                            {
                                string updateQuery = " { _id : \"" + item.Ean + "\" } ";
                                InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<double>(updateQuery, "BuyItNowPrice", item.BuyItNowPrice, false, true);
                                InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<double>(updateQuery, "StartPrice", item.StartPrice);
                                InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<int>(updateQuery, "Quantity", item.Quantity);
                                InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<int>(updateQuery, "QuantityAvailable", item.QuantityAvailable);
                                //listingDatabase.dataWriter().Update<string>(updateQuery, "ItemID", testExist.ItemID);
                                InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<string>(updateQuery, "ListingStatus", i.ListingStatus.ToString());
                                //listingDatabase.dataWriter().Update<string>(updateQuery, "Ean", testExist.Ean);
                            }
                        }

                    }
                    else
                    {
                        var testActiveDuplicate = itemsSameEan.Find(x => x.ListingStatus == ListingStatusCodeType.Active);
                        if (testActiveDuplicate != null)
                        {
                            string updateQuery = " { _id : \"" + testActiveDuplicate.Ean + "\" } ";
                            InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<double>(updateQuery, "BuyItNowPrice", testActiveDuplicate.BuyItNowPrice, false, true);
                            InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<double>(updateQuery, "StartPrice", testActiveDuplicate.StartPrice);
                            InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<int>(updateQuery, "Quantity", testActiveDuplicate.Quantity);
                            InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<int>(updateQuery, "QuantityAvailable", testActiveDuplicate.QuantityAvailable);
                            InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<string>(updateQuery, "ItemID", testActiveDuplicate.ItemId);
                            InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<string>(updateQuery, "ListingStatus", i.ListingStatus.ToString());
                            InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<string>(updateQuery, "Ean", testActiveDuplicate.Ean);

                        }
                            var testEndedDuplicate = itemsSameEan.Find(x => x.ListingStatus == ListingStatusCodeType.Completed || x.ListingStatus == ListingStatusCodeType.Ended);
                            string deleteQuery = " { ItemID : \"" + testEndedDuplicate.ItemId + "\" } ";
                            InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Delete(deleteQuery);
                        
                    }
                }
                else
                {
                    string updateQuery = " { _id : \"" + i.Ean + "\" } ";
                    InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<double>(updateQuery, "BuyItNowPrice", i.BuyItNowPrice, false, true);
                    InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<double>(updateQuery, "StartPrice", i.StartPrice);
                    InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<int>(updateQuery, "Quantity", i.Quantity);
                    InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<int>(updateQuery, "QuantityAvailable", i.QuantityAvailable);
                    InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<string>(updateQuery, "ItemID", i.ItemId);
                    InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<string>(updateQuery, "ListingStatus", i.ListingStatus.ToString());
                    InventoryStorage.InventorySource.InventoryDbConnection().DataAccess.Commands.Update<string>(updateQuery, "Ean", i.Ean);
                }


                sbStatusLog.AppendLine(i.ListingStatus + "\t" + i.Ean + "\t" + i.ItemId + "\t" + i.StartPrice + "\t" + i.Quantity);


            }

            File.WriteAllText(@"C:\EbayInventory\Log_Inventory_Items.txt", sbStatusLog.ToString());
            File.WriteAllText(@"C:\EbayInventory\Log_Duplicate_Items.txt", sbDups.ToString());
            File.WriteAllText(@"C:\EbayInventory\Log_Completed_Items.txt", sbCompletedLog.ToString());
            File.WriteAllText(@"C:\EbayInventory\Log_Ended_Items.txt", sbEndedLog.ToString());
        }
    }
}
