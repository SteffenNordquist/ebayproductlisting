﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayListingV2.Inventory.DbUpdate
{
    public interface IDbUpdater
    {
        void Update<TSource>(IEnumerable<TSource> tSources);
    }
}
