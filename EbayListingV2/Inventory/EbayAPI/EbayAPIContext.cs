﻿using eBay.Service.Core.Sdk;
using eBay.Service.Core.Soap;
using eBay.Service.Util;
using EbayCSV.Core.Settings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayListingV2.Inventory.EbayAPI
{
    public class EbayAPIContext :IEbayAPIContext
    {
        private readonly ISettings settings;

        public EbayAPIContext(ISettings settings)
        {
            this.settings = settings;
        }

        public ApiContext GetApiContext()
        {
            string ebaytoken = settings.GetValue("ebaytoken");

            ApiContext apiContext = new ApiContext();

            //set Api Server Url
            apiContext.SoapApiServerUrl = "https://api.ebay.com/wsapi";

            //set Api Token to access eBay Api Server
            ApiCredential apiCredential = new ApiCredential();

            apiCredential.eBayToken = ebaytoken;


            apiContext.ApiCredential = apiCredential;
            //set eBay Site target to US
            apiContext.Site = SiteCodeType.US;



            //set Api logging
            apiContext.ApiLogManager = new ApiLogManager();
            apiContext.ApiLogManager.ApiLoggerList.Add(
                new FileLogger("listing_log.txt", true, true, true)
                );
            apiContext.ApiLogManager.EnableLogging = false;


            return apiContext;
        }
    }
}
