﻿using eBay.Service.Core.Sdk;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayListingV2.Inventory.EbayAPI
{
    public interface IEbayAPIContext
    {
        ApiContext GetApiContext();
    }
}
