﻿using DN_Classes.Agent;
using DN_Classes.Logger;
using DN_Classes.Supplier;
using EbayCSV.Core.Category;
using EbayCSV.Core.Category.Factory;
using EbayCSV.Core.CsvGenerator;
using EbayCSV.Core.CsvItem.ActionFormats;
using EbayCSV.Core.CsvItem.ActionFormats.Basic;
using EbayCSV.Core.CsvItem.ActionFormats.Catalog;
using EbayCSV.Core.CsvItem.CsvHeader;
using EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats;
using EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Basic;
using EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Catalog;
using EbayCSV.Core.Factories;
using EbayCSV.Core.Factories.CategoryFactory;
using EbayCSV.Core.Factories.CustomFields;
using EbayCSV.Core.Factories.CustomLabelFactory;
using EbayCSV.Core.Factories.DispatchTimeFactory;
using EbayCSV.Core.Factories.DurationFactory;
using EbayCSV.Core.Factories.FormatFactory;
using EbayCSV.Core.Factories.ImageFactory;
using EbayCSV.Core.Factories.ListingActionFactory;
using EbayCSV.Core.Factories.LocationFactory;
using EbayCSV.Core.Factories.PaypalAcceptFactory;
using EbayCSV.Core.Factories.PaypalEmailFactory;
using EbayCSV.Core.Factories.RelationshipDetailsFactory;
using EbayCSV.Core.Factories.RelationshipFactory;
using EbayCSV.Core.Factories.ReturnsOptionFactory;
using EbayCSV.Core.Factories.SellpriceFactory;
using EbayCSV.Core.Factories.ShippingService1CostFactory;
using EbayCSV.Core.Factories.ShippingService1FreeShippingFactory;
using EbayCSV.Core.Factories.ShippingService1OptionFactory;
using EbayCSV.Core.Factories.StockFactory;
using EbayCSV.Core.Factories.StoreCategoryFactory;
using EbayCSV.Core.Factories.TemplateFactory;
using EbayCSV.Core.Factories.TitleFactory;
using EbayCSV.Core.Factories.TitleFactory.SubTitleFactory;
using EbayCSV.Core.FileWriter;
using EbayCSV.Core.Filter;
using EbayCSV.Core.HtmlData;
using EbayCSV.Core.HtmlData.BK;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.Inventory.InventorySource;
using EbayCSV.Core.Inventory.InventorySource.ListingInventory.BK;
using EbayCSV.Core.Mapper;
using EbayCSV.Core.Mapper.VariationMapper;
using EbayCSV.Core.RawData;
using EbayCSV.Core.ResultDirectory;
using EbayCSV.Core.Settings;
using Microsoft.Practices.Unity;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EbayListingV2
{
    public class EbayCsvConfiguration
    {
        private readonly IUnityContainer container;
        private readonly IDatabase supplierDb;
        private readonly Agent agent;

        public EbayCsvConfiguration(IDatabase supplierDb, Agent agent)
        {
            this.container = new UnityContainer();
            this.supplierDb = supplierDb;
            this.agent = agent;
            RegisterDependencies();
        }

        private void RegisterDependencies()
        {
            ILogFileLocation logFileLocation = new DefaultLogFileLocation { LogDirectory = "C:\\DevNetworks\\Logs\\EbayListing", LogFileName = DateTime.Now.Ticks + "logs.txt" };
            container.RegisterInstance(logFileLocation);
            container.RegisterType<ILogger, DefaultLogger>();

            container.RegisterInstance(supplierDb);
            //container.RegisterType<IRawItems, BKRawItems>();
            container.RegisterType<IRawItems, BKRawItemsWithJtlJpcSupplier>();
            container.RegisterInstance(GetSettings());

            container.RegisterType<IListingDatabaseFactory, BKListingDatabaseFactory>();

            container.RegisterType<IHtmlDownloader, HtmlDownloader>();
            container.RegisterType<IHtmlCompressor, HtmlCompressor>();
            container.RegisterType<IHtmlDataEntries, HtmlDataEntries>();
            container.RegisterType<IHtmlDataFeeder, HtmlDataFeeder>();
            container.RegisterType<IHtml, BKHtml>();

            container.RegisterType<IMaxTitleLength, MaxTitleLength>();

            container.RegisterType<IListingFilter, InvalidCategoryFilter>();

            container.RegisterType<IListingActionFactory, BKListingActionFactory>();
            container.RegisterType<ICustomLabelFactory, DefaultCustomLabelFactory>();
            container.RegisterType<ISubTitleFactory, BKSubTitleFactory>();
            //container.RegisterType<ICategoryFactory, BKCategoryFactory>();
            container.RegisterType<IImageFactory, BKItemImageFactory>();
            container.RegisterType<ISellpriceFactory, BKSellpriceFactory>();

            //wuerth title has different title format
            if (supplierDb.GetType().Name.ToLower().Contains("wuerth"))
            {
                container.RegisterType<ITitleFactory, BKWuerthTitleFactory>();
            }
            else
            {
                container.RegisterType<ITitleFactory, BKTitleFactory>();
            }

            container.RegisterType<IPaypalEmailFactory, DefaultPaypalEmailFactory>();
            container.RegisterType<IFormatFactory, DefaultFormatFactory>();
            container.RegisterType<IDurationFactory, DefaultDurationFactory>();
            container.RegisterType<IPaypalAcceptFactory, DefaultPaypalAcceptFactory>();
            container.RegisterType<ILocationFactory, DefaultLocationFactory>();
            container.RegisterType<IDispatchTimeFactory, DefaultDispatchTimeFactory>();
            container.RegisterType<IReturnsOptionFactory, DefaultReturnsOptionFactory>();
            container.RegisterType<IRelationshipFactory, DefaultRelationshipFactory>();
            container.RegisterType<IShippingService1OptionFactory, BkShippingService1OptionFactory>();
            container.RegisterType<IShippingService1CostFactory, BKShippingService1CostFactory>();
            container.RegisterType<IShippingService1FreeShippingFactory, BKShippingService1FreeShippingFactory>();

            //no bk implementation(yet)
            container.RegisterType<IRelationshipDetailsFactory, BKVariationRelationshipDetailsFactory>();
            container.RegisterType<IStoreCategoryFactory, BKStoreCategoryFactory>();
            container.RegisterType<IStockFactory, BKStockFactory>();
            container.RegisterType<ICsvItemAttributesFactory, CsvItemAttributesFactory>();
            container.RegisterType<IVariationMapper, BKVariationMapper>();

            container.RegisterType<ICustomFieldsValues, BKCustomFieldsValues>();

            container.RegisterType<ICustomFieldsHeaders, BKCustomFieldsHeaders>();

            ICategoriesDatabaseFactory categoriesDatabaseFactory = new CategoriesDatabaseFactory(container.Resolve<ISettings>());
            container.RegisterInstance(categoriesDatabaseFactory.GetCategoriesDatabase());

            container.RegisterType<ICategoryFactory, BKCategoryFactory>();

            if (GetTemplate() == TemplateType.basic)
            {
                container.RegisterType<IMapper, BKtoBasicItemMapper>();

                container.RegisterType<IAdditionHeader, BasicDefaultAdditionHeader>();
                container.RegisterType<IRevisionHeader, BasicDefaultRevisionHeader>();
                //container.RegisterType<IAdditionHeader, BasicCsvAdditionHeader>();
                //container.RegisterType<IRevisionHeader, BasicCsvRevisionHeader>();

                container.RegisterType<ICsvHeader, BasicHeader>();

                container.RegisterType<IRevisionFormat, BasicDefaultRevision>();
                container.RegisterType<IAdditionFormat, BasicDefaultAddition>();
                //container.RegisterType<IRevisionFormat, BasicCsvRevision>();
                //container.RegisterType<IAdditionFormat, BasicCsvAddition>();

                
                //container.RegisterType<IFileWriter, CsvFileWriter>();
            }
            else
            {
                container.RegisterType<IMapper, BKtoCatalogItemMapper>();

                container.RegisterType<IAdditionHeader, CatalogDefaultAdditionHeader>();
                container.RegisterType<IRevisionHeader, CatalogDefaultRevisionHeader>();

                container.RegisterType<ICsvHeader, CatalogHeader>();
                container.RegisterType<IRevisionFormat, CatalogDefaultRevision>();
                container.RegisterType<IAdditionFormat, CatalogDefaultAddition>();

                //container.RegisterType<IFileWriter, TxtFileWriter>();
            }

            container.RegisterType<IResultDirectory, BKResultDirectory>();

            container.RegisterInstance(Encoding.UTF8);
            container.RegisterType<IFileWriter, TxtFileWriter>();

            SetInventoryConfiguration();

            ISupplierEans supplierEansInterface = (ISupplierEans)supplierDb;
            container.RegisterInstance(supplierEansInterface);

            container.RegisterType<ICsvGenerator, BKCsvGenerator>();
        }

        private TemplateType GetTemplate()
        {
            ITemplateFactory templateFactory = new BKTemplateFactory();
            TemplateType templateType = templateFactory.GetMapper<string>(supplierDb.GetType().ToString().ToLower().Replace("db", ""));

            return templateType;
        }

        private ISettings GetSettings()
        {
            AppConfigurationManager appConfiguration = new AppConfigurationManager();
            ISettings settings = new DefaultSettings();

            List<Supplier> suppliers = new List<Supplier>();
            if (agent.agentEntity.agent.platforms.Find(x => x.name.ToLower() == "ebay") != null)
            {
                suppliers = agent.agentEntity.agent.platforms.Find(x => x.name.ToLower() == "ebay").suppliers;
            }

            foreach (var supplier in suppliers)
            {
                int daysToDelivery = 0;
                if (supplier.DaysToDelivery != null)
                {
                    daysToDelivery = supplier.DaysToDelivery;
                }

                settings.SetValue(supplier.name.ToLower() + "DispatchTimeMax", daysToDelivery.ToString());
            }

            
            settings.SetValue("imageforwarderlink", agent.agentEntity.agent.htmlgeneratorproperties.imageforwarderlink);
            settings.SetValue("imagelinkformat", agent.agentEntity.agent.htmlgeneratorproperties.imagelinkformat);
            settings.SetValue("ebayname", agent.agentEntity.agent.ebayname);
            settings.SetValue("agentid", agent.agentEntity.agent.agentID.ToString());
            settings.SetValue("logolink", agent.agentEntity.agent.htmlgeneratorproperties.logolink);
            settings.SetValue("mvcviewlink", agent.agentEntity.agent.htmlgeneratorproperties.mvcviewlink);
            settings.SetValue("PaypalEmail", agent.agentEntity.agent.keys.ebay.email);
            settings.SetValue("Duration", appConfiguration.GetValue("Duration"));
            settings.SetValue("Format", appConfiguration.GetValue("Format"));
            settings.SetValue("Location", appConfiguration.GetValue("Location"));
            settings.SetValue("AcceptPaypal", appConfiguration.GetValue("AcceptPaypal"));
            settings.SetValue("ReturnsAcceptedOption", appConfiguration.GetValue("ReturnsAcceptedOption"));
            settings.SetValue("ShippingService1Option", appConfiguration.GetValue("ShippingService1Option"));
            settings.SetValue("ShippingService1Cost", appConfiguration.GetValue("ShippingService1Cost"));
            settings.SetValue("ShippingService1FreeShipping", appConfiguration.GetValue("ShippingService1FreeShipping"));
            settings.SetValue("actionlimit", "500");

            settings.SetValue("listingdbconnectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/EAD_");
            settings.SetValue("listingdbdatabasename", "EAD_");
            settings.SetValue("listingdbcollectionname", "listing");

            settings.SetValue("ebaycategoriesconnectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/EbayCategories");
            settings.SetValue("ebaycategoriesdatabasename", "EbayCategories");
            settings.SetValue("ebaycategoriescollectionname", "ebaycategoriespaths");

            return settings;
        }

        private void SetInventoryConfiguration()
        {
            ISettings settings = container.Resolve<ISettings>();

            IListingDatabaseFactory listingDatabaseFactory = new BKListingDatabaseFactory(settings);

            IListingDatabase listingDatabase = listingDatabaseFactory.GetListingDatabase<string>(settings.GetValue("ebayname"));

            EbayInventory.DataReader = listingDatabase.dataReader();

            EbayInventory.DataWriter = listingDatabase.dataWriter();

            EbayInventory.InventorySource = new BKInventorySource(listingDatabase.dataReader());
        }

        public TResult Resolve<TResult>()
        {
            return container.Resolve<TResult>();
        }
    }
}
