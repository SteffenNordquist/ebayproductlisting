﻿using DN_Classes.Agent;
using DN_Classes.AppStatus;
using DN_Classes.Logger;
using EbayCSV.Core.Generator;
using EbayCSV.Core.FileWiper;
using EbayCSV.Core.ResultDirectory;
using EbayCSV.Core.Suppliers;
using EbayListingV2.IOCConfigurations;
using ProcuctDB;
using ProcuctDB.Loqi;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Upload;
using DN.DataAccess.ConfigurationData;

namespace EbayListingV2
{
    public class ListingProcessor
    {
        private readonly Agent _agent;
        private readonly IConfigurationManager _appConfiguration;

        public ListingProcessor(Agent agent, IConfigurationManager appConfiguration)
        {
            this._agent = agent;
            this._appConfiguration = appConfiguration;
        }

        public void Process()
        {
            using (ApplicationStatus appStatus = new ApplicationStatus("EbayListing_" + _agent.agentEntity.agent.ebayname))
            {
                try
                {
                    foreach (IDatabase supplierDb in GetDatabases())
                    {
                        AppLogger.Info("supplier process : " + supplierDb.GetType().Name);

                        EbayCsvConfiguration ebayCsvConfig = new EbayCsvConfiguration(supplierDb, _agent, this._appConfiguration);

                        IGenerator csvGenerator = ebayCsvConfig.Resolve<IGenerator>();
                        csvGenerator.Generate();

                        List<string> generatedData = csvGenerator.GeneratedCsvData().ToList();

                        AppLogger.Info("generated files " + generatedData.Count());
                        appStatus.AddMessagLine("generated files " + generatedData.Count());

                        List<string> uploadErrorMessages = new List<string>();
                        uploadErrorMessages = AppLogger.Logger().ErrorLogsContainer.ToString().Split('\n').ToList().FindAll(x => x.ToLower().Contains("upload error"));

                        uploadErrorMessages.ForEach(x =>
                            appStatus.AddMessagLine(x));

                        if (Boolean.Parse(_appConfiguration.GetValue("WipeAfterGeneration")))
                        {
                            AppLogger.Info("wiping generated files");
                            //wipe all generated files after uploading
                            ebayCsvConfig.Resolve<IWiper>().Wipe();
                        }

                        
                                 
                    }

                    appStatus.AddMessagLine("Success");
                    appStatus.Successful();


                }
                catch (Exception ex)
                {
                    appStatus.AddMessagLine(ex.Message + "\n" + ex.StackTrace);
                }
            }
        }

        private IEnumerable<IDatabase> GetDatabases()
        {
            List<IDatabase> supplierDatabaseList = new List<IDatabase>();

            List<string> ebaySupplierNames = new List<string>();

            if (_appConfiguration.GetValue("DatabaseToProcess").ToLower() == "all")
            {
                ebaySupplierNames = _agent.agentEntity.agent.platforms.Where(x => x.name == "ebay").First().suppliers.Select(x => x.name).ToList();  
            }
            else
            {
                ebaySupplierNames = _appConfiguration.GetValue("DatabaseToProcess").Split('|').ToList();
            }

            IAgentEbaySuppliers ebaySuppliers = new AgentEbaySuppliers();
            supplierDatabaseList = ebaySuppliers.GetSuppliersDatabases<IDatabase, string>(ebaySupplierNames).ToList();

            return supplierDatabaseList;
        }
    }
}
