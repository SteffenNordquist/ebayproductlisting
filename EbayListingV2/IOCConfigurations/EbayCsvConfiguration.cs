﻿using DN_Classes.Agent;
using DN_Classes.Logger;
using DN_Classes.Supplier;
using EbayCSV.Core.Blacklist.Factory;
using EbayCSV.Core.Category;
using EbayCSV.Core.Category.Factory;
using EbayCSV.Core.Generator;
using EbayCSV.Core.CsvItem.ActionFormats;
using EbayCSV.Core.CsvItem.ActionFormats.Basic;
using EbayCSV.Core.CsvItem.ActionFormats.Catalog;
using EbayCSV.Core.CsvItem.CsvHeader;
using EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats;
using EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Basic;
using EbayCSV.Core.CsvItem.CsvHeader.HeaderFormats.Catalog;
using EbayCSV.Core.Factories;
using EbayCSV.Core.Factories.CategoryFactory;
using EbayCSV.Core.Factories.CustomFields;
using EbayCSV.Core.Factories.CustomLabelFactory;
using EbayCSV.Core.Factories.DispatchTimeFactory;
using EbayCSV.Core.Factories.DurationFactory;
using EbayCSV.Core.Factories.FormatFactory;
using EbayCSV.Core.Factories.ImageFactory;
using EbayCSV.Core.Factories.ListingActionFactory;
using EbayCSV.Core.Factories.LocationFactory;
using EbayCSV.Core.Factories.PaypalAcceptFactory;
using EbayCSV.Core.Factories.PaypalEmailFactory;
using EbayCSV.Core.Factories.RelationshipDetailsFactory;
using EbayCSV.Core.Factories.RelationshipFactory;
using EbayCSV.Core.Factories.ReturnsOptionFactory;
using EbayCSV.Core.Factories.SellpriceFactory;
using EbayCSV.Core.Factories.ShippingService1CostFactory;
using EbayCSV.Core.Factories.ShippingService1FreeShippingFactory;
using EbayCSV.Core.Factories.ShippingService1OptionFactory;
using EbayCSV.Core.Factories.StockFactory;
using EbayCSV.Core.Factories.StoreCategoryFactory;
using EbayCSV.Core.Factories.TemplateFactory;
using EbayCSV.Core.Factories.TitleFactory;
using EbayCSV.Core.Factories.TitleFactory.SubTitleFactory;
using EbayCSV.Core.FileWiper;
using EbayCSV.Core.FileWriter;
using EbayCSV.Core.Filter;
using EbayCSV.Core.HtmlData;
using EbayCSV.Core.HtmlData.BK;
using EbayCSV.Core.Inventory;
using EbayCSV.Core.Mapper;
using EbayCSV.Core.Mapper.VariationMapper;
using EbayCSV.Core.RawData;
using EbayCSV.Core.ResultDirectory;
using EbayCSV.Core.Settings;
using Microsoft.Practices.Unity;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using EbayCSV.Core.Factories.PaymentProfileName;
using EbayCSV.Core.Factories.ReturnProfileName;
using EbayCSV.Core.Factories.SalesTaxPercent;
using EbayCSV.Core.Factories.ShippingProfileName;
using EbayCSV.Core.Factories.Supplier;
using EbayCSV.Core.Generator.EndListing;
using EbayCSV.Core.Inventory.Source;
using EbayCSV.Core.Upload;
using EbayCSV.Core.Upload.UploadResponse;
using Microsoft.Practices.Unity.Configuration;
using DN.DataAccess.ConfigurationData;

namespace EbayListingV2.IOCConfigurations
{
    public class EbayCsvConfiguration
    {
        private readonly IUnityContainer _container;
        private readonly IDatabase _supplierDb;
        private readonly Agent _agent;
        private readonly IConfigurationManager _appConfiguration;

        public EbayCsvConfiguration(IDatabase supplierDb, Agent agent, IConfigurationManager appConfiguration)
        {
            this._container = new UnityContainer();
            this._supplierDb = supplierDb;
            this._agent = agent;
            this._appConfiguration = appConfiguration;

            _container.LoadConfiguration("IOCContainer");

            RegisterDependencies();
            RegisterUploadRequirements();
        }

        private void RegisterDependencies()
        {
            
            _container.RegisterInstance(_supplierDb);
            //container.RegisterType<IRawItems, BKRawItems>();
            _container.RegisterType<IRawItems, BkRawItemsWithWarehouseSpecificSupplier>();
            _container.RegisterInstance(GetSettings());

            //container.RegisterType<IListingDatabaseFactory, BKListingDatabaseFactory>();

            _container.RegisterType<IHtmlDownloader, HtmlDownloader>();
            _container.RegisterType<IHtmlCompressor, HtmlCompressor>();
            _container.RegisterType<IHtmlDataEntries, HtmlDataEntries>();
            _container.RegisterType<IHtmlDataFeeder, HtmlDataFeeder>();
            _container.RegisterType<IHtml, BKHtml>();

            _container.RegisterType<IMaxTitleLength, MaxTitleLength>();

            _container.RegisterType<IListingFilter, InvalidCategoryFilter>("InvalidCategoryFilter");
            _container.RegisterType<IListingFilter, BlacklistFilter>("BlacklistFilter");
            _container.RegisterType<IListingFilter, PriceFilter>("PriceFilter");

            _container.RegisterType<IListingActionFactory, BkListingActionFactory>();
            _container.RegisterType<ICustomLabelFactory, DefaultCustomLabelFactory>();
            _container.RegisterType<ISubTitleFactory, BKSubTitleFactory>();
            //container.RegisterType<ICategoryFactory, BKCategoryFactory>();
            _container.RegisterType<IImageFactory, BkItemImageFactory>();
            //container.RegisterType<ISellpriceFactory, BKSellpriceFactory>();
            //_container.RegisterType<ISellpriceFactory, BKSellpriceDeductedByShippingFactory>();
            //_container.RegisterType<IShippingProfileName,ShippingProfileNameFromAppSettings>();
            //_container.RegisterType<IReturnProfileName, ReturnProfileNameFromAppSettings>();
            //_container.RegisterType<IPaymentProfileName, PaymentProfileNameFromAppSettings>();
            //_container.RegisterType<ISalesTaxPercent, ProductSalesTaxPercent>();
            _container.RegisterType<ISupplierFactory, SupplierFactory>();

            SetCategoriesDatabase();
            SetBlacklistDatabase();

            //wuerth title has different title format
            if (_supplierDb.GetType().Name.ToLower().Contains("wuerth"))
            {
                _container.RegisterType<ITitleFactory, BKWuerthTitleFactory>();
            }
            else if (_supplierDb.GetType().Name.ToLower().Contains("haba"))
            {
                _container.RegisterType<ITitleFactory, BK_S_A_T_E_TitleFormatFactory>();
            }
            else
            {
                _container.RegisterType<ITitleFactory, BKTitleFactory>();
            }

            _container.RegisterType<IPaypalEmailFactory, DefaultPaypalEmailFactory>();
            _container.RegisterType<IFormatFactory, DefaultFormatFactory>();
            _container.RegisterType<IDurationFactory, DefaultDurationFactory>();
            _container.RegisterType<IPaypalAcceptFactory, DefaultPaypalAcceptFactory>();
            _container.RegisterType<ILocationFactory, DefaultLocationFactory>();
            _container.RegisterType<IDispatchTimeFactory, DefaultDispatchTimeFactory>();
            _container.RegisterType<IReturnsOptionFactory, DefaultReturnsOptionFactory>();
            _container.RegisterType<IVariationRowRelationshipFactory, VariationRowRelationshipFactory>();
            _container.RegisterType<IShippingService1OptionFactory, BkShippingService1OptionFactory>();
            //_container.RegisterType<IShippingService1OptionFactory, EmptyShippingService1OptionFactory>();
            _container.RegisterType<IShippingService1CostFactory, GlobalShippingService1CostFactory>();
            _container.RegisterType<IShippingService1FreeShippingFactory, BKShippingService1FreeShippingFactory>();

            //no bk implementation(yet)
            _container.RegisterType<IRelationshipDetailsFactory, BKVariationRelationshipDetailsFactory>();
            _container.RegisterType<IStoreCategoryFactory, BKStoreCategoryFactory>();
            _container.RegisterType<IStockFactory, BKStockFactory>();
            _container.RegisterType<IFactories, Factories>();
            _container.RegisterType<IVariationImageFactory, DefaultVariationImageFactory>();
            _container.RegisterType<IVariationMapper, BkVariationMapper>();

            _container.RegisterType<ICustomFieldsValues, BKCustomFieldsValues>();

            _container.RegisterType<ICustomFieldsHeaders, BKCustomFieldsHeaders>();

            _container.RegisterType<ICategoryFactory, BKCategoryFactory>();

            if (GetTemplate() == TemplateType.basic)
            {
                _container.RegisterType<IMapper, BktoBasicItemMapper>();

                _container.RegisterType<IAdditionHeader, BasicDefaultAdditionHeader>();
                _container.RegisterType<IRevisionHeader, BasicFullRevisionFormat>();
                //container.RegisterType<IRevisionHeader, BasicDefaultRevisionHeader>();
                //container.RegisterType<IAdditionHeader, BasicCsvAdditionHeader>();
                //container.RegisterType<IRevisionHeader, BasicCsvRevisionHeader>();

                _container.RegisterType<ICsvHeader, BasicHeader>();

                //container.RegisterType<IRevisionFormat, BasicDefaultRevision>();
                _container.RegisterType<IRevisionFormat, BasicFullRevision>();
                _container.RegisterType<IAdditionFormat, BasicDefaultAddition>();
                //container.RegisterType<IRevisionFormat, BasicCsvRevision>();
                //container.RegisterType<IAdditionFormat, BasicCsvAddition>();

                
                //container.RegisterType<IFileWriter, CsvFileWriter>();
            }
            else
            {
                _container.RegisterType<IMapper, BktoCatalogItemMapper>();

                _container.RegisterType<ICsvHeader, CatalogHeader>();

                //container.RegisterType<IRevisionFormat, CatalogDefaultRevision>();
                _container.RegisterType<IRevisionFormat, CatalogFullRevision>();
                _container.RegisterType<IRevisionHeader, CatalogFullRevisionHeaderFormat>();
                //container.RegisterType<IRevisionHeader, CatalogDefaultRevisionHeader>();

                if (Boolean.Parse(_appConfiguration.GetValue("CatalogTemplateRequireCategory")))
                {
                    _container.RegisterType<IAdditionHeader, CatalogWithCategoryRequiredAdditionHeader>();
                    _container.RegisterType<IAdditionFormat, CatalogWithCategoryRequiredAddition>();
                }
                else
                {
                    _container.RegisterType<IAdditionHeader, CatalogDefaultAdditionHeader>();
                    _container.RegisterType<IAdditionFormat, CatalogDefaultAddition>();
                }

                //container.RegisterType<IFileWriter, TxtFileWriter>();
            }

            _container.RegisterType<IResultDirectory, BKResultDirectory>();

            _container.RegisterInstance(Encoding.UTF8);
            _container.RegisterType<IFileWriter, TxtFileWriter>();

            SetInventoryConfiguration();

            ISupplierEans supplierEansInterface = (ISupplierEans)_supplierDb;
            _container.RegisterInstance(supplierEansInterface);

            _container.RegisterType<IEndListings, BkEndListings>();
            _container.RegisterType<IGenerator, BkCsvGenerator>();

            _container.RegisterType<IWiper, Wiper>();
        }

        private void RegisterUploadRequirements()
        {
            #region dn data access
            IDbConnectionFactory connectionFactory = new MongoConnectionFactory(new MongoConnectionProperties());
            connectionFactory.ConnectionProperties.SetProperty("connectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/FileExchangeResponses");
            connectionFactory.ConnectionProperties.SetProperty("databasename", "FileExchangeResponses");
            connectionFactory.ConnectionProperties.SetProperty("collectionname", "responses");
            #endregion

            IDbConnection dbConnection = connectionFactory.CreateConnection();
            _container.RegisterInstance(dbConnection);

            _container.RegisterType<IListingFileUploader, FileExchangeCsvFileUploader>();
            _container.RegisterType<IReferenceParser, ReferenceParser>();
            _container.RegisterType<IMessageParser, MessageParser>();
            _container.RegisterType<IUploadResponseManager, UploadResponseManager>();
            _container.RegisterType<IEbayUploader, EbayUploader>();
        }

        private TemplateType GetTemplate()
        {
            ITemplateFactory templateFactory = new BkTemplateFactory(this.Resolve<ISettings>());
            TemplateType templateType = templateFactory.GetMapper<string>(_supplierDb.GetType().Name.ToString().ToLower().Replace("db", ""));

            return templateType;
        }

        private ISettings GetSettings()
        {

            ISettings settings = new DefaultSettings();

            List<Supplier> suppliers = new List<Supplier>();
            if (_agent.agentEntity.agent.platforms.Find(x => x.name.ToLower() == "ebay") != null)
            {
                suppliers = _agent.agentEntity.agent.platforms.Find(x => x.name.ToLower() == "ebay").suppliers;
            }

            foreach (var supplier in suppliers)
            {
                int daysToDelivery = 0;
                if (supplier.DaysToDelivery != null)
                {
                    daysToDelivery = supplier.DaysToDelivery;
                }

                settings.SetValue(supplier.name.ToLower() + "DispatchTimeMax", daysToDelivery.ToString());
            }

            
            settings.SetValue("imageforwarderlink", _agent.agentEntity.agent.htmlgeneratorproperties.imageforwarderlink);
            settings.SetValue("imagelinkformat", _agent.agentEntity.agent.htmlgeneratorproperties.imagelinkformat);
            settings.SetValue("ebayname", _agent.agentEntity.agent.ebayname);
            settings.SetValue("agentid", _agent.agentEntity.agent.agentID.ToString());
            settings.SetValue("logolink", _agent.agentEntity.agent.htmlgeneratorproperties.logolink);
            settings.SetValue("mvcviewlink", _agent.agentEntity.agent.htmlgeneratorproperties.mvcviewlink);
            settings.SetValue("PaypalEmail", _agent.agentEntity.agent.keys.ebay.email);
            settings.SetValue("Duration", _appConfiguration.GetValue("Duration"));
            settings.SetValue("Format", _appConfiguration.GetValue("Format"));
            settings.SetValue("Location", _appConfiguration.GetValue("Location"));
            settings.SetValue("AcceptPaypal", _appConfiguration.GetValue("AcceptPaypal"));
            settings.SetValue("ReturnsAcceptedOption", _appConfiguration.GetValue("ReturnsAcceptedOption"));

            settings.SetValue("ShippingService1Option", _appConfiguration.GetValue("GeneralShippingService1Option"));
            settings.SetValue("ShippingService1Cost", _appConfiguration.GetValue("GeneralShippingService1Cost"));
            settings.SetValue("ShippingService1FreeShipping", _appConfiguration.GetValue("GeneralShippingService1FreeShipping"));

            settings.SetValue("returnProfileName", _appConfiguration.GetValue("ReturnProfileName"));
            settings.SetValue("shippingProfileName", _appConfiguration.GetValue("ShippingProfileName"));
            settings.SetValue("paymentProfileName", _appConfiguration.GetValue("PaymentProfileName"));
            

            settings.SetValue("actionlimit", _appConfiguration.GetValue("ActionLimit"));
            settings.SetValue("itemsPerFile", _appConfiguration.GetValue("ItemsPerFile"));

            settings.SetValue("listingdbconnectionstring", _appConfiguration.GetValue("ListingDBConnectionStringFormat"));
            settings.SetValue("listingdbdatabasename", _appConfiguration.GetValue("ListingDBNameFormat"));
            settings.SetValue("listingdbcollectionname", _appConfiguration.GetValue("ListingCollectionName"));

            settings.SetValue("ebaycategoriesconnectionstring", _appConfiguration.GetValue("EbayCategoriesConnectionString"));
            settings.SetValue("ebaycategoriesdatabasename", _appConfiguration.GetValue("EbayCategoriesDB"));
            settings.SetValue("ebaycategoriescollectionname", _appConfiguration.GetValue("EbayCategoriesCollection"));

            settings.SetValue("ebayblacklistconnectionstring", _appConfiguration.GetValue("BlacklistConnectionString"));
            settings.SetValue("ebayblacklistdatabasename", _appConfiguration.GetValue("BlacklistDB"));
            settings.SetValue("ebayblacklistcollectionname", _appConfiguration.GetValue("BlacklistCollection"));

            settings.SetValue("catalogSuppliers", _appConfiguration.GetValue("CatalogSuppliers"));

            settings.SetValue("catalogTemplateRequireCategory", _appConfiguration.GetValue("CatalogTemplateRequireCategory"));

            settings.SetValue("excludeDescription", _appConfiguration.GetValue("ExcludeDescription"));

            settings.SetValue("endUnexistingListings", _appConfiguration.GetValue("EndUnexistingListings"));

            settings.SetValue("uploadListing", _appConfiguration.GetValue("UploadListing"));

            settings.SetValue("ebaytoken", _agent.agentEntity.agent.keys.ebay.token);
            settings.SetValue("agentid", _agent.agentEntity.agent.agentID.ToString());
            settings.SetValue("supplier", _supplierDb.GetType().ToString().ToLower().Replace("db", ""));
            settings.SetValue("revisionOnly", _appConfiguration.GetValue("RevisionOnly"));

            return settings;
        }

        private void SetInventoryConfiguration()
        {
            ISettings settings = _container.Resolve<ISettings>();

            IInventoryDbFactory inventoryDbFactory = new InventoryDbFactory(settings, new MongoConnectionFactory(new MongoConnectionProperties()));

            InventoryStorage.InventorySource = new InventorySource(inventoryDbFactory.InventoryDbConnection(settings.GetValue("ebayname")));
        }

        private void SetCategoriesDatabase()
        {
            ICategoriesDatabaseFactory categoriesDatabaseFactory = new CategoriesDatabaseFactory(_container.Resolve<ISettings>());
            _container.RegisterInstance(categoriesDatabaseFactory.GetCategoriesDatabase());
        }

        private void SetBlacklistDatabase()
        {
            IBlacklistDatabaseFactory blacklistDatabaseFactory = new BlacklistDatabaseFactory(_container.Resolve<ISettings>());
            _container.RegisterInstance(blacklistDatabaseFactory.GetBlacklistDatabase());
        }

        public T Resolve<T>()
        {
            return _container.Resolve<T>();
        }

        public void Register<T>(T registree)
        {
            _container.RegisterInstance(registree);
        }

    }
}
