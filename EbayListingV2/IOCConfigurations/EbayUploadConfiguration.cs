﻿using DN.DataAccess;
using DN.DataAccess.ConfigurationData;
using DN.DataAccess.ConnectionFactory;
using DN.DataAccess.ConnectionFactory.Mongo;
using DN_Classes.Agent;
using EbayCSV.Core.Settings;
using Microsoft.Practices.Unity;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EbayCSV.Core.Upload;
using EbayCSV.Core.Upload.UploadResponse;

namespace EbayListingV2.IOCConfigurations
{
    public class EbayUploadConfiguration
    {
        private readonly IUnityContainer _container;
        private readonly Agent _agent;
        private readonly IDatabase _database;
        private readonly IConfigurationManager _appConfiguration;

        public EbayUploadConfiguration(Agent agent, IDatabase database, IConfigurationManager appConfiguration)
        {
            this._container = new UnityContainer();
            this._agent = agent;
            this._database = database;
            this._appConfiguration = appConfiguration;

            RegisterDependencies();
        }

        private void RegisterDependencies()
        {
            _container.RegisterInstance(GetSettings());
            _container.RegisterInstance(GetResponseDBConnection());
            _container.RegisterType<IListingFileUploader, FileExchangeCsvFileUploader>();
            _container.RegisterType<IReferenceParser, ReferenceParser>();
            _container.RegisterType<IMessageParser, MessageParser>();
            _container.RegisterType<IUploadResponseManager, UploadResponseManager>();
            _container.RegisterType<IEbayUploader, EbayUploader>();
        }

        private ISettings GetSettings()
        {
            ISettings settings = new DefaultSettings();
            settings.SetValue("ebaytoken", _agent.agentEntity.agent.keys.ebay.token);
            settings.SetValue("agentid", _agent.agentEntity.agent.agentID.ToString());
            settings.SetValue("supplier", _database.GetType().ToString().ToLower().Replace("db", ""));
            settings.SetValue("revisionOnly", _appConfiguration.GetValue("RevisionOnly"));

            return settings;
        }

        private IDbConnection GetResponseDBConnection()
        {
            #region dn data access
            IDbConnectionFactory connectionFactory = new MongoConnectionFactory(new MongoConnectionProperties());
            connectionFactory.ConnectionProperties.SetProperty("connectionstring", "mongodb://client148:client148devnetworks@136.243.44.111/FileExchangeResponses");
            connectionFactory.ConnectionProperties.SetProperty("databasename", "FileExchangeResponses");
            connectionFactory.ConnectionProperties.SetProperty("collectionname", "responses");
            #endregion

            IDbConnection dbConnection = connectionFactory.CreateConnection();

            return dbConnection;
        }

        public TResult Resolve<TResult>()
        {
            return _container.Resolve<TResult>();
        }
    }
}
