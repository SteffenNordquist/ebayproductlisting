﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace FileExchangeResponseReader
{
    class Program
    {
        static void Main(string[] args)
        {
            //folder where file exchange response files located
            DirectoryInfo fileExchangeResponseDirectory = new DirectoryInfo(@"C:\FileExchangeResponseFiles");

            //initialized empty list to be filled with failed lines from file exchange response files
            List<string> addFailedItems = new List<string>();
            List<string> reviseFailedItems = new List<string>();
            List<string> reviseFailedItemsIds = new List<string>();

            //filenames in the file exchange response directory
            List<string> fileNames = fileExchangeResponseDirectory.GetFiles().Select(x => x.FullName).ToList();

            foreach (string fileName in fileNames)
            {
                //current file lines
                List<string> currentFileList = File.ReadAllLines(fileName).ToList();

                //extracting lines where the word "Failure" exists
                List<string> currentFileFailedItems = currentFileList.Where(x => x.Contains("Failure")).ToList();

                foreach (string failedItemLine in currentFileFailedItems)
                {
                    //extracting the item id from the failed item line
                    Regex itemIdRegex = new Regex(@"\d{12}");
                    string itemID = itemIdRegex.Match(failedItemLine).Value;
                    if (itemID == "")
                    {
                        itemID = "NoItemIDFound";
                    }////////////////////////////////////////////////////


                    //extracting the item id from the failed item line
                    Regex eanRegex = new Regex(@"\d{13}");
                    string ean = eanRegex.Match(failedItemLine).Value;
                    if (ean == "")
                    {
                        ean = "NoEanFound";
                    }/////////////////////////////////////////////////


                    if (failedItemLine.Contains("Add"))
                    {
                        addFailedItems.Add(itemID + "\t" + ean + "\t" + failedItemLine);
                        
                    }
                    else if (failedItemLine.Contains("Revise"))
                    {
                        reviseFailedItems.Add(itemID + "\t" + ean + "\t" + failedItemLine);
                        reviseFailedItemsIds.Add(itemID);
                    }
                    
                }

            }

            File.WriteAllLines(@"C:\FileExchangeResponseFiles\addFailedItems.txt", addFailedItems);
            File.WriteAllLines(@"C:\FileExchangeResponseFiles\reviseFailedItems.txt", reviseFailedItems);
            File.WriteAllLines(@"C:\FileExchangeResponseFiles\reviseFailedItemsIds.txt", reviseFailedItemsIds);
        }
    }
}
