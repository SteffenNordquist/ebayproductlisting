﻿
using HtmlAgilityPack;
using ProcuctDB;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using MongoDB.Driver;
using DNEbaySystem.Lister.ListingConditions;
using DNEbaySystem.ListingDatabase.Interfaces;
using DNEbaySystem.Logging;
using DNEbaySystem.Lister;
using DNEbaySystem.ListingDatabase;
using DNEbaySystem.Lister.Objects;
using System.Threading;

namespace DNEbaySystem.HtmlManager
{
    public class HtmlDataFeeder
    {
        private HtmlData htmlData;
        private string htmlString;
        private EbayAgent Agent;
        private IListingDatabase ListingDatabase;

        

        /// <summary>
        /// feeds html data
        /// </summary>
        /// <param name="productInformation"></param>
        /// <param name="supplier"></param>
        /// <param name="agentid"></param>
        /// <param name="upsert"></param>
        public void Feed(IProductSalesInformation productInformation, string supplier, int agentid, string ebayname, string imagelinkformat, string logoLink, string mvcView)
        {
            ConditionsProcessor conditions = new ConditionsProcessor();
            ICondition notNullCondition = new NotNullCondition(productInformation);
            ICondition notEmptyCondition = new NotEmptyCondition(supplier);
            conditions.AddCondition(notNullCondition);
            conditions.AddCondition(notEmptyCondition);

            if (conditions.HavePassed())
            {
                this.Agent = new EbayAgent
                {
                    Id = agentid,
                    MvcView = mvcView,
                    Name = ebayname
                };

                this.htmlData = new HtmlData();

                SetListingDatabase();

                List<string> imageNames = new List<string>();
                productInformation.GetMainImage().ForEach(imageName => imageNames.Add(string.Format(imagelinkformat, supplier, imageName, ebayname.Replace(".", ""), ProductBrowsing.IsLocal)));


                if (productInformation.GetAttributeList() != null && productInformation.GetAttributeList().Count > 0)
                {
                    HtmlDataEntries htmlDataEntries = new HtmlDataEntries();

                    this.htmlData.ItemId = ListingDatabase.Find(productInformation.GetEan()).ItemID;
                    this.htmlData.id = agentid + "_" + supplier + productInformation.GetEan();
                    this.htmlData.attributeList = productInformation.GetAttributeList();
                    this.htmlData.mainimages = imageNames;
                    this.htmlData.mainimage = imageNames[0];
                    this.htmlData.title = productInformation.getTitle()[0];
                    this.htmlData.logoLink = logoLink;

                   
                        htmlDataEntries.Upsert(htmlData);
                        ProcessHtml();
                    
                }
                else
                {
                    SystemLogging.Error.AddLog("Feeder Message: Product has no attributes or null");
                }
            }
            
        }

        /// <summary>
        /// feeds html data
        /// </summary>
        /// <param name="rawItem"></param>
        /// <param name="agent"></param>
        public void Feed(RawItem rawItem, EbayAgent agent)
        {
            ConditionsProcessor conditions = new ConditionsProcessor();
            ICondition notNullCondition = new NotNullCondition(rawItem);
            ICondition notEmptyCondition = new NotEmptyCondition(rawItem.Supplier);
            ICondition imageCondition = new ImageCondition(rawItem);
            conditions.AddCondition(notNullCondition);
            conditions.AddCondition(notEmptyCondition);
            conditions.AddCondition(imageCondition);

            if (conditions.HavePassed())
            {
                this.Agent = agent;

                this.htmlData = rawItem.HtmlProperties;

                SetListingDatabase();

                ThreadStart ts = delegate
                {
                    HtmlDataEntries htmlDataEntries = new HtmlDataEntries();
                    htmlDataEntries.Upsert(htmlData);
                };
                new Thread(ts).Start();

                ProcessHtml();

            }

        }

        /// <summary>
        /// processes the html
        /// </summary>
        private void ProcessHtml()
        {
            HtmlDocument htmlDocument = new HtmlDocument();

            string htmlString = DownloadHtmlString();

            string htmlBody = "";

            if (htmlString != "")
            {
                htmlDocument.LoadHtml(htmlString);

                htmlBody = htmlDocument.DocumentNode.SelectSingleNode("//body").InnerHtml.Replace("\r\n", "").Replace("\t", "").Replace("\n", "").Trim().TrimStart().TrimEnd();

                htmlBody = htmlBody.Replace("\"", "\"\"");
                htmlBody = "\"" + "<meta charset=\"\"utf-8\"\">" + htmlBody + "\"";
            }

            this.htmlString = htmlBody;
        }

        /// <summary>
        /// downloads html from 144 mvc
        /// </summary>
        /// <returns>string</returns>
        private string DownloadHtmlString()
        {

            string mvclink = String.Format("{0}id={1}", Agent.MvcView, this.htmlData.id);

            Console.WriteLine("Mvc Link " + mvclink);

            if (ProductBrowsing.IsLocal) { Process.Start(mvclink); }

            WebClient wc = new WebClient();
            wc.Encoding = Encoding.UTF8;

            string html = "";

            try { html = wc.DownloadString(mvclink); }
            catch { }

            return html;
        }

        /// <summary>
        /// sets/finds agent's ebay account data including listing database
        /// </summary>
        private void SetListingDatabase()
        {
            ListingDatabaseFactory databaseFactory = new ListingDatabaseFactory();
            ListingDatabase = databaseFactory.GetListingDatabase(Agent.Name);
        }

        /// <summary>
        /// returns html-ready string
        /// </summary>
        /// <returns></returns>
        public string GetHtmlString()
        {
            return htmlString;
        }
    }
}
