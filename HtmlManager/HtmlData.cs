﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.HtmlManager
{
    public class HtmlData
    {
        public string id { get; set; }
        public List<string> mainimages { get; set; }
        public string mainimage { get; set; }
        public Dictionary<string, List<string>> attributeList { get; set; }
        public string title { get; set; }
        public string logoLink { get; set; }
        public string ItemId { get; set; }
        public double Price { get; set; }
    }
}
