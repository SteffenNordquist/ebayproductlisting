﻿
using MongoDB.Driver;
using MongoDB.Driver.Builders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DNEbaySystem.HtmlManager
{
    public class HtmlDataEntries
    {
        private MongoCollection<HtmlData> EntriesCollection = null;

        public HtmlDataEntries()
        {
            EntriesCollection = new MongoClient("mongodb://client148:client148devnetworks@148.251.0.235/HtmlGenerationEntries")
                .GetServer()
                .GetDatabase("HtmlGenerationEntries")
                .GetCollection<HtmlData>("entries");
        }

        /// <summary>
        /// inserts or updates item's html data
        /// </summary>
        /// <param name="htmlData"></param>
        public void Upsert(HtmlData htmlData)
        {
            EntriesCollection.Update(Query.EQ("_id", htmlData.id), Update<HtmlData>.Set(x => x.mainimages, htmlData.mainimages)
                            .Set(x => x.mainimage, htmlData.mainimages[0])
                            .Set(x => x.attributeList, htmlData.attributeList)
                            .Set(x => x.title, htmlData.title)
                            .Set(x => x.logoLink, htmlData.logoLink)
                            .Set(x => x.ItemId, htmlData.ItemId),
                            UpdateFlags.Upsert);
        }

        /// <summary>
        /// returns item's html data
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        public HtmlData FindById(string id)
        {
            return EntriesCollection.FindOne(Query.EQ("_id", id));
        }

        /// <summary>
        /// updates item's price in database
        /// </summary>
        /// <param name="ean"></param>
        /// <param name="price"></param>
        public void UpdatePrice(string ean, string suppliername, double price)
        {
            EntriesCollection.Update(Query.Matches("_id", suppliername + ean), Update<HtmlData>.Set(x => x.Price, price), UpdateFlags.Multi);
        }
    }
}
